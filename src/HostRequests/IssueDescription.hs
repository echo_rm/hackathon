{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module HostRequests.IssueDescription
    ( getRenderedDescriptionForIssue
    ) where

import           AesonHelpers
import           Application
import           Data.Aeson
import           Data.Aeson.Types                  (fieldLabelModifier)
import qualified Data.ByteString.Char8             as B
import           Data.Monoid                       (mempty)
import qualified Data.Text                         as T
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import           GHC.Generics
import qualified Snap.Snaplet                      as SS

type RenderedIssueDescription = T.Text

getRenderedDescriptionForIssue :: AC.Tenant -> AC.IssueId -> AppHandler (Either AC.ProductErrorResponse RenderedIssueDescription)
getRenderedDescriptionForIssue tenant issueKey = do
    result <- SS.with connect $ AC.hostGetRequest tenant issueDetailsUrl parameters mempty
    return $ fmap (irfDescription . irRenderedFields) result
    where 
        issueDetailsUrl = "/rest/api/2/issue/" `B.append` (B.pack . show $ issueKey)
        parameters = 
            [ ("expand", Just "renderedFields")
            ]

data IssueResult = IssueResult
    { irRenderedFields :: IssueRenderedFields
    } deriving (Show, Generic)

data IssueRenderedFields = IssueRenderedFields
    { irfDescription :: T.Text
    } deriving (Show, Generic)

instance FromJSON IssueResult where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "ir" }

instance FromJSON IssueRenderedFields where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "irf" }