define(function() {
    /*
    There are three requirements to this class.
    The masterSelect (a jQuery object) that represents the checkbox that is the (de)select all.
    The checkboxContainer is the object that all of the other checkbox's are inside.
    The otherCheckboxSelector is the selector for the checkbox's inside the container.
     */
    return function(masterSelect, checkboxContainer, otherCheckboxSelector) {
        var getAllSelects = function() {
            return checkboxContainer.find(otherCheckboxSelector);
        };

        var getSelected = function() {
            return AJS.$.grep(getAllSelects(), function(e) { return e.checked; });
        };

        var currentlySelecting;

        var reset = function() {
            currentlySelecting = true;
            checkAll(false);
            setIndeterminateState();
        };

        var setIndeterminateState = function() {
            var allSelects = getAllSelects();
            var selectedReminders = getSelected();

            masterSelect.prop("indeterminate", selectedReminders.length > 0 && selectedReminders.length != allSelects.length);
            check(masterSelect, selectedReminders.length > 0 && selectedReminders.length === allSelects.length);
        };

        var check = function(element, checked) {
            if(checked) {
                element.attr("checked", "checked");
            } else {
                element.removeAttr("checked");
            }
        };

        var checkAll = function(checked) {
            AJS.$.each(getAllSelects(), function (i, e) {
                check(AJS.$(e), checked);
            });
        };

        masterSelect.change(function() {
            var isChecked = AJS.$(this).is(':checked');
            var allSelects = getAllSelects();
            var selectedReminders = getSelected();

            var allSelected = selectedReminders.length > 0 && selectedReminders.length == allSelects.length;

            var selectAll = isChecked;
            if(allSelects.length > 0 && !allSelected) {
                var isEmpty = selectedReminders.length == 0;
                selectAll = isEmpty || currentlySelecting;
                check(masterSelect, selectAll);
            }
            checkAll(selectAll);
        });

        checkboxContainer.on("change", otherCheckboxSelector, function() {
            currentlySelecting = AJS.$(this).is(':checked');
            setIndeterminateState();
        });

        var update = function() {
            setIndeterminateState();
        };

        reset();
        return {
            reset: reset,
            update: update,
            getSelected: getSelected,
            getAll: getAllSelects
        };
    };
});