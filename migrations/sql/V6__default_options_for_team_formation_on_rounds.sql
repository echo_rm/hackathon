-- We want to make it so that the options for TeamFormation are always True by default
UPDATE round SET transition_tied_teams = TRUE, match_unallocated_teams = TRUE WHERE type = 'TeamFormation';
