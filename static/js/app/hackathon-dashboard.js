define([
   "../helpers/PageContext",
   '../helpers/MustacheLoader',
   '../helpers/MultiSelect',
   '../helpers/Meta',
   '../helpers/Links',
   '../helpers/Handler',
   '../helpers/Restrictions',
   'connect/pagetoken'
],
function(PC, MustacheLoader, MS, Meta, Links, H, R) {
    var requestCurrentState = function(projectId) {
        var acptToken = Meta.get('acpt');
        return AJS.$.ajax({
            url: "/rest/round-room/current",
            type: "POST",
            cache: false,
            data: JSON.stringify({
                token: {
                    acptToken: acptToken
                },
                projectId: projectId
            })
        });
    };

    var requestVoteableRounds = function(projectId) {
        return AJS.$.ajax({
            url: "/rest/hackathon/round/voteable",
            type: "GET",
            cache: false,
            data: {
                projectId: projectId
            }
        });
    };

    var requestAddonPermissions = function(projectId) {
       return AJS.$.ajax({
            url: '/rest/addon/permission-check',
            type: 'GET',
            data: {
               projectId: projectId
            },
            cache: 'false'
       });
    };

    var lozengeForStatus = function(status) {
        if(status === "Active") return "aui-lozenge-success";
        if(status === "Locked") return "aui-lozenge-error";
        return "";
    };

    var statusToOrder = function(status) {
      if(status === "Active") return 0;
      if(status === "Locked") return 1;
      return 2;
    };

    var refreshCurrentState = function(pageContext, roomSelector, hideInactive) {
        requestCurrentState(pageContext.project.id).done(function(data) {
            var currentRoomState = AJS.$("#current-room-state tbody");
            currentRoomState.empty();

            // Sort the rooms by name
            var sortedRooms = data.roundRooms.sort(function(a, b) {
              // Compare by round name
              var roundCompare = b.round.name.localeCompare(a.round.name);
              if(roundCompare != 0) return roundCompare;

              // Compare by status
              var statusCompare = statusToOrder(a.status) - statusToOrder(b.status);
              if(statusCompare != 0) return statusCompare;

              // Sort by room name
              return a.room.name.localeCompare(b.room.name);
            });

            AJS.$.each(sortedRooms, function(i, roundRoom) {
                if(hideInactive && roundRoom.status === "Inactive") roundRoom.hideRow = true;
                roundRoom.statusLozengeClass = lozengeForStatus(roundRoom.status);
                roundRoom.viewTeamsUrl = Links.viewTeams(pageContext.productBaseUrl, { projectKey: pageContext.project.key }, roundRoom.round, roundRoom.room);
                currentRoomState.append(templates.render("round-room-row", roundRoom));
            });

            AJS.$("#no-rooms-warning").toggleClass("hidden", sortedRooms.length > 0);

            roomSelector.reset();
        });
    };

    var getVotingRestrictions = function(projectId) {
        return AJS.$.ajax({
            url: "/rest/hackathon/restrictions",
            type: "GET",
            cache: false,
            data: {
                projectId: projectId
            }
        });
    };

    var setVotingRestrictions = function(projectId, votingRestriction) {
        return AJS.$.ajax({
            url: "/rest/hackathon/restrictions",
            type: "PUT",
            cache: false,
            data: JSON.stringify({
                projectId: projectId,
                restriction: votingRestriction
            })
        });
    };

    var getMultipleRoomVoting = function(projectId) {
        return AJS.$.ajax({
            url: "/rest/hackathon/votingrules",
            type: "GET",
            cache: false,
            data: {
                projectId: projectId
            }
        });
    };

    var setMultipleRoomVoting = function(projectId, multipleRoomVoting) {
        return AJS.$.ajax({
            url: "/rest/hackathon/votingrules",
            type: "PUT",
            cache: false,
            data: JSON.stringify({
                projectId: projectId,
                multipleRoomVoting: multipleRoomVoting
            })
        });
    };

    AJS.$(function() {
        var pageContext = PC.load();
        templates = MustacheLoader.load();

        // Show the permissions warning message if required
        requestAddonPermissions(pageContext.project.id).done(function(data) {
           AJS.$("#missing-browse-users").toggleClass('hidden', data.hasBrowseUsers);
           if(data.hasBrowseUsers) {
              var projectRolesPage = pageContext.productBaseUrl + '/plugins/servlet/project-config/' + pageContext.project.key + '/roles';
              AJS.$("#missing-project-admin-roles-link").attr('href', projectRolesPage);
              AJS.$("#missing-project-admin").toggleClass('hidden', data.hasProjectAdmin);
           }

           // Hide everything from the user if these errors exist
           if(!data.hasBrowseUsers || !data.hasProjectAdmin) {
              AJS.$("#missing-project-admin").nextAll().addClass('hidden');
           }
        });

        AJS.$("#view-summary").attr('href', Links.summary(pageContext, { projectKey: pageContext.project.key }));

        var roomSelector = MS(AJS.$("#round-room-select-all"), AJS.$("#current-room-state tbody"), ".round-room-selector");
        var roundSelector = MS(AJS.$("#round-settings-select-all"), AJS.$("#round-settings tbody"), ".round-selector");

        var hideInactive = false;

        AJS.$("#hide-inactive-rooms").change(function() {
            hideInactive = AJS.$(this).is(':checked');

            // Now hide or show all of the currently showing elements
            refreshCurrentState(pageContext, roomSelector, hideInactive);
        });

        var setupCurrentRoundOptions = function(sortedRounds) {
            var currentRoundList = AJS.$("#set-current-round-options ul");
            currentRoundList.empty();
            AJS.$.each(sortedRounds, function(i, round) {
                currentRoundList.append(templates.render("round-option", round));
            });
        };

        var setupRoundSettingRows = function(sortedRounds) {
            var container = AJS.$("#round-settings tbody");
            container.empty();
            AJS.$.each(sortedRounds, function(i, round) {
                container.append(templates.render("round-settings-row", round));
            });
        };

        var refreshVoteableRounds = function() {
            requestVoteableRounds(pageContext.project.id).done(function (data) {
                var sortedRounds = data.rounds.sort(function (a, b) {
                    return a.sequenceNumber - b.sequenceNumber;
                });

                setupCurrentRoundOptions(sortedRounds);
                setupRoundSettingRows(sortedRounds);

                roundSelector.reset();
            });
        };

        AJS.$("#create-new-room").click(H.h(function() {
            var roomName = AJS.$("#new-room-name").val();

            var createRoomRequest = AJS.$.ajax({
                url: "/rest/hackathon/room",
                type: "PUT",
                cache: false,
                data: JSON.stringify({
                    projectId: pageContext.project.id,
                    projectKey: pageContext.project.key,
                    roomName: roomName
                })
            });

            createRoomRequest.done(function (data) {
                refreshCurrentState(pageContext, roomSelector, hideInactive);
                AJS.$("#new-room-name").val('');
            });

            createRoomRequest.fail(function() {
                AP.require("messages", function(messages){
                    messages.error('Room creation failed', 'Please try again. Or refresh the page', {
                        fadeout: true,
                        delay: 5000
                    });
                });
            });
        }));

        var getAllSelectedRoundRooms = function() {
            return AJS.$.map(roomSelector.getSelected(), function(element) {
                var roundRoomBlock = AJS.$(element).closest('tr');
                return {
                    roundId: roundRoomBlock.data("round-id"),
                    roomId: roundRoomBlock.data("room-id")
                };
            });
        };

        var getAllSelectedRounds = function() {
            return AJS.$.map(roundSelector.getSelected(), function(element) {
                return AJS.$(element).closest('tr').data("round-id");
            });
        };

        var getAllSelectedRooms = function() {
            return AJS.$.map(roomSelector.getSelected(), function(element) {
                return AJS.$(element).closest('tr').data("room-id");
            });
        };

        AJS.$("#set-status-options a").click(H.h(function(e) {
            var newStatus = AJS.$(e.target).data("status");

            var selectedIds = getAllSelectedRoundRooms();
            if(selectedIds.length > 0) {
                var updateRequest = AJS.$.ajax({
                    url: "/rest/hackathon/round-room/status",
                    type: "PUT",
                    cache: false,
                    data: JSON.stringify({
                        projectId: pageContext.project.id,
                        roundRoomIds: selectedIds,
                        newStatus: newStatus
                    })
                });

                updateRequest.done(function() {
                    refreshCurrentState(pageContext, roomSelector, hideInactive);
                });

                updateRequest.fail(function() {
                    AP.require("messages", function(messages){
                        messages.error('Could not update rooms', 'Could not set status on rooms.', {
                            fadeout: true,
                            delay: 5000
                        });
                    });
                });
            } else {
                showNoRoomsSelectedWarning();
            }
        }));

        AJS.$("#set-current-round-options").on("click", "a.round-option", H.h(function(e) {
            var newCurrentRound = AJS.$(e.target).data("round-id");
            console.log("Current round id: " + newCurrentRound);

            var selectedIds = getAllSelectedRooms();
            if(selectedIds.length > 0) {
                var updateRequest = AJS.$.ajax({
                    url: "/rest/hackathon/room/current-round",
                    type: "PUT",
                    cache: false,
                    data: JSON.stringify({
                        projectId: pageContext.project.id,
                        roomIds: selectedIds,
                        currentRoundId: newCurrentRound
                    })
                });

                updateRequest.done(function() {
                    refreshCurrentState(pageContext, roomSelector, hideInactive);
                });

                updateRequest.fail(function() {
                    AP.require("messages", function(messages){
                        messages.error('Could not update rooms', 'Could not update round on rooms.', {
                            fadeout: true,
                            delay: 5000
                        });
                    });
                });
            } else {
                showNoRoomsSelectedWarning();
            }
        }));

        var showNoRoomsSelectedWarning = function () {
            AP.require("messages", function(messages){
                messages.warning('No rooms selected', 'You need to select rooms for the update to take effect.', {
                    fadeout: true,
                    delay: 5000
                });
            });
        };

        AJS.$("#round-configuration-update").click(H.h(function() {
            var selectedRounds = getAllSelectedRounds();

            if(selectedRounds.length > 0) {
                var requestData = {
                    projectId: pageContext.project.id,
                    roundIds: selectedRounds
                };

                var votesPerRound = parseInt(AJS.$("#votes-per-round").val());
                var votesPerTeam = parseInt(AJS.$("#votes-per-team").val());

                if (votesPerRound) requestData.newVotesPerUser = votesPerRound;
                if (votesPerTeam) requestData.newVotesUserTeam = votesPerTeam;

                var updateRequest = AJS.$.ajax({
                    url: "/rest/hackathon/round/configuration",
                    type: "PUT",
                    cache: "false",
                    data: JSON.stringify(requestData)
                });

                updateRequest.done(function () {
                    refreshVoteableRounds();
                });

                updateRequest.fail(function() {
                    AP.require("messages", function(messages){
                        messages.error('Could not update rounds', 'The round update could not be performed. You may need to refresh the page.', {
                            fadeout: true,
                            delay: 5000
                        });
                    });
                });
            } else {
                AP.require("messages", function(messages){
                    messages.warning('No rounds selected', 'You need to select rounds for the update to take effect.', {
                        fadeout: true,
                        delay: 5000
                    });
                });
            }
        }));

        AJS.$(".help-tooltip").tooltip();

        getVotingRestrictions(pageContext.project.id).done(function(data) {
            var element;
            if(data.restriction === R.restriction.loginAndTeamMember) {
                element = AJS.$("#vr-login-and-membership");
            } else if(data.restriction === R.restriction.loginOnly) {
                element = AJS.$("#vr-login-without-membership");
            } else if(data.restriction === R.restriction.anonymous) {
                element = AJS.$("#vr-anonymous");
            } else {
                AP.require('messages', function(message) {
                   message.error("The voting restrictions cannot be read for this hackathon.", "Please override them");
                });
            }

            if(element) {
                element.prop("checked", true);
            }
        });

        var setTo = function(restriction) {
            return function() {
                var request = setVotingRestrictions(pageContext.project.id, restriction);

                AP.require("messages", function(messages){
                    //create a message
                    request.done(function() {
                        messages.success("Voting restrictions updated", "", {
                            fadeout: true,
                            delay: 3000
                        });
                    });

                    request.fail(function() {
                        messages.error("Failed to update voting restrictions", "Please refresh the page.", {
                            fadeout: true,
                            delay: 5000
                        });
                    });
                });
            };
        };

        AJS.$("#vr-login-and-membership").click(setTo(R.restriction.loginAndTeamMember));
        AJS.$("#vr-login-without-membership").click(setTo(R.restriction.loginOnly));
        AJS.$("#vr-anonymous").click(setTo(R.restriction.anonymous));

        AJS.$.fn.toggleCheckbox = function(checked) {
            var s = $(this);
            if(!checked) {
                s.removeAttr("checked");
            } else {
                s.attr("checked", "checked");
            }
        };

        getMultipleRoomVoting(pageContext.project.id).done(function(data) {
            AJS.$("#vr-multiple-room-voting").toggleCheckbox(data.multipleRoomVoting);
        });

        AJS.$("#vr-multiple-room-voting").click(H.h(function(e) {
            var multipleRoomVoting = AJS.$(e.target).attr("checked") === "checked";
            var request = setMultipleRoomVoting(pageContext.project.id, multipleRoomVoting);

            AP.require("messages", function(messages){
                //create a message
                request.done(function() {
                    AJS.$(e.target).toggleCheckbox(multipleRoomVoting);
                    messages.success("Multiple room voting updated", "", {
                        fadeout: true,
                        delay: 3000
                    });
                });

                request.fail(function() {
                    messages.error("Failed to update multiple room voting", "Please refresh the page.", {
                        fadeout: true,
                        delay: 5000
                    });
                });
            });
        }));

        refreshVoteableRounds();
        refreshCurrentState(pageContext, roomSelector, hideInactive);
    });
});
