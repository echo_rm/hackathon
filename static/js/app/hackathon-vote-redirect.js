/*
 Important: This file is just one step in the chain. It should be redirected to from hackathon-vote and will pass
 the user to token-save. For more information please see: https://ecosystem.atlassian.net/wiki/display/HACK/Voting+flow+for+all+users
 */
define(["../helpers/PageContext", "../helpers/Meta", 'moment', '../lib/js.cookie', 'connect/pagetoken'], function(PC, Meta, moment, Cookie) {
    "use strict";

    var requestTenantKey = function() {
        return AJS.$.ajax({
            url: "/rest/tenant",
            type: "GET",
            cache: true
        });
    };

    AJS.$(function() {
        var pageContext = PC.load();
        var acptToken = Meta.get("acpt");

        var request = requestTenantKey();

        if(acptToken) {
            AJS.log("Got acptToken: " + acptToken);
            Cookie.set('acpt', acptToken, {
                expires: moment().add(1, 'hours').toDate(),
                path: '/page'
            });
        } else {
            AJS.log("There was no token, this is clearly a mistake, redirecting this user to the hackathon vote url anyway.");
        }

        request.done(function(data) {
            var tenantKey = data.key;
            if(!pageContext.project.id || !tenantKey) {
                AJS.$("#redirect-failed").removeClass("hidden");
            } else {
                window.top.location.href = "/page/hackathon-vote?tenant_key=" + encodeURIComponent(tenantKey) + "&project_id=" + pageContext.project.id + "&acpt=" + encodeURIComponent(acptToken);
            }
        });
    });
});
