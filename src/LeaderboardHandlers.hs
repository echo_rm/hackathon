{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module LeaderboardHandlers
    ( handleLeaderboard
    , handleLeaderboardVotes
    ) where

import           AesonHelpers
import           Application
import           Control.Monad                  (forM)
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Either
import           Control.Monad.Trans.Maybe
import           Data.Aeson
import           Data.Aeson.Types               (fieldLabelModifier)
import qualified Data.HackathonRequest          as HR
import           Data.MaybeUtil
import qualified Entity.TeamDetails             as ETD
import           HandlerHelpers
import           GHC.Generics
import qualified Persistence.Hackathon          as PH
import qualified Persistence.HackathonIds       as HI
import qualified Persistence.HackathonRoom      as HRM
import qualified Persistence.HackathonRound     as RND
import qualified Persistence.HackathonRoundRoom as HRR
import qualified Persistence.HackathonTeam      as HT
import qualified Persistence.HackathonVote      as HV
import qualified Persistence.TenantUser         as TU
import qualified Snap.AtlassianConnect          as AC
import qualified Snap.Core                      as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import           TenantWithVoteableUser

handleLeaderboard :: AppHandler ()
handleLeaderboard = handleMethods
    [ (SC.POST, getLeaderboardResults)
    ]

data LeaderboardRequest = LeaderboardRequest
    { lrHackathon :: HR.HackathonRequest
    , lrRoundId   :: HI.HackathonRoundId
    , lrRoomId    :: HI.HackathonRoomId
    } deriving (Show, Generic)

instance FromJSON LeaderboardRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "lr"
        }

getLeaderboardResults :: AppHandler ()
getLeaderboardResults = do
    request <- SC.readRequestBody size10KB
    let potentialRequest = eitherDecode request :: Either String LeaderboardRequest
    case potentialRequest of
        Left errMsg -> respondWithError badRequest $ "Could not parse json request: " ++ errMsg
        Right lr -> tenantWithVoteableUserFromHackathonRequest (lrHackathon lr) (getLeaderboardResultsWithDetails lr)

getLeaderboardResultsWithDetails :: LeaderboardRequest -> AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
getLeaderboardResultsWithDetails _ _ _ Nothing = respondWithError unauthorised "You are not logged in and you need to be logged in to make this request."
getLeaderboardResultsWithDetails lr tenant hackathon (Just _) = withPG $ do
    loadedData <- runMaybeT $ do
        hRound <- MaybeT $ RND.getHackathonRoundById hackathon (lrRoundId lr)
        hRoom <- MaybeT $ HRM.getRoomById hackathon (lrRoomId lr)
        hRoundRoom <- MaybeT $ HRR.getRoundRoom hRound hRoom
        return (hRound, hRoom, hRoundRoom)
    case loadedData of
        Just (hRound, hRoom, hRoundRoom) ->
            case HRR.hrrStatus hRoundRoom of
                HRR.Locked -> do
                    teamVotes <- HV.countAllVotesOnRoundByRoomByTeam hRound hRoom
                    hTeams <- forM teamVotes $ \(votes, team) -> do
                        teamMembers <- TU.getTeamMembersForIssueId tenant (HT.htIssueId team)
                        return $ ETD.toTeamDetails team teamMembers (Just votes)
                    writeJson . LeaderboardResponse $ hTeams
                _ -> respondWithError unauthorised "You cannot get the leaderboard results until the round-room is locked."
        _ -> respondWithError notFound "Could not find a round-room with the provided id."

data LeaderboardResponse = LeaderboardResponse
    { ldrTeams :: [ETD.TeamDetails]
    } deriving (Show, Generic)

instance ToJSON LeaderboardResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "ldr"
        }

handleLeaderboardVotes :: AppHandler ()
handleLeaderboardVotes = handleMethods
    [ (SC.GET, getVotesForRoundRoom)
    ]

-- Important: This call is completely anonymous. Nothing needs to be provided to make this call. That is why we have
-- tried to optimise it to be lightning fast. I expect this query to be hit a lot.
getVotesForRoundRoom :: AppHandler ()
getVotesForRoundRoom =
    writeError . withPG . runEitherT $ do
        roundId <- EitherT (m2e missingRoundId <$> getIntegerQueryParam "roundId")
        roomId <- EitherT (m2e missingRoomId <$> getIntegerQueryParam "roomId")
        voteCount <- lift $ HV.countAllVotesOnRoundByRoom roundId roomId
        lift $ writeJson . LeaderboardVotesResponse $ voteCount
    where
        missingRoundId = (badRequest, "Missing the roundId query parameter.")
        missingRoomId = (badRequest, "Missing the roomId query parameter.")

data LeaderboardVotesResponse = LeaderboardVotesResponse
    { lvrspVotes :: Integer
    } deriving (Show, Generic)

instance ToJSON LeaderboardVotesResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "lvrsp"
        }
