{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Persistence.RoundTransition
    ( getRoundDetails
    , getRoundTransitions
    , createRoundTransition
    , getRoomsForTransition
    , updateRoundTransitions
    , RoundTransitionDetails
    , RoundTransition(..)
    , RoundTransitionRoom(..)
    , TransitionUpdate(..)
    ) where

import           Application
import           Control.Monad                      (forM)
import qualified Data.JQL                           as J
import qualified Data.Text                          as T
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.SqlQQ
import qualified Persistence.Hackathon              as PH
import qualified Persistence.HackathonIds           as HI
import qualified Persistence.HackathonRoom          as HRM
import qualified Persistence.HackathonRound         as RND
import           Snap.Snaplet.PostgresqlSimple


data RoundTransition = RoundTransition
    { rtId          :: HI.RoundTransitionId
    , rtRoundId     :: HI.HackathonRoundId
    , rtOrderNumber :: Integer
    , rtJQL         :: Maybe J.RawJQL
    }

instance FromRow RoundTransition where
   fromRow = RoundTransition <$> field <*> field <*> field <*> field

data RoundTransitionRoom = RoundTransitionRoom
    { rtrTransitionId :: HI.RoundTransitionId
    , rtrRoomId       :: HI.HackathonRoomId
    }

type RoundTransitionDetails = (RoundTransition, [HRM.HackathonRoom])

getRoundDetails :: RND.HackathonRound -> AppHandler [RoundTransitionDetails]
getRoundDetails hRound = withPG $ do
   transitions <- getRoundTransitions hRound
   forM transitions $ \transition -> do
      rooms <- getRoomsForTransition transition
      return (transition, rooms)

getRoundTransitions :: RND.HackathonRound -> AppHandler [RoundTransition]
getRoundTransitions hRound = query
   [sql|
   SELECT id, round_id, order_number, jql
   FROM round_transition
   WHERE round_id = ?
   |] (Only . RND.hroundId $ hRound)

createRoundTransition :: RND.HackathonRound -> [(Integer, J.RawJQL)] -> AppHandler [RoundTransition]
createRoundTransition = undefined

data TransitionUpdate = TransitionUpdate
   { tuJql :: Maybe T.Text
   , tuRooms :: [HI.HackathonRoomId]
   }

updateRoundTransitions :: RND.HackathonRound -> [TransitionUpdate] -> AppHandler Bool
updateRoundTransitions currentRound updates = withTransaction $ do
   -- Delete all of the existing transitions
   execute 
      [sql|
      DELETE FROM round_transition WHERE round_id = ?
      |] (Only roundId)
   roundTransitionIds <- returning
      [sql|
      INSERT INTO round_transition (round_id, order_number, jql)
      VALUES (?, ?, ?)
      RETURNING id
      |] (zip3 (repeat roundId) nats (fmap tuJql updates))
   executeMany
      [sql|
      INSERT INTO round_transition_room (round_transition_id, room_id)
      VALUES (?, ?)
      |] (roomInserts (fmap fromOnly roundTransitionIds) updates)
   return True
   where
      roundId = RND.hroundId currentRound

      nats :: [Integer]
      nats = [1..]

roomInserts :: [HI.RoundTransitionId] -> [TransitionUpdate] -> [(HI.RoundTransitionId, HI.HackathonRoomId)]
roomInserts transitionIds updates = concat $ zipWith (\tId update -> zip (repeat tId) (tuRooms update)) transitionIds updates
   
getRoomsForTransition :: RoundTransition -> AppHandler [HRM.HackathonRoom]
getRoomsForTransition rt = query
   [sql|
   SELECT r.id, r.hackathon_id, r.current_round_id, r.component_id, r.name, r.short_url
   FROM round_transition_room rtr, room r
   WHERE rtr.round_transition_id = ?
   AND rtr.room_id = r.id
   |] (Only . rtId $ rt)
