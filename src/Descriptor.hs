{-# LANGUAGE OverloadedStrings #-}
module Descriptor
    ( atlassianConnectDescriptor
    ) where

import           Data.Connect.Descriptor
import qualified Data.HackathonConstants as HC
import qualified Data.HashMap.Strict     as HM
import           Data.Maybe              (fromJust)
import           Network.URI

atlassianConnectDescriptor :: Plugin
atlassianConnectDescriptor = baseDescriptor
    { pluginName = Just . Name $ "Hackathon"
    , pluginDescription = Just "The Atlassian Cloud add-on that lets your run hackathon with ease."
    , vendor = Just atlassian
    , modules = Just addonModules
    , lifecycle = Just emptyLifecycle
        { installed = Just . toRelativeURI $ "/installed"
        }
    , scopes = Just [ Read, Write, ProjectAdmin ]
    }

atlassian :: Vendor
atlassian = Vendor
    { vendorName = Name "Atlassian"
    , vendorUrl = toURI "http://www.atlassian.com"
    }

addonModules :: Modules
addonModules = Modules addonJiraModules emptyConfluenceModules

addonJiraModules :: JIRAModules
addonJiraModules = emptyJIRAModules
    { jmWebPanels = Just
        -- Issue Side Panel WebPanel
        [ WebPanel
            { wpKey = "hackathon-team-view"
            , wpName = simpleText "Hackathon"
            , wpTooltip = Just $ simpleText "Hackathon details for this team"
            , wpUrl = "/panel/hackathon-team-view?issue_key={issue.key}&issue_id={issue.id}"
            , wpLocation = "atl.jira.view.issue.right.context"
            , wpConditions = [isHackathonTeamCondition]
            -- When it is a hackathon team this is the most important panel on the issue so put it at the top.
            , wpWeight = Just 0
            , wpLayout = Nothing
            , wpParams = noParams
            }
        , WebPanel
            { wpKey = "hackathon-summary-panel"
            , wpName = simpleText "Hackathon summary"
            , wpTooltip = Just . simpleText $ "The hackathon sumary page"
            , wpUrl = "/panel/hackathon-summary?project_id={project.id}&project_key={project.key}"
            , wpLocation = "{pluginKey}__hackathon-summary-web-item"
            , wpConditions = [isHackathonCondition]
            , wpWeight = Nothing
            , wpLayout = Nothing
            , wpParams = noParams
            }
        ]
    , jmGeneralPages = Just
        [ JIRAPage
            { jiraPageKey = "hackathons"
            , jiraPageName = simpleText "Hackathons"
            , jiraPageUrl = "/page/hackathons"
            , jiraPageLocation = Just "system.top.navigation.bar"
            , jiraPageWeight = Nothing -- Use the default weight
            , jiraPageIcon = Nothing
            , jiraPageConditions = [] -- Always show unconditionally
            , jiraPageParams = noParams
            }
        , JIRAPage
            { jiraPageKey = "hackathon-vote-redirect"
            , jiraPageName = simpleText "Hackathon vote redirect"
            , jiraPageUrl = "/page/hackathon-vote-redirect?project_id={project.id}"
            , jiraPageLocation = Just "none"
            , jiraPageWeight = Nothing -- Use the default weight
            , jiraPageIcon = Nothing
            , jiraPageConditions = [staticJiraCondition UserIsLoggedInJiraCondition]
            , jiraPageParams = noParams
            }
        ]
    , jmWebItems = Just 
        [ WebItem 
            { wiKey = "hackathon-summary-web-item"
            , wiName = simpleText "Hackathon summary"
            , wiLocation = "jira.project.sidebar.plugins.navigation"
            , wiUrl = "/projects/{project.key}?selectedItem={pluginKey}__hackathon-summary-web-item"
            , wiTooltip = Just . simpleText $ "The summary for this hackathon"
            , wiIcon = Just IconDetails
               { iconUrl = "/static/images/hackathon-icon-16.png"
               , iconWidth = Just 16
               , iconHeight = Just 16
               }
            , wiWeight = Nothing
            , wiTarget = Nothing
            , wiStyleClasses = []
            , wiContext = Just ProductContext
            , wiConditions = [isHackathonCondition]
            , wiParams = noParams
            }
        ]
    , jmWebSections = Just
        [ JIRAWebSection
            { jwsKey = "hackathon-project-admin"
            , jwsName = simpleText "Hackathon"
            , jwsLocation = "atl.jira.proj.config"
            , jwsTooltip = Just . simpleText $ "Hackathon administration"
            , jwsConditions = [isHackathonCondition]
            , jwsWeight = Just 0
            , jwsParams = noParams
            }
        ]
    , jmJiraProjectAdminTabPanels = Just
        [ JIRAProjectAdminTabPanel
            { jpatpKey = "hackathon-activation"
            , jpatpName = simpleText "Activate Hackathon"
            , jpatpLocation = "projectgroup1"
            , jpatpUrl = "/panel/activate-hackathon-project?project_id={project.id}&project_key={project.key}"
            , jpatpConditions = [invertCondition isHackathonCondition]
            , jpatpWeight = Just 100
            , jpatpParams = noParams
            }
        , JIRAProjectAdminTabPanel
            { jpatpKey = "hackathon-dashboard"
            , jpatpName = simpleText "Dashboard"
            , jpatpLocation = "hackathon-project-admin"
            , jpatpUrl = "/panel/hackathon-dashboard?project_id={project.id}&project_key={project.key}"
            , jpatpConditions = [] -- The web section makes the condition check for us
            , jpatpWeight = Just 100
            , jpatpParams = noParams
            }
        , JIRAProjectAdminTabPanel
            { jpatpKey = "hackathon-round-transition"
            , jpatpName = simpleText "Round transitions"
            , jpatpLocation = "hackathon-project-admin"
            , jpatpUrl = "/panel/hackathon-round-transition?project_id={project.id}&project_key={project.key}"
            , jpatpConditions = [] -- The web section makes the condition check for us
            , jpatpWeight = Just 110
            , jpatpParams = noParams
            }
        , JIRAProjectAdminTabPanel
            { jpatpKey = "hackathon-statistics"
            , jpatpName = simpleText "Statistics"
            , jpatpLocation = "hackathon-project-admin"
            , jpatpUrl = "/panel/hackathon-statistics?project_id={project.id}&project_key={project.key}"
            , jpatpConditions = [] -- The web section makes the condition check for us
            , jpatpWeight = Just 120
            , jpatpParams = noParams
            }
        ]
    , jmWebhooks = Just
        [ Webhook { webhookEvent = JiraIssueCreated, webhookUrl = "/rest/webhook/issue/create" }
        , Webhook { webhookEvent = JiraIssueUpdated, webhookUrl = "/rest/webhook/issue/update" }
        , Webhook { webhookEvent = JiraIssueDeleted, webhookUrl = "/rest/webhook/issue/delete" }
        ]
    , jmJiraEntityProperties = Just
        [ JIRAEntityProperties
            { jepKey = "hackathon-team-properties"
            , jepName = simpleText "Hackathon Team Properties"
            , jepEntityType = Just IssueEntityType
            , jepKeyConfigurations =
               [ KeyConfiguration
                  { kcPropertyKey = "hackathon"
                  , kcExtractions =
                     [ Extraction
                        { extractionObjectName = "teamMembers"
                        , extractionType = ExtractionTypeString
                        , extractionAlias = Just "hackathonTeamMembers"
                        }
                     , Extraction
                        { extractionObjectName = "teamMemberCount"
                        , extractionType = ExtractionTypeNumber
                        , extractionAlias = Just "hackathonTeamMemberCount"
                        }
                     , Extraction
                        { extractionObjectName = "isTeam"
                        , extractionType = ExtractionTypeString
                        , extractionAlias = Just "isHackathonTeam"
                        }
                     ]
                  }
               ]
            }
        ]
    , jmJiraSearchRequestViews = Just
        [ JIRASearchRequestView
            { jsrvKey = "hackathon-jql-statistics" 
            , jsrvName = simpleText "Hackathon statistics"
            , jsrvDescription = Just . simpleText $ "Get the Hackathon specific statistics for the issues selected by this query."
            , jsrvUrl = "/export/hackathon-statistics.json"
            , jsrvWeight = Nothing
            , jsrvConditions = []
            , jsrvParams = noParams
            }
        ]  
    }

isHackathonCondition :: Condition
isHackathonCondition = (staticJiraCondition EntityPropertyEqualToJiraCondition)
   { conditionParams = HM.fromList
      [ ("entity", "project")
      , ("propertyKey", HC.projectSettingsKey)
      , ("objectName", "isHackathon")
      , ("value", "true")
      ]
   }

isHackathonTeamCondition :: Condition
isHackathonTeamCondition = (staticJiraCondition EntityPropertyEqualToJiraCondition)
   { conditionParams = HM.fromList
      [ ("entity", "issue")
      , ("propertyKey", "hackathon")
      , ("objectName", "isTeam")
      , ("value", "true")
      ]
   }

-- Required Modules:
-- General Page for Room Selection
-- General Page for Voting
-- Admin page for Hackathon conversion when not a Hackathon
-- Admin pages for admin operations. Each which appears in the sidebar
-- Webhooks for issue and project update events

-- Potential Modules:
-- hackathon History Module (Past Votes, Past States etc)

baseDescriptor :: Plugin
baseDescriptor = pluginDescriptor
    (PluginKey "com.atlassian.cloud.hackathon")
    defaultBaseUrl
    (Authentication Jwt)

defaultBaseUrl :: URI
defaultBaseUrl = toURI "http://localhost:8000"

toURI :: String -> URI
toURI = fromJust . parseURI

toRelativeURI :: String -> URI
toRelativeURI = fromJust . parseRelativeReference
