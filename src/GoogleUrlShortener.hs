{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module GoogleUrlShortener
  ( shortenUrl
  , ShortenedUrl(..)
  ) where

import           AesonHelpers
import qualified AppConfig                         as CONF
import           Application
import           Data.Aeson
import           Data.Aeson.Types                  (fieldLabelModifier)
import           Data.ByteString.Char8
import           Data.Maybe                        (fromJust)
import           Data.Monoid
import qualified Data.Text                         as T
import           GHC.Generics
import           Network.Api.Support.Request
import           Network.Helpers
import           Network.HTTP.Types.Method         (StdMethod (POST))
import           Network.URI
import qualified Snap.AtlassianConnect.HostRequest as AC

data ShortenRequest = ShortenRequest
   { srLongUrl :: URI
   } deriving (Show, Generic)

instance ToJSON ShortenRequest where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "sr" }

data ShortenedUrl = ShortenedUrl
   { suKind    :: T.Text
   , suId      :: URI
   , suLongUrl :: URI
   } deriving (Show, Generic)

instance FromJSON ShortenedUrl where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "su" }

shortenUrl :: URI -> AppHandler (Either AC.ProductErrorResponse ShortenedUrl)
shortenUrl longUrl = do
   conf <- CONF.getAppConf
   webRequest POST uri
      (  setQueryParams (params conf)
      <> setJson (ShortenRequest longUrl)
      )
   where
      uri = fromJust . parseURI $ "https://www.googleapis.com/urlshortener/v1/url"
      params :: CONF.AppConf -> [ (ByteString, Maybe ByteString) ]
      params conf =
         [ ("key", Just . CONF.acUrlShortenerKey $ conf)
         ]
