module ShortenHelper where

import qualified Snap.AtlassianConnect as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import Links
import GoogleUrlShortener
import Application
import qualified Persistence.Hackathon as PH
import qualified Persistence.HackathonRoom as HRM

ensureHackathonWithShortUrl :: AC.Tenant -> PH.Hackathon -> AppHandler (Either AC.ProductErrorResponse PH.Hackathon)
ensureHackathonWithShortUrl tenant h =
   case PH.hackathonShortUrl h of
      (Just _) -> return . Right $ h
      Nothing -> do
         vUrl <- votingLink tenant h Nothing
         potentialVotingUrl <- shortenUrl vUrl
         case potentialVotingUrl of
           (Left x) -> return . Left $ x
           (Right shortenedUrl) -> do
              PH.updateHackathonShortUrl h (suId shortenedUrl)
              return . Right $ h { PH.hackathonShortUrl = Just . show . suId $ shortenedUrl }

ensureRoomWithShortUrl :: AC.Tenant -> PH.Hackathon -> HRM.HackathonRoom -> AppHandler (Either AC.ProductErrorResponse HRM.HackathonRoom)
ensureRoomWithShortUrl tenant h room = 
   case HRM.hrShortUrl room of
     (Just _) -> return . Right $ room
     Nothing -> do
        vUrl <- votingLink tenant h (Just room)
        potentialVotingUrl <- shortenUrl vUrl
        case potentialVotingUrl of
          (Left x) -> return . Left $ x
          (Right shortenedUrl) -> do
             HRM.updateShortUrl room (suId shortenedUrl)
             return . Right $ room { HRM.hrShortUrl = Just . show . suId $ shortenedUrl }
