{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}

module TeamMemberHandlers
  ( handleTeamMember
  , handleTeamMembers
  ) where

import           AesonHelpers
import           Application
import qualified Control.Arrow                   as A
import           Control.Monad                   (void)
import           Control.Monad.Trans.Class       (lift)
import           Control.Monad.Trans.Either
import           Data.Aeson
import           Data.Aeson.Types                (fieldLabelModifier)
import           Data.HackathonConstants
import qualified Data.HTProperties               as HTP
import           Data.MaybeUtil
import           Data.Ord
import qualified Entity.TeamMember               as ETM
import           GHC.Generics
import           HandlerHelpers
import qualified HostRequests.EntityProperties   as EP
import qualified HostRequests.UserDetails        as UD
import qualified HostRequests.WatchIssue         as WI
import qualified Persistence.Hackathon           as PH
import qualified Persistence.HackathonRound      as RND
import qualified Persistence.HackathonTeam       as PHT
import qualified Persistence.HackathonTeamMember as PHTM
import qualified Persistence.TenantUser          as TU
import qualified Snap.AtlassianConnect           as AC
import qualified Snap.Core                       as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import qualified TimezoneLabels                  as TL
import           UserUtil
import qualified WithToken                       as WT

data ChangeTeamMemberRequest = CTMR
    { ctmrIssueId :: AC.IssueId
    , ctmrUserKey :: AC.UserKey
    } deriving (Show, Generic)

instance FromJSON ChangeTeamMemberRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "ctmr"
        }

data TeamMembersResponse = TeamMembersResponse
    { tmrsTeamMembers :: [ETM.TeamMemberResponse]
    } deriving (Eq, Show, Generic)

instance ToJSON TeamMembersResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "tmrs"
        }

handleTeamMember :: AppHandler ()
handleTeamMember = handleMethods
   [ (SC.PUT, WT.tenantFromToken . changeTeamMemberHandler $ addTeamMember)
   , (SC.DELETE, WT.tenantFromToken . changeTeamMemberHandler $ removeTeamMember)
   ]

handleTeamMembers :: AppHandler ()
handleTeamMembers = handleMethods
   [ (SC.GET, WT.tenantFromToken getTeamMembers)
   ]

standardAuthError :: AppHandler ()
standardAuthError = respondWithError unauthorised "You need to login before you query for team members."

getTeamMembers :: AC.TenantWithUser -> AppHandler ()
getTeamMembers (_, Nothing) = standardAuthError
getTeamMembers (tenant, Just userKey) = do
   potentialIssueId <- getIntegerQueryParam "issueId"
   case potentialIssueId of
      Just issueId -> do
         tenantUsers <- TU.getTeamMembersForIssueId tenant issueId
         writeJson (TeamMembersResponse $ fmap ETM.toTeamMemberResponse tenantUsers)
      Nothing -> respondWithError badRequest "No issueId is passed into the get call."

changeTeamMemberHandler :: (AC.Tenant -> AC.UserKey -> ChangeTeamMemberRequest -> AppHandler ()) -> AC.TenantWithUser -> AppHandler ()
changeTeamMemberHandler _ (_, Nothing) = respondWithError unauthorised "You need to be logged in so that you can add a team member."
changeTeamMemberHandler handler (tenant, Just requestUser) = do
    request <- SC.readRequestBody size10KB
    let maybeChangeRequest = eitherDecode request :: Either String ChangeTeamMemberRequest
    case maybeChangeRequest of
        Left err -> respondWithError badRequest err
        Right changeRequest -> handler tenant requestUser changeRequest

addTeamMember :: AC.Tenant -> AC.UserKey -> ChangeTeamMemberRequest -> AppHandler ()
addTeamMember tenant requestUser ctmr = writeError . withPG . runEitherT $ do
   hackathonTeam <- EitherT (m2e noTeamWithIssueId <$> PHT.getHackathonTeamByIssueId tenant (ctmrIssueId ctmr))
   hackathon <- lift $ PHT.getHackathonFromTeam hackathonTeam
   teamMembers <- lift $ TU.getTeamMembersForIssueId tenant (ctmrIssueId ctmr)
   guardCanChangeTeamMembers tenant requestUser hackathon hackathonTeam teamMembers
   tenantUser <- EitherT (withErrCode badRequest <$> getOrCreateTenantUserByKey tenant (ctmrUserKey ctmr))
   void . lift $ PHTM.addTeamMember tenantUser (PHT.htId hackathonTeam)
   let allTeamMembers = tenantUser : filter (not . sameTenantUser tenantUser) teamMembers
   TL.updateTimezoneLabels tenant hackathonTeam allTeamMembers
   EitherT (A.left (const watcherUpdateFailed) <$>  WI.watchTeam tenant hackathonTeam (ctmrUserKey ctmr))
   EitherT (A.left (const htpUpdateFailed) <$> EP.updateIssueProperties tenant (ctmrIssueId ctmr) HTP.issuePropertyKey (HTP.fromUserKeys . fmap TU.tuKey $ allTeamMembers))
   void . lift $ respondNoContent

noTeamWithIssueId :: (Int, String)
noTeamWithIssueId = (badRequest, "There is no issue with that key on this tenant that is a Hackathon team.")

watcherUpdateFailed :: (Int, String)
watcherUpdateFailed = (internalServer, "Could not make this user a watcher of the issue")

htpUpdateFailed :: (Int, String)
htpUpdateFailed = (internalServer, "Could not update the hackathon properties on this issue.")

-- If team members are allowed to vote on their own teams then you can ignore all of this logic and can add and remove team members as you wish
-- (Is team member AND in team formation) OR is project admin
ensureTeamMember :: Monad b => AC.UserKey -> [TU.TenantUser] -> EitherT (Int, String) b ()
ensureTeamMember requestUser teamMembers = if requestUser `elem` teamMemberKeys
   then return ()
   else left (unauthorised, "To add a team member to this team you need to be a team member yourself and the team needs to be in 'team formation'. If you should be in this team please contact the person running your Hackathon.")
   where
      teamMemberKeys = fmap TU.tuKey teamMembers

isTeamFormation :: RND.HackathonRound -> Bool
isTeamFormation = (==) TeamFormation . RND.hroundType

selfVotingBlocked :: PH.Hackathon -> Bool
selfVotingBlocked = (==) LoginAndTeamMember . PH.hackathonVotingRestriction

guardCanChangeTeamMembers :: AC.Tenant -> AC.UserKey -> PH.Hackathon -> PHT.HackathonTeam -> [TU.TenantUser] -> EitherT (Int, String) AppHandler ()
guardCanChangeTeamMembers tenant requestUser hackathon hTeam teamMembers =
   if selfVotingBlocked hackathon
      then do
         hRound <- lift $ PHT.getRoundForTeam hTeam
         if maybe True isTeamFormation hRound
            then ensureTeamMember requestUser teamMembers
            else EitherT (A.left (const notInTeamFormation) <$> requirePermissionsE [UD.ProjectAdmin] tenant requestUser hackathon)
      else return () -- Team members don't affect voting so let anybody manipulate them
   where
      notInTeamFormation = (unauthorised, "You cannot make this request because this team is not in team formation and you are not a project administrator.")


removeTeamMember :: AC.Tenant -> AC.UserKey -> ChangeTeamMemberRequest -> AppHandler ()
removeTeamMember tenant requestUser ctmr = writeError . withPG . runEitherT $ do
    teamMembers <- lift $ TU.getTeamMembersForIssueId tenant (ctmrIssueId ctmr)
    guardMoreThanOneRemains teamMembers
    hackathonTeam <- EitherT (m2e noTeamWithIssueId <$> PHT.getHackathonTeamByIssueId tenant (ctmrIssueId ctmr))
    hackathon <- lift $ PHT.getHackathonFromTeam hackathonTeam
    guardCanChangeTeamMembers tenant requestUser hackathon hackathonTeam teamMembers
    tenantUser <- EitherT (withErrCode badRequest <$> getOrCreateTenantUserByKey tenant (ctmrUserKey ctmr))
    void . lift $ PHTM.removeTeamMemberByIssueId tenant tenantUser (ctmrIssueId ctmr)
    let remainingTeamMembers = filter (not . sameTenantUser tenantUser) teamMembers
    TL.updateTimezoneLabels tenant hackathonTeam remainingTeamMembers
    EitherT (A.left (const htpUpdateFailed) <$> EP.updateIssueProperties tenant (ctmrIssueId ctmr) HTP.issuePropertyKey (HTP.fromUserKeys . fmap TU.tuKey $ remainingTeamMembers))
    void . lift $ respondNoContent
    where
       guardMoreThanOneRemains :: Monad x => [TU.TenantUser] -> EitherT (Int, String) x ()
       guardMoreThanOneRemains xs
         | length xs <= 1 = left (unauthorised, "You cannot remove the last member of a Hackathon team. Add somebody else first and then try again.")
         | otherwise = return ()

sameTenantUser :: TU.TenantUser -> TU.TenantUser -> Bool
sameTenantUser a = (==) EQ . comparing TU.tuId a
