define([
   '../helpers/PageContext',
   '../helpers/Meta',
   '../helpers/MustacheLoader',
   'underscore',
   'moment',
   '../helpers/Links',
   '../helpers/Handler',
   'connect/pagetoken'
],
function (PageContext, Meta, MustacheLoader, _, moment, Links, H) {
    "use strict";

    var formatDate = function (date) {
        var prettyDate = "DD MMM YYYY hh:mmA";
        return moment(date).format(prettyDate);
    };

    var acptToken = Meta.get("acpt");
    var token = {
        acptToken: acptToken
    };

    var requestTenantKey = function() {
        return AJS.$.ajax({
            url: "/rest/tenant",
            type: "GET",
            cache: true
        });
    };

    var requestHackathonSummary = function(voteableUser, projectId) {
        return AJS.$.ajax({
            url: "/rest/hackathon/summary",
            type: "POST",
            cache: false,
            data: JSON.stringify({
                token: voteableUser,
                projectId: projectId
            })
        });
    };

    var requestHackathonStatistics = function(projectId) {
       return AJS.$.ajax({
          url: "/rest/statistics/hackathon",
          type: "GET",
          cache: false,
          data: {
             projectId: projectId
          }
       });
    };

    var requestMyTeams = function(projectId, userKey) {
       return AJS.$.Deferred(function(self) {
          AP.require(['request'], function(request) {
             request({
                url: '/rest/api/2/search',
                type: 'GET',
                data: {
                   jql: 'project = ' + projectId + ' AND hackathonTeamMembers = "' + userKey + '" ORDER BY key DESC'
                },
                cache: false,
                success: function(data) {
                   self.resolve(JSON.parse(data));
                }, error: self.reject
             });
          });
       });
    };

    AJS.$(function() {
        var pageContext = PageContext.load();
        var templates = MustacheLoader.load();

        var getSumOfField = function (data, field) {
            return _.reduce(_.pluck(data, field), function (memo, num) {
                return memo + num;
            }, 0);
        };

        var lozengeForStatus = function(status) {
            if(status === "Active") return "aui-lozenge-success";
            if(status === "Locked") return "aui-lozenge-error";
            return "";
        };

        var lozengeForStatusCategory = function(statusCategory) {
           if(statusCategory.colorName === 'yellow') return "aui-lozenge-current";
           if(statusCategory.colorName === 'green') return "aui-lozenge-success";
           if(statusCategory.colorName === 'blue-gray') return "aui-lozenge-complete";
           return "";
        };

        AJS.$(window).focus(function() {
           populateSummary();
        });

        var isActive = function(roundRoom) { return roundRoom.status === "Active"; };
        var isLocked = function(roundRoom) { return roundRoom.status === "Locked"; };

        var populateSummary = function () {
            var summaryRequest = requestHackathonSummary(token, pageContext.project.id);
            var tenantRequest = requestTenantKey();
            var ftcRequest = requestHackathonStatistics(pageContext.project.id);
            var myTeamsRequest = requestMyTeams(pageContext.project.id, pageContext.user.key);

            var request = AJS.$.when(summaryRequest, tenantRequest, ftcRequest);

            request.done(function (summaryResult, tenantKeyResult, ftcResult){
                var sr = summaryResult[0];
                var tenantKey = tenantKeyResult[0].key;
                var ftcCount = ftcResult[0].formingTeams;
                var competitorCount = ftcResult[0].competitors;

                var roundRoomVotes = sr.roundRoomVotes;
                var activeContainer = AJS.$('#active-room-summary-table tbody');
                var lockedContainer = AJS.$('#locked-room-summary-table tbody');

                AJS.$("#project-title").text(sr.projectName);

                AJS.$("#go-create").click(H.h(function() {
                    var handleIssuesCreated = function() {
                       // After a team is created then refresh everything
                       populateSummary();
                    };

                    var createData = {
                       pid: pageContext.project.id,
                       issueType: sr.teamIssueTypeId
                    };

                    AP.require('jira', function(jira) {
                       jira.openCreateIssueDialog(handleIssuesCreated, createData);
                    });
                }));

                // Setup the vote url
                var voteUrl = sr.shortUrl;
                AJS.$("#go-vote").click(function() {
                   window.top.location.href = voteUrl;
                });
                AJS.$(".go-vote-link").attr('href', voteUrl);

                var isCurrentActiveOrLocked = function(roundRoom) {
                   return (isActive(roundRoom) && roundRoom.isCurrentRound) || isLocked(roundRoom);
                };

                roundRoomVotes = _.filter(roundRoomVotes, isCurrentActiveOrLocked);

                roundRoomVotes = roundRoomVotes.sort(function(left, right) {
                   if(left.round.sequenceNumber !== right.round.sequenceNumber) return right.round.sequenceNumber - left.round.sequenceNumber;
                   if(left.numberOfVotes !== right.numberOfVotes) return right.numberOfVotes - left.numberOfVotes;
                   return right.numberOfTeams - left.numberOfTeams;
                });

                var activeRoundRoomVotes = _.filter(roundRoomVotes, isActive);
                var totalTeams = getSumOfField(activeRoundRoomVotes, 'numberOfTeams');
                var totalVotes = getSumOfField(activeRoundRoomVotes, 'numberOfVotes');

                AJS.$('#hackathon-statistics').empty().append(templates.render('summary-statistics', {
                    formingTeams: ftcCount,
                    competitors: competitorCount,
                    presentingTeams: totalTeams,
                    totalVotes: totalVotes,
                    formattedStartDate: formatDate(sr.startDate),
                    formattedEndDate: formatDate(sr.endDate)
                }));

                var lockedRoundRoomVotes = _.filter(roundRoomVotes, isLocked);

                var activeDisplay = AJS.$(".active-rooms");
                if(activeRoundRoomVotes.length > 0) {
                   // show the active round room section
                   activeDisplay.removeClass("hidden");
                   var lastRound = null;
                   activeContainer.empty();
                   _.each(activeRoundRoomVotes, function(roundRoom) {
                      // Draw the header for the round
                      if(lastRound != roundRoom.round.id) {
                         activeContainer.append(templates.render('room-summary-round-heading', roundRoom.round));
                      }
                      lastRound = roundRoom.round.id;

                      // Draw the round itself
                      roundRoom.statusLozengeClass = lozengeForStatus(roundRoom.status);
                      roundRoom.leaderboardUrl = Links.leaderboard(tenantKey, pageContext.project.id, pageContext.project.key, roundRoom.round.id, roundRoom.room.id, acptToken);
                      roundRoom.viewTeamsUrl = Links.viewTeams(pageContext.productBaseUrl, sr, roundRoom.round, roundRoom.room);
                      roundRoom.showVotingLink = true; 
                      activeContainer.append(templates.render('room-summary-row', roundRoom));
                   });
                } else {
                   activeDisplay.addClass("hidden");
                }

                var lockedDisplay = AJS.$(".locked-rooms");
                if(lockedRoundRoomVotes.length > 0) {
                   lockedDisplay.removeClass("hidden");
                   var lastRound = null;
                   lockedContainer.empty();
                   _.each(lockedRoundRoomVotes, function(roundRoom) {
                      // Draw the header for the round
                      if(lastRound != roundRoom.round.id) {
                         lockedContainer.append(templates.render('room-summary-round-heading', roundRoom.round));
                      }
                      lastRound = roundRoom.round.id;

                      // Draw the round itself
                      roundRoom.statusLozengeClass = lozengeForStatus(roundRoom.status);
                      roundRoom.leaderboardUrl = Links.leaderboard(tenantKey, pageContext.project.id, pageContext.project.key, roundRoom.round.id, roundRoom.room.id, acptToken);
                      roundRoom.viewTeamsUrl = Links.viewTeams(pageContext.productBaseUrl, sr, roundRoom.round, roundRoom.room);
                      lockedContainer.append(templates.render('room-summary-row', roundRoom));
                   });
                } else {
                   lockedDisplay.addClass("hidden");
                }
            });

            request.fail(function() {
               AP.require('messages', function(message) {
                  message.warning("Could not load the latest summary information", "Please refresh the page to try again.", {
                     delay: 5000
                  });
               });
            });

            myTeamsRequest.done(function(searchResults) {
               var haveResults = searchResults.total > 0;
               if(haveResults) {
                  AJS.$(".your-teams h4").text(searchResults.total == 1 ? "Your team" : "Your teams");

                  var myTeams = AJS.$("#my-teams");
                  myTeams.empty();
                  AJS.$.each(searchResults.issues, function(i, issue) {
                     issue.viewUrl = pageContext.productBaseUrl + '/browse/' + issue.key;
                     issue.lozengeClass = lozengeForStatusCategory(issue.fields.status.statusCategory);
                     myTeams.append(templates.render('my-team', issue));
                  });
               }
               AJS.$(".your-teams").toggleClass('hidden', !haveResults);
            });
        };


        populateSummary();
    });


});
