{-# LANGUAGE OverloadedStrings #-}
module StandardJQL
  ( jqlForHackathonIssues
  , jqlForTeam
  ) where

import           Data.JQL
import qualified Data.Text                 as T
import qualified Persistence.Hackathon     as PH
import qualified Persistence.HackathonTeam as HT

s :: Show a => a -> T.Text
s = T.pack . show

jqlForHackathonIssues :: PH.Hackathon -> JQL
jqlForHackathonIssues hackathon = And
    (Eq "project" projectId)
    [Eq "issuetype" issueTypeId
    ]
    where
        projectId = s . PH.hackathonProjectId $ hackathon
        issueTypeId = s . PH.hackathonTeamIssueTypeId $ hackathon

jqlForTeam :: HT.HackathonTeam -> JQL
jqlForTeam ht = Eq "id" (s . HT.htIssueId $ ht)
