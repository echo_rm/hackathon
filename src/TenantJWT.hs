module TenantJWT (
  withTenant,
  withMaybeTenant
  ) where

import           Application
import qualified Control.Arrow              as A
import           Control.Monad              (guard, when)
import           Control.Monad.IO.Class     (liftIO)
import           Control.Monad.Trans.Class  (lift)
import           Control.Monad.Trans.Either
import qualified Data.ByteString.Char8      as B
import qualified Data.CaseInsensitive       as DC
import           Data.Maybe                 (isJust)
import           Data.MaybeUtil
import qualified Data.Text                  as T
import qualified Data.Time.Clock.POSIX      as X
import           HandlerHelpers
import qualified Persistence.Tenant         as PT
import qualified Snap.AtlassianConnect      as AC
import qualified Snap.Core                  as SC
import qualified Snap.Helpers               as SH
import qualified Web.JWT                    as J

-- TODO Should this be moved into the Atlassian connect code? Or does the app handler code make it too specific?
-- TODO Can we make it not wrap the request but instead run inside the request? That will let it be moved out.

type UnverifiedJWT = J.JWT J.UnverifiedJWT

withTenant :: (AC.TenantWithUser -> AppHandler ()) -> AppHandler ()
withTenant tennantApply = writeErrors . runEitherT $ do
  unverifiedJwt <- EitherT (withErrs SH.badRequest . firstRightOrLefts <$> sequence [getJWTTokenFromParam, getJWTTokenFromAuthHeader])
  tenant <- EitherT (withErrs SH.badRequest . A.left return <$> getTenant unverifiedJwt)
  lift . tennantApply $ tenant

withMaybeTenant :: (Maybe AC.TenantWithUser -> AppHandler ()) -> AppHandler ()
withMaybeTenant tenantApply = do
  parsed <- sequence [getJWTTokenFromParam, getJWTTokenFromAuthHeader]
  case firstRightOrLefts parsed of
    Left _ -> tenantApply Nothing
    Right unverifiedJwt -> do
      possibleTenant <- either (const Nothing) Just <$> getTenant unverifiedJwt
      tenantApply possibleTenant

decodeByteString :: B.ByteString -> Maybe UnverifiedJWT
decodeByteString = J.decode . SH.byteStringToText

-- Standard GET requests (and maybe even POSTs) from Atlassian Connect will put the jwt header in a
-- param in either the query params or in form params. This method will extract it from either.
getJWTTokenFromParam :: AppHandler (Either String UnverifiedJWT)
getJWTTokenFromParam = m2e noJwtParam <$> getJwt
   where
      getJwt = readJwt =<< getRawJwt

      getRawJwt :: AppHandler (Maybe B.ByteString)
      getRawJwt = SC.getParam . B.pack $ "jwt"

      readJwt :: Maybe B.ByteString -> AppHandler (Maybe UnverifiedJWT)
      readJwt x = return (decodeByteString =<< x)

      noJwtParam = "There was no JWT param in the request"

-- Sometimes Atlassian Connect will pass the JWT token in an Authorization header in your requests
-- in this format:
-- Authorization: JWT <token>
-- This method will extract the JWT token from the Auth header if it is present.
getJWTTokenFromAuthHeader :: AppHandler (Either String UnverifiedJWT)
getJWTTokenFromAuthHeader = do
   authHeader <- fmap (SC.getHeader authorizationHeaderName) SC.getRequest
   case authHeader of
      Just firstHeader -> if B.isPrefixOf jwtPrefix firstHeader
         then return $ maybe (Left "The JWT Auth header could not be parsed.") Right (decodeByteString . dropJwtPrefix $ firstHeader)
         else return . Left $ "The Authorization header did not contain a JWT token: " ++ show firstHeader
      _ -> return . Left $ "There was no Authorization header in the request."
   where
      jwtPrefix = B.pack "JWT "
      dropJwtPrefix = B.drop (B.length jwtPrefix)

authorizationHeaderName :: DC.CI B.ByteString
authorizationHeaderName = DC.mk . B.pack $ "Authorization"

firstRightOrLefts :: [Either b a] -> Either [b] a
firstRightOrLefts = flipEither . sequence . fmap flipEither

flipEither :: Either a b -> Either b a
flipEither (Left x)  = Right x
flipEither (Right x) = Left x

getTenant :: UnverifiedJWT -> AppHandler (Either String AC.TenantWithUser)
getTenant unverifiedJwt = runEitherT $ do
  currentPosixTime <- liftIO X.getPOSIXTime
  when (J.numericDate currentPosixTime > (J.exp . J.claims $ unverifiedJwt)) (left tokenExpired)
  key <- hoistEither . m2e jwtParseFail . getClientKey $ unverifiedJwt
  unverifiedTenant <- EitherT (m2e (noSuchTenant key) <$> PT.lookupTenant (T.pack . show $ key))
  verifiedTenant <- hoistEither . m2e invalidSignature $ verifyTenant unverifiedTenant unverifiedJwt
  return (verifiedTenant, getUserKey unverifiedJwt)
  where
     tokenExpired = "The JWT token in this request has expired."
     jwtParseFail = "Could not parse the JWT message."
     noSuchTenant key = "Could not find a tenant with that id: " ++ show key
     invalidSignature = "Invalid signature for request. Danger! Request ignored."

verifyTenant :: AC.Tenant -> UnverifiedJWT -> Maybe AC.Tenant
verifyTenant tenant unverifiedJwt = do
  guard (isJust $ J.verify tenantSecret unverifiedJwt)
  pure tenant
  where
    tenantSecret = J.secret . AC.sharedSecret $ tenant

getClientKey :: J.JWT a -> Maybe J.StringOrURI
getClientKey jwt = J.iss . J.claims $ jwt

getUserKey :: J.JWT a -> Maybe T.Text
getUserKey jwt = fmap (T.pack . show) (getSub jwt)
   where
      getSub = J.sub . J.claims
