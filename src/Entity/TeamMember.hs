{-# LANGUAGE DeriveGeneric #-}
module Entity.TeamMember
    ( TeamMemberResponse
    , toTeamMemberResponse
    ) where

import           AesonHelpers
import           Data.Aeson
import           Data.Aeson.Types         (fieldLabelModifier)
import qualified Data.Text                as T
import qualified Data.Text.Encoding       as T
import           Data.Time.Clock          (UTCTime)
import           GHC.Generics
import qualified Persistence.HackathonIds as HI
import qualified Persistence.TenantUser   as TU
import qualified Snap.AtlassianConnect    as AC

data TeamMemberResponse = TeamMemberResponse
    { tmrId             :: HI.TenantUserId
    , tmrKey            :: AC.UserKey
    , tmrEmail          :: T.Text
    , tmrDisplayName    :: T.Text
    , tmrSmallAvatarUrl :: Maybe T.Text
    , tmrLastUpdated    :: UTCTime
    } deriving (Eq, Show, Generic)

instance ToJSON TeamMemberResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "tmr"
        }

toTeamMemberResponse :: TU.TenantUser -> TeamMemberResponse
toTeamMemberResponse tu = TeamMemberResponse
   { tmrId = TU.tuId tu
   , tmrKey = TU.tuKey tu
   , tmrEmail = T.decodeUtf8 . TU.tuEmail $ tu
   , tmrDisplayName = TU.tuDisplayName tu
   , tmrSmallAvatarUrl = TU.tuSmallAvatarUrl tu
   , tmrLastUpdated = TU.tuLastUpdated tu
   }
