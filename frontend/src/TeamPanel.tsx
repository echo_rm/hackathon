import * as React from 'react';
import { PageContext } from './page-context';
import styled from 'styled-components';
import * as akss from '@atlaskit/util-shared-styles';
import { TeamMembersResponse, TeamDetailsWithPastResults } from './hackathon-client';
import Badge from '@atlaskit/badge';
import Avatar from '@atlaskit/avatar';
import Button from '@atlaskit/button';
import EditorRemoveIcon from '@atlaskit/icon/glyph/editor/remove';
import RefreshIcon from '@atlaskit/icon/glyph/refresh';
import Lozenge, { LozengeAppearance } from '@atlaskit/lozenge';
import { AsyncSelect } from '@atlaskit/select';

export type TeamPanelProps = {
   pc: PageContext;
   teamDetails: TeamDetailsWithPastResults;
   teamMembers: TeamMembersResponse;
   onRefreshClick(): void;
   onTeamMemberRemove(userKey: string): void;
};

export class TeamPanel extends React.PureComponent<TeamPanelProps> {
   private static TopHeading = styled.p`
      color: ${akss.akColorN200};
      padding-bottom: 8px;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
   `;

   private static Heading = styled.p`
      color: ${akss.akColorN200};
      padding-bottom: 8px;
   `;

   private static TeamMemberContainer = styled.div`
      display: flex;
      flex-direction: row;
      align-items: center;
      width: 100%;
   `;

   private static TeamMemberName = styled.div`
      flex-grow: 1;
      padding: 0 8px;
   `;

   private static getLozengeForRank(rank: number): LozengeAppearance {
      if (rank <= 3) {
         return 'success';
      }
      if (rank <= 6) {
         return 'inprogress';
      }
      return 'default';
   }

   render() {
      const { teamDetails, onRefreshClick, onTeamMemberRemove } = this.props;
      const teamMembers = this.props.teamMembers.teamMembers;

      const renderedTeamMembers = teamMembers.map(teamMember => {
         return (
            <TeamPanel.TeamMemberContainer key={teamMember.id}>
               <Avatar
                  appearance="square"
                  size="small"
                  name={teamMember.displayName}
                  src={teamMember.smallAvatarUrl}
               />
               <TeamPanel.TeamMemberName>{teamMember.displayName}</TeamPanel.TeamMemberName>
               <Button appearance="subtle" spacing="compact" onClick={() => onTeamMemberRemove(teamMember.key)}>
                  <EditorRemoveIcon label="delete"/>
               </Button>
            </TeamPanel.TeamMemberContainer>
         );
      });

      const roundName: string = teamDetails.round === undefined ? 'Not in a round' : teamDetails.round.name;
      const roomName: string = teamDetails.room === undefined ? 'Not in a room' : teamDetails.room.name;
      return (
         <div>
            <TeamPanel.TopHeading>
               <span>Team members <Badge>{teamMembers.length}</Badge></span>
               <Button appearance="subtle" onClick={() => onRefreshClick()}><RefreshIcon label="refresh" /></Button>
            </TeamPanel.TopHeading>
            {renderedTeamMembers}
            <AsyncSelect
               defaultOptions={true}
               loadOptions={async () => [{ label: 'Test', value: 'test' }]}
               placeholder="Add team member..."
            />
            <TeamPanel.Heading>Round</TeamPanel.Heading>
            <Lozenge>{roundName}</Lozenge>
            <TeamPanel.Heading>Room</TeamPanel.Heading>
            <Lozenge>{roomName}</Lozenge>
            {this.renderPastResults()}
         </div>
      );
   }

   private renderPastResults(): JSX.Element[] {
      const pastResults = this.props.teamDetails.pastResults;

      if (pastResults.length === 0) {
         return [];
      }

      const pastResultRows = pastResults
         .sort((a, b) => b.roundSequenceNumber - a.roundSequenceNumber)
         .map(pastResult => {
            // TODO Show a link to the leaderboard in these results, requires the tenant key and room / round ids
            return (
               <tr key={pastResult.roundName}>
                  <td>{pastResult.roundName}</td>
                  <td><Badge max={10000}>{pastResult.votes}</Badge></td>
                  <td>
                     <Lozenge appearance={TeamPanel.getLozengeForRank(pastResult.rank)}>
                        {`#${pastResult.rank}`}
                     </Lozenge>
                  </td>
               </tr>
            );
         });

      return [
         <TeamPanel.Heading key="pastResultsHeading">Past results</TeamPanel.Heading>,
         (
            <table key="pastResults">
               <thead>
                  <th>Round</th>
                  <th>Votes</th>
                  <th>Rank</th>
               </thead>
               <tbody>
                  {pastResultRows}
               </tbody>
            </table>
         )
      ];
   }
}