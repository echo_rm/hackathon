{-# LANGUAGE DeriveGeneric     #-}
module Entity.TeamDetails
    ( TeamDetails
    , toTeamDetails
    ) where

import           AesonHelpers
import           Data.Aeson
import           Data.Aeson.Types          (fieldLabelModifier)
import qualified Entity.TeamMember         as ETM
import qualified Persistence.HackathonIds  as HI
import qualified Persistence.HackathonTeam as HT
import qualified Persistence.TenantUser    as TU
import qualified Snap.AtlassianConnect     as AC
import GHC.Generics

toTeamDetails :: HT.HackathonTeam -> [TU.TenantUser] -> Maybe Integer -> TeamDetails
toTeamDetails team teamMembers votes = TeamDetails
    { tdId = HT.htId team
    , tdRoomId = HT.htRoomId team
    , tdRoundId = HT.htRoundId team
    , tdIssueId = HT.htIssueId team
    , tdIssueKey = HT.htIssueKey team
    , tdIssueSummary = HT.htIssueSummary team
    , tdVotes = votes
    , tdTeamMembers = fmap ETM.toTeamMemberResponse teamMembers
    }

data TeamDetails = TeamDetails
    { tdId           :: HI.HackathonTeamId
    , tdRoomId       :: Maybe HI.HackathonRoomId
    , tdRoundId      :: Maybe HI.HackathonRoundId
    , tdIssueId      :: AC.IssueId
    , tdIssueKey     :: AC.IssueKey
    , tdIssueSummary :: AC.IssueSummary
    , tdVotes        :: Maybe Integer
    , tdTeamMembers  :: [ETM.TeamMemberResponse]
    } deriving (Show, Generic)

instance ToJSON TeamDetails where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "td"
        }
