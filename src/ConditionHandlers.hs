{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module ConditionHandlers
   ( isHackathonCondition
   , handleTeamCondition
   ) where

import           Application
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Either
import           Data.Aeson
import qualified Data.ByteString.Char8      as BSC
import           Data.Maybe                 (isJust)
import           Data.MaybeUtil
import           GHC.Generics
import           HandlerHelpers
import qualified Persistence.Hackathon      as PH
import qualified Persistence.HackathonTeam  as HT
import qualified Snap.AtlassianConnect      as AC
import qualified Snap.Core                  as SC
import qualified Snap.Helpers               as SH
import qualified TenantJWT                  as TJ

isHackathonCondition :: AppHandler ()
isHackathonCondition = SH.handleMethods
    [ (SC.GET, TJ.withTenant isHackathonConditionHandleGet)
    ]

data ConditionResponse = CR Bool
    deriving (Show, Generic)

instance ToJSON ConditionResponse where
    toJSON (CR shouldDisplay) = object
        [ "shouldDisplay" .= shouldDisplay
        ]

isHackathonConditionHandleGet :: AC.TenantWithUser -> AppHandler ()
isHackathonConditionHandleGet (tenant, _) = writeError . runEitherT $ do
   projectId <- EitherT (m2e missingProjectId <$> SH.getIntegerQueryParam "projectId")
   potentialProject <- lift $ PH.getHackathonByProjectId tenant projectId
   lift . SH.writeJson . CR . isJust $ potentialProject
   where
      missingProjectId = (SH.badRequest, "You need to provide the project id to this condition")

handleTeamCondition :: AppHandler ()
handleTeamCondition = SH.handleMethods
   [ (SC.GET, TJ.withTenant isHackathonTeam)
   ]

isHackathonTeam :: AC.TenantWithUser -> AppHandler ()
isHackathonTeam (tenant, _) = writeError . runEitherT $ do
   issueId <- EitherT (m2e missingIssueId <$> SH.getIntegerQueryParam "issueId")
   potentialTeam <- lift $ HT.getHackathonTeamByIssueId tenant issueId
   lift . SH.writeJson . CR . isJust $ potentialTeam
   where
      missingIssueId = (SH.badRequest, "You need to provide the issue id to this condition.")
