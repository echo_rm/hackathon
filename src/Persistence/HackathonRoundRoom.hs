{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Persistence.HackathonRoundRoom
    ( HackathonRoundRoom(..)
    , RoundRoomStatus(..)
    , getOrCreateRoundRoom
    , getOrCreateCurrentRoundRoomForRoom
    , getRoundRoom
    , getCurrentRoundRoomForRoom
    , updateStatus
    , getRoundRoomsForRound
    , getRoomDetailsForRound
    , getUnallocatedRooms
    , updateMaximumTeams
    , updateWinners
    ) where

import           Application
import           Control.Monad                      (forM)
import           Data.HackathonConstants
import           Data.Int                           (Int64)
import           Data.Maybe                         (listToMaybe)
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.SqlQQ
import qualified Persistence.HackathonIds           as HI
import qualified Persistence.HackathonRoom          as HRM
import qualified Persistence.HackathonRound         as RND
import           Snap.Snaplet.PostgresqlSimple

data HackathonRoundRoom = HackathonRoundRoom
    { hrrRoundId      :: HI.HackathonRoundId
    , hrrRoomId       :: HI.HackathonRoomId
    , hrrStatus       :: RoundRoomStatus
    , hrrMaximumTeams :: Maybe Integer
    , hrrWinners      :: Integer
    } deriving (Show)

instance FromRow HackathonRoundRoom where
    fromRow = HackathonRoundRoom <$> field <*> field <*> field <*> field <*> field

defaultMaximumTeams :: Maybe Integer
defaultMaximumTeams = Nothing

defaultWinners :: Integer
defaultWinners = 3 -- By default 3 teams can win a round

getOrCreateCurrentRoundRoomForRoom :: HRM.HackathonRoom -> RoundRoomStatus -> AppHandler (Maybe HackathonRoundRoom)
getOrCreateCurrentRoundRoomForRoom hRoom defaultStatus = do
    potentialRR <- getCurrentRoundRoomForRoom hRoom
    case potentialRR of
        v@(Just _) -> return v
        Nothing -> createCurrentRoundRoomForRoom hRoom defaultStatus

getCurrentRoundRoomForRoom :: HRM.HackathonRoom -> AppHandler (Maybe HackathonRoundRoom)
getCurrentRoundRoomForRoom hRoom = listToMaybe <$> query
    [sql|
        SELECT rr.round_id, rr.room_id, rr.status, rr.maximum_teams, rr.winners
        FROM round_room rr, room r
        WHERE rr.round_id = r.current_round_id
        AND r.id = ?
        AND rr.room_id = ?
    |] (roomId, roomId)
    where
        roomId = HRM.hrId hRoom

createCurrentRoundRoomForRoom :: HRM.HackathonRoom -> RoundRoomStatus -> AppHandler (Maybe HackathonRoundRoom)
createCurrentRoundRoomForRoom hRoom status = listToMaybe <$> query
    [sql|
        INSERT INTO round_room (round_id, room_id, status, maximum_teams, winners, last_updated)
        VALUES ((SELECT current_round_id FROM room WHERE id = ?), ?, ?, ?, ?, now())
        RETURNING round_id, room_id, status, maximum_teams, winners
    |] (roomId, roomId, status, defaultMaximumTeams, defaultWinners)
    where
        roomId = HRM.hrId hRoom

getOrCreateRoundRoom :: RND.HackathonRound -> HRM.HackathonRoom -> RoundRoomStatus -> AppHandler (Maybe HackathonRoundRoom)
getOrCreateRoundRoom hRound hRoom defaultStatus = do
    potentialRoom <- getRoundRoom hRound hRoom
    case potentialRoom of
        v@(Just _) -> return v
        Nothing -> createRoundRoom hRound hRoom defaultStatus

getRoundRoom :: RND.HackathonRound -> HRM.HackathonRoom -> AppHandler (Maybe HackathonRoundRoom)
getRoundRoom hRound hRoom = listToMaybe <$> query
    [sql|
        SELECT round_id, room_id, status, maximum_teams, winners
        FROM round_room
        WHERE round_id = ?
        AND room_id = ?
    |] (RND.hroundId hRound, HRM.hrId hRoom)

createRoundRoom :: RND.HackathonRound -> HRM.HackathonRoom -> RoundRoomStatus -> AppHandler (Maybe HackathonRoundRoom)
createRoundRoom hRound hRoom status = listToMaybe <$> query
    [sql|
        INSERT INTO round_room (round_id, room_id, status, maximum_teams, winners, last_updated)
        VALUES (?, ?, ?, ?, ?, now())
        RETURNING round_id, room_id, status, maximum_teams, winners
    |] (RND.hroundId hRound, HRM.hrId hRoom, status, defaultMaximumTeams, defaultWinners)

updateStatus :: HackathonRoundRoom -> RoundRoomStatus -> AppHandler Int64
updateStatus roundRoom status = execute
    [sql|
        UPDATE round_room
        SET status = ?, last_updated = now()
        WHERE round_id = ?
        AND room_id = ?
    |] (status, hrrRoundId roundRoom, hrrRoomId roundRoom)

getRoomDetailsForRound :: RND.HackathonRound -> AppHandler [(HackathonRoundRoom, HRM.HackathonRoom)]
getRoomDetailsForRound hRound = do
    res <- query
        [sql|
            SELECT
              rr.round_id, rr.room_id, rr.status, rr.maximum_teams, rr.winners,
              r.id, r.hackathon_id, r.current_round_id, r.component_id, r.name, r.short_url
            FROM round_room rr, room r
            WHERE rr.room_id = r.id
            AND rr.round_id = ?
        |] (Only . RND.hroundId $ hRound)
    forM res $ \(hRoundRoom :. rnd) -> return (hRoundRoom, rnd)

getRoundRoomsForRound :: RND.HackathonRound -> AppHandler [HackathonRoundRoom]
getRoundRoomsForRound hRound = query
   [sql|
   SELECT rr.round_id, rr.room_id, rr.status, rr.maximum_teams, rr.winners
   FROM round_room rr
   WHERE rr.round_id = ?
   |] (Only . RND.hroundId $ hRound)

updateMaximumTeams :: HackathonRoundRoom -> Maybe Integer -> AppHandler Int64
updateMaximumTeams roundRoom maximumTeams = execute
   [sql|
   UPDATE round_room
   SET maximum_teams = ?
   WHERE round_id = ?
   AND room_id = ?
   |] (maximumTeams, hrrRoundId roundRoom, hrrRoomId roundRoom)

updateWinners :: HackathonRoundRoom -> Integer -> AppHandler Int64
updateWinners roundRoom winners = execute
   [sql|
   UPDATE round_room
   SET winners = ?
   WHERE round_id = ?
   AND room_id = ?
   |] (winners, hrrRoundId roundRoom, hrrRoomId roundRoom)

getUnallocatedRooms :: RND.HackathonRound -> AppHandler [HRM.HackathonRoom]
getUnallocatedRooms hRound = query
   [sql|
   SELECT * 
   FROM room 
   WHERE hackathon_id = ?
   AND id NOT IN (SELECT room_id FROM round_room WHERE round_id = ?)
   |] (RND.hroundHackathonId hRound, RND.hroundId hRound)
