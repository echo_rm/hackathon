{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module HostRequests.IssueUpdate
    ( updateIssueDetails
    , emptyIssueUpdate
    , IssueUpdateRequest(..)
    , IssueUpdateDetails(..)
    ) where

import           AesonHelpers
import           Application
import           Data.Aeson
import           Data.Aeson.Types                  (Pair, fieldLabelModifier)
import qualified Data.ByteString                   as BS
import qualified Data.ByteString.Char8             as BSC
import           Data.Maybe                        (catMaybes)
import qualified Data.Text                         as T
import qualified Data.Vector                       as V
import           GHC.Generics
import           Network.Helpers
import qualified Persistence.HackathonIds          as HI
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Snaplet                      as SS

updateIssueDetails :: AC.Tenant -> AC.IssueId -> IssueUpdateRequest -> AppHandler (Either AC.ProductErrorResponse ())
updateIssueDetails tenant issueId updateRequest = errorOnContent <$> (SS.with connect $ AC.hostPutRequestExtended tenant issueUpdateUrl [] (AC.setJson updateRequest))
 where
   issueUpdateUrl :: BS.ByteString
   issueUpdateUrl = "/rest/api/2/issue/" `BS.append` (BSC.pack . show $ issueId)

data IssueUpdateRequest = IssueUpdateRequest
    { iurUpdate :: IssueUpdateDetails
    -- The fields are not strictly required so I have not implemented them yet
    --, iurFields :: M.Map T.Text T.Text
    -- The issue history metadata is not required strictly speaking, but it might be nice to have later
    --, iurHistoryMetadata :: IssueHistoryMetadata
    } deriving (Show, Generic)

instance ToJSON IssueUpdateRequest where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "iur"
        }

emptyIssueUpdate :: IssueUpdateDetails
emptyIssueUpdate = IssueUpdateDetails Nothing Nothing

data IssueUpdateDetails = IssueUpdateDetails
    { iudComponents :: Maybe [HI.ComponentId]
    , iudLabels     :: Maybe [T.Text]
    } deriving (Show)

instance ToJSON IssueUpdateDetails where
    toJSON iud = object . catMaybes $
        [ toComponentOperations <$> iudComponents iud
        , toLabelOperations <$> iudLabels iud
        ]

toComponentOperations :: [HI.ComponentId] -> Pair
toComponentOperations componentIds = "components" .= wrapInArray componentSet
    where
       componentSet = object [ "set" .= fmap toId componentIds]

       toId :: HI.ComponentId -> Value
       toId componentId = object [ "id" .= show componentId ]

toLabelOperations :: [T.Text] -> Pair
toLabelOperations labels = "labels" .= wrapInArray labelSet
   where
      labelSet = object [ "set" .= labels ]

wrapInArray :: Value -> Value
wrapInArray x = Array . V.fromList $ [ x ]
