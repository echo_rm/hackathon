import * as React from 'react';
import { HackathonDetails } from './hackathon-client';
import DynamicTable, { DynamicTableRow, DynamicTableHead } from '@atlaskit/dynamic-table';
import * as moment from 'moment';
import { linkToSummary, linkToDashboard, HackathonContext } from './Links';
import { PageContext } from './page-context';
import DropdownMenu, { DropdownItem } from '@atlaskit/dropdown-menu';
import Tooltip from '@atlaskit/tooltip';
import Lozenge from '@atlaskit/lozenge';
import EmptyState from '@atlaskit/empty-state';
import FieldText from '@atlaskit/field-text';

export type HackathonState = Array<HackathonDetails> | 'loading' | 'loading-failed';

export type HackathonOverviewProps = {
   pc: PageContext;
   hackathons: HackathonState;
};

export type HackathonOverviewState = {
   hackathons: HackathonState;
   filter?: string;
};

function formatDate(date: moment.Moment) {
   var prettyDate = 'DD MMM YYYY';
   return date.format(prettyDate);
}

export class HackathonsOverview extends React.PureComponent<HackathonOverviewProps, HackathonOverviewState> {
   private static getStatus(start: moment.Moment, end: moment.Moment): JSX.Element {
      const now = moment();
      if (now.isBefore(start)) {
         return <Lozenge appearance="inprogress">Coming soon</Lozenge>;
      }
      if (now.isAfter(end)) {
         return <Lozenge>Finished</Lozenge>;
      }
      return <Lozenge appearance="success">On now!</Lozenge>;
   }

   componentWillMount() {
      this.setState({
         hackathons: this.props.hackathons
      });
   }

   componentDidUpdate() {
      if (this.state.hackathons !== this.props.hackathons) {
         this.setState({
            hackathons: this.props.hackathons
         });
      }
   }

   render() {
      const { pc } = this.props;
      const { hackathons, filter } = this.state;

      if (hackathons === 'loading-failed') {
         return (
            <EmptyState
               header="We could not find the Hackathons you are looking for"
               description="This may be because you are not logged in. Please make sure you are logged in first
               and then contact your administrator if this message persists."
            />
         );
      }

      if (hackathons === 'loading') {
         return (
            <EmptyState
               header="Loading your hackathons..."
               description="We are attempting to load your hackathons. Please make sure that you are logged in."
            />
         );
      }

      const hs = hackathons.filter(h => filter === undefined || h.projectName.toLocaleLowerCase().indexOf(filter) >= 0);

      // Sort the hackathons by the one starting soonest.
      hs.sort((a, b) => {
         return b.startDate.getTime() - a.startDate.getTime();
      });

      const header: DynamicTableHead = {
         cells: [
            {
               key: 'name',
               content: <span>Hackathon</span>,
               isSortable: true,
               shouldTruncate: false
            },
            {
               key: 'state',
               content: <span>State</span>,
               isSortable: false,
               shouldTruncate: false
            },
            {
               key: 'startDate',
               content: <span>Start date</span>,
               isSortable: true,
               shouldTruncate: false
            },
            {
               key: 'endDate',
               content: <span>End date</span>,
               isSortable: true,
               shouldTruncate: false
            },
            {
               key: 'actions',
               content: <span>Actions</span>,
               isSortable: false,
               shouldTruncate: false
            }
         ]
      };

      const rows: DynamicTableRow[] = hs.map(h => {
         const hc: HackathonContext = {
            projectId: h.projectId,
            projectKey: h.projectKey
         };

         const summaryLink = linkToSummary(pc, hc);
         const dashboardLink = linkToDashboard(pc, hc);

         const menuItems: JSX.Element[] = new Array<JSX.Element>();
         if (h.shortUrl !== undefined) {
            menuItems.push(<DropdownItem href={h.shortUrl}>Vote</DropdownItem>);
         }
         menuItems.push(<DropdownItem href={dashboardLink}>Manage</DropdownItem>);

         const startDate = moment(h.startDate);
         const endDate = moment(h.endDate);

         return {
            key: h.projectKey,
            cells: [
               { content: <a href={summaryLink}>{h.projectName}</a>, key: h.projectName },
               { content: HackathonsOverview.getStatus(startDate, endDate), key: 'state'},
               {
                  content: (
                     <Tooltip content={startDate.fromNow()}>
                        <span>{formatDate(startDate)}</span>
                     </Tooltip>
                  ),
                  key: startDate.toDate().getTime()
               },
               {
                  content: (
                     <Tooltip content={endDate.fromNow()}>
                        <span>{formatDate(endDate)}</span>
                     </Tooltip>
                  ),
                  key: endDate.toDate().getTime()
               },
               {
                  content: (
                     <DropdownMenu
                        trigger="Actions"
                        triggerType="button"
                        position="right top"
                     >
                        {menuItems}
                     </DropdownMenu>
                  ),
                  key: 'actions'
               }
            ]
         };
      });

      return (
         <div>
            <h1>Hackathons</h1>
            <p>To create a new Hackathon you must first create a new Jira project
               for your Hackathon and then Activate it. <a target="_blank" href="/docs/setup">Learn
               more about creating Hackathon projects!</a>
            </p>
            <FieldText
               placeholder="Search for a hackathon..."
               shouldFitContainer={true}
               onChange={e => this.setState({ filter: e.currentTarget.value })}
            />
            <DynamicTable
               head={header}
               rows={rows}
               rowsPerPage={20}
               emptyView={(
                  <EmptyState
                     header="No hackathons have been found"
                     description="To create a new Hackathon, please create a Jira project and Activate it
                      as a Hackathon."
                  />
               )}
               defaultSortKey="endDate"
               defaultSortOrder="DESC"
            />
         </div>
      );
   }
}