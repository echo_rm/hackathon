{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module HostRequests.UserDetails
  ( getUserDetails
  , UserWithDetails(..)
  , usersWithPermissions
  , ProjectOrIssueKey(..)
  , JIRAPermission(..)
  ) where

import           Application
import           Data.Aeson
import qualified Data.ByteString                   as B
import qualified Data.ByteString.Char8             as BSC
import qualified Data.Map                          as M
import           Data.Monoid                       (mempty)
import qualified Data.Text                         as T
import           Data.Text.Encoding                (encodeUtf8)
import           GHC.Generics
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Snaplet                      as SS

data UserWithDetails = UserWithDetails
  { key          :: AC.UserKey
  , emailAddress :: T.Text
  , avatarUrls   :: M.Map String String
  , displayName  :: T.Text
  , active       :: Bool
  , timeZone     :: T.Text
  } deriving (Show, Generic)

instance FromJSON UserWithDetails
instance ToJSON UserWithDetails

getUserDetails :: AC.Tenant -> AC.UserKey -> AppHandler (Either AC.ProductErrorResponse UserWithDetails)
getUserDetails tenant userKey = SS.with connect $ AC.hostGetRequest tenant usernameUrl queryParams mempty
  where
    usernameUrl :: B.ByteString
    usernameUrl = "/rest/api/2/user"

    queryParams :: [(B.ByteString, Maybe B.ByteString)]
    queryParams = [("key", Just . encodeUtf8 $ userKey)]

usersWithPermissions
  :: AC.Tenant
  -> Maybe AC.UserKey
  -> [JIRAPermission]
  -> ProjectOrIssueKey
  -> AppHandler (Either AC.ProductErrorResponse [UserWithDetails])
usersWithPermissions tenant potentialUserKey permissions projectOrIssueKey = SS.with connect $ AC.hostGetRequest tenant userPermissionUrl queryParams mempty
  where
    userPermissionUrl :: B.ByteString
    userPermissionUrl = "/rest/api/2/user/permission/search"

    queryParams :: [(B.ByteString, Maybe B.ByteString)]
    queryParams =
      [ toQueryParam projectOrIssueKey
      , ("permissions", Just . BSC.intercalate "," . fmap permissionToString $ permissions)
      ] ++ userKeyParams potentialUserKey

    userKeyParams :: Maybe AC.UserKey -> [(B.ByteString, Maybe B.ByteString)]
    userKeyParams Nothing = []
    userKeyParams (Just userKey) = [ ("username", Just . encodeUtf8 $ userKey) ]

    toQueryParam :: ProjectOrIssueKey -> (B.ByteString, Maybe B.ByteString)
    toQueryParam (PK projectKey) = ("projectKey", Just . encodeUtf8 $ projectKey)
    toQueryParam (IK issueKey) = ("issueKey", Just . encodeUtf8 $ issueKey)

data ProjectOrIssueKey
  = PK AC.ProjectKey
  | IK AC.IssueKey

data JIRAPermission
  = ProjectAdmin

permissionToString :: JIRAPermission -> B.ByteString
permissionToString ProjectAdmin = "PROJECT_ADMIN"
