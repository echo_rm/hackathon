module Data.TextUtil (ellipsisTruncate) where

import qualified Data.Text as T

ellipsisTruncate :: Int -> T.Text -> T.Text
ellipsisTruncate len input
    | T.length input <= len = input
    | otherwise = T.take (len - T.length ellipsis) input `T.append` ellipsis
    where
        ellipsis = T.pack "..."

