#!/bin/bash -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_DIR="$DIR/.."
SCRIPTS_DIR="${BASE_DIR}/scripts"

set -e

# Usage: build-production
IMAGE_TAG=`git describe`
IMAGE_BUILD_TAG="${IMAGE_TAG}-build"
DOCKER_CMD=${DOCKER_CMD:-docker}

echo "## Building code in image: $IMAGE_BUILD_TAG"
cd "$BASE_DIR"
${DOCKER_CMD} build --rm=true --tag="$IMAGE_BUILD_TAG" "${BASE_DIR}"

echo "## Building production image: $IMAGE_TAG"
bash $SCRIPTS_DIR/production-from-image.bash "$IMAGE_BUILD_TAG" "$IMAGE_TAG"
echo "## Built production image: $IMAGE_TAG"

exit 0
