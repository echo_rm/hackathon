{-# LANGUAGE OverloadedStrings #-}
module AdminHandlers
   ( handleAdminHackathon
   ) where

import qualified AppConfig                     as CONF
import qualified AppHelpers                    as AH
import           Application
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Either
import           Data.MaybeUtil
import qualified Data.Text.Encoding            as T
import           HandlerHelpers
import qualified Persistence.Hackathon         as PH
import qualified Persistence.Tenant            as PT
import qualified Snap.Core                     as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple

handleAdminHackathon :: AppHandler ()
handleAdminHackathon = handleMethods
   [ (SC.DELETE, handleDeleteHackathon)
   ]

-- Accept two params, the instance name and the project id
handleDeleteHackathon :: AppHandler ()
handleDeleteHackathon = AH.getKeyAndConfirm CONF.acAdminKey $ do
   writeError . withPG . runEitherT $ do
      clientKey <- EitherT (m2e missingClientKey <$> SC.getQueryParam "clientKey")
      projectId <- EitherT (m2e missingProjectId <$> getIntegerQueryParam "projectId")
      tenant <- EitherT (m2e tenantNotFound <$> PT.lookupTenant (T.decodeUtf8 clientKey))
      lift $ PH.purgeHackathonById tenant projectId
   where
      missingClientKey = (badRequest, "You must provide a tenant client key with this request.")
      missingProjectId = (badRequest, "You must provide a projectId with this request.")
      tenantNotFound = (notFound, "Could not find a tenant with the provided clientKey.")
