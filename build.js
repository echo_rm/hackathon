({
   'appDir': 'static/js',
   'baseUrl': '.',
   'dir': 'static-js',
   'paths': {
      'moment': 'lib/moment',
      'mustache': 'lib/mustache',
      'marked': 'lib/marked',
      'underscore': 'lib/underscore',
      'jquery': '//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js',
      'jquery-ui': 'lib/jquery-ui',
      'd3': 'lib/d3'
   },
   'shim': {
      'jquery': {
         deps: [],
         exports: '$'
      },
      'aui': {
         'deps': ['jquery'],
         'exports': 'AJS'
      }
   },
   'wrapShim': true,
   'modules': [
      {
         'name': 'app/activate-hackathon-project'
      },
      {
         'name': 'app/hackathon-dashboard'
      },
      {
         'name': 'app/hackathon-summary'
      },
      {
         'name': 'app/hackathon-team-panel'
      },
      {
          'name': 'app/hackathons'
      },
      {
          'name': 'app/hackathon-vote-redirect'
      },
      {
         'name': 'app/hackathon-vote'
      },
      {
         'name': 'app/hackathon-leaderboard'
      },
      {
         'name': 'app/hackathon-round-transition'
      },
      {
         'name': 'app/hackathon-statistics'
      },
      {
         'name': 'app/doc-page'
      }
   ]
})
