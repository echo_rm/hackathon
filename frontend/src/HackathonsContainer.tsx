import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { PageContext } from './page-context';

export type HackathonsContainerProps = {
   pageContext: PageContext;
};

export type HackathonsContainerState = {

};

export class HackathonsContainer
    extends React.PureComponent<RouteComponentProps<void> & HackathonsContainerProps, HackathonsContainerState> {
}