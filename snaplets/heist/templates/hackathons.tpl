<apply template="connect-panel">
    <bind tag="body-class">hackathons-list</bind>
    <bind tag="header-extra">
        <js src="/static/${resourcesVersion}/js/app/hackathons.js" />
        <script>require(['app/hackathons']);</script>

        <script id="create-hackathon-help-text" type="x-tmpl-mustache">
            {{#isEmpty}}
            <p>You don't have any Hackathons at the moment.</p>
            {{/isEmpty}}
            {{^isEmpty}}
            <h3>Create a new hackathon</h3>
            {{/isEmpty}}
            <div class="aui-help aui-help-text">
                <div class="aui-help-content">
                    <p>To create a Hackathon you must first create a new project for your Hackathon.</p>
                    <ul class="aui-nav-actions-list">
                        <li><a href="{{baseUrl}}/secure/BrowseProjects.jspa?selectedCategory=all" class="aui-button" target="_top">Create project</a></li>
                        <li><a target="_blank" href="/docs/setup">Learn more about creating Hackathon projects</a></li> <!-- Link to video? -->
                    </ul>
                </div>
            </div>
        </script>

        <script id="hackathon-row" type="x-tmpl-mustache">
            <tr>
                <td headers="name"><a href="{{{summaryLink}}}" target="_top">{{name}}</a></td>
                <td headers="start">{{startDate}}</td>
                <td headers="end">{{endDate}}</td>
                <td class="action">
                    <ul class="menu">
                        <li><a href="{{{voteLink}}}" target="_top">Vote</a></li>
                        <li><a href="{{{dashboardLink}}}" target="_top">Manage</a></li>
                    </ul>
                </td>
            </tr>
        </script>
    </bind>

    <apply template="aui-page">
        <h2>Hackathons</h2>

        <div id="no-content-message" class="aui-message aui-message-warning">
           <p class="title">
              <strong>We could not find Hackathon information</strong>
           </p>
           <p>This may be because you are not logged in. Please make sure you are logged in first and then contact your administrator if this message persists.</p>
        </div>

        <div id="current-hackathons" class="hidden">
            <h3>Current and upcoming hackathons</h3>
            <table class="hackathon-table aui">
                <thead>
                <tr>
                    <th class="name">Hackathon</th>
                    <th class="start">Start date</th>
                    <th class="end">End date</th>
                    <th class="action">Action</th>
                </tr>
                </thead>
                <tbody class="hackathon-rows-container"></tbody>
            </table>
            <p></p>
        </div>

        <div id="past-hackathons" class="hidden">
            <h3>Previous hackathons</h3>
            <table class="hackathon-table aui">
                <thead>
                <tr>
                    <th class="name">Hackathon</th>
                    <th class="start">Start date</th>
                    <th class="end">End date</th>
                    <th class="action">Action</th>
                </tr>
                </thead>
                <tbody class="hackathon-rows-container"></tbody>
            </table>
            <p></p>
        </div>

        <div id="new-hackathons"></div>
    </apply>
</apply>
