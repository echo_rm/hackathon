define(function() {
    "use strict";

    /*
    The options object can contain an:
    emptyMessageLoader :: function() - The purpose of this method is to add a permanent message that is never hidden.
     */
    return function(footerContainer, userOptions) {
        var running = false;
        var messageQueue = [];
        var originalClasses = footerContainer.attr('class');

        var defaultOptions = {};
        var options = AJS.$.extend({}, defaultOptions, userOptions);

        var showFooterMessage = function(message, userOptions) {
            var defaultOptions = {
                permanent: false,
                inDuration: 200,
                outDuration: 200,
                duration: 2000,
                extraClasses: ""
            };

            var options = AJS.$.extend({}, defaultOptions, userOptions);

            if(!options.permanent) {
                running = true;
            }

            // Put the message in the container
            footerContainer.hide();
            footerContainer.html(message);
            footerContainer.attr('class', originalClasses).addClass(options.extraClasses);

            // Setup for removing the container
            var goOut = function() {
                running = false;
                if(options.complete) {
                    options.complete();
                }
                showNextMessage(message);
            };

            var goIn = function() {
                if(!options.permanent) {
                    setTimeout(function() {
                        if(options.outDuration > 0) {
                            footerContainer.fadeOut(options.outDuration, goOut);
                        } else {
                            footerContainer.hide();
                            goOut();
                        }
                    }, options.duration);
                }
            };

            if(options.inDuration > 0) {
                footerContainer.fadeIn(options.inDuration, goIn);
            } else {
                footerContainer.show();
                goIn();
            }
        };

        var showMessage = function (message, options) {
            messageQueue.push({
                message: message,
                options: options
            });

            showNextMessage();
        };

        var showNextMessage = function(lastMessage) {
            if(!running) {
                var next;
                do {
                    next = messageQueue.shift();
                } while(messageQueue.length > 0 && next.message == lastMessage);

                if(next && next.message != lastMessage) {
                    showFooterMessage(next.message, next.options);
                } else if(options.emptyMessageLoader) {
                    next = options.emptyMessageLoader();
                    next.options = AJS.$.extend({}, next.options, {
                        permanent: true,
                        inDuration: 0,
                        outDuration: 0
                    });
                    showFooterMessage(next.message, next.options);
                } else {
                    // There is nothing to show
                    footerContainer.hide();
                }
            }
        };

        showNextMessage();
        return {
            showMessage: showMessage,
            hide: function() { footerContainer.hide(); },
            show: function() { 
               footerContainer.show(); 
               showNextMessage();
            }
        };
    };
});
