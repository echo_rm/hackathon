import { PageContext } from '../page-context';

export const PageContextExample: PageContext = {
  acpt: '12345',
  pluginKey: 'test.plugin.key',
  productBaseUrl: 'https://your-domain.atlassian.net',
  project: {
    id: 41337,
    key: 'DEMO'
  }
};