define([
   'underscore', 
   'helpers/PageContext', 
   '../helpers/MustacheLoader', 
   '../helpers/Meta', 
   '../helpers/Handler',
   '../helpers/transition',
   'connect/pagetoken', 
   'jquery-ui'
], 
function(_, PC, MustacheLoader, Meta, H, Transition) {
   "use strict";

   var baseSearchPath = "/issues/?jql=";

   (function($) {
      $.fn.toggleCheckbox = function(checked) {
         $(this).toggleAttr("checked", "checked", checked);
      };

      $.fn.isChecked = function() {
         return !!$(this).attr("checked");
      };

      $.fn.toggleAttr = function(name, value, show) {
         var s = $(this);
         if(!show) {
            s.removeAttr(name);
         } else {
            s.attr(name, value);
         }
      };

      $.fn.timeout = function(time, callback) {
         var s = $(this);

         var currentTimeout = s.data("timeout-id");

         if(currentTimeout) {
            clearTimeout(currentTimeout);
         }

         currentTimeout = setTimeout(function() {
            s.data("timeout-id", null);
            callback && callback();
         }, time);

         s.data("timeout-id", currentTimeout);
      };

      $.fn.auiIconStatus = function(status) {
         var s = $(this);

         var isSuccess = status === "success";
         var isWarning = status === "warning";
         var isError   = status === "error";
         var shouldHide = !(isSuccess || isWarning || isError);
         s.toggleClass('aui-iconfont-success', isSuccess);
         s.toggleClass('aui-iconfont-warning', isWarning);
         s.toggleClass('aui-iconfont-error'  , isError);
         s.toggleClass('hidden'              , shouldHide);
      };
   }(AJS.$));

   var getRoundsForHackathon = function(projectId) {
      return AJS.$.ajax({
         url: "/rest/hackathon/rounds",
         type: "GET",
         cache: false,
         data: {
            projectId: projectId
         }
      });
   };

   var getRoomsForRound = function(projectId, hackathonRoundId) {
      return AJS.$.ajax({
         url: "/rest/round-rooms",
         type: "POST",
         cache: false,
         data: JSON.stringify({
            hackathon: {
               token: {
                   acptToken: Meta.get("acpt")
               },
               projectId: projectId
            },
            roundId: hackathonRoundId
         })
      });
   };

   var getUnallocatedRoomsForRound = function(projectId, hackathonRoundId) {
      return AJS.$.ajax({
         url: "/rest/round-room/unallocated",
         type: "GET",
         cache: false,
         data: {
            projectId: projectId,
            roundId: hackathonRoundId
         }
      });
   };

   var getRoundTransitions = function(projectId, roundId) {
      return AJS.$.ajax({
         url: "/rest/hackathon/round/transitions",
         type: "GET",
         cache: false,
         data: {
            projectId: projectId,
            roundId: roundId
         }
      });
   };

   var saveCurrentTransitions = function(projectId, roundId, newTransitions) {
      return AJS.$.ajax({
         url: "/rest/hackathon/round/transitions",
         type: "POST",
         cache: false,
         data: JSON.stringify({
            projectId: projectId,
            roundId: roundId,
            transitions: newTransitions
         })
      });
   };

   var testJql = function(projectId, rawJql) {
      return AJS.$.ajax({
         url: "/rest/hackathon/jql/test",
         type: "GET",
         cache: false,
         data: {
            projectId: projectId,
            jql: rawJql
         }
      });
   };

   var updateTransitionOptions = function(projectId, roundId, transitionTied, matchUnallocated) {
      return AJS.$.ajax({
         url: "/rest/hackathon/round/transition/options",
         type: "PUT",
         cache: false,
         data: JSON.stringify({
            projectId: projectId,
            roundId: roundId,
            transitionTied: transitionTied,
            matchUnallocated: matchUnallocated
         })
      });
   };

   var updateRoundRoomWinners = function(projectId, roundId, roomId, winners) {
      return AJS.$.ajax({
         url: "/rest/hackathon/round-room/update-winners",
         type: "PUT",
         cache: false,
         data: JSON.stringify({
            projectId: projectId,
            roundId: roundId,
            roomId: roomId,
            winners: winners
         })
      });
   };

   var updateRoundRoomMaxTeams = function(projectId, roundId, roomId, maxTeams) {
      return AJS.$.ajax({
         url: "/rest/hackathon/round-room/update-maximum-teams",
         type: "PUT",
         cache: false,
         data: JSON.stringify({
            projectId: projectId,
            roundId: roundId,
            roomId: roomId,
            maxTeams: maxTeams
         })
      });
   };

   var calculateTransition = function(projectId, roundId) {
      return AJS.$.ajax({
         url: "/rest/hackathon/round/transition/calculate",
         type: "GET",
         cache: false,
         data: {
            projectId: projectId,
            roundId: roundId
         }
      });
   };

   // Get the rounds first
   // Then:
   //  - get the rooms for the current round
   //  - get the rooms for the next round
   //  - get the transitions for the current round
   var loadTransitionableRounds = function(pageContext) {
      return AJS.$.Deferred(function(defer) {
         var request = getRoundsForHackathon(pageContext.project.id);

         request.then(function(data) {
            // Filter out transitionable rooms
            var orderableSequenceNumber = function(sn) { return sn || -1; };
            var operableRounds = _.filter(data.rounds, function(room) {
               return room.type == "Round" || room.type == "TeamFormation";
            });
            operableRounds = operableRounds.sort(function(left, right) {
               return orderableSequenceNumber(left.sequenceNumber) - orderableSequenceNumber(right.sequenceNumber);
            });
            var transitionableRounds = [];
            for(var i = 0; i < operableRounds.length - 1; ++i) {
               transitionableRounds.push({
                  current: operableRounds[i],
                  next: operableRounds[i + 1]
               });
            }
            defer.resolve(transitionableRounds);
         });

         request.fail(defer.reject);
      });
   };

   var AssignableRooms = (function () {
      var rooms = [];

      return {
         get: function() { return rooms; },
         set: function(newRooms) { rooms = newRooms; }
      };
   } ());

   AJS.$(function() {
      var pageContext = PC.load();
      var templates = MustacheLoader.load();
      var selectedRound;

      var trRequest;
      var reloadTransitionableRounds = function() {
         trRequest = loadTransitionableRounds(pageContext);
      };
      reloadTransitionableRounds();

      trRequest.done(function(trs) {
         renderRoundOptions(trs);

         loadTransitionableRound(trs[0]);
      });

      var selectRoundFrom = AJS.$("#select-round-from");

      var renderRoundOptions = function(trs) {
         selectRoundFrom.empty();

         _.each(trs, function(round) {
            selectRoundFrom.append(templates.render("transition-option", round.current));
         });
      };

      selectRoundFrom.change(function() {
         clearRoomsPreview();

         var selectedRoundId = selectRoundFrom.val();
         trRequest.done(function(trs) {
            var selectedRound = _.find(trs, function(round) { return round.current.id == selectedRoundId; });
            if(selectedRound) {
               loadTransitionableRound(selectedRound);
            } else {
               AP.require('messages', function(message) {
                  message.error("Could not find selected round");
               });
            }
         });
      });

      var updateToRound = function(toRound) {
         AJS.$(".transition-round-selector .round-to .round-name").text(toRound.name);
         AJS.$(".transition-round-selector .round-to").data("next-round-id", toRound.id);
      };

      var getNextRoundId = function() {
         return parseInt(AJS.$(".transition-round-selector .round-to").data("next-round-id"));
      };

      var loadTransitionableRound = function(round) {
         selectedRound = round;
         updateToRound(round.next);

         // If this is the Team formation round then hide current rooms and transition options
         var isTeamFormation = round.current.type === "TeamFormation";
         AJS.$(".current-rooms").toggleClass('hidden', isTeamFormation);
         AJS.$(".round-options").toggleClass('hidden', isTeamFormation);

         // Setup the options for this round
         AJS.$("#include-tied").toggleCheckbox(round.current.transitionTied);
         AJS.$("#unallocated-teams").toggleCheckbox(round.current.matchUnallocated);

         // Load the round rooms
         var currentRooms = getRoomsForRound(pageContext.project.id, round.current.id);

         currentRooms.done(function(details) {
            var container = AJS.$(".current-rooms tbody");
            container.find(".current-room").remove();
            var emptyContainer = AJS.$("#current-rooms-empty-container");

            if(details.roundRooms.length > 0) {
               var notInactiveRoundRooms = _.filter(details.roundRooms, function(roundRoom) { return roundRoom.status !== "Inactive"; });
               _.each(notInactiveRoundRooms, function(roundRoom) {
                  var room = {
                     id: roundRoom.room.id,
                     name: roundRoom.room.name,
                     isLocked: roundRoom.status == 'Locked',
                     winners: roundRoom.winners
                  };
                  container.append(templates.render('current-room-row', room));
               });

               emptyContainer.empty();
            } else {
               var data = {
                  unallocatedTeams: !!AJS.$("#unallocated-teams").attr("checkbox")
               };
               emptyContainer.append(templates.render('current-rooms-empty-message', data));
            }
         });

         loadAssignableRooms(round);

         // Load the transitions
         var transitions = getRoundTransitions(pageContext.project.id, round.current.id);

         transitions.done(function(data) {
            // We want to load the transition data into the boxes
            var transitionsContainer = AJS.$("#transitions tbody");
            transitionsContainer.find(".transition-row").remove();
            if(data.transitions.length > 0) {
               var nextRoundRooms = _.pluck(AssignableRooms.get(), 'room');
               var updatedTransitions = _.map(data.transitions, function(transition) {
                  transition.avaliableRooms = nextRoundRooms;
                  return transition;
               });

               var getRoomId = function(room) { return room.id; };
               var otherwiseTransition = AJS.$("#transitions .otherwise-transition");
               _.each(_.initial(updatedTransitions), function(updatedTransition) {
                  var newCondition = AJS.$(templates.render('round-transition-row', updatedTransition));
                  newCondition.insertBefore(otherwiseTransition);
                  var newRooms = newCondition.find("select.rooms");
                  newRooms.val(_.map(updatedTransition.rooms, getRoomId));
                  newRooms.auiSelect2();

                  // Update jql row
                  var jqlRow = newCondition.find(".jql-query");
                  var testRequest = testJql(pageContext.project.id, jqlRow.val());
                  testRequest.done(function(data) {
                     handleTestResult(jqlRow, data);
                  });
                  updateJQLLink(jqlRow);
               });

               var lastRooms = _.last(updatedTransitions).rooms;
               var otherwiseRooms = otherwiseTransition.find("select.rooms");
               otherwiseRooms.val(_.map(lastRooms, getRoomId));
               otherwiseRooms.auiSelect2();
            } else {
               // Clear the data back to an initial state, maybe we should even do this before this
               // block
            }
            recomputeTransitionOrder();
         });
         // We need to be able to get all of the transitions so that we can populate them
      };

      var addConditionRow = function() {
         clearRoomsPreview();
         var otherwiseTransition = AJS.$("#transitions .otherwise-transition");
         
         var data = {};
         data.orderNumber = AJS.$("#transitions .transition-row").length + 1;
         data.avaliableRooms = _.pluck(AssignableRooms.get(), 'room');
         var newCondition = AJS.$(templates.render('round-transition-row', data));
         newCondition.insertBefore(otherwiseTransition);
         newCondition.find(".rooms").auiSelect2();

         // You have added in a new row, now show the error messages
         recomputeTransitionOrder();
         updateRoundTransitions();
      };

      var refreshRoomsForConditionSelect = function(selectElements) {
         // What we need to do is add in elements that don't exist already, and leave elements that
         // already exist...but maybe update their names.
         selectElements.each(function(i, se) {
            var selectElement = AJS.$(se);
            var rooms = _.pluck(AssignableRooms.get(), 'room');
            var roomIds = _.pluck(rooms, 'id');
            var getValue = function(i, opt) { return AJS.$(opt).attr('value'); };
            var options = selectElement.find('option');
            var currentIds = options.map(getValue).toArray();

            // Get all of the options to delete and delete them
            options.filter(function(i, option) {
               return roomIds.indexOf(getValue(i, option)) < 0;
            }).remove();
            // Get all of the options to add and add them
            var missingRooms = _.filter(rooms, function(room) {
               return currentIds.indexOf(room.id) < 0;
            });
            _.each(missingRooms, function(missingRoom) {
               selectElement.append(templates.render('select-option', missingRoom));
            });
            // Reload select2 on the element
            selectElement.auiSelect2();
         });
      };

      AJS.$("#add-transition-condition").click(H.h(addConditionRow));

      var buildTransitionsFromPage = function() {
         var transitionRows = AJS.$("#transitions .transition-row");

         var roundTransitions = transitionRows.map(function(i, row) {
            var transitionRow = AJS.$(row);
            var ret = {};
            ret.jql = transitionRow.find(".jql-query").val();
            ret.rooms = transitionRow.find("select.rooms").val();
            ret.row = transitionRow;
            return ret;
         });
         
         var otRow = AJS.$("#transitions .otherwise-transition");
         var otherwiseTransition = {
            rooms: otRow.find("select.rooms").val(),
            row: otRow
         };

         roundTransitions.push(otherwiseTransition);

         return roundTransitions;
      };

      var validateConditions = function(conditions) {
         var valid = true;
         conditions.each(function(i, condition) {
            // Make sure that the jql query is not empty
            var isLastCondition = i + 1 === conditions.length;
            var hasJqlError = !isLastCondition && !condition.jql;
            valid &= !hasJqlError;
            var jqlQuery = condition.row.find(".jql-query");
            jqlQuery.toggleClass("error", hasJqlError);

            // Make sure that the query is valid
            valid &= isLastCondition || jqlQuery.hasClass("jql-valid");

            // Make sure that atleast one room has been requested
            var missingRooms = !condition.rooms || condition.rooms.length === 0;
            valid &= !missingRooms;
            condition.row.find(".rooms").toggleClass("error", missingRooms);
         });
         return valid;
      };

      var updateRoundTransitions = function() {
         // Create a JSON entry that matches the current state
         var transitions = buildTransitionsFromPage();
         // Validate that this JSON entry is correct
         var isValid = validateConditions(transitions);
         if(isValid) {
            // Prepare the data that we want to send back to the server
            var pi = function(x) { return parseInt(x); };
            
            var validTransitions = _.map(transitions, function(rawTransition) {
               return {
                  jql: rawTransition.jql,
                  rooms: _.map(rawTransition.rooms, pi)
               };
            });

            // Send the json entry back to the host
            var roundId = parseInt(selectRoundFrom.val());
            saveCurrentTransitions(pageContext.project.id, roundId, validTransitions);
         } else {
            AJS.log("Transition rules are invalid.");
            AJS.log(transitions);
         }
      };

      var transitions = AJS.$("#transitions");
      
      transitions.on('change', '.rooms', function() { 
         clearRoomsPreview();
         updateRoundTransitions(); 
      });
      
      var handleTestResult = function(self, data) {
         var rsp = {};
         rsp.isValid = data.result === "Valid";
         rsp.isInvalid = data.result === "Invalid";
         rsp.isFailed = data.result === "Failed";

         self.toggleClass("error", rsp.isFailed);
         self.toggleClass("jql-failed", rsp.isFailed);
         self.toggleClass("warning", rsp.isInvalid);
         self.toggleClass("jql-invalid", rsp.isInvalid);
         self.toggleClass("success", rsp.isValid);
         self.toggleClass("jql-valid", rsp.isValid);

         return rsp;
      };

      transitions.on('keyup', '.jql-query', function() { 
         clearRoomsPreview();

         var self = AJS.$(this);
         self.timeout(500, function() {
            var testRequest = testJql(pageContext.project.id, self.val());

            testRequest.done(function(data) {
               var rsp = handleTestResult(self, data);

               if(rsp.isValid) {
                  updateRoundTransitions(); 
               }
            });
         });

         updateJQLLink(self);

         return true;
      });

      var updateJQLLink = function(self) {
         var row = self.closest('.transition-row');
         var link = row.find('.jql-editor-link');
         var jqlQuery = self.val();
         var restrictedJqlQuery = 
         link.attr("href", pageContext.productBaseUrl + baseSearchPath + encodeURIComponent(jqlQuery));
      };

      transitions.on('click', '.transition-remove', function() {
         clearRoomsPreview();

         var self = AJS.$(this);
         self.closest('.transition-row').remove();
         updateRoundTransitions();
         recomputeTransitionOrder();
      });

      var loadAssignableRooms = function(round) {
         var nextRooms = getRoomsForRound(pageContext.project.id, round.next.id);
         var unallocatedRooms = getUnallocatedRoomsForRound(pageContext.project.id, round.next.id);

         nextRooms.done(function(details) {
            var container = AJS.$(".assignable-rooms table tbody");
            container.find(".assignable-room").remove();
            var afterAdd = container.find(".room-adder-row");

            AssignableRooms.set(details.roundRooms);

            var hasRooms = details.roundRooms.length > 0;
            AJS.$(".assignable-rooms thead").toggleClass("hidden", !hasRooms);

            if(hasRooms) {
               _.each(details.roundRooms, function(roundRoom) {
                  var room = {
                     id: roundRoom.room.id,
                     name: roundRoom.room.name,
                     unlimited: !roundRoom.maximumTeams,
                     maximumTeams: roundRoom.maximumTeams
                  };
                  afterAdd.before(templates.render('assignable-room-row', room));
               });
            }

            refreshRoomsForConditionSelect(AJS.$("#transitions select.rooms"));
         });

         unallocatedRooms.done(function(data) {
            var roomAdderRow = AJS.$(".assignable-rooms .room-adder-row");
            if(data.rooms.length > 0) {
               roomAdderRow.removeClass("hidden");
               roomAdderRow.find('.assignable-room-select').remove();

               var roomAdderCell = roomAdderRow.find('td');
               roomAdderCell.prepend(templates.render('select-unallocated-room', data));
               roomAdderCell.find('.assignable-room-select').auiSelect2();
            } else {
               roomAdderRow.addClass("hidden");
            }
         });
      };

      AJS.$(".assignable-rooms .room-adder-row .add-room").click(H.h(function(e) {
         clearRoomsPreview();

         var roomSelect = AJS.$('.assignable-rooms .room-adder-row select.assignable-room-select');

         if(selectedRound) {
            // Create the room in the requested state
            var request = AJS.$.ajax({
               url: "/rest/round-room/create",
               type: "PUT",
               cache: false,
               data: JSON.stringify({
                  projectId: pageContext.project.id,
                  roundId: selectedRound.next.id,
                  roomId: parseInt(roomSelect.val())
               })
            });

            request.done(function() {
               // Cause the transitions to reload
               loadAssignableRooms(selectedRound);
            });

            request.fail(function() {
               AP.require('messages', function(message) {
                  message.error("Failed to create assignable room.", "Please try again or contact support.");
               });
            });
         } 
      }));

      var updateRoundState = function() {
         clearRoomsPreview();
         var projectId = pageContext.project.id;

         var currentRoomId = parseInt(selectRoundFrom.val());
         var currentTiedState = AJS.$("#include-tied").is(':checked');
         var currentMatchUnallocated = AJS.$("#unallocated-teams").is(':checked');

         var request = updateTransitionOptions(projectId, currentRoomId, currentTiedState, currentMatchUnallocated);

         AP.require("messages", function(messages) {
            request.done(function() {
               reloadTransitionableRounds();
               messages.success('Options have been updated', '', {
                  delay: 1500,
                  closeable: true,
                  fadeout: true
               });
            });

            request.fail(function() {
               messages.error('Could not update options', 'Please refresh the page and try again, if the problem persists please raise an issue.', {
                  delay: 7000,
                  closeable: true,
                  fadeout: true
               });
            });
         });
      };

      AJS.$("#include-tied").change(function() {
         updateRoundState();
      });

      AJS.$("#unallocated-teams").change(function() {
         updateRoundState();
      });

      AJS.$("table.current-rooms").on('keyup', '.winning-teams', function() {
         clearRoomsPreview();

         var self = AJS.$(this);
         var updatedWinners = parseInt(self.val());
         var row = self.closest('tr.current-room');
         var roomId = parseInt(row.data('room-id'));
         var roundId = parseInt(selectRoundFrom.val());

         var status = self.siblings('.status');
         if(updatedWinners && roundId && roomId) {
            var request = updateRoundRoomWinners(pageContext.project.id, roundId, roomId, updatedWinners);

            request.done(function() {
               status.auiIconStatus("success");
               status.timeout(1000, function() {
                  status.auiIconStatus("hide");
               });
            });

            request.fail(function() {
               status.auiIconStatus("error");
            });
         } else {
            // Typing in invalid input is fail
            status.auiIconStatus("error");
         }
      });

      var assignableRooms = AJS.$(".assignable-rooms");

      assignableRooms.on('change', '.unlimited', function() {
         clearRoomsPreview();

         var self = AJS.$(this);
         var isChecked = self.isChecked();
         var row = self.closest('.assignable-room');
         var maxTeams = row.find('.maximum-teams');

         maxTeams.val("");
         maxTeams.toggleAttr("disabled", "", isChecked);

         handleAssignableRooms(row);
      });

      assignableRooms.on('keyup', '.maximum-teams', function() {
         clearRoomsPreview();

         // If you are getting a keyup on this event then it must not have the checkbox off
         var self = AJS.$(this);
         var row = self.closest('.assignable-room');

         handleAssignableRooms(row);
      });

      var handleAssignableRooms = function(row) {
         var $maxTeams = row.find('.maximum-teams');
         var $unlimited = row.find('.unlimited');
         var $status = row.find('.status');

         var roundId = getNextRoundId();
         var roomId = parseInt(row.data('room-id'));
         var maxTeams = null; // An empty value is unlimited
         if(!$unlimited.isChecked()) {
            maxTeams = parseInt($maxTeams.val());
            if(!maxTeams) {
               $status.auiIconStatus("error");
               return;
            }
         }

         $unlimited.timeout(500, function() {
            var request = updateRoundRoomMaxTeams(pageContext.project.id, roundId, roomId, maxTeams);

            request.done(function() {
               $status.auiIconStatus("success");
               $status.timeout(1000, function() {
                  $status.auiIconStatus("hide");
               });
            });

            request.fail(function() {
               $status.auiIconStatus("error");
            });
         });
      };

      AJS.$(".select2-load").auiSelect2();

      var fixHelperModified = function(e, tr) {
         var $originals = tr.children();
         var $helper = tr.clone();
         $helper.children().each(function(index) {
            AJS.$(this).width($originals.eq(index).width())
         });
         return $helper;
      };

      var recomputeTransitionOrder = function() {
         var rows = AJS.$("#transitions tbody tr");
         rows.each(function(index, element) {
            var self = AJS.$(element);
            var oneBasedIndex = 1 + index;
            var numberCell = self.find(".order-cell");
            numberCell.text("#" + oneBasedIndex);
         });
      };

      var sortMoveFinished = function() {
         recomputeTransitionOrder();
         updateRoundTransitions();
      };

      AJS.$("#transitions tbody").sortable({
         items: ".sortable",
         handle: ".selector-handle",
         helper: fixHelperModified,
         stop: sortMoveFinished
      });

      var runCalculate = function() {
         var previewSpinner = AJS.$(".preview-spinner");
         previewSpinner.spin();
         calculateButton.attr('aria-disabled', 'true');
         calculateButton.attr('disabled', 'disabled');
         AP.resize();

         var roundId = parseInt(selectRoundFrom.val());
         var request = calculateTransition(pageContext.project.id, roundId);

         request.always(function() {
            previewSpinner.spinStop();
            calculateButton.removeAttr('aria-disabled');
            calculateButton.removeAttr('disabled');
         });

         request.done(function(data) {
            var hasRooms = !!data.rooms;
            if(hasRooms) {
               var toIssueUrl = function(issueKey) {
                  return pageContext.productBaseUrl + "/browse/" + issueKey;
               };

               _.each(data.rooms, function(room) {
                  _.each(room.teams, function(team, i) {
                     team.isAllocated = team.allocationReason.reason === "Transitioned";
                     team.allocationClass = team.isAllocated ? "allocated" : "existing";
                     team.issueUrl = toIssueUrl(team.details.issueKey);
                     team.even = i % 2 == 0 ? "even" : "odd";
                  });
               });

               data.rooms.sort(function(a, b) {
                  return a.room.name.localeCompare(b.room.name);
               });

               data.hasOutvoted = data.outvotedTeams.length > 0;
               _.each(data.outvotedTeams, function(team, i) {
                  team.issueUrl = toIssueUrl(team.issueKey);
                  team.even = i % 2 == 0 ? "even" : "odd";
               });
               data.hasUnallocated = data.unallocatedTeams.length > 0;
               _.each(data.unallocatedTeams, function(team) {
                  team.issueUrl = toIssueUrl(team.issueKey);
               });

               var preview = AJS.$('.rooms-preview');
               preview.empty();
               preview.append(templates.render('rounds-preview', data));

               // Put the tooltips on all of the preview items
               AJS.$(".rooms-preview .tooltip-required").tooltip({ gravity: 'se' });
            } else {
               AP.require("messages", function(messages) {
                  if(data.errorType) {
                     if(data.errorType === "no-space-left-in-rooms") {
                        messages.error("No space left in rooms", "There is not enough space left in the rooms we are trying to transition into for transition #" + data.roundTransitionOrderNumber + ". Please add more assignable rooms or let the rooms hold more teams.");
                     }
                  }
               });
            }
         });

         request.fail(function() {
            AP.require("messages", function(messages) {
               // TODO only show this on a HTTP 401
               messages.error("Session has expired", "Please refresh the page and try again.");
            });
         });
      };

      var calculateButton = AJS.$(".transition-actions .calculate");

      var clearRoomsPreview = function(dontClear) {
         if(!dontClear) {
            AJS.$(".rooms-preview").empty();
         }
      };

      calculateButton.click(H.h(function() {
         runCalculate();
      }));

      AJS.$(".rooms-preview").on('click', '.apply-allocation', function() {
         var self = AJS.$(this);
         var isOutvoted = self.closest('.outvoted').length > 0;

         // Disable button on click
         self.attr('aria-disabled', 'true');
         self.attr('disabled', 'disabled');

         var room = self.closest('.room');

         var teams = room.find('.team');

         var teamData = AJS.$.map(teams, function(team) {
            var s = AJS.$(team);
            return {
               self: s,
               issueId: parseInt(s.data('issue-id')),
               issueKey: s.data('issue-key')
            };
         });

         var teamTransitioned = function(team) {
            var status = team.self.find(".status");
            status.removeClass("aui-lozenge aui-lozenge-success hidden");
            status.addClass("aui-icon aui-icon-small aui-iconfont-locked");
         };

         var request = getRoundsForHackathon(pageContext.project.id);

         request.done(function(data) {
            var outvotedRound = _.find(data.rounds, function(round) { return round.type === "Outvoted"; });

            var transitionData = {
               roundFrom: selectedRound.current,
               roundTo: isOutvoted ? outvotedRound : selectedRound.next,
               teams: teamData,
               updateTeam: teamTransitioned
            };

            if(!isOutvoted) {
               var roomComponentId = parseInt(room.data('component-id'));
               transitionData.room = { componentId: roomComponentId };
            }

            var transitionRequest = Transition.transitionTeams(transitionData);

            transitionRequest.always(function() {
               self.removeAttr('aria-disabled');
               self.removeAttr('disabled');
            });

            transitionRequest.done(function() {
               AP.require('messages', function(message) {
                  message.success('Successfully transitioned teams into the next round.', '', {
                     delay: 2000,
                     closeable: true,
                     fadeout: true
                  });
               });
            });

            transitionRequest.fail(function() {
               AP.require('messages', function(message) {
                  message.error = message.error || "";
                  message.error("Failed to transition a team: ", message.error);
               });
            });
         });

         request.fail(function() {
            AP.require('messages', function(message) {
               message.error("Could not request rounds for this hackathon", 'Please refresh the page and try again.');
            });
         });
      });
   });
});
