{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module TeamHandlers
    ( handleTeamDetails
    , handleTeamRefresh
    , handleGetTeamDescription
    ) where

import           AesonHelpers
import           Application
import qualified Control.Arrow                 as A
import           Control.Monad                 (void)
import           Control.Monad.Trans.Class     (lift)
import           Control.Monad.Trans.Either
import           Data.Aeson
import           Data.Aeson.Types              (fieldLabelModifier)
import qualified Data.HackathonRequest         as HR
import           Data.MaybeUtil
import qualified Data.Text                     as T
import           GHC.Generics
import           HandlerHelpers
import qualified HostRequests.IssueDescription as ID
import qualified Persistence.Hackathon         as PH
import qualified Persistence.HackathonIds      as HI
import qualified Persistence.HackathonRoom     as HRM
import qualified Persistence.HackathonRound    as RND
import qualified Persistence.HackathonTeam     as PHT
import qualified Persistence.HackathonVote     as HV
import qualified Snap.AtlassianConnect         as AC
import qualified Snap.Core                     as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import           TeamLoader                    (refreshTeam)
import           TenantWithVoteableUser
import qualified WithToken                     as WT

handleTeamDetails :: AppHandler ()
handleTeamDetails = handleMethods
   [ (SC.GET, WT.tenantFromToken getTeamDetails)
   ]

standardAuthError :: AppHandler ()
standardAuthError = respondWithError unauthorised "You need to login before you query for team members."

getTeamDetails :: AC.TenantWithUser -> AppHandler ()
getTeamDetails (_, Nothing) = standardAuthError
getTeamDetails (tenant, _) = do
    potentialTeamDetails <- withPG . runEitherT $ do
       issueId <- EitherT (m2e noIssueId <$> getIntegerQueryParam "issueId")
       team <- EitherT (m2e noSuchTeam <$> PHT.getHackathonTeamByIssueId tenant issueId)
       hRound <- lift $ PHT.getRoundForTeam team
       hRoom <- lift $ PHT.getRoomForTeam team
       pastResults <- lift $ HV.getAllRankedVotesForTeam team
       return $ toTeamDetails team hRound hRoom pastResults
    case potentialTeamDetails of
       Left err -> uncurry respondWithError err
       Right teamDetails -> writeJson teamDetails
    where
       noIssueId = (badRequest, "You need to provide an issue id to get team details.")
       noSuchTeam = (notFound, "The requested issue is not a hackathon team.")


toTeamDetails :: PHT.HackathonTeam -> Maybe RND.HackathonRound -> Maybe HRM.HackathonRoom -> [HV.RankedResult] -> TeamDetails
toTeamDetails team potentialRound potentialRoom pastResults = TeamDetails
    { tdId = PHT.htId team
    , tdRoom = fmap toTeamRoom potentialRoom
    , tdRound = fmap toTeamRound potentialRound
    , tdPastResults = fmap toTeamPastResult pastResults
    }

toTeamRound :: RND.HackathonRound -> TeamRound
toTeamRound hr = TeamRound
    { troundId              = RND.hroundId hr
    , troundStatusId        = RND.hroundStatusId hr
    , troundName            = RND.hroundName hr
    , troundType            = T.pack . show . RND.hroundType $ hr
    , troundSequenceNumber  = RND.hroundSequenceNumber hr
    }

toTeamRoom :: HRM.HackathonRoom -> TeamRoom
toTeamRoom hr = TeamRoom
    { troomId               = HRM.hrId hr
    , troomName             = HRM.hrName hr
    , troomCurrentRoundId   = HRM.hrCurrentRoundId hr
    , troomComponentId      = HRM.hrComponentId hr
    }

data TeamDetails = TeamDetails
    { tdId    :: Integer
    , tdRoom  :: Maybe TeamRoom
    , tdRound :: Maybe TeamRound
    , tdPastResults :: [TeamPastResult]
    } deriving (Show, Generic)

instance ToJSON TeamDetails where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "td"
        }

data TeamRound = TeamRound
    { troundId             :: HI.HackathonRoundId
    , troundStatusId       :: HI.AtlassianStatusId
    , troundName           :: T.Text
    , troundType           :: T.Text
    , troundSequenceNumber :: Maybe Integer -- If nothing then this round is not voteable
    } deriving (Show, Generic)

instance ToJSON TeamRound where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "tround"
        }

data TeamRoom = TeamRoom
    { troomId             :: HI.HackathonRoomId
    , troomName           :: T.Text
    , troomCurrentRoundId :: HI.HackathonRoundId
    , troomComponentId    :: HI.ComponentId
    } deriving (Show, Generic)

instance ToJSON TeamRoom where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "troom"
        }

toTeamPastResult :: HV.RankedResult -> TeamPastResult
toTeamPastResult rr = TeamPastResult
    { tprRoundName = HV.rrRoundName rr
    , tprRoundSequenceNumber = HV.rrRoundSequenceNumber rr
    , tprRoomName = HV.rrRoomName rr
    , tprVotes = HV.rrVotes rr
    , tprRank = HV.rrRank rr
    }

data TeamPastResult = TeamPastResult
    { tprRoundName :: T.Text
    , tprRoundSequenceNumber :: Integer
    , tprRoomName :: T.Text
    , tprVotes :: Integer
    , tprRank :: Integer
    } deriving (Show, Generic)

instance ToJSON TeamPastResult where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "tpr"
        }

handleTeamRefresh :: AppHandler ()
handleTeamRefresh = handleMethods
   [ ( SC.PUT, WT.tenantFromToken refreshTeamDetails)
   ]

refreshTeamDetails :: AC.TenantWithUser -> AppHandler ()
refreshTeamDetails (_, Nothing) = respondWithError unauthorised "You do not have permission to refresh this teams details."
refreshTeamDetails (tenant, Just _) = writeError . runEitherT $ do
   issueId <- EitherT (m2e missingIssueId <$> getIntegerQueryParam "issueId")
   EitherT (A.left toError <$> (refreshTeam tenant issueId))
   void . lift $ respondNoContent
   where
      missingIssueId = (badRequest, "You need to provide an issue id to make this request.")
      toError x = (internalServer, "Could not refresh the issue: " ++ show x)

handleGetTeamDescription :: AppHandler ()
handleGetTeamDescription = handleMethods
   [ ( SC.POST, getTeamDescription)
   ]

data TeamDescriptionRequest = RoomRequest
    { tdrIssueId   :: AC.IssueId
    , tdrHackathon :: HR.HackathonRequest
    } deriving (Show, Generic)

instance FromJSON TeamDescriptionRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "tdr"
        }

data TeamDescriptionResponse = TeamDescriptionResponse
    { tdrRenderedDescription :: T.Text
    } deriving (Show, Generic)

instance ToJSON TeamDescriptionResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "tdr"
        }

getTeamDescription :: AppHandler ()
getTeamDescription = do
    request <- SC.readRequestBody size10KB
    writeError . runEitherT $ do
        descriptionRequest <- hoistEither . withErr badRequest parseFail . eitherDecode $ request
        EitherT (const (Right ()) <$> tenantWithVoteableUserFromHackathonRequest (tdrHackathon descriptionRequest) (getTeamDescriptionHelper descriptionRequest))
    where
        parseFail = "Could not parse the request:"

getTeamDescriptionHelper :: TeamDescriptionRequest -> AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
getTeamDescriptionHelper descriptionRequest tenant _ _ = do
    descriptionResult <- ID.getRenderedDescriptionForIssue tenant (tdrIssueId descriptionRequest)
    case descriptionResult of
        Left e -> respondWithError notFound ("Could not find a description for the provided issue. (" ++ (show . tdrIssueId $ descriptionRequest) ++ ") " ++ show e)
        Right renderedDescription -> writeJson . TeamDescriptionResponse $ renderedDescription
