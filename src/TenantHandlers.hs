{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module TenantHandlers
    ( handleTenantRequest
    , handleTenantBaseUrlRequest
    , handleAnonymousTokenRequest
    ) where

import           AesonHelpers
import           Application
import           Data.Aeson
import           Data.Aeson.Types                (fieldLabelModifier)
import qualified Data.Text.Encoding              as T
import           Data.Time.Units
import           GHC.Generics
import qualified Network.URI                     as NU
import qualified Persistence.AnonymousTenantUser as ATU
import qualified Persistence.Tenant              as PT
import qualified Snap.AtlassianConnect           as AC
import qualified Snap.Cache                      as CH
import qualified Snap.Core                       as SC
import           Snap.Helpers
import qualified WithToken                       as WT

handleTenantRequest :: AppHandler ()
handleTenantRequest = handleMethods
    [ (SC.GET, WT.tenantFromToken getTenantKey)
    ]

-- NOTE: If you ever add more than the key and base url to this data type then you must copy it and make it again
-- for the base url code.
data TenantDetails = TenantDetails
    { tdKey     :: AC.ClientKey
    , tdBaseUrl :: NU.URI
    } deriving (Show, Generic)

instance ToJSON TenantDetails where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "td"
        }

toTenantDetails :: AC.Tenant -> TenantDetails
toTenantDetails t = TenantDetails
    { tdKey = AC.key t
    , tdBaseUrl = AC.getURI . AC.baseUrl $ t
    }

-- Only logged in users can lookup the tenant key
getTenantKey :: AC.TenantWithUser -> AppHandler ()
getTenantKey (_, Nothing) = respondWithError unauthorised "You need to be logged in in order to get tenant details."
getTenantKey (tenant, _) = do
    CH.cacheFor (1 :: Minute)
    writeJson (toTenantDetails tenant)

handleTenantBaseUrlRequest :: AppHandler ()
handleTenantBaseUrlRequest = handleMethods
    [ (SC.GET, getTenantBaseUrl)
    ]

-- Anybody with the tenant key can look up the base url
getTenantBaseUrl :: AppHandler ()
getTenantBaseUrl = do
    potentialTenant <- getTenantFromTenantKey
    case potentialTenant of
        Nothing -> respondWithError notFound "We could not find a tenant with the provided key."
        Just tenant -> do
            CH.cacheFor (1 :: Minute)
            writeJson (toTenantDetails tenant)

handleAnonymousTokenRequest :: AppHandler ()
handleAnonymousTokenRequest = handleMethods
    [ (SC.GET, createAnonymousToken)
    ]

createAnonymousToken :: AppHandler ()
createAnonymousToken = do
    potentialTenant <- getTenantFromTenantKey
    case potentialTenant of
        Nothing -> respondWithError notFound "We could not find a tenant with the provided key."
        Just tenant -> do
            user <- ATU.createAnonymousUser tenant
            writeJson (AnonymousTokenResponse . ATU.atuToken $ user)

data AnonymousTokenResponse = AnonymousTokenResponse
    { atrToken :: ATU.AnonymousUserToken
    } deriving (Show, Generic)

instance ToJSON AnonymousTokenResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "atr"
        }

getTenantFromTenantKey :: AppHandler (Maybe AC.Tenant)
getTenantFromTenantKey = do
    potentialTenantKey <- SC.getQueryParam "tenantKey"
    case potentialTenantKey of
        Nothing -> return Nothing
        Just tenantKey -> PT.lookupTenant . T.decodeUtf8 $ tenantKey
