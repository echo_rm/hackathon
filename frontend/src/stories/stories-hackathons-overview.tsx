import * as React from 'react';
import { partialStoriesOf } from './stories-common';
import { HackathonsOverview } from '../HackathonsOverview';
import { PageContextExample } from './PageContextData';
import { ExampleHackathons } from './HackathonDetailsData';

export function stories(storyCreator: partialStoriesOf) {
   storyCreator('Hackathons Overview')
     .add('Hackathons not loaded yet', () => {
       return <HackathonsOverview pc={PageContextExample} hackathons="loading" />;
     })
     .add('Hackathons loading failed', () => {
       return <HackathonsOverview pc={PageContextExample} hackathons="loading-failed" />;
     })
     .add('No hackathons yet', () => {
       return <HackathonsOverview pc={PageContextExample} hackathons={[]} />;
     })
     .add('Default Hackathons View', () => {
       return <HackathonsOverview pc={PageContextExample} hackathons={ExampleHackathons} />;
     });
}