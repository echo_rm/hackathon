# About

Hackathon for Cloud is the spiritual successor of the ShipIt Voting Plugin written primarily by
Robert Massaioli while working at Atlassian. This plugin was built so that we could provide an
excellent experience running ShipIt's or Hackathons to our Cloud customers. Inbuilt into the addon
is all of lessons that Atlassian has learned over the years about how to run a successful Hackathon.
It is our hope that this free addon will allow you to run your own successful hackathons too.

Primary developer: Robert Massaioli <rmassaioli@atlassian.com>

Contributors:

 - Ashley Valent
 - Peggy Kuo
 - Kate West Walker
 - Alex Wei
 - Kai Forsyth

We continue to monitor and maintain Hackathon for Cloud and ensure that it is a world class service.
