define(function() {
   var public = {};

   public.h = function (f) {
      return function(event) {
         event.preventDefault();
         event.stopImmediatePropagation();
         f && f(event);
      };
   };

   return public;
});
