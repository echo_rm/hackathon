module Persistence.HackathonIds
    ( TenantId
    , HackathonId
    , TenantUserId
    , AnonymousTenantUserId
    , HackathonTeamId
    , HackathonRoomId
    , HackathonRoundId
    , RoundTransitionId
    , RoundTransitionRoomId
    , VoteId
    , ComponentId
    , AtlassianStatusId
    ) where

type TenantId = Integer
type HackathonId = Integer
type TenantUserId = Integer
type AnonymousTenantUserId = Integer
type HackathonTeamId = Integer
type HackathonRoomId = Integer
type HackathonRoundId = Integer
type RoundTransitionId = Integer
type RoundTransitionRoomId = Integer
type VoteId = Integer
type ComponentId = Integer
type AtlassianStatusId = Integer
