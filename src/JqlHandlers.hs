{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module JqlHandlers
   ( handleJqlTest
   ) where

import           AesonHelpers
import           Application
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Either
import           Data.Aeson
import           Data.Aeson.Types                  (fieldLabelModifier)
import qualified Data.JQL                          as J
import           Data.MaybeUtil
import qualified Data.Text.Encoding                as T
import           GHC.Generics
import           HandlerHelpers
import qualified HostRequests.IssueSearch          as IS
import qualified HostRequests.UserDetails          as UD
import qualified Persistence.Hackathon             as PH
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Core                         as SC
import           Snap.Helpers
import           StandardJQL
import qualified WithToken                         as WT

handleJqlTest :: AppHandler ()
handleJqlTest = handleMethods
   [ (SC.GET, WT.tenantFromToken testJql)
   ]

testJql :: AC.TenantWithUser -> AppHandler ()
testJql (_, Nothing) = respondWithError unauthorised "You need to be logged in to make this request."
testJql (tenant, Just userKey) = do
   potentialTestResult <- runEitherT $ do
      hackathon <- EitherT $ getHackathonFromProjectId tenant
      EitherT $ requirePermissionsE [UD.ProjectAdmin] tenant userKey hackathon
      rawJql <- EitherT ((m2e noJqlParam . fmap T.decodeUtf8) <$> SC.getQueryParam "jql")
      lift (handleSearch <$> IS.searchForIssues tenant (jqlForHackathon hackathon rawJql) oneResult)
   case potentialTestResult of
      Left err -> uncurry respondWithError err
      Right testResult -> writeJson . TestResultResponse $ testResult
   where
      noJqlParam = (badRequest, "You need to provide a JQL query that we will run.")

      handleSearch :: Either AC.ProductErrorResponse IS.IssueSearchResponse -> TestResult
      handleSearch (Left per) =
         case AC.perCode per of
            400 -> Invalid
            _   -> Failed
      handleSearch (Right _) = Valid

data TestResultResponse = TestResultResponse
   { trrResult :: TestResult
   } deriving (Show, Generic)

instance ToJSON TestResultResponse where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "trr" }

data TestResult
  = Valid
  | Invalid
  | Failed
  deriving (Show)

instance ToJSON TestResult where
   toJSON = toJSON . show

oneResult :: IS.Page
oneResult = IS.Page 0 1

jqlForHackathon :: PH.Hackathon -> J.RawJQL -> J.JQL
jqlForHackathon hackathon raw = J.And (jqlForHackathonIssues hackathon) [ J.CustomJQL raw ]
