module HostRequests.EntityProperties
   ( getIssueProperties
   , updateIssueProperties
   , updateProjectProperties
   , PropertyKey
   ) where

import           Application
import           Data.Aeson
import qualified Data.ByteString.Char8             as BSC
import           Data.Monoid                       (mempty)
import qualified Network.URI                       as U
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Snaplet                      as SS

type PropertyKey = String

getIssueProperties :: FromJSON a => AC.Tenant -> AC.IssueId -> PropertyKey -> AppHandler (Either AC.ProductErrorResponse a)
getIssueProperties tenant issueId propertyKey = SS.with connect $ AC.hostGetRequest tenant url [] mempty
   where
      url = issuePropertiesUrl issueId propertyKey

updateIssueProperties :: ToJSON a => AC.Tenant -> AC.IssueId -> PropertyKey -> a -> AppHandler (Either AC.ProductErrorResponse ())
updateIssueProperties tenant issueId propertyKey propertyData = SS.with connect $ swallowSuccess <$> AC.hostPutRequest tenant url [] (AC.setJson propertyData)
   where
      url = issuePropertiesUrl issueId propertyKey

updateProjectProperties :: ToJSON a => AC.Tenant -> AC.ProjectId -> PropertyKey -> a -> AppHandler (Either AC.ProductErrorResponse ())
updateProjectProperties tenant projectId propertyKey propertyData = SS.with connect $ swallowSuccess <$> AC.hostPutRequest tenant url [] (AC.setJson propertyData)
   where
      url = projectPropertiesUrl projectId propertyKey

issuePropertiesUrl :: AC.IssueId -> PropertyKey -> BSC.ByteString
issuePropertiesUrl issueId propertyKey = BSC.pack $ "/rest/api/2/issue/" ++ show issueId ++ "/properties/" ++ escapeInUri propertyKey

projectPropertiesUrl :: AC.ProjectId -> PropertyKey -> BSC.ByteString
projectPropertiesUrl projectId propertyKey = BSC.pack $ "/rest/api/2/project/" ++ show projectId ++ "/properties/" ++ escapeInUri propertyKey

-- According to the documentation a 'Created' is returned the first time you create the property so we should just eat it
swallowSuccess :: Either AC.ProductErrorResponse () -> Either AC.ProductErrorResponse ()
swallowSuccess x@(Right _) = x
swallowSuccess (Left (AC.ProductErrorResponse 200 _)) = Right ()
swallowSuccess (Left (AC.ProductErrorResponse 201 _)) = Right ()
swallowSuccess x@(Left _) = x

escapeInUri :: String -> String
escapeInUri = U.escapeURIString U.isUnescapedInURI
