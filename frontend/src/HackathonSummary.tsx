import * as React from 'react';
import {
   HackathonSummary as HS,
   RoundRoomStatus,
   RoundRoomVoteDetails,
   HackathonSummaryStats
} from './hackathon-client';
import Button, { ButtonGroup } from '@atlaskit/button';
import DynamicTable, { DynamicTableHead, DynamicTableRow } from '@atlaskit/dynamic-table';
import Lozenge, { LozengeAppearance } from '@atlaskit/lozenge';
import Badge from '@atlaskit/badge';
import { linkToViewTeams, HackathonContext, linkToDashboard } from './Links';
import { PageContext } from './page-context';
import { CheckboxSelect, OptionTypes, Option, Action, GroupOption } from '@atlaskit/select';
import FieldText from '@atlaskit/field-text';
import styled from 'styled-components';
import DropdownMenu, { DropdownItem } from '@atlaskit/dropdown-menu';
import { ToggleStateless } from '@atlaskit/toggle';
import * as moment from 'moment';
import Tooltip from '@atlaskit/tooltip';
import { IssueDetails, IssueFieldStatusCategory } from './HostRequest';

function formatDate(date: moment.Moment) {
   var prettyDate = 'DD MMM YYYY hh:mmA';
   return date.format(prettyDate);
}

function pageContextToHackathonContext(pc: PageContext): HackathonContext | undefined {
   if (pc.project === undefined) {
      return undefined;
   }

   return {
      projectId: pc.project.id,
      projectKey: pc.project.key
   };
}

function isOption(option: Option | GroupOption): option is Option {
   return 'value' in option;
}

export type HackathonSummaryProps = {
   pc: PageContext;
   summary: HS;
   stats: HackathonSummaryStats;
   myIssues: IssueDetails[];
   onCreateTeam(): void;
};

export type HackathonSummaryState = {
   roomFilter?: string;
   roundFilter?: string[];
   showLocked: boolean;
};

export class HackathonSummary extends React.PureComponent<HackathonSummaryProps, HackathonSummaryState> {
   private static RoundRoomActions = styled.div`
      display: flex;
      flex-direction: row;
      justify-content: flex-start;
      align-items: center;
   `;

   private static RoomFilterContainer = styled.div`
      width: 300px;
   `;

   private static RoundSelectContainer = styled.div`
      min-width: 300px;
      padding-left: 8px;
   `;

   private static ShowLockedContainer = styled.div`
      padding-left: 8px;
   `;

   private static RoundRoomTableHead: DynamicTableHead = {
      cells: [{
         key: 'room',
         content: <span>Room</span>,
         isSortable: true
      }, {
         key: 'round',
         content: <span>Round</span>,
         isSortable: true
      }, {
         key: 'status',
         content: <span>Status</span>,
         isSortable: true
      }, {
         key: 'teams',
         content: <span>Teams</span>,
         isSortable: true
      }, {
         key: 'votes',
         content: <span>Votes</span>,
         isSortable: true
      }, {
         key: 'actions',
         content: <span>Actions</span>,
         isSortable: false
      }]
   };

   private static lozengeForStatusCategoryMap: { [colorName: string]: LozengeAppearance } = {
      yellow: 'moved',
      green: 'success',
      'blue-grap': 'inprogress'
   };

   private static getLozengeForStatus(status: RoundRoomStatus): JSX.Element {
      if (status === RoundRoomStatus.Active) {
         return <Lozenge appearance="success">Active</Lozenge>;
      } else if (status === RoundRoomStatus.Locked) {
         return <Lozenge appearance="removed">Locked</Lozenge>;
      } else if (status === RoundRoomStatus.Inactive) {
         return <Lozenge>Inactive</Lozenge>;
      } else {
         return <Lozenge appearance="inprogress">Unknown</Lozenge>;
      }
   }

   private static lozengeForStatusCategory(statusCategory: IssueFieldStatusCategory): LozengeAppearance {
      return HackathonSummary.lozengeForStatusCategoryMap[statusCategory.colorName] || 'default';
   }

   private static getUniqueRounds(roundRooms: RoundRoomVoteDetails[]): { [roundId: number]: string } {
      const result: { [id: number]: string } = {};
      roundRooms.forEach(rr => result[rr.round.id] = rr.round.name);
      return result;
   }

   componentWillMount() {
      this.setState({
         showLocked: true
      });
   }

   render() {
      const { summary, stats, pc, onCreateTeam } = this.props;
      const { roundFilter, roomFilter, showLocked } = this.state;
      const startDate = moment(summary.startDate);
      const endDate = moment(summary.endDate);

      const selectedRoundRooms = summary.roundRoomVotes
         .filter(rrd =>
            (rrd.isCurrentRound && rrd.status === RoundRoomStatus.Active) ||
            (showLocked && rrd.status === RoundRoomStatus.Locked)
         )
         .filter(rrd =>
            roomFilter === undefined ||
            rrd.room.name.toLocaleLowerCase().includes(roomFilter.toLocaleLowerCase())
         )
         .filter(rrd =>
            roundFilter === undefined || roundFilter.length === 0 ||
            roundFilter.some(id => id === `${rrd.round.id}`)
         );

      const hackathonContext = pageContextToHackathonContext(pc);
      const roundRoomRows: DynamicTableRow[] = selectedRoundRooms.map(rrDetails => {
         const viewTeamsLink = hackathonContext === undefined
            ? '#'
            : linkToViewTeams(
               pc.productBaseUrl,
               hackathonContext,
               { name: rrDetails.round.name },
               { name: rrDetails.room.name }
            );

         const menuItems: JSX.Element[] = new Array<JSX.Element>();
         if (hackathonContext !== undefined) {
            if (rrDetails.status === RoundRoomStatus.Active && rrDetails.room.shortUrl !== undefined) {
               menuItems.push(<DropdownItem key="vote" href={rrDetails.room.shortUrl}>Vote</DropdownItem>);
            }
            menuItems.push(
               <DropdownItem key="leaderboard" href={linkToDashboard(pc, hackathonContext)}>
                  View leaderboard
               </DropdownItem>
            );
         }

         return {
            key: `${rrDetails.round.name}-${rrDetails.room.name}`,
            cells: [{
               key: rrDetails.room.name,
               content: <a target="_blank" href={viewTeamsLink}>{rrDetails.room.name}</a>
            }, {
               key: rrDetails.round.name,
               content: <Lozenge>{rrDetails.round.name}</Lozenge>
            }, {
               key: rrDetails.status,
               content: HackathonSummary.getLozengeForStatus(rrDetails.status)
            }, {
               key: rrDetails.numberOfTeams,
               content: <Badge max={9999}>{rrDetails.numberOfTeams}</Badge>
            }, {
               key: rrDetails.numberOfVotes,
               content: <Badge max={99999}>{rrDetails.numberOfVotes}</Badge>
            }, {
               key: 'actions',
               content: (
                  <DropdownMenu
                     trigger="Actions"
                     triggerType="button"
                     position="right top"
                  >
                     {menuItems}
                  </DropdownMenu>
               )
            }]
         };
      });

      const rounds = HackathonSummary.getUniqueRounds(summary.roundRoomVotes);
      const roundOptions: OptionTypes[] = Object.keys(rounds).map<Option>(roundId => {
         return {
            label: rounds[roundId],
            value: `${roundId}`
         };
      }).sort((a, b) => {
         return b.label.localeCompare(a.label);
      });

      const toggleShowLocked = () => this.setState(s => ({ showLocked: !s.showLocked}));

      const activeRoundRooms = summary.roundRoomVotes
         .filter(rr => rr.status === RoundRoomStatus.Active);

      const presentingTeams = activeRoundRooms
         .map(rr => rr.numberOfTeams)
         .reduce((p, c) => p + c, 0);

      const activeVotes = activeRoundRooms
         .map(rr => rr.numberOfVotes)
         .reduce((p, c) => p + c, 0);

      const totalVotes = summary.roundRoomVotes
         .map(rr => rr.numberOfVotes)
         .reduce((p, c) => p + c, 0);

      return (
         <div>
            <h1>{summary.projectName}</h1>
            <ButtonGroup>
               <Button onClick={() => onCreateTeam()}>Create a team</Button>
               {summary.shortUrl && <Button href={summary.shortUrl}>Go vote!</Button>}
            </ButtonGroup>
            <h2>Information</h2>
            <table>
               <tbody>
                  <tr>
                     <td>
                        <Tooltip content="The start and end dates of the Hackathon.">
                           <span>When</span>
                        </Tooltip>
                     </td>
                     <td>
                        <Lozenge>{formatDate(startDate)}</Lozenge> &mdash; <Lozenge>{formatDate(endDate)}</Lozenge>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <Tooltip content="The number of unique people competing in the Hackathon.">
                           <span>Competitors</span>
                        </Tooltip>
                     </td>
                     <td><Badge max={100000}>{stats.competitors}</Badge></td>
                  </tr>
                  <tr>
                     <td>
                        <Tooltip content="The number of teams in the 'Team Formation' state.">
                           <span>Forming teams</span>
                        </Tooltip>
                     </td>
                     <td><Badge max={10000}>{stats.formingTeams}</Badge></td>
                  </tr>
                  <tr>
                     <td>
                        <Tooltip content="The number of teams in 'Active' rooms.">
                           <span>Presenting teams</span>
                        </Tooltip>
                     </td>
                     <td><Badge max={10000}>{presentingTeams}</Badge></td>
                  </tr>
                  <tr>
                     <td>
                        <Tooltip content="The number of votes placed in 'Active' rooms.">
                           <span>Total active votes</span>
                        </Tooltip>
                     </td>
                     <td><Badge max={1000000}>{activeVotes}</Badge></td>
                  </tr>
                  <tr>
                     <td>
                        <Tooltip content="The number of votes placed in this Hackathon.">
                           <span>Total votes</span>
                        </Tooltip>
                     </td>
                     <td><Badge max={1000000}>{totalVotes}</Badge></td>
                  </tr>
               </tbody>
            </table>
            {this.renderMyTeams()}
            <h2>Rooms</h2>
            <HackathonSummary.RoundRoomActions>
               <HackathonSummary.RoomFilterContainer>
                  <FieldText
                     isLabelHidden={true}
                     shouldFitContainer={true}
                     placeholder="Filter by room..."
                     onChange={e => this.setState({ roomFilter: e.currentTarget.value })}
                  />
               </HackathonSummary.RoomFilterContainer>
               <HackathonSummary.RoundSelectContainer>
                  <CheckboxSelect
                     options={roundOptions}
                     placeholder="Showing all rounds"
                     onChange={(option, action) => this.onRoundFilterChanged(option, action)}
                  />
               </HackathonSummary.RoundSelectContainer>
               <HackathonSummary.ShowLockedContainer>
                  Show locked?
                  <ToggleStateless isChecked={showLocked} onChange={toggleShowLocked} />
               </HackathonSummary.ShowLockedContainer>
            </HackathonSummary.RoundRoomActions>
            <DynamicTable
               head={HackathonSummary.RoundRoomTableHead}
               rows={roundRoomRows}
               defaultSortKey="round"
               defaultSortOrder="DESC"
            />
         </div>
      );
   }

   private renderMyTeams(): JSX.Element[] {
      const { myIssues, pc } = this.props;

      if (myIssues.length === 0) {
         return [];
      }

      const teamsListItems = myIssues.map(issue => {
         const viewUrl = `${pc.productBaseUrl}/browse/${issue.key}`;
         return (
            <li key={issue.key}>
               <a href={viewUrl} target="_top">{issue.key}: {issue.fields.summary}</a>&nbsp;
               <Lozenge appearance={HackathonSummary.lozengeForStatusCategory(issue.fields.status.statusCategory)}>
                  {issue.fields.status.name}
               </Lozenge>
            </li>
         );
      });

      return [
         <h2 key="teamListHeading">{myIssues.length === 1 ? 'Your team' : 'Your teams'}</h2>,
         <ul key="teamList">{teamsListItems}</ul>
      ];
   }

   private onRoundFilterChanged(options: OptionTypes | OptionTypes[] | null, action: { action: Action }): void {
      if (Array.isArray(options)) {
         const roundIds = options.filter(isOption).map(o => o.value);
         this.setState({
            roundFilter: roundIds
         });
      }
   }
}