<apply template="connect-panel">
   <bind tag="body-class">hackathon-statistics</bind>
   <bind tag="header-extra">
      <js src="/static/${resourcesVersion}/js/app/hackathon-statistics.js" />
      <script>require(['app/hackathon-statistics']);</script>
   </bind>

   <h2>Statistics</h2>
   <p>These are the statistics for the current hackathon at this current point in time.</p>

   <h3>Team Sizes</h3>
   <p>The following graph displays how many teams have N team members. The blue bars represent the
   number of teams of that size. The yellow bars represent how many people would say that they are
   in a team of that size (aka how many people are in each of those team sizes).</p>
   <div id="users-in-teams"></div>
   <div id="no-users-in-teams" class="aui-message hidden">
        <p class="title"><strong>There are no teams in this hackathon.</strong></p>
        <p>When one or more teams have been created in this hackathon then statistics will be revealed here.</p>
   </div>

   <h3>Votes per Round</h3>
   <p>How many votes have been placed in each round?</p>
   <div id="votes-per-round"></div>
   <div id="no-votes-per-round" class="aui-message hidden">
        <p class="title"><strong>There are no votes in this hackathon.</strong></p>
        <p>When one or more votes have been placed in this hackathon then statistics will be revealed here.</p>
   </div>
</apply>
