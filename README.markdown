# Hackathon Atlassian add-on for Cloud

This is the Hackathonaddon for cloud. To build the code:

    cabal sandbox init
    cabal install

Once you've built it, run the app using:

    CONNECT_BASE_URL="http://localhost:<your-app-port>" bash scripts/dev-run.bash .cabal-sandbox/bin/hackathon --port <your-app-port> --access-log=- --error-log=stderr
    
Then install the App via the "Manage apps" page in your Atlassian Cloud instance.

## Setting up the local database

In order to setup the local database:

    bash init-postgresql.bash

It will give you a command that you can run to start the database . Look at the logfile to confirm that it has started.

Then you need to create the database and the user:

    createuser hackathon
    createdb hackathon

Then you can test that this has worked by:

    bash scripts/recreate-database.bash
    bash scripts/run-psql.bash

These two commands should recreate your database tables and open up a psql terminal. Once that is done you have a working local 
database for this service. Congratulations.

During development, if you make changes to migrations that have already run locally, you'll need to run:

    ./migrations/flyway clean && ./migrations/flyway migrate
    
However - once migrations are run on production, you're out of luck.

## Compiling the static resources

Both the javascript and css in this project need to be processed. You will find all of the static
resources in the /static directory before they are processed. Processed javascript files will end up
in the /static-js directory and processed css files will end up in /static-css.

Note: You do not need to restart the application for the updated static resources to be loaded by
the browser in dev. The StaticHandler makes that work for you.

After changing the templates, to see your changes reflected in the addon, just hit this url:

    http://localhost:<your-app-port>/admin/reload

### Building the Javascript

The javascript uses requirejs to give us convinient AMD modules and a unique "single page app" of
javascript for each panel. To build the javascript:

    bash scripts/build-javascript.bash optimize=none

When you are ready for production then drop the optimize=none.

### Building the Less into CSS

The css uses lesscss to give us easy to write css. We compile it from the command line by doing the
following:

    bash scripts/build-css.bash

When you are for production then add a '-x' flag to that command.
    
    
