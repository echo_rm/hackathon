define([
   "../helpers/PageContext", 
   "../host/request", 
   '../helpers/MustacheLoader', 
   'underscore', 
   'moment', 
   '../helpers/Handler',
   '../helpers/Links',
   'connect/pagetoken', 
   '../lib/jquery.datetimepicker'
], 
function(PC, HostRequest, MustacheLoader, _, moment, H, Links) {
    var pageContext = PC.load();
    var templates;

    var updateHackathonHoursDifference = function() {
        var start = moment(AJS.$("#hackathon-start").val());
        var end = moment(AJS.$("#hackathon-end").val());
        if(start.isValid() && end.isValid()) {
            AJS.$("#hackathon-ends-description").text(templates.render("hackathon-duration", { hours: end.diff(start, 'hours') }));
        }
    };

    var requestAddonPermissions = function(projectId) {
       return AJS.$.ajax({
            url: '/rest/addon/permission-check',
            type: 'GET',
            data: {
               projectId: projectId 
            },
            cache: 'false'
       });
    };

    AJS.$(function() {
        // Show the permissions warning message if required
        requestAddonPermissions(pageContext.project.id).done(function(data) {
           AJS.$("#missing-browse-users").toggleClass('hidden', data.hasBrowseUsers);
           if(data.hasBrowseUsers) {
              var projectRolesPage = pageContext.productBaseUrl + '/plugins/servlet/project-config/' + pageContext.project.key + '/roles';
              AJS.$("#missing-project-admin-roles-link").attr('href', projectRolesPage);
              AJS.$("#missing-project-admin").toggleClass('hidden', data.hasProjectAdmin);
           }

           // Hide everything from the user if these errors exist
           if(!data.hasBrowseUsers || !data.hasProjectAdmin) {
              AJS.$("#missing-project-admin").nextAll().addClass('hidden');
           }
        });

        var issueTypeSelector = AJS.$("#hackathon-issue-type");

        var loadMustacheTemplates = function() {
            templates = MustacheLoader.load();
        };

        var currentProjectName;
        var loadIssueTypes = function() {
            var request = HostRequest.projectDetails(pageContext.project.id);

            request.done(function(rawJson) {
                var json = JSON.parse(rawJson);
                console.log(json);
                currentProjectName = json.name;

                issueTypeSelector.empty();

                AJS.$.each(json.issueTypes, function(i, issueType) {
                    var optionHtml = templates.render("hackathon-option", issueType);
                    issueTypeSelector.append(optionHtml);
                });

                issueTypeSelector.auiSelect2();

                loadWorkflowStatuses(issueTypeSelector.val());
            });

            request.fail(function() {
               AP.require("messages", function(message) {
                  message.error("Issue types could not be loaded.", "We could not load the issue types for this JIRA project. Something has gone very wrong. Raise an issue with Hackathon for Cloud.");
               });
            });
        };

        var loadWorkflowStatuses = function(issueTypeId) {
            var request = HostRequest.projectStatuses(pageContext.project.id);

            request.done(function(rawJson) {
                var json = JSON.parse(rawJson);
                console.log(json);
                var matchingIssueType = _.find(json, function(issueType) { return issueType.id == issueTypeId; });
                AJS.log(matchingIssueType);

                var allRounds = AJS.$(".round.select");
                AJS.$.each(allRounds, function(i, round) {
                    var statusCopy = _.map(matchingIssueType.statuses, _.clone);
                    updateStatuses(round, statusCopy);
                });
            });

            request.fail(function() {
               AP.require("messages", function(message) {
                  message.error("Workflow statuses could not be loaded.", "We could not load the issue types for this JIRA project. Something has gone very wrong. Raise an issue with Hackathon for Cloud.");
               });
            });
        };

        var addRound = function() {
            var statusButtonContainer = AJS.$("#hackathon-status-buttons");
            var dynamicRounds = AJS.$(".round-container .dynamic-round");
            dynamicRounds.removeClass("last-round");
            var roundCount = dynamicRounds.length + 1; // Add the +1 because Round 1 always exists
            statusButtonContainer.before(templates.render("round-status-mapper", {
                roundNumber: roundCount + 1
            }));

            var selectedIssueType = issueTypeSelector.val();
            loadWorkflowStatuses(selectedIssueType);
        };

        var removeLastRound = function(round) {
            var lastRound = AJS.$(".round-container .dynamic-round.last-round");
            if(lastRound.length > 0) {
                lastRound.prev().addClass("last-round");
                lastRound.remove();
            }
        };

        var updateStatuses = function(roundMapping, statuses) {
            var $roundMapping = AJS.$(roundMapping);
            var currentSelectedValue = $roundMapping.find('option:selected').text();
            $roundMapping.empty();
            AJS.$.each(statuses, function(i, status) {
                if(status.name == currentSelectedValue) {
                    status.selected = true;
                }
                $roundMapping.append(templates.render("hackathon-option", status));
            });
        };

        var showRoomNameInput = function() {
            AJS.$("#hackathon-voting-room-name").toggleClass("hidden", !usesSingleRoom());

        };

        // TODO when an issue type is selected then we should clear out the round mappings and reload them

        var roundTypes = {
            teamFormation: "TEAM_FORMATION",
            outvoted: "OUTVOTED",
            winner: "WINNER",
            round: "ROUND"
        };

        var makeRound = function(roundSelector, roundType, roundNumber) {
            var round = {
                workflowStatusId: parseInt(roundSelector.val()),
                name: roundSelector.find("option:selected").text(),
                type: roundType
            };

            if(roundNumber) {
                round.number = roundNumber;
            }

            return round;
        };

        var usesSingleRoom = function() {
            return AJS.$("#single-hackathon-voting-room-checkbox").is(':checked');
        };

        var getSingleRoomName = function() {
            return AJS.$("#hackathon-voting-room-name").val();
        };

        var activationContext = function() {
            var context = {};

            context.projectId = pageContext.project.id;
            context.projectKey = pageContext.project.key;
            context.projectName = currentProjectName;

            context.startDate = moment(AJS.$("#hackathon-start").val()).utc().toISOString();
            context.endDate = moment(AJS.$("#hackathon-end").val()).utc().toISOString();
            context.issueTypeId = parseInt(issueTypeSelector.val());

            if (usesSingleRoom()) {
                context.singleRoomName = getSingleRoomName();
            }

            // Now setup the rounds
            var teamFormationRound = makeRound(AJS.$("#status-static-team-formation"), roundTypes.teamFormation);
            var outvotedRound = makeRound(AJS.$("#status-static-outvoted"), roundTypes.outvoted);
            var winnerRound = makeRound(AJS.$("#status-static-winner"), roundTypes.winner);
            context.rounds = [teamFormationRound, outvotedRound, winnerRound];
            AJS.$(".round-container .voteable-round").each(function() {
                var self = AJS.$(this);
                context.rounds.push(makeRound(self, roundTypes.round, self.data("sequence-number")));
            });

            return context;
        };

        var defaultHackathonDates = (function() {
            var dateFormat = "YYYY/MM/DD HH:mm";

            var startDate = moment().minutes(0).add(1, 'hours');
            var endDate = moment().minutes(0).add(1, 'hours').add(1, 'days');

            return {
                start: startDate.format(dateFormat),
                end: endDate.format(dateFormat)
            };
        }());

        var init = function() {
            loadMustacheTemplates();

            AJS.$('#hackathon-start').val(defaultHackathonDates.start);
            AJS.$('#hackathon-end').val(defaultHackathonDates.end);
            AJS.$('#hackathon-start').datetimepicker({
                onChangeDateTime: updateHackathonHoursDifference
            });
            AJS.$('#hackathon-end').datetimepicker({
                onChangeDateTime: updateHackathonHoursDifference
            });
            loadIssueTypes();

            issueTypeSelector.change(function() {
                var selectedIssueType = issueTypeSelector.val();
                AJS.log("Selected issue type: " + selectedIssueType);
                loadWorkflowStatuses(selectedIssueType);
            });

            AJS.$("#add-round-button").click(H.h(addRound));
            AJS.$(".round-container").on('click', '.dynamic-round.last-round .remove-round', H.h(removeLastRound));
            AJS.$("#single-hackathon-voting-room-checkbox").on('change', H.h(showRoomNameInput));

            var activating = false;

            AJS.$("#activate-hackathon").click(H.h(function(e) {
               if(!activating) {
                  activating = true;

                  var s = AJS.$(e.target);
                  s.attr("aria-disabled", "true");
                  s.attr("disabled", "disabled");

                  var context = activationContext();

                  var request = AJS.$.ajax({
                     url: "/rest/activate-hackathon",
                     type: "POST",
                     cache: false,
                     contentType: "application/json",
                     dataType: "text",
                     data: JSON.stringify(context)
                  });

                  request.done(function() {
                     // Redirect to the dashboard
                     window.top.location.href = Links.dashboard(pageContext, { projectId: pageContext.project.id, projectKey: pageContext.project.key });
                  });

                  request.fail(function() {
                     activating = false;
                     s.removeAttr("aria-disabled");
                     s.removeAttr("disabled");
                     AP.require('messages', function(message) {
                        message.error("Could not activate the hackathon project.", "It may already exist in the system.");
                     });
                  });
               }
            }));
        };

        init();
    });
});
