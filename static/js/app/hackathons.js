define(['../helpers/PageContext', '../helpers/MustacheLoader', '../host/request', 'moment', 'underscore', '../helpers/Links', 'connect/pagetoken'],
    function(PC, MustacheLoader, HostRequest, moment, _, Links) {

    AJS.$(function() {
        var pageContext = PC.load();
        var templates = MustacheLoader.load();

        var formatDate = function (date) {
            var prettyDate = "DD MMM YYYY";
            return moment(date).format(prettyDate);
        };

        var appendHackathonTableRows = function (hackathonTable, hackathons) {
            var rowEntry = hackathonTable.find('.hackathon-rows-container');
            _.each(hackathons, function (hackathon) {
                rowEntry.append(templates.render('hackathon-row', hackathon));
            });
        };

        var requestTenantKey = function() {
              return AJS.$.ajax({
                  url: "/rest/tenant",
                  type: "GET",
                  cache: true
              });
        };

        var populateHackathonsTable = function () {
            var hackathonsRequest = AJS.$.ajax({
                url: '/rest/hackathons'
            });

            AJS.$.when(HostRequest.allProjects(), hackathonsRequest, requestTenantKey()).then(function (jiraProjects, hackathonProjects, tenantData) {
                AJS.$("#no-content-message").addClass("hidden");
                var tenantKey = tenantData[0].key;

                var projects = {};
                _.each(JSON.parse(jiraProjects[0]), function (project){
                    projects[project.id] = project.name;
                });

                var hackathons = _.sortBy(hackathonProjects[0].hackathons, function (hackathon) {
                    return hackathon.startDate;
                });

                var currentHackathons = [];
                var pastHackathons = [];

                _.each(hackathons, function(hackathon) {
                    var hackathonObj = {
                        name: projects[hackathon.projectId],
                        startDate: formatDate(hackathon.startDate),
                        endDate: formatDate(hackathon.endDate),
                        voteLink: hackathon.shortUrl,
                        dashboardLink: Links.dashboard(pageContext, hackathon),
                        summaryLink: Links.summary(pageContext, hackathon)
                    };
                    if (moment(hackathon.endDate).isAfter()) {
                        currentHackathons.push(hackathonObj);
                    } else {
                        pastHackathons.push(hackathonObj);
                    }
                });

                // Past hackathons should be in descending order
                pastHackathons = pastHackathons.reverse();

                if (currentHackathons.length !== 0) {
                    var currentHackathonsDiv = AJS.$('#current-hackathons');
                    currentHackathonsDiv.show();
                    appendHackathonTableRows(currentHackathonsDiv.find('table'), currentHackathons);
                }

                if (pastHackathons.length !== 0) {
                    var pastHackathonsDiv = AJS.$('#past-hackathons');
                    pastHackathonsDiv.show();
                    appendHackathonTableRows(pastHackathonsDiv.find('table'), pastHackathons);
                }

                AJS.$('#new-hackathons').append(templates.render('create-hackathon-help-text', {
                    isEmpty: hackathons.length === 0,
                    baseUrl: pageContext.productBaseUrl
                }));
            });
        };

        populateHackathonsTable();
    });

});
