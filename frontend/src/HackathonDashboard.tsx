import * as React from 'react';
import { RadioGroup, RadioGroupOption } from '@atlaskit/radio';
import { HackathonRestriction, RoundRoomDetails } from './hackathon-client';
import QuestionCircleIcon from '@atlaskit/icon/glyph/question-circle';
import Tooltip from '@atlaskit/tooltip';
import styled from 'styled-components';
import Form, { Field } from '@atlaskit/form';
import { Checkbox } from '@atlaskit/checkbox';
import { HackathonRoomSettings } from './HackathonRoomSettings';

class HackathonSettings extends React.PureComponent {
   private static VotingRestrictionLabel = styled.div`
      display: flex;
      flex-direction: row;
   `;

   private static VotingRestrictionOptions: RadioGroupOption[] = [
      {
         label: (
            <HackathonSettings.VotingRestrictionLabel>
               <span>Login required. Users can not vote on their own team. </span>
               <Tooltip
                  content="Users are required to login to vote. If a user is a member of a team then they
                  cannot vote for that team."
               >
                  <QuestionCircleIcon size="small" label="help" />
               </Tooltip>
            </HackathonSettings.VotingRestrictionLabel>
         ),
         value: HackathonRestriction.LoginAndTeamMember
      },
      {
         label: (
            <HackathonSettings.VotingRestrictionLabel>
               <span>Login required. Users can vote on any team.</span>
               <Tooltip content="Users are required to login to vote. Users can vote on any team.">
                  <QuestionCircleIcon size="small" label="help" />
               </Tooltip>
            </HackathonSettings.VotingRestrictionLabel>
         ),
         value: HackathonRestriction.LoginOnly
      },
      {
         label: (
            <HackathonSettings.VotingRestrictionLabel>
               <span>Login optional. Users can vote on any team.</span>
               <Tooltip
                  content="Users do not need to log in to vote (anonymous users can vote). Users can vote on any team."
               >
                  <QuestionCircleIcon size="small" label="help" />
               </Tooltip>
            </HackathonSettings.VotingRestrictionLabel>
         ),
         value: HackathonRestriction.Anonymous
      }
   ];

   render() {
      return (
         <>
            <h2>Hackathon Settings</h2>
            <p>
               These settings apply to the entire Hackathon. Make sure that you want to apply these changes to all rooms
               and rounds in your Hackathon.
            </p>
            <Form>
               <Field label="Voting restrictions">
                  <RadioGroup
                     options={HackathonSettings.VotingRestrictionOptions}
                  />
               </Field>
               <Field label="Multi-room voting per Round">
                  <Checkbox
                     label={(
                        <HackathonSettings.VotingRestrictionLabel>
                           <span>Allow spectators to vote in more that one room in every round.</span>
                           <Tooltip
                              content="Setting this option to true will not change the number of votes avalable to each
                              spectator per round."
                           >
                              <QuestionCircleIcon size="small" label="help" />
                           </Tooltip>
                        </HackathonSettings.VotingRestrictionLabel>
                     )}
                     id="vr-multiple-room-voting"
                     value="multiple-room-voting"
                     onChange={() => {}}
                     name="checkbox-valid"
                  />
               </Field>
            </Form>
         </>
      );
   }
}

export type HackathonDashboardProps = {
   restriction: HackathonRestriction;
   roundRooms: Array<RoundRoomDetails>;
};

export class HackathonDashboard extends React.PureComponent<HackathonDashboardProps> {
   render() {
      const { roundRooms } = this.props;

      return (
         <div>
            <h2>Hackathon dashboard</h2>
            <HackathonSettings />
            <HackathonRoomSettings roundRooms={roundRooms} />
            <h2>Round Settings</h2>
         </div>
      );
   }
}