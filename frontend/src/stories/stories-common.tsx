import { Story } from '@storybook/react';

export type partialStoriesOf = (name: string) => Story;