-- Adding in short urls to both the hackathon and room tables.
-- We will be automatically generating short urls to both for future consumption.
ALTER TABLE hackathon ADD COLUMN short_url text;
ALTER TABLE room ADD COLUMN short_url text;
