<!DOCTYPE html>
<html lang="en">
    <head profile="http://www.w3.org/2005/10/profile">
        <apply template="headers/requirejs" />

        <link rel="icon" type="image/ico" href="/static/${resourcesVersion}/images/hackathon-favicon.ico" />
        <css href="/static/${resourcesVersion}/css/app.css" />

        <apply template="headers/aui" />
        <apply template="headers/aui-experimental" />

        <js src="/static/${resourcesVersion}/js/app/hackathon-vote.js" />
        <script>require(['app/hackathon-vote']);</script>

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/static/images/apple-touch-icon-144-precomposed.png">

        <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes"/>

        <script id="round-divider" type="x-tmpl-mustache">
            <div class="list-divider" data-round-id="{{id}}" data-round-sequence-number="{{sequenceNumber}}">{{name}}</div>
        </script>

        <script id="teams-divider" type="x-tmpl-mustache">
            <div class="list-divider">Teams</div>
        </script>

        <script id="room-row" type="x-tmpl-mustache">
            <a href="#" class="list-item with-border padded-section hackathon-room" data-room-id="{{room.id}}" data-room-name="{{room.name}}" data-round-id="{{round.id}}">
                <div class="list-item-section details">
                    <span class="list-item-title room-name">{{room.name}}</span>
                </div>
                <div class="list-item-section vote-count">&nbsp;</div>
                <div class="list-item-section next"><span class="glyphicon glyphicon-chevron-right"></span></div>
            </a>
        </script>

        <script id="team-row" type="x-tmpl-mustache">
            <div href="#" class="list-item with-border hackathon-team" data-team-id="{{id}}">
               <div class="cover" style="display: none;"><div class="full"><span>&nbsp;</span></div></div>
               <div class="contents with-border padded-section">
                  <div class="list">
                      <div class="list-item-section details">
                          <span class="list-item-title team-name">{{issueSummary}}</span>
                          <span class="team-members">
                              {{#teamMembers}}
                                  {{displayName}}{{^noComma}}, {{/noComma}}
                              {{/teamMembers}}
                          </span>
                      </div>
                      <div class="list-item-section vote-count">
                          {{#nonZeroVoteCount}}
                          <span>{{votes}}</span>
                          {{/nonZeroVoteCount}}
                          {{^nonZeroVoteCount}}&nbsp;{{/nonZeroVoteCount}}
                      </div>
                      <div class="list-item-section next"><span class="glyphicon glyphicon-chevron-right"></span></div>
                  </div>
               </div>
            </div>
        </script>

        <script id="project-view" type="x-tmpl-mustache">
            <h1 class="team-summary">{{issueSummary}}</h1>
            <div class="info-box">
                <div class="team-details">
                    <h2>Team</h2>
                    {{#teamMembers}}
                    <div class="team-member">
                        <img src="{{smallAvatarUrl}}" /><span>{{displayName}}</span>
                    </div>
                    {{/teamMembers}}
                </div>
            </div>
            <div class="vote-container" data-team-id="{{id}}" data-round-id="{{roundId}}" data-room-id="{{roomId}}">
                <div class="vote-operations">
                    <a class="vote" href="#"><div class="vote-icon"><span class="glyphicon glyphicon-ok"></span></div><span class="message">Vote</span></a>
                    <a class="undo disabled" href="#"><div class="vote-icon"><span class="glyphicon glyphicon-remove"></span></div><span>Undo</span></a>
                </div>
            </div>
            <div class="description hidden">
                <h2>Description</h2>
                <div class='rendered-description'></div>
            </div>
        </script>

        <script id="votes-remaining-message" type="x-tmpl-mustache">
            <span>Votes left in this round</span> <span class="votes-remaining">{{votesRemaining}}</span> <span class="aui-icon aui-icon-wait hidden spinner">Wait</span>
        </script>

        <script id="project-vote-count" type="x-tmpl-mustache">
            {{#votesNonZero}}Voted <span class="count">{{votes}}</span>{{/votesNonZero}}
            {{^votesNonZero}}Vote{{/votesNonZero}}
        </script>

        <title>Hackathon Vote</title>
    </head>
    <body class="body hackathon-vote">
        <header class="title">
            <a id="back-button" class="button left" href="#">
                <span class="visible-button glyphicon glyphicon-arrow-left hidden"></span>
                <span class="invisible-spacer">&nbsp;</span>
            </a>
            <span class="project-title">Hackathon</span>
            <a id="dummy-button" class="button right" href="#">&nbsp;</a>
        </header>
        <section class="content"></section>
        <div id="load-failed" class="aui-message aui-message-error hidden">
            <p class="title">
                <strong>Failed to load the details for this room.</strong>
            </p>
            <p>Please refresh the page and try again, if the problem persists then <a href="/redirect/raise-issue">raise an issue</a>.</p>
        </div>
        <div id="no-rooms" class="aui-message aui-message-warning hidden">
            <p class="title">
                <strong>There are currently no active rooms for this Hackathon.</strong>
            </p>
            <p>Please come back later when voting has begun.</p>
        </div>
        <div id="no-teams" class="aui-message aui-message-warning hidden">
            <p class="title">
                <strong>There are no teams in this room</strong>
            </p>
            <p>
                If you expected there to be teams in this room then please contact your Hackathon administrator.
                The Hackathon administrator needs to place teams into this room in order for them to be present and voteable.
            </p>
        </div>
        <div id="anon-token-load-failed" class="aui-message aui-message-warning hidden">
            <p class="title">
                <strong>Could not login as an anonymous user</strong>
            </p>
            <p>
                Please refresh the page to try again. If the problem persists then contact your Hackathon administrator.
            </p>
        </div>
        <div class="footer">&nbsp;</div>
    </body>
</html>
