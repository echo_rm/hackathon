{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module HostRequests.MyPermissions
   ( getMyPermissions
   , Permissions(..)
   , PermissionDetails(..)
   ) where

import           AesonHelpers
import           Application
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString                   as B
import           Data.Monoid                       (mempty)
import qualified Data.Text                         as T
import           GHC.Generics
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Snaplet                      as SS

data PermissionsContainer = PermissionsContainer
   { pcPermissions :: Permissions
   } deriving (Show, Generic)

instance FromJSON PermissionsContainer where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "pc"
        }

data Permissions = Permissions
   { pUserPicker :: PermissionDetails
   } deriving (Show)

instance FromJSON Permissions where
   parseJSON (Object v) = Permissions
      <$> v .: "USER_PICKER"
   parseJSON _ = mempty

data PermissionDetails = PermissionDetails
   { pdId             :: T.Text
   , pdKey            :: T.Text
   , pdName           :: T.Text
   , pdHavePermission :: Bool
   } deriving (Show, Generic)

instance FromJSON PermissionDetails where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "pd"
        }

getMyPermissions :: AC.Tenant -> AppHandler (Either AC.ProductErrorResponse Permissions)
getMyPermissions tenant = (fmap pcPermissions) <$> (SS.with connect $ AC.hostGetRequest tenant permissionsUrl [] mempty)
   where
      permissionsUrl :: B.ByteString
      permissionsUrl = "/rest/api/2/mypermissions"
