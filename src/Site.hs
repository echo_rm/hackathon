{-# LANGUAGE OverloadedStrings #-}

------------------------------------------------------------------------------
-- | This module is where all the routes and handlers are defined for your
-- site. The 'app' function is the initializer that combines everything
-- together and is exported by this module.
module Site
  ( app
  ) where

------------------------------------------------------------------------------
import           ActivationHandlers
import           AdminHandlers
import           AppConfig
import           Application
import           ConditionHandlers
import           Control.Monad           (unless)
import           Control.Monad.IO.Class  (liftIO)
import           CustomSplices
import           Data.ByteString         (ByteString)
import qualified Data.Connect.Descriptor as CD
import qualified Data.EnvironmentHelpers as DE
import qualified Data.List               as DL
import           Data.Maybe              (fromMaybe)
import qualified Data.Text               as T
import           Data.Text.Encoding      (decodeUtf8)
import qualified DatabaseSnaplet         as DS
import           Descriptor
import           HackathonHandlers
import           HackathonRoomHandlers
import           HackathonRoundHandlers
import           Healthcheck
import           Heartbeat
import qualified Heist.Interpreted       as HI
import           JqlHandlers
import           LeaderboardHandlers
import           LifecycleHandlers
import qualified MicrosZone              as MZ
import           MigrationHandlers
import           PermissionChecks
import           PurgeHandlers
import           RoundRoomHandlers
import           RoundTransitionHandlers
import           SearchRequestViewHandlers
import qualified Snap.AtlassianConnect   as AC
import qualified Snap.Core               as SC
import qualified Snap.Helpers            as SH
import qualified Snap.Snaplet            as SS
import           Snap.Snaplet.Heist
import qualified Snap.Snaplet.Heist      as SSH
import           Snap.Util.FileServe
import           StaticSnaplet
import           StatisticsHandlers
import           TeamHandlers
import           TeamMemberHandlers
import           TenantHandlers
import qualified TenantJWT               as TJ
import           TransitionHandlers
import           VoteHandlers
import           WebhookHandlers
import qualified Data.Map.Syntax        as MS

import qualified Paths_hackathon         as PH

sendHomePage :: SS.Handler b v ()
sendHomePage = SC.redirect' "/docs/home" SH.temporaryRedirect

showDocPage :: SSH.HasHeist b => SS.Handler b v ()
showDocPage = do
   fileName <- SC.getParam "fileparam"
   case fileName of
      Nothing -> SH.respondNotFound
      Just rawFileName -> SSH.heistLocal (environment . decodeUtf8 $ rawFileName) $ SSH.render "docs"
   where
      environment fileName = HI.bindSplices $ "fileName" MS.## HI.textSplice fileName

data ConnectAllJSOption
    = SizeToParent -- ^ It is important to note that sizeToParent does not mean fullscreen. You still have the chrome.
    deriving (Show)

concatOptions :: [ConnectAllJSOption] -> T.Text
concatOptions = DL.foldl' T.append T.empty . fmap optionToText

optionToText :: ConnectAllJSOption -> T.Text
optionToText SizeToParent = "sizeToParent:true"

createConnectPanelWithOptions :: ByteString -> [ConnectAllJSOption] -> AppHandler ()
createConnectPanelWithOptions panelTemplate options = withTokenAndTenant $ \token (tenant, userKey) -> do
  connectData <- AC.getConnect
  SSH.heistLocal (HI.bindSplices $ context connectData tenant token userKey) $ SSH.render panelTemplate
  where
    context connectData tenant token userKey = do
      "productBaseUrl" MS.## HI.textSplice $ T.pack . show . AC.getURI . AC.baseUrl $ tenant
      "connectPageToken" MS.## HI.textSplice $ SH.byteStringToText (AC.encryptPageToken (AC.connectAES connectData) token)
      "userKey" MS.## HI.textSplice $ fromMaybe T.empty userKey
      "pluginKey" MS.## HI.textSplice . getPluginName . CD.pluginKey . AC.connectPlugin $ connectData
      unless (null options) ("connectAllJsOptions" MS.## HI.textSplice (concatOptions options))

createConnectPanel :: ByteString -> AppHandler ()
createConnectPanel path = createConnectPanelWithOptions path []

nonProductPage :: ByteString -> AppHandler ()
nonProductPage = SSH.render

getPluginName :: CD.PluginKey -> T.Text
getPluginName (CD.PluginKey n) = n

withTokenAndTenant :: (AC.PageToken -> AC.TenantWithUser -> AppHandler ()) -> AppHandler ()
withTokenAndTenant processor = TJ.withTenant $ \ct -> do
  token <- liftIO $ AC.generateTokenCurrentTime ct
  processor token ct

------------------------------------------------------------------------------
-- | The application's routes.
routes :: [(ByteString, SS.Handler App App ())]
routes = lifecycleRoutes ++ applicationRoutes ++ connectPanels ++ conditions ++ webhooks ++ redirects

applicationRoutes :: [(ByteString, SS.Handler App App ())]
applicationRoutes =
    [ ("/"                                  , sendHomePage)
    , ("/docs/:fileparam"                   , showDocPage)
    , ("/export/hackathon-statistics.json"  , calculateHackathonStatistics)
    , ("/rest"                              , SH.respondWithError SH.notFound "No such rest resource.")
    , ("/rest/tenant/base-url"              , handleTenantBaseUrlRequest)
    , ("/rest/tenant/anonymous-token"       , handleAnonymousTokenRequest)
    , ("/rest/tenant"                       , handleTenantRequest)
    , ("/rest/activate-hackathon"           , handleHackathonActivation)
    , ("/rest/hackathon/summary"            , handleHackathonSummary)
    , ("/rest/hackathon/restrictions"       , handleHackathonRestrictions)
    , ("/rest/hackathon/votingrules"        , handleHackathonVotingRules)
    , ("/rest/hackathon/round/voteable"     , handleHackathonVoteable)
    , ("/rest/hackathon/room"               , handleHackathonRoom)
    , ("/rest/hackathon/room/current-round" , handleHackathonRoomCurrentRound)
    , ("/rest/hackathon/room/current-round/teams" , handleHackathonRoomTeams)
    , ("/rest/hackathon/round-room/status"  , handleHackathonRoundRoomStatus)
    , ("/rest/hackathon/round-room/update-winners", handleRoundRoomUpdateWinners)
    , ("/rest/hackathon/round-room/update-maximum-teams", handleRoundRoomUpdateMaxTeams)
    , ("/rest/hackathon/rounds"             , handleHackathonRounds)
    , ("/rest/hackathon/round/configuration", handleHackathonRoundConfiguration)
    , ("/rest/hackathon/round/my-votes"     , handleHackathonRoundMyVotes)
    , ("/rest/hackathon/vote"               , handleHackathonVote)
    , ("/rest/hackathon/leaderboard/votes"  , handleLeaderboardVotes)
    , ("/rest/hackathon/leaderboard"        , handleLeaderboard)
    , ("/rest/hackathon/round/transition/calculate", handleTransitionCaculate)
    , ("/rest/hackathon/round/transitions"  , handleRoundTransitions)
    , ("/rest/hackathon/round/transition/options", handleRoundTransitionOptions)
    , ("/rest/hackathon/jql/test"           , handleJqlTest)
    , ("/rest/hackathons"                   , handleHackathons)
    , ("/rest/hackathon"                    , handleHackathon)
    , ("/rest/team/team-member"             , handleTeamMember)
    , ("/rest/team/team-members"            , handleTeamMembers)
    , ("/rest/team/refresh"                 , handleTeamRefresh)
    , ("/rest/team"                         , handleTeamDetails)
    , ("/rest/team/description"             , handleGetTeamDescription)
    , ("/rest/round-room/create"            , handleRoundRoomCreate)
    , ("/rest/round-room/unallocated"       , handleRoundRoomUnallocated)
    , ("/rest/round-room"                   , handleRoundRoomDetails)
    , ("/rest/round-rooms"                  , handleRoundRoomsDetails)
    , ("/rest/round-room/current"           , handleCurrentRoundRoomDetails)
    , ("/rest/statistics/team-sizes"        , handleTeamMemberStats)
    , ("/rest/statistics/round-votes"       , handleRoundVotesStats)
    , ("/rest/statistics/hackathon"         , handleHackathonSummaryStatistics)
    , ("/rest/statistics"                   , getStats)
    , ("/rest/admin/hackathon"              , handleAdminHackathon)
    , ("/rest/migration"                    , migrationRequest)
    , ("/rest/purge"                        , handlePurgeRequest)
    , ("/rest/heartbeat"                    , heartbeatRequest)
    , ("/rest/healthcheck"                  , healthcheckRequest)
    , ("/rest/addon/permission-check"       , handlePermissionsCheck)
    , ("/robots.txt"                        , serveFile "static/files/robots.txt")
    ]

-- These are passed into a separate handler, not the default routes
staticRoutes :: [(ByteString, SS.Handler a StaticConf ())]
staticRoutes =
  [ ("css"        , serveDirectory "static-css")
  , ("images"     , serveDirectory "static/images")
  , ("fonts"      , serveDirectory "static/fonts")
  , ("js"         , serveDirectory "static-js")
  , ("workflow"   , serveDirectory "static/workflow")
  ]

connectPanels :: [(ByteString, SS.Handler App App ())]
connectPanels =
    [ ("/panel/hackathon-dashboard"        , createConnectPanel "hackathon-dashboard")
    , ("/panel/hackathon-summary"          , createConnectPanel "hackathon-summary")
    , ("/panel/hackathon-team-view"        , createConnectPanel "hackathon-team-panel")
    , ("/panel/activate-hackathon-project" , createConnectPanel "activate-hackathon-project")
    , ("/panel/hackathon-round-transition" , createConnectPanel "hackathon-round-transition")
    , ("/panel/hackathon-statistics"       , createConnectPanel "hackathon-statistics")
    , ("/page/hackathons"                  , createConnectPanel "hackathons")
    , ("/page/hackathon-vote"              , nonProductPage "hackathon-vote")
    , ("/page/hackathon-vote-redirect"     , createConnectPanel "hackathon-vote-redirect")
    , ("/page/hackathon-leaderboard"       , nonProductPage "hackathon-leaderboard")
    ]

conditions :: [(ByteString, SS.Handler App App ())]
conditions =
    [ ("/condition/is-hackathon", isHackathonCondition)
    , ("/condition/is-hackathon-team", handleTeamCondition)
    ]

webhooks :: [(ByteString, SS.Handler App App ())]
webhooks =
    [ ("/rest/webhook/issue/create"     , handleIssueCreateWebhook)
    , ("/rest/webhook/issue/update"     , handleIssueUpdateWebhook)
    , ("/rest/webhook/issue/delete"     , handleIssueDeleteWebhook)
    ]

redirects :: [(ByteString, SS.Handler a b ())]
redirects =
    [ ("/redirect/raise-issue", SC.redirect "https://ecosystem.atlassian.net/secure/RapidBoard.jspa?rapidView=192")
    , ("/redirect/install", SC.redirect "https://marketplace.atlassian.com/plugins/com.atlassian.cloud.hackathon")
    , ("/redirect/jira-signup", SC.redirect "https://www.atlassian.com/ondemand/signup/?product=jira-core.ondemand")
    ]

------------------------------------------------------------------------------
-- | The application initializer.
app :: SS.SnapletInit App App
app = SS.makeSnaplet "hackathon" "Hackathon Atlassian Cloud Add-on." Nothing $ do
    liftIO . putStrLn $ "## Starting Init Phase"
    zone <- liftIO MZ.fromEnv
    liftIO . putStrLn $ "## Zone: " ++ DE.showMaybe zone
    SS.addRoutes routes
    appHeist <- SS.nestSnaplet "" heist $ heistInit "templates"
    SSH.addConfig appHeist spliceConfig
    appDb <- SS.nestSnaplet "db" db (DS.dbInitConf zone)
    let modifiedDescriptor = MZ.modifyDescriptorUsingZone zone atlassianConnectDescriptor
    appConnect <- SS.nestSnaplet "connect" connect $ AC.initConnectSnaplet modifiedDescriptor
    appConf <- SS.nestSnaplet "app-config" appconf initAppConfOrFail
    appStatic <- SS.nestSnaplet "static" static (initStaticSnaplet PH.version staticRoutes appHeist)
    return $ App appHeist appDb appConnect appConf appStatic
