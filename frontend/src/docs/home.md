## Hackathon for Cloud

TL;DR: Hackathon is awesome, to get started just visit our [Getting started][1] page.

Make running your Hackathon easy with the efficient Hackathon add-on for Atlassian Cloud. All you need to do is get yourself
an Atlassian JIRA Cloud instance and install the Hackathon add-on and you are ready to go.

The Hackathon add-on for Atlassian Cloud is written by Atlassian developers and used in the Atlassian ShipIt event that is
run quarterly by the entire company! It allows the ShipIt event to scale far beyond its humble beginnings and it can give
you the same advantages too.

The Hackathon add-on has all of the features that you need to run a successful event. 

 - Gives teams (or individuals) the ability to register their projects.
 - Allows all viewers to vote on the participating teams.
 - Allows you as the admin to easily co-ordinate hackathons of any size. From a small Hackathons that fit in one room to
   fully global Hackathons that occur all over the world and have semis and finals.
   
And best of all, the Hackathon add-on for Cloud is completely free. So what are you waiting for?
[Create your hackathon][1] now.

### Screenshots

#### Creating a hackathon team

<img src="/static/images/docs/creating-team.gif" />

 [1]: /docs/setup
