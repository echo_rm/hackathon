{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module PermissionChecks
  ( handlePermissionsCheck
  ) where

import           AesonHelpers
import           Application
import qualified Control.Arrow                 as A
import           Control.Monad.Trans.Class     (lift)
import           Control.Monad.Trans.Either
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.Connect.Descriptor       as CD
import           Data.Either                   (isRight)
import qualified Data.Text                     as T
import           GHC.Generics
import           HandlerHelpers
import           HostRequests.MyPermissions
import qualified HostRequests.UserDetails      as HU
import qualified Snap.AtlassianConnect         as AC
import qualified Snap.Core                     as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import qualified WithToken                     as WT

handlePermissionsCheck :: AppHandler ()
handlePermissionsCheck = handleMethods
   [ (SC.GET, WT.tenantFromToken hasRequiredPermissions)
   ]

data PermissionsResponse = PermissionsResponse
   { prHasBrowseUsers  :: Bool
   , prHasProjectAdmin :: Bool
   } deriving (Show, Generic)

instance ToJSON PermissionsResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "pr"
        }

hasRequiredPermissions :: AC.TenantWithUser -> AppHandler ()
hasRequiredPermissions (tenant, _) = do
   connectData <- AC.getConnect
   writeError . withPG . runEitherT $ do
      potentialHackathon <- lift $ getHackathonFromProjectId tenant
      permissions <- EitherT (A.left queryMyPermissionsFail <$> getMyPermissions tenant)
      let hasBrowseUsers = pdHavePermission . pUserPicker $ permissions
      case potentialHackathon of
         Left _ -> lift . writeJson $ PermissionsResponse hasBrowseUsers True
         Right hackathon -> do
            hasProjectAdmin <- lift $ requirePermissionsE [HU.ProjectAdmin] tenant (addonUsername connectData) hackathon
            lift . writeJson $ PermissionsResponse hasBrowseUsers (isRight hasProjectAdmin)
   where
      addonUsername :: AC.Connect -> T.Text
      addonUsername connectData = "addon_" `T.append` (getPluginName . CD.pluginKey . AC.connectPlugin $ connectData)

      getPluginName :: CD.PluginKey -> T.Text
      getPluginName (CD.PluginKey n) = n

      queryMyPermissionsFail _ = (internalServer, "Could not query JIRA for the permissions of the add-on user")
