{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Persistence.HackathonRound
    ( HackathonRound(..)
    , getHackathonRoundById
    , createRounds
    , createRound
    , getFirstRoundForHackathon
    , getNextRound
    , getOutvotedRound
    , getRoundBySequenceNumber
    , getRoundInProject
    , getRoundInHackathon
    , getRoundsInHackathon
    , getVoteableRoundsInHackathon
    , updateVotesPerUser
    , updateVotesUserTeam
    , updateTransitionOptions
    , defaultTransitionTied
    , defaultMatchUnallocated
    ) where

import           Application
import           Control.Monad                        (join)
import qualified Data.HackathonConstants              as HC
import           Data.Int                             (Int64)
import           Data.Maybe                           (fromMaybe, listToMaybe)
import qualified Data.Text                            as T
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.SqlQQ
import qualified Persistence.Hackathon                as PH
import qualified Persistence.HackathonIds             as HI
import qualified Snap.AtlassianConnect                as AC
import           Snap.Snaplet.PostgresqlSimple

data HackathonRound = HackathonRound
    { hroundId               :: HI.HackathonRoundId
    , hroundHackathonId      :: HI.HackathonId
    , hroundStatusId         :: HI.AtlassianStatusId
    , hroundName             :: T.Text
    , hroundType             :: HC.HackathonRoundType
    , hroundSequenceNumber   :: Maybe Integer -- If nothing then this round is not voteable
    , hroundVotesPerUser     :: Integer
    , hroundVotesUserTeam    :: Integer -- The number of votes per user, per team, per round
    , hroundTransitionTied   :: Bool
    , hroundMatchUnallocated :: Bool
    } deriving (Show)

defaultTransitionTied :: Bool
defaultTransitionTied = True

defaultMatchUnallocated :: Bool
defaultMatchUnallocated = False

instance FromRow HackathonRound where
    fromRow = HackathonRound <$> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field

getHackathonRoundById :: PH.Hackathon -> HI.HackathonRoundId -> AppHandler (Maybe HackathonRound)
getHackathonRoundById hackathon hId = listToMaybe <$> query
    [sql|
        SELECT r.id, r.hackathon_id, r.workflow_status_id, r.name, r.type, r.sequence_number, r.votes_per_user, r.votes_user_team, r.transition_tied_teams, r.match_unallocated_teams
        FROM round r
        WHERE r.hackathon_id = ? AND r.id = ?
    |] (PH.hackathonId hackathon, hId)

getOutvotedRound :: PH.Hackathon -> AppHandler (Maybe HackathonRound)
getOutvotedRound hackathon = listToMaybe <$> query
   [sql|
        SELECT r.id, r.hackathon_id, r.workflow_status_id, r.name, r.type, r.sequence_number, r.votes_per_user, r.votes_user_team, r.transition_tied_teams, r.match_unallocated_teams
        FROM round r
        WHERE r.hackathon_id = ? AND r.type = ?
   |] (PH.hackathonId hackathon, HC.Outvoted)

createRounds :: [HackathonRound] -> AppHandler [Maybe HI.HackathonRoundId]
createRounds = sequence . fmap createRound

createRound :: HackathonRound -> AppHandler (Maybe HI.HackathonRoundId)
createRound hr = do
    hid <- query
        [sql|
          INSERT INTO round (hackathon_id, workflow_status_id, name, type, sequence_number, votes_per_user, votes_user_team, transition_tied_teams, match_unallocated_teams)
          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id
        |]
        ( hroundHackathonId hr
        , hroundStatusId hr
        , hroundName hr
        , hroundType hr
        , hroundSequenceNumber hr
        , hroundVotesPerUser hr
        , hroundVotesUserTeam hr
        , hroundTransitionTied hr
        , hroundMatchUnallocated hr
        )
    return . listToMaybe $ join hid

getNextRound :: HackathonRound -> AppHandler (Maybe HackathonRound)
getNextRound hRound
    | hroundType hRound `elem` [HC.Outvoted, HC.Winner] = return Nothing
    | otherwise = listToMaybe <$> query
        [sql|
            SELECT r.id, r.hackathon_id, r.workflow_status_id, r.name, r.type, r.sequence_number, r.votes_per_user, r.votes_user_team, r.transition_tied_teams, r.match_unallocated_teams
            FROM round r
            WHERE r.hackathon_id = ?
            AND r.sequence_number > ?
            ORDER BY r.sequence_number ASC
            LIMIT 1
        |] (hroundHackathonId hRound, fromMaybe 0 (hroundSequenceNumber hRound))

getFirstRoundForHackathon :: PH.Hackathon -> AppHandler (Maybe HackathonRound)
getFirstRoundForHackathon hackathon = getRoundBySequenceNumber hackathon 1

getRoundBySequenceNumber :: PH.Hackathon -> Integer -> AppHandler (Maybe HackathonRound)
getRoundBySequenceNumber hackathon sequenceNumber = listToMaybe <$> query
    [sql|
        SELECT r.id, r.hackathon_id, r.workflow_status_id, r.name, r.type, r.sequence_number, r.votes_per_user, r.votes_user_team, r.transition_tied_teams, r.match_unallocated_teams
        FROM round r
        WHERE r.hackathon_id = ?
        AND r.sequence_number = ?
    |] (PH.hackathonId hackathon, sequenceNumber)

getRoundInProject :: AC.Tenant -> AC.ProjectId -> HI.AtlassianStatusId -> AppHandler (Maybe HackathonRound)
getRoundInProject tenant projectId statusId = do
    rounds <- query
        [sql|
            SELECT r.id, r.hackathon_id, r.workflow_status_id, r.name, r.type, r.sequence_number, r.votes_per_user, r.votes_user_team, r.transition_tied_teams, r.match_unallocated_teams
            FROM round r, hackathon h
            WHERE r.hackathon_id = h.id
            AND r.workflow_status_id = ?
            AND h.project_id = ?
            AND h.tenant_id = ?
        |] (statusId, projectId, AC.tenantId tenant)
    return . listToMaybe $ rounds

getRoundInHackathon :: PH.Hackathon -> HI.AtlassianStatusId -> AppHandler (Maybe HackathonRound)
getRoundInHackathon hackathon statusId = listToMaybe <$> query
   [sql|
      SELECT r.id, r.hackathon_id, r.workflow_status_id, r.name, r.type, r.sequence_number, r.votes_per_user, r.votes_user_team, r.transition_tied_teams, r.match_unallocated_teams
      FROM round r
      WHERE r.hackathon_id = ?
      AND r.workflow_status_id = ?
   |] (PH.hackathonId hackathon, statusId)

getRoundsInHackathon :: PH.Hackathon -> AppHandler [HackathonRound]
getRoundsInHackathon hackathon = query
    [sql|
        SELECT r.id, r.hackathon_id, r.workflow_status_id, r.name, r.type, r.sequence_number, r.votes_per_user, r.votes_user_team, r.transition_tied_teams, r.match_unallocated_teams
        FROM round r
        WHERE r.hackathon_id = ?
    |] (Only . PH.hackathonId $ hackathon)

getVoteableRoundsInHackathon :: PH.Hackathon -> AppHandler [HackathonRound]
getVoteableRoundsInHackathon hackathon = query
    [sql|
        SELECT r.id, r.hackathon_id, r.workflow_status_id, r.name, r.type, r.sequence_number, r.votes_per_user, r.votes_user_team, r.transition_tied_teams, r.match_unallocated_teams
        FROM round r
        WHERE r.hackathon_id = ?
        AND r.sequence_number IS NOT NULL
    |] (Only . PH.hackathonId $ hackathon)

updateVotesPerUser :: PH.Hackathon -> [HI.HackathonRoundId] -> Integer -> AppHandler Int64
updateVotesPerUser hackathon roundIds votesPerUser = execute
    [sql|
        UPDATE round
        SET votes_per_user = ?
        FROM hackathon h
        WHERE round.hackathon_id = h.id
        AND h.id = ?
        AND round.id in ?
    |] (votesPerUser, PH.hackathonId hackathon, In roundIds)

updateVotesUserTeam :: PH.Hackathon -> [HI.HackathonRoundId] -> Integer -> AppHandler Int64
updateVotesUserTeam hackathon roundIds votesUserTeam = execute
    [sql|
        UPDATE round
        SET votes_user_team = ?
        FROM hackathon h
        WHERE round.hackathon_id = h.id
        AND h.id = ?
        AND round.id in ?
    |] (votesUserTeam, PH.hackathonId hackathon, In roundIds)

updateTransitionOptions :: HackathonRound -> Bool -> Bool -> AppHandler Int64
updateTransitionOptions hRound tied matchUnallocated = execute
   [sql|
   UPDATE round
   SET transition_tied_teams = ?, match_unallocated_teams = ?
   WHERE id = ?
   |] (tied, matchUnallocated, hroundId hRound)
