import * as React from 'react';
import { partialStoriesOf } from './stories-common';
import { HackathonSummaryStats } from '../hackathon-client';
import { HackathonSummaryExample } from './HackathonSummaryData';
import { HackathonSummary } from '../HackathonSummary';
import { action } from '@storybook/addon-actions';
import { PageContextExample } from './PageContextData';
import { IssueDetails } from 'src/HostRequest';

export function stories(storyCreator: partialStoriesOf) {
   storyCreator('Summary')
    .add('Hackathon summary view - no teams', () => {
      const stats: HackathonSummaryStats = {
        competitors: 1325,
        formingTeams: 18
      };

      return (
        <HackathonSummary
          pc={PageContextExample}
          summary={HackathonSummaryExample}
          stats={stats}
          myIssues={[]}
          onCreateTeam={action('Clicked Create team')}
        />
      );
    })
    .add('Hackathon summary view - one team', () => {
      const stats: HackathonSummaryStats = {
        competitors: 1325,
        formingTeams: 18
      };

      // tslint:disable
      const myTeams: IssueDetails[] = [
        {
          "key" : "SHPXLIV-424",
          "id" : "48023",
          "fields" : {
             "summary" : "Accessibility improvements to the Jira mobile apps",
             "status" : {
                "name" : "Trashed",
                "statusCategory" : {
                  "colorName" : "green",
                  "name" : "Done"
               }
             }
          }
       }
      ];
      // tslint:enable

      return (
        <HackathonSummary
          pc={PageContextExample}
          summary={HackathonSummaryExample}
          stats={stats}
          myIssues={myTeams}
          onCreateTeam={action('Clicked Create team')}
        />
      );
    })
    .add('Hackathon summary view - multiple teams', () => {
      const stats: HackathonSummaryStats = {
        competitors: 1325,
        formingTeams: 18
      };

      // tslint:disable
      const myTeams: IssueDetails[] = [
         {
            "key" : "SHPXLIV-424",
            "id" : "48023",
            "fields" : {
               "summary" : "Accessibility improvements to the Jira mobile apps",
               "status" : {
                  "name" : "Trashed",
                  "statusCategory" : {
                     "colorName" : "green",
                     "name" : "Done"
                  }
               }
            }
         },
         {
            "key" : "SHPXLIV-424",
            "id" : "12345",
            "fields" : {
               "summary" : "Team two",
               "status" : {
                  "name" : "In progress",
                  "statusCategory" : {
                     "colorName" : "green",
                     "name" : "Done"
                  }
               }
            }
         }
      ];
      // tslint:enable

      return (
        <HackathonSummary
          pc={PageContextExample}
          summary={HackathonSummaryExample}
          stats={stats}
          myIssues={myTeams}
          onCreateTeam={action('Clicked Create team')}
        />
      );
    });
}