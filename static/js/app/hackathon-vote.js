define([
   '../lib/URI', 
   '../lib/js.cookie', 
   'moment', 
   'underscore', 
   '../helpers/MustacheLoader', 
   '../helpers/Status',
   '../helpers/Handler',
   '../helpers/Restrictions',
   '../lib/hammer',
   '../helpers/PreventGhostClick'
],
function(URI, Cookie, moment, _, MustacheLoader, Status, H, Restrictions, Hammer) {
    "use strict";

    var screen = {
        roomsView: 0,
        teamsView: 1,
        projectView: 2
    };

    var pageState = (function() {
        var token;
        var currentRoom;
        var currentRound;
        var currentScreen;
        var lastVotesPlaced;
        var lastTeamsRequest;
        var lastRoomName;
        var lastRoundConfiguration;

        return {
            getToken: function () { return token; },
            setToken: function(newTok) { token = newTok; },
            getCurrentRoom: function() { return currentRoom; },
            setCurrentRoom: function(newRoomId) { return currentRoom = newRoomId; },
            getCurrentRound: function() { return currentRound; },
            setCurrentRound: function(newRoundId) { currentRound = newRoundId; },
            getCurrentScreen: function() { return currentScreen; },
            setCurrentScreen: function(newScreenId) { currentScreen = newScreenId; },
            getLastVotesPlaced: function() { return lastVotesPlaced; },
            setLastVotesPlaced: function(votesData) { lastVotesPlaced = votesData; },
            getLastTeamsRequest: function() {return lastTeamsRequest;},
            setLastTeamsRequest: function(teamsData) { lastTeamsRequest = teamsData; },
            getLastRoomName: function() { return lastRoomName; },
            setLastRoomName: function(newRoomName) { lastRoomName = newRoomName; },
            getLastRoundConfiguration: function() { return lastRoundConfiguration; },
            setLastRoundConfiguration: function(roundConfig) { lastRoundConfiguration = roundConfig; }
        }
    }());

    pageState.setCurrentScreen(screen.roomsView);

    var templates;
    AJS.$(function() {
        templates = MustacheLoader.load();
    });

    // get the tenant key and project id from the url
    var queryParams = URI(window.location.href).query(true);
    var pageContext = {
        projectId: parseInt(queryParams['project_id']),
        tenantKey: queryParams['tenant_key'],
        initialRoom: parseInt(queryParams['initial_room_id']),
        acptToken: queryParams['acpt']
    };

    delete queryParams['acpt'];
    var newUri = URI(window.location.href).query(queryParams);
    window.history.pushState('',"Hackathon Vote",newUri);

    var getRestrictions = function(pageContext) {
        return AJS.$.ajax({
            url: "/rest/hackathon/restrictions",
            type: "GET",
            cache: "false",
            data: {
                projectId: pageContext.projectId,
                tenantKey: pageContext.tenantKey
            }
        });
    };

    var anonymousCookiePrefix = "anonymous";
    var jwtCookie = "acpt";

    var getTenantBaseUrl = function (tenantKey) {
        return AJS.$.ajax({
            url: "/rest/tenant/base-url",
            type: "GET",
            cache: true, // This will never change
            data: {
                tenantKey: tenantKey
            }
        });
    };

    var getDescriptor = function() {
        return AJS.$.ajax({
            url: "/connect/atlassian-connect.json",
            type: "GET",
            cache: false
        });
    };

    var getAnonymousUserToken = function(tenantKey) {
        return AJS.$.ajax({
            url: "/rest/tenant/anonymous-token",
            type: "GET",
            cache: true,
            data: {
                tenantKey: tenantKey
            }
        });
    };

    var requestCurrentState = function(voteableUser, projectId) {
        return AJS.$.ajax({
            url: "/rest/round-room/current",
            type: "POST",
            cache: false,
            data: JSON.stringify({
                token: voteableUser,
                projectId: projectId
            })
        });
    };

    var requestHackathonDetails = function(voteableUser, projectId) {
        return AJS.$.ajax({
            url: "/rest/hackathon",
            type: "POST",
            cache: false,
            data: JSON.stringify({
                token: voteableUser,
                projectId: projectId
            })
        });
    };

    var requestTeamsForRoom = function(voteableUser, projectId, roomId) {
        return AJS.$.ajax({
            url: "/rest/hackathon/room/current-round/teams",
            type: "POST",
            cache: false,
            data: JSON.stringify({
                hackathon: {
                    token: voteableUser,
                    projectId: projectId
                },
                roomId: roomId
            })
        });
    };

    var requestDescriptionForTeam = function(voteableUser, projectId, issueId) {
        return AJS.$.ajax({
            url: "/rest/team/description",
            type: 'POST',
            cache: false,
            data: JSON.stringify({
                hackathon: {
                    token: voteableUser,
                    projectId: projectId
                },
                issueId: issueId
            })
        });
    };

    var requestVotesForUser = function(voteableUser, projectId, roundId) {
        var request = AJS.$.ajax({
            url: "/rest/hackathon/round/my-votes",
            type: "POST",
            cache: false,
            data: JSON.stringify({
                hackathon: {
                    token: voteableUser,
                    projectId: projectId
                },
                roundId: roundId
            })
        });

        request.done(function(data) {
            pageState.setLastVotesPlaced(data);
        });

        return request;
    };

    var requestRoundConfiguration = function(voteableUser, projectId, roundId) {
        var request = AJS.$.ajax({
            url: "/rest/hackathon/round/configuration",
            type: "POST",
            cache: false,
            data: JSON.stringify({
                hackathon: {
                    token: voteableUser,
                    projectId: projectId
                },
                roundId: roundId
            })
        });

        request.done(function(data) {
            pageState.setLastRoundConfiguration(data);
        });

        return request;
    };

    var redirectToLoginFlow = function(tenantKey, projectId) {
        AJS.log("Login is required but this user has no token. Redirecting through login flow to get a token.");
        AJS.$.when(getTenantBaseUrl(tenantKey), getDescriptor()).done(function(tenantRequest, descriptorRequest) {
            var baseUrl = tenantRequest[0].baseUrl;
            var pluginKey = descriptorRequest[0].key;

            // Note: I have tested that this works by hitting:
            // http://shipit.atlassian.net/login.jsp?os_destination=/browse/PIWIV-1
            // However, currently this is obviously a bit hacky and not well supported. However, it should continue to work.
            // For now.
            var redirectUrl = "/plugins/servlet/ac/" + pluginKey + "/hackathon-vote-redirect?project.id=" + projectId;
            window.location.href = baseUrl + "/login.jsp?os_destination=" + encodeURIComponent(redirectUrl);
        });
    };

    var handleRestriction = function(restriction) {
        if(Restrictions.loginRequired(restriction)) {
            AJS.log("Login required, following the login flow.");
            var acptToken = pageContext.acptToken || Cookie.get(jwtCookie);
            if(acptToken) {
                AJS.log("This user has an acpt cookie. Continuing to load content");
                processWithToken({ acptToken: acptToken });
            } else {
                redirectToLoginFlow(pageContext.tenantKey, pageContext.projectId);
            }
        } else {
            AJS.log("Anonymous voting on this hackathon. Following the anonymous voting flow.");
            var cookieName = anonymousCookiePrefix + "-" + pageContext.tenantKey + "-" + pageContext.projectId;
            var anonCookie = Cookie.get(cookieName);
            if(anonCookie) {
                AJS.log("This user already had an anonymous voting token. Continuing to load content.");
                processWithToken({
                    tenantKey: pageContext.tenantKey,
                    anonToken: anonCookie
                });
            } else {
                AJS.log("This user has no anonymous token, generating a new anonymous token now.");

                var request = getAnonymousUserToken(pageContext.tenantKey);

                request.done(function(data) {
                    Cookie.set(cookieName, data.token, {
                        expires: moment().add(3, 'years').toDate(),
                        path: '/page'
                    });
                    processWithToken({
                        tenantKey: pageContext.tenantKey,
                        anonToken: data.token
                    });
                });

                request.fail(function() {
                    AJS.$("#anon-token-load-failed").removeClass("hidden");
                });
            }
        }
    };

    var processWithToken = function(token) {
        pageState.setToken(token);
        pageState.setCurrentScreen(screen.roomsView);
        loadHackathonTitle();
        loadRoomsView(true);
    };

    var loadHackathonTitle = function() {
        var token = pageState.getToken();

        var hackathonRequest = requestHackathonDetails(token, pageContext.projectId);

        hackathonRequest.done(function(hackathon) {
            AJS.$(".hackathon-vote .title .project-title").text(hackathon.projectName);
        });

        hackathonRequest.fail(function() {
            location.reload();
        });
    };

    var roomIdIs = function(rid) {
       return function(roundRoom) { return rid === roundRoom.room.id; };
    };

    var loadRoomsView = function(isInitialLoad) {
        var token = pageState.getToken();
        var currentStateRequest = requestCurrentState(token, pageContext.projectId);

        currentStateRequest.done(function(data) {
            var activeRooms = _.filter(data.roundRooms, function(roundRoom) { return roundRoom.status === "Active"; });
            AJS.$("#no-rooms").toggleClass("hidden", activeRooms.length > 0);
            if(activeRooms.length > 0) {
                var groupedRoundRooms = _.groupBy(activeRooms, function(roundRoom) {
                    return roundRoom.round.sequenceNumber;
                });

                var parsedRoundSequences = _.map(_.keys(groupedRoundRooms), function(i) { return parseInt(i); });
                var sortedRoundSequences = parsedRoundSequences.sort(function(a, b) {return b - a;});

                var loaded = false;

                if(isInitialLoad) {
                   var initialRoom = _.find(activeRooms, roomIdIs(pageContext.initialRoom));
                   if(initialRoom) {
                      selectRoom(initialRoom.round.id, initialRoom.room.id, initialRoom.room.name);
                      loaded = true;
                   }
                }

                if(!loaded) {
                   var contentBox = getContentBox();
                   contentBox.empty();

                   _.each(sortedRoundSequences, function(sequenceNumber) {
                       var roundRooms = groupedRoundRooms[sequenceNumber + ""];
                       contentBox.append(templates.render("round-divider", roundRooms[0].round));
                       var roomList = AJS.$(document.createElement('div'));
                       roomList.addClass("list");
                       _.each(roundRooms, function(roundRoom) {
                           roomList.append(templates.render("room-row", roundRoom));
                       });
                       contentBox.append(roomList);
                   });
                }
            }
        });

        currentStateRequest.fail(function() {
            location.reload();
        });
    };

     var selectRoom = function(roundId, roomId, roomName) {
         pageState.setCurrentRound(roundId);
         pageState.setCurrentRoom(roomId);
         pageState.setLastRoomName(roomName);

         requestRoundConfiguration(pageState.getToken(), pageContext.projectId, pageState.getCurrentRound()).done(function() {
             loadTeamsForRoom();
         });
     };

     var loadTeamsForRoom = function() {
         var processTeamsState = function(data) {
             // When they come back then set the currentroom to this room
             pageState.setLastTeamsRequest(data);
             pageState.setCurrentScreen(screen.teamsView);
             // activate the back button and setup what it points back to
             showBackButton(true);

             // change the title to reflect the rooms name
             AJS.$(".hackathon-vote .title .project-title").text(pageState.getLastRoomName());

             if(data.teams.length > 0) {
                 AJS.$("#no-teams").addClass("hidden");

                 var teamList = AJS.$(document.createElement('div'));
                 teamList.addClass("list");
                 _.each(data.teams, function(team) {
                     team.teamMembers[team.teamMembers.length-1].noComma = true;
                     team.nonZeroVoteCount = team.votes > 0;
                     teamList.append(templates.render('team-row', team));
                 });
                 getContentBox().empty().append(templates.render('teams-divider')).append(teamList);

                 var maxDelta = 48;
                 var yBound = 60;
                 var selectedAlpha = 0.9;
                 var green = function(alpha) { return "rgba(20, 137, 44, " + alpha + ")" };
                 var yellow = function(alpha) { return "rgba(246, 195, 66, " + alpha + ")" };

                 getContentBox().find(".hackathon-team").each(function(index, element) {
                    var hammer = new Hammer(element);
                    var team = AJS.$(element);
                    var cover = team.find('.cover');
                    var coverFull = team.find('.full');
                    var textInner = coverFull.find('span');

                    PreventGhostClick(element);

                    hammer.get('pan').set({ direction: Hammer.DIRECTION_HORIZONTAL });

                    // This makes sure that people with fat fingers (like me) can tap safely
                    hammer.get('tap').set({
                       threshold: 4
                    });

                    hammer.on('tap', function(ev) {
                       onTeamTap(element);
                    });

                    hammer.on('pan', function(ev) {
                       var isVote = ev.deltaX > 0;
                       var limitCrossed = Math.abs(ev.deltaX) > maxDelta;
                       var outsideCover = Math.abs(ev.deltaY) > yBound;

                       var interp = Math.min(maxDelta, Math.abs(ev.deltaX)) / maxDelta;
                       var alpha = 0.2 + 0.3 * interp;

                       var offset = 0;
                       var width = offset + (30 - offset) * interp;

                       cover.css('width', (width > 5 ? width : 0) + '%');
                       textInner.toggleClass('glyphicon', limitCrossed);
                       textInner.toggleClass('glyphicon-ok', isVote && limitCrossed);
                       textInner.toggleClass('glyphicon-remove', !isVote && limitCrossed);
                       cover.toggleClass('vote', isVote && limitCrossed);
                       cover.toggleClass('undo', !isVote && limitCrossed);
                       cover.css('left', isVote ? '0px' : '');
                       cover.css('right', !isVote ? '0px' : '');
                       var colorFunc = isVote ? green : yellow;
                       coverFull.css('background-color', colorFunc(limitCrossed ? 1 : selectedAlpha));
                       
                       cover.toggle(!outsideCover);
                    });

                    hammer.on('panend', function(ev) {
                       var isVote = ev.deltaX > 0;
                       var limitCrossed = Math.abs(ev.deltaX) > maxDelta;
                       var outsideCover = Math.abs(ev.deltaY) > yBound;

                       if(limitCrossed && !outsideCover) {
                          var action = isVote ? voteOnTeam : undoOnTeam;

                          action(team.data('team-id'), pageState.getCurrentRound(), pageState.getCurrentRoom(), function() {
                             loadTeamsForRoom();
                          });

                          cover.css('width', '');
                          coverFull.css('background-color', '');
                          cover.hide();
                       } else {
                          if(outsideCover) {
                             cover.hide();
                          } else {
                             cover.animate({width:'toggle'}, 100);
                          }
                       }
                    });
                 });
             } else {
                 getContentBox().empty();
                 AJS.$("#no-teams").removeClass("hidden");
             }

             footerStatus.show();
         };

         var teamsRequest = requestTeamsForRoom(pageState.getToken(), pageContext.projectId, pageState.getCurrentRoom());
         var votesRequest = requestVotesForUser(pageState.getToken(), pageContext.projectId, pageState.getCurrentRound());
         AJS.$.when(teamsRequest, votesRequest).done(function(tr, vr) {
             var teamData = tr[0];
             var myVoteData = vr[0];

             // Clear out all of the vote information on the teams
             teamData.teams = _.map(teamData.teams, function(team) {
                 var foundVote = _.find(myVoteData.teamVotes, function(teamVote) { return team.id === teamVote.teamId; });
                 team.votes = foundVote ? foundVote.count : 0;
                 return team;
             });

             processTeamsState(teamData);
         });
     };

    // if any of the page context details are missing then show an error
    if(!pageContext.projectId || !pageContext.tenantKey) {
        // Throw an error when the page finishes loading
        AJS.log("You require both a project id and tenant key to do anything on this page.");
        showError("Url incorrect. Contact administrator.");
    } else {
        // query for the voting restriction on this hackathon
        var restrictionsRequest = getRestrictions(pageContext);

        restrictionsRequest.fail(function() {
            AJS.$("#load-failed").removeClass("hidden");
        });

        restrictionsRequest.done(function(data) {
            // get the restriction and then act upon it.
            AJS.log("This hackathon has the restriction: " + data.restriction);
            handleRestriction(data.restriction);
        });
    }

    var getBackButton = function() {
       return AJS.$("#back-button");
    };

    var showBackButton = function(show) {
       var backButton = getBackButton();
       backButton.find('.visible-button').toggleClass("hidden", !show);
       backButton.find('.invisible-spacer').toggleClass("hidden", !!show);
    };

    var getContentBox = function() {
       return AJS.$(".hackathon-vote .content");
    };

    var loadVotesRemaining = function() {
      var votesRemaining = 0;

      var roundData = pageState.getLastRoundConfiguration();
      if(roundData) {
          var totalVotesPlaced = 0;
          var votesData = pageState.getLastVotesPlaced();
          if(votesData) {
              var counts = _.map(votesData.teamVotes, function(teamVote) { return teamVote.count; });
              var add = function(a, b) { return a + b; };
              totalVotesPlaced = _.reduce(counts, add, 0);
          }

          votesRemaining = roundData.votesPerUser - totalVotesPlaced;
      }

      return {
          message: templates.render("votes-remaining-message", {
              votesRemaining: votesRemaining
          })
      };
    };

    var footerStatus;

    var initFooterStatus = function() {
      footerStatus = Status(AJS.$(".hackathon-vote .footer"), {
         emptyMessageLoader: loadVotesRemaining
      });
      footerStatus.hide();
    };

    var showError = function(message) {
       if(footerStatus) {
         footerStatus.showMessage(message, {
             extraClasses: "error"
         });
       }
    };

    var showWarning = function(message) {
       if(footerStatus) {
         footerStatus.showMessage(message, {
             extraClasses: "warning"
         });
       }
    };

    var onTeamTap = function(e) {
      var self = AJS.$(e).closest('.hackathon-team');

      var teamId = self.data('team-id');
      var teamData = _.find(pageState.getLastTeamsRequest().teams, function(team) {
          return team.id == teamId;
      });

      getContentBox().empty().append(templates.render('project-view', teamData));
      refreshVoteOperations(teamId);
      pageState.setCurrentScreen(screen.projectView);

      // Load the description
      requestDescriptionForTeam(pageState.getToken(), pageContext.projectId, teamData.issueId).done(function(resp) {
        var description = getContentBox().find(".description");
        description.removeClass("hidden");
        description.find('.rendered-description').html(resp.renderedDescription);
      });
    };

    var isTeamWithId = function(teamId) {
      return function(teamVote) { return teamVote.teamId == teamId; };
    };

    var refreshVoteOperations = function(teamId) {
      var votes = _.find(pageState.getLastVotesPlaced().teamVotes, isTeamWithId(teamId));
      
      // Handle the case where the team has no votes
      if(!votes) {
          votes = {count: 0};
      }

      var context = {
          votes: votes.count,
          votesNonZero: votes.count > 0
      };

      var contentBox = getContentBox();
      contentBox.find(".vote-operations .vote").toggleClass("has-votes", context.votesNonZero);
      contentBox.find(".vote-operations .undo").toggleClass("disabled", !context.votesNonZero);
      contentBox.find(".vote-container .vote .message").html(templates.render('project-vote-count', context));
    };

    var voteOnTeam = function(teamId, roundId, roomId, callback) {
      var context = {
          hackathon: {
              token: pageState.getToken(),
              projectId: pageContext.projectId
          },
          teamId: teamId,
          roundId: roundId,
          roomId: roomId
      };

      var voteRequest = AJS.$.ajax({
          url: "/rest/hackathon/vote",
          type: "POST",
          cache: false,
          data: JSON.stringify(context)
      });

      voteRequest.done(function(data) {
          switch(data.status) {
              case "VoteSuccessful":
                  footerStatus.showMessage("Vote placed!", {
                      extraClasses: "success"
                  });
                  break;
              case "CannotVoteOnYourOwnTeam":
                  showError("Vote not placed: You are a team member");
                  break;
              case "MaxVotesForTeamReached":
                  showError("You cannot place more votes on this team.");
                  break;
              case "MaxVotesForRoundReached":
                  showError("You have placed all of your votes.");
                  break;
              case "CannotVoteInDifferentRooms":
                  showError("Your votes must be placed in the same room.");
                  break;
              case "RoundAndRoomDoesNotExist":
                  showError("Room and round not configured.");
                  break;
              case "RoundMissing":
                  showError("Round does not exist.");
                  break;
              case "RoomMissing":
                  showError("Room does not exist.");
                  break;
              case "RoundAndRoomInactive":
                  showError("Voting has not yet begun.");
                  break;
              case "RoundAndRoomLocked":
                  showError("Vote not placed: voting has ended!");
                  break;
              case "DatabaseFailure":
                  showError("Oops, vote failed. Please try again.");
                  break;
              default:
                  AJS.log("Unexpected vote data: (next line)");
                  AJS.log(data);
                  showError("Vote not placed.");
                  break;
          }

          callback && callback(context.teamId);
      });

      voteRequest.fail(function() {
          footerStatus.showMessage("Not authorised; please refresh!", {
              extraClasses: "error"
          });
      });
    };

    var undoOnTeam = function(teamId, roundId, roomId, callback) {
      var context = {
          hackathon: {
              token: pageState.getToken(),
              projectId: pageContext.projectId
          },
          teamId: teamId,
          roundId: roundId,
          roomId: roomId
      };

      var removeVoteRequest = AJS.$.ajax({
          url: "/rest/hackathon/vote",
          type: "DELETE",
          cache: false,
          data: JSON.stringify(context)
      });

      removeVoteRequest.done(function(data) {
          switch(data.status) {
              case "UndoSuccessful":
                  footerStatus.showMessage("Vote removed!", {
                      extraClasses: "warning"
                  });
                  break;
              case "NoVoteToUndo":
                  showError("No votes on this team to undo.");
                  break;
              case "UndoNoSuchTeam":
                  showError("No such team.");
                  break;
              case "UndoNoSuchRound":
                  showError("No such round.");
                  break;
              case "UndoNoSuchRoom":
                  showError("No such room.");
                  break;
              case "UndoNoSuchRoundRoom":
                  showError("No such round-room.");
                  break;
              case "UndoBlockedIsInactive":
                  showError("Votes cannot be removed yet.");
                  break;
              case "UndoBlockedIsLocked":
                  showError("Voting has ended!");
                  break;
              default:
                  AJS.log("Unexpected undo data: (next line)");
                  AJS.log(data);
                  showError("Could not undo vote.");
                  break;
          }

          callback && callback(context.teamId);
      });

      removeVoteRequest.fail(function() {
          showError("Not logged in. Please refresh.");
      });
    };

    AJS.$(function() {
        initFooterStatus();
        // This class was meant to simplify showing error messages in the footer using the defaults
        // footerStatus.showMessage("A quick message test", { permanent: true });

        // Handle clicking on a room
        getContentBox().on('click', '.hackathon-room', H.h(function(e) {
            var self = AJS.$(e.target).closest('.hackathon-room');
            selectRoom(self.data('round-id'), self.data('room-id'), self.data('room-name'));
        }));

        // Just a snipped of code to ignore all clicks of the top right dummy button.
        AJS.$("#dummy-button").click(H.h());

        // Handle clicking on the vote button
        getContentBox().on('click', '.vote-container .vote-operations .vote', H.h(function(e) {
            var container = AJS.$(e.target).closest('.vote-container');

            voteOnTeam(container.data('team-id'), container.data('round-id'), container.data('room-id'), function(teamId) {
               requestVotesForUser(pageState.getToken(), pageContext.projectId, pageState.getCurrentRound()).done(function() {
                  refreshVoteOperations(teamId);
               });
            });
        }));

        // Handle clicking the undo button on a team
        getContentBox().on('click', '.vote-container .vote-operations .undo', H.h(function(e) {
            var container = AJS.$(e.target).closest('.vote-container');

            undoOnTeam(container.data('team-id'), container.data('round-id'), container.data('room-id'), function(teamId) {
               requestVotesForUser(pageState.getToken(), pageContext.projectId, pageState.getCurrentRound()).done(function() {
                  refreshVoteOperations(teamId);
               });
            });
        }));

        // This code handles the back button. It's like a poor mans page model.
        getBackButton().click(H.h(function() {
            var originalScreen = pageState.getCurrentScreen();
            switch(originalScreen) {
                case screen.roomsView:
                    // Ignoring this button click, it was an accident
                    showBackButton(false);
                    break;

                case screen.teamsView:
                    showBackButton(false);
                    pageState.setCurrentRoom(null);
                    pageState.setCurrentScreen(screen.roomsView);
                    loadHackathonTitle();
                    loadRoomsView();
                    footerStatus.hide();
                    AJS.$("#no-teams").addClass("hidden");
                    break;

                case screen.projectView:
                    loadTeamsForRoom();
                    pageState.setCurrentScreen(screen.teamsView);
                    footerStatus.show();
                    break;
            }
        }));
    });
});
