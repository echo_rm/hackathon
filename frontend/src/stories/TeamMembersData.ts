import { TeamMembersResponse } from '../hackathon-client';

// tslint:disable
export const TeamMembersExample: TeamMembersResponse = {
  "teamMembers" : [
     {
        "displayName" : "User A",
        "smallAvatarUrl" : "https://avatar-cdn.atlassian.com/f088cfdcbb290834115483a94b90f9c7?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Ff088cfdcbb290834115483a94b90f9c7%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue",
        "lastUpdated" : new Date("2018-11-16T01:25:17.327346Z"),
        "email" : "usera@atlassian.com",
        "id" : 442,
        "key" : "usera"
     },
     {
        "displayName" : "User B",
        "smallAvatarUrl" : "https://avatar-cdn.atlassian.com/46a48af35d8c69a7d04af6b87509141b?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F46a48af35d8c69a7d04af6b87509141b%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue",
        "lastUpdated" : new Date("2018-11-16T02:59:49.638672Z"),
        "email" : "userb@atlassian.com",
        "id" : 532,
        "key" : "userb"
     },
     {
        "lastUpdated" : new Date("2018-11-16T00:32:23.404643Z"),
        "smallAvatarUrl" : "https://avatar-cdn.atlassian.com/4f4ff83fb1de9977a5f15adad304ecdb?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F4f4ff83fb1de9977a5f15adad304ecdb%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue",
        "displayName" : "User C",
        "key" : "userc",
        "id" : 3468,
        "email" : "userc@atlassian.com"
     },
     {
        "smallAvatarUrl" : "https://avatar-cdn.atlassian.com/0993883e2ad7791f970203256569b786?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F0993883e2ad7791f970203256569b786%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue",
        "displayName" : "User D",
        "lastUpdated" : new Date("2018-11-16T01:20:20.262469Z"),
        "id" : 5070,
        "email" : "userd@atlassian.com",
        "key" : "userd"
     },
     {
        "displayName" : "User E",
        "smallAvatarUrl" : "https://avatar-cdn.atlassian.com/c2c311f413b59501c56b976035b33107?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Fc2c311f413b59501c56b976035b33107%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue",
        "lastUpdated" : new Date("2018-11-17T09:46:11.983625Z"),
        "id" : 3710,
        "email" : "usere@atlassian.com",
        "key" : "usere"
     }
  ]
};
