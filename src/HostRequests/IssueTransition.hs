{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module HostRequests.IssueTransition
   ( getTransitionsForIssue
   , transitionIssue
   , JiraTransitions(..)
   , JiraTransition(..)
   ) where

import           AesonHelpers
import           Application
import           Control.Monad                     (mzero)
import           Data.Aeson
import           Data.Aeson.Types                  (fieldLabelModifier)
import qualified Data.ByteString.Char8             as B
import           Data.Monoid                       (mempty)
import qualified Data.Text                         as T
import qualified Data.Text.Encoding                as T
import           GHC.Generics
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Snaplet                      as SS
import           Text.Read                         (readMaybe)

getTransitionsForIssue :: AC.Tenant -> AC.IssueKey -> AppHandler (Either AC.ProductErrorResponse JiraTransitions)
getTransitionsForIssue tenant issueKey = SS.with connect $ AC.hostGetRequest tenant url [] mempty
  where
    url :: B.ByteString
    url = "/rest/api/2/issue/" `B.append` T.encodeUtf8 issueKey `B.append` "/transitions"

data JiraTransitions = JiraTransitions
   { jTransitions :: [JiraTransition]
   } deriving (Show, Generic)

instance FromJSON JiraTransitions where
   parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "j" }

data JiraTransition = JiraTransition
   { jtId       :: Integer
   , jtName     :: T.Text
   , jtStatusId :: Integer
   } deriving (Show)

instance FromJSON JiraTransition where
   parseJSON (Object v) = do
      pId <- readMaybe <$> (v .: "id")
      name <- v .: "name"
      status <- v .: "to"
      pStatusId <- readMaybe <$> (status .: "id")
      case (pId, pStatusId) of
         (Just tId, Just statusId) -> return $ JiraTransition tId name statusId
         _ -> mzero
   -- A non-Object value is of the wrong type, so fail.
   parseJSON _          = mzero

transitionIssue :: AC.Tenant -> AC.IssueKey -> JiraTransition -> AppHandler (Either AC.ProductErrorResponse ())
transitionIssue tenant issueKey transition = errorOnContent <$> (SS.with connect $ AC.hostPostRequestExtended tenant url [] (AC.setJson request))
  where
    url :: B.ByteString
    url = "/rest/api/2/issue/" `B.append` T.encodeUtf8 issueKey `B.append` "/transitions"

    request :: TransitionRequest
    request = TransitionRequest
      { trTransitionId = jtId transition
      }

data TransitionRequest = TransitionRequest
   { trTransitionId :: Integer
   } deriving (Show)

instance ToJSON TransitionRequest where
   toJSON tr = object
      [ "transition" .= object
         [ "id" .= (toJSON . show . trTransitionId $ tr) ]
      ]
