import * as React from 'react';
import { partialStoriesOf } from './stories-common';
import { TeamPanel } from '../TeamPanel';
import { PageContextExample } from './PageContextData';
import { TeamMembersExample } from './TeamMembersData';
import { TeamDetailsExample } from './TeamDetailsData';
import { TeamDetailsWithPastResults } from '../hackathon-client';
import { action } from '@storybook/addon-actions';

export function stories(storyCreator: partialStoriesOf) {
   storyCreator('Team Panel')
      .add('No past results, room or round', () => {
         const teamDetails: TeamDetailsWithPastResults = {
            id: TeamDetailsExample.id,
            pastResults: []
         };
         return (
            <TeamPanel
               pc={PageContextExample}
               teamDetails={teamDetails}
               teamMembers={TeamMembersExample}
               onRefreshClick={action("Refresh clicked")}
               onTeamMemberRemove={action("Remove team member")}
            />
         );
      })
      .add('No past results', () => {
         const teamDetails: TeamDetailsWithPastResults = {
            id: TeamDetailsExample.id,
            round: TeamDetailsExample.round,
            room: TeamDetailsExample.room,
            pastResults: []
         };
         return (
            <TeamPanel
               pc={PageContextExample}
               teamDetails={teamDetails}
               teamMembers={TeamMembersExample}
               onRefreshClick={action("Refresh clicked")}
               onTeamMemberRemove={action("Remove team member")}
            />
         );
      })
      .add('Full Team Panel View', () => (
         <TeamPanel
            pc={PageContextExample}
            teamDetails={TeamDetailsExample}
            teamMembers={TeamMembersExample}
            onRefreshClick={action("Refresh clicked")}
            onTeamMemberRemove={action("Remove team member")}
         />
      ));
}