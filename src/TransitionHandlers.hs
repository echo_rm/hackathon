{-# LANGUAGE CPP               #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module TransitionHandlers
    ( handleTransitionCaculate
    ) where

import           AesonHelpers
import           Application
import qualified Control.Arrow                  as A
import           Control.Monad                  (forM)
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Either
import           Data.Aeson
import           Data.Aeson.Types               (fieldLabelModifier)
import qualified Data.ByteString.Lazy.Char8     as BSL
import           Data.Foldable                  (foldlM)
import           Data.Function
import qualified Data.HackathonConstants        as HC
import qualified Data.JQL                       as J
import           Data.List
import           Data.Maybe                     (catMaybes)
import           Data.MaybeUtil
import           Data.Ord
import qualified Data.Set                       as S
import qualified Data.Text                      as T
import           GHC.Generics
import qualified Persistence.Hackathon          as PH
import qualified Persistence.HackathonIds       as HI
import qualified Persistence.HackathonRoom      as HRM
import qualified Persistence.HackathonRound     as RND
import qualified Persistence.HackathonRoundRoom as HRR
import qualified Persistence.HackathonTeam      as HT
import qualified Persistence.HackathonVote      as HV
import qualified Persistence.RoundTransition    as RT
import qualified Snap.AtlassianConnect          as AC
import qualified Snap.Core                      as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import           StandardJQL
import           System.Random
import           System.Random.Shuffle
import qualified TeamLoader                     as TL
import qualified WithToken                      as WT

handleTransitionCaculate :: AppHandler ()
handleTransitionCaculate = handleMethods
    [ (SC.GET, WT.tenantFromToken calculateTransitionResult)
    ]

authError :: AppHandler ()
authError = respondWithError unauthorised "You must be logged in to make this request."

calculateTransitionResult :: AC.TenantWithUser -> AppHandler ()
calculateTransitionResult (_, Nothing) = authError
calculateTransitionResult (tenant, Just _) = do
   result <- withPG . runEitherT $ do
      projectId <- EitherT (m2e noProjectId <$> getIntegerQueryParam "projectId")
      roundId <- EitherT (m2e noRoundId <$> getIntegerQueryParam "roundId")
      hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant projectId)
      currentRound <- EitherT (m2e noCurrentRound <$> RND.getHackathonRoundById hackathon roundId)
      nextRound <- EitherT (m2e noNextRound <$> RND.getNextRound currentRound)
      calculateBalancedAllocationFor tenant hackathon (currentRound, nextRound)
   case result of
      Left err -> do
         uncurry respondPlainWithError err
         SC.modifyResponse . SC.setContentType $ "application/json"
      Right ba -> writeJson . toBalancedAllocationResponse $ ba
   where
      noProjectId = (badRequest, er "You need to provide a projectId to this resource to get answers.")
      noRoundId = (badRequest, er "You need to provide a roundId to this resource to get answers.")

er :: String -> String
er = BSL.unpack . encode . ErrorResponse . return

calculateBalancedAllocationFor :: AC.Tenant -> PH.Hackathon -> (RND.HackathonRound, RND.HackathonRound) -> EitherT (Int, String) AppHandler BalancedAllocation
calculateBalancedAllocationFor tenant hackathon (currentRound, nextRound) = do
   transitionDetails <- lift (fmap fst <$> RT.getRoundDetails currentRound)
   requestedAllocations <- EitherT (A.left afToError <$> getRequestedAllocations tenant hackathon currentRound transitionDetails)
   (currentAllocations, unallocated) <- lift $ getCurrentAllocations currentRound
   existingAllocations <- lift $ getExistingAllocations nextRound
   hoistEither (A.left afToError $ performAllocation currentRound unallocated currentAllocations existingAllocations requestedAllocations)

noCurrentRound :: (Int, String)
noCurrentRound = (notFound, er "There was no round with the provided roundId.")

noNextRound :: (Int, String)
noNextRound = (badRequest, er "You can not transition a round that does not have a next round.")

noSuchHackathon :: (Int, String)
noSuchHackathon = (notFound, er "Could not find a hackathon with the provided projectId.")

afToError :: AllocationFailureResponse -> (Int, String)
afToError af = (ok, BSL.unpack . encode $ af) -- We respond okay but with an error block

getCurrentAllocations :: RND.HackathonRound -> AppHandler (CurrentAllocations, HackathonTeams)
getCurrentAllocations currentRound = do
   roundRooms <- HRR.getRoundRoomsForRound currentRound
   allocations <- forM roundRooms $ \rr -> do
      teamsAndVotes <- HV.countAllVotesOnRoundRoomByTeam rr
      return $ CurrentAllocation rr teamsAndVotes
   unallocated <- HT.getUnallocatedTeamsInRound currentRound
   return (allocations, unallocated)
-- Get all round rooms and then get the teams that were voted on in those round rooms and the number of votes that they recieved

getExistingAllocations :: RND.HackathonRound -> AppHandler ExistingAllocations
getExistingAllocations nextRound = do
   roundRoomDetails <- HRR.getRoomDetailsForRound nextRound
   forM roundRoomDetails $ \(hRoundRoom, hRoom) -> do
      teams <- HT.getTeamsInRoundRoom hRoundRoom
      return $ ExistingAllocation hRoundRoom hRoom teams

getRequestedAllocations :: AC.Tenant -> PH.Hackathon -> RND.HackathonRound -> [RT.RoundTransition] -> AppHandler (Either AllocationFailureResponse RequestedAllocations)
getRequestedAllocations tenant hackathon fromRound roundTransitions = sequence <$> mapM (getRequestedAllocation tenant hackathon fromRound) roundTransitions

getRequestedAllocation :: AC.Tenant -> PH.Hackathon -> RND.HackathonRound -> RT.RoundTransition -> AppHandler (Either AllocationFailureResponse RequestedAllocation)
getRequestedAllocation tenant hackathon hRound rt = runEitherT $ do
   loadedTeams <- EitherT (A.left fromFailure <$> TL.loadTeamsFromJQL tenant hackathon searchJQL)
   rooms <- lift $ RT.getRoomsForTransition rt
   return $ RequestedAllocation loadedTeams rooms rt
   where
        searchJQL :: J.JQL
        searchJQL = J.And (jqlForHackathonIssues hackathon) (inRound : extraConditions)

        inRound :: J.JQL
        inRound = J.Eq "status" (T.pack . show . RND.hroundStatusId $ hRound)

        extraConditions :: [J.JQL]
        extraConditions = maybe [] (return . J.CustomJQL) (RT.rtJQL rt)

fromFailure :: TL.TeamLoadFailure -> AllocationFailureResponse
fromFailure tlf = AllocationFailureResponse (fromILF . TL.tlfError $ tlf) (TL.tlfMessage tlf) 

fromILF :: TL.IssueLoadFailure -> AllocationFailure
fromILF TL.TeamCreationFailure = DatabaseTeamCreationFailure
fromILF TL.RoomCreationFailure = DatabaseRoomCreationFailure
fromILF TL.JQLSearchFailure = TransitionJQLSearchFailed
fromILF TL.TeamMemberLookupFailure = TeamMemberLookupFailure
fromILF TL.EntityPropertyUpdateFailure = IssuePropertyUpdateFailure
fromILF TL.NoSuchHackathon = error "The hackathon being missing is a terminal error case."

type HackathonTeams = [HT.HackathonTeam]
type CurrentAllocations = [CurrentAllocation]
type ExistingAllocations = [ExistingAllocation]
type RequestedAllocations = [RequestedAllocation]

data CurrentAllocation = CurrentAllocation
    { caRoundRoom :: HRR.HackathonRoundRoom
    , caTeams     :: [(HT.HackathonTeam, Integer)]
    }

data ExistingAllocation = ExistingAllocation
    { eaRoundRoom :: HRR.HackathonRoundRoom
    , eaRoom      :: HRM.HackathonRoom
    , eaTeams     :: HackathonTeams
    }

data RequestedAllocation = RequestedAllocation
    { raTeams      :: HackathonTeams
    , raRooms      :: [HRM.HackathonRoom]
    , raTransition :: RT.RoundTransition
    }

performAllocation :: RND.HackathonRound
                  -> HackathonTeams
                  -> CurrentAllocations
                  -> ExistingAllocations
                  -> RequestedAllocations
                  -> Either AllocationFailureResponse BalancedAllocation
performAllocation fromRound unallocated currentAllocations existingAllocations requestedAllocations = do
    balancedRooms' <- getBalancedRooms
    return BalancedAllocation
        { balancedRooms = balancedRooms'
        , pendingTeams = pending
        , outvotedTeams = outvoted
        , unallocatedTeams = remainUnallocated
        }
    where
        -- Step 1
        -- Turn the existing allocations into the base allocations
        getBalancedRooms = runRequestedAllocations randomGenerator balanceableTeams initialRooms requestedAllocations
        randomGenerator = mkStdGen . fromIntegral . RND.hroundId $ fromRound

        -- Step 0.1
        balanceableTeams = winners ++ toBalance
        initialRooms = baseAllocation existingAllocations
        (toBalance, remainUnallocated) = if RND.hroundMatchUnallocated fromRound then (unallocated, []) else ([], unallocated)

        -- Step 0
        (winners, outvoted) = foldl appendPair ([], []) . fmap (splitCurrentAllocation fromRound) $ lockedRoundRooms
        pending = concat (fmap fst . caTeams <$> unlockedRoundRooms)
        (lockedRoundRooms, unlockedRoundRooms) = partition (isLocked . caRoundRoom) currentAllocations
        isLocked x = HRR.hrrStatus x == HC.Locked

runRequestedAllocations :: RandomGen g => g -> HackathonTeams -> BalancedRooms -> RequestedAllocations -> Either AllocationFailureResponse BalancedRooms
runRequestedAllocations rg balanceableTeams initialRooms ras = do
    (remainingTeams, result) <- foldlM (runRequestedAllocation rg) (S.fromList balanceableTeams, initialRooms) ras
    if S.null remainingTeams
        then return result
        else Left $ AllocationFailureResponse NotAllTeamsAllocated "Not all of the teams could be allocated."

-- When you are attempting to run a requested allocation then you need to know
-- The initial state of the balanced rooms that you can merge in
-- The set of teams that are balanceable
-- The requested allocation itself (which contains all teams that matched the query and the rooms to balance them into)
-- And it returns either an allocation failure or the remaining unallocated teams and an updated list of balanced rooms
runRequestedAllocation :: RandomGen g => g -> (S.Set HT.HackathonTeam, [BalancedRoom]) -> RequestedAllocation -> Either AllocationFailureResponse (S.Set HT.HackathonTeam, [BalancedRoom])
runRequestedAllocation rg (balanceableTeams, initialAllocation) ra =
    case calculateAllocation shuffledApplicableTeams calculableRooms of
        Nothing -> Left $ AllocationFailureResponse (NoSpaceLeftInRooms transition) "There was no space left in one of the required rooms for this transition."
        Just allocation -> do
            let updatedAllocation = applyAllocation transition allocation initialAllocation
            return (remainingTeams, updatedAllocation)
    where
        calculableRooms = filter (\br -> (HRM.hrId . hackathonRoom $ br) `elem` calculableRoomIds) initialAllocation
        calculableRoomIds = fmap HRM.hrId . raRooms $ ra
        transition = raTransition ra
        -- Shuffle and allocate the applicable teams and have a list of the remaining teams
        shuffledApplicableTeams = safeShuffle lApplicableTeams rg
        lApplicableTeams = S.toList applicableTeams
        (applicableTeams, remainingTeams) = S.partition (`S.member` requestedTeams) balanceableTeams
        requestedTeams = S.fromList . raTeams $ ra

-- The shuffle' function will fall into an infinite loop if given a length of 0. Strange but true.
safeShuffle :: RandomGen g => [a] -> g -> [a]
safeShuffle [] _  = []
safeShuffle xs rg = shuffle' xs (length xs) rg

applyAllocation :: RT.RoundTransition -> [(AssignableRoom, [HT.HackathonTeam])] -> [BalancedRoom] -> [BalancedRoom]
applyAllocation _ _ [] = []
applyAllocation rt allocations (room : rooms) = updatedRoom : remainingAllocations
    where
        updatedRoom = room { balancedTeams = toBalancedTeams teamsForRoom ++ balancedTeams room}
        toBalancedTeams = fmap (`BalancedTeam` transitionReason)
        transitionReason = TransitionedByReason rt
        teamsForRoom = concatMap snd allocationsForRoom
        remainingAllocations = applyAllocation rt otherAllocations rooms
        (allocationsForRoom, otherAllocations) = partition matchesRoom allocations

        matchesRoom :: (AssignableRoom, b) -> Bool
        matchesRoom (AssignableRoom rId, _) = rId == roomId

        roomId = HRM.hrId . hackathonRoom $ room

calculateAllocation :: [HT.HackathonTeam] -> [BalancedRoom] -> Maybe [(AssignableRoom, [HT.HackathonTeam])]
calculateAllocation teams rooms = mergeAvaliableSpaces <$> (allocateAvaliableSpaces teams . getAvaliableSpaces $ rooms)

mergeAvaliableSpaces :: [(AssignableRoom, HT.HackathonTeam)] -> [(AssignableRoom, [HT.HackathonTeam])]
mergeAvaliableSpaces = catMaybes . fmap mergeFst . groupBy ((==) `on` fst) . sortBy (compare `on` fst)
    where
        mergeFst :: [(a, b)] -> Maybe (a, [b])
        mergeFst []            = Nothing
        mergeFst ((x, y) : ys) = Just (x, y : fmap snd ys)

-- We assume that the teams are sorted at that the assignable rooms may be unbounded
allocateAvaliableSpaces :: [HT.HackathonTeam] -> [AssignableRoom] -> Maybe [(AssignableRoom, HT.HackathonTeam)]
allocateAvaliableSpaces [] _ = return []
allocateAvaliableSpaces _ [] = Nothing
allocateAvaliableSpaces (t : ts) (ar : ars) = do
    remainder <- allocateAvaliableSpaces ts ars
    return $ (ar, t) : remainder

-- Given a list of potential rooms this generated a potentially infinite list of assignable rooms
getAvaliableSpaces :: [BalancedRoom] -> [AssignableRoom]
getAvaliableSpaces inputRooms = go 0
    where
        boundsAndRooms = fmap ((A.&&&) balancedRoomBound id) inputRooms

        toAssignableRoom = AssignableRoom . HRM.hrId . hackathonRoom

        go :: Integer -> [AssignableRoom]
        go level = if noRoomsLeft
            then []
            else matchingRooms ++ go (level + 1)
            where
                noRoomsLeft = all ((==) Above . fst) comparisonResults
                matchingRooms = fmap (toAssignableRoom . snd) . filter ((==) Inside . fst) $ comparisonResults
                comparisonResults = fmap (A.first (boundCompare level)) boundsAndRooms

data AssignableRoom = AssignableRoom HI.HackathonRoomId
    deriving (Eq, Ord, Show)

data BoundComparison
    = Above
    | Below
    | Inside
    deriving (Eq)

boundCompare :: Integer -> Bound -> BoundComparison
boundCompare x (Unbounded lower) = if x < lower then Below else Inside
boundCompare x (Bounded lower upper)
    | x < lower = Below
    | x >= upper = Above
    | otherwise = Inside

-- The lower bound starts at zero and goes up from there
balancedRoomBound :: BalancedRoom -> Bound
balancedRoomBound br = case roomCapacity br of
    Nothing -> Unbounded lower
    Just upper -> Bounded lower upper
    where
        lower = genericLength . balancedTeams $ br

-- The bounds are indexed as zero based. That means that if there are no teams in a room then the lower bound should be 0
-- This is because 0 will be the first location that you can put a room.
-- That means that if the room starts with one team and can only hold three teams then the bound should look like: Bounded 1 3
-- This means that the allocable spaces left are [1, 3) => 1, 2. This is important to avoid fencepost errors in the above logic.
data Bound
    = Bounded Integer Integer
    | Unbounded Integer

baseAllocation :: ExistingAllocations -> BalancedRooms
baseAllocation = fmap toInitialBalancedRoom

toInitialBalancedRoom :: ExistingAllocation -> BalancedRoom
toInitialBalancedRoom ea = BalancedRoom
    { balancedTeams = fmap toExistingTeam . eaTeams $ ea
    , hackathonRoom = eaRoom ea
    , roomCapacity = HRR.hrrMaximumTeams . eaRoundRoom $ ea
    }

toExistingTeam :: HT.HackathonTeam -> BalancedTeam
toExistingTeam ht = BalancedTeam
    { hackathonTeam    = ht
    , allocationReason = AlreadyInRoomReason
    }

appendPair :: ([a], [b]) -> ([a], [b]) -> ([a], [b])
appendPair (x1, y1) (x2, y2) = (x1 ++ x2, y1 ++ y2)

-- Split the current allocation into winners and losers
splitCurrentAllocation :: RND.HackathonRound -> CurrentAllocation -> ([HT.HackathonTeam], [HT.HackathonTeam])
splitCurrentAllocation hRound ca = getFirstN takeTied numberOfWinners . fmap (fmap fst) . sortAndGroup . caTeams $ ca
    where
        numberOfWinners = HRR.hrrWinners . caRoundRoom $ ca
        takeTied = RND.hroundTransitionTied hRound

getFirstN :: Bool -> Integer -> [[a]] -> ([a], [a])
getFirstN _ _ [] = ([], [])
getFirstN takeTied toTake r@(x : xs)
    | toTake <= 0 = ([], concat r)
    | fromInteger toTake < length x && not takeTied = ([], concat r)
    | otherwise = (x ++ first, remainder)
        where
            (first, remainder) = getFirstN takeTied (toTake - (fromIntegral . length $ x)) xs

sortAndGroup :: Ord b => [(a, b)] -> [[(a, b)]]
sortAndGroup = groupBy ((==) `on` snd) . sortBy (comparing (Down . snd))

-- To be calculated:
-- balanceableTeams :: [HT.HackathonTeam] -- these teams will be going forwards and should be balanced into teams
-- requestedAllocation :: [([HT.HackathonTeam], [HRM.HackathonRoom])] -- This shows the teams that we would like to put in various rooms

-- The teams in a requestedAllocation are all of the teams that match the jql query with the rooms that we would like to put them in.
-- But maybe we should filter them down to the teams that we should balance into rooms

-- Step 0: Split the fromRoundRoomTeams into the winners, outvoted and pending. Putting pending and outvoted teams
--               straight into the correct columns in the balanced allocation.
-- Step 0.1: Create a list of "balanceable" teams that are the winners plus the unallocated teams if the setting is
--           checked to apply to unallocated teams. If it is not checked then put the unallocated teams straight into the
--           right column of the balanced allocation.
-- Step 1: Turn the toRoundRoomTeams into the base balanced allocation sorted by the least full to most full teams.
-- Step 2: For each transition in the fromTransitions (make sure they are ordered) (And fold from left to right)
-- Step 2.0: Filter the hackathon teams that the JQL query returned by those that are balanceable teams (from Step 0.1).
-- Step 2.1: Sort the remaining hackathon teams by issue_id.
-- Step 2.2: Shuffle the teams randomly using the hackathon project id as a seed.
-- Step 2.3: Fill up the balanced allocation from least filled to most filled, one team
--           at a time, until all teams are allocated or you have run out of space in the rooms.
-- Step 3: Return the new allocation.

-- Expanding on Step 2.3:
-- Note: The "next room" operation steps through the rooms in a cycle from least full room to most.
-- Sort the balanced allocation by least full to most full
-- Set the current room to be the least full room
-- For each team:
--   try and place this team in the current room
--     if the room is full then you cannot so move to the next room with the same team
--     if the the room is not full then allocate this team to that room and move onto the next room with the remaining teams
--     if you have made a complete cycle of all of the rooms and could not place the team in any of them then error out, this allocation is impossible

data AllocationFailureResponse = AllocationFailureResponse
   { afrError   :: AllocationFailure
   , afrMessage :: T.Text
   } deriving (Generic)

instance ToJSON AllocationFailureResponse where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "afr" }

data AllocationFailure
    = NoSpaceLeftInRooms RT.RoundTransition -- There were more teams than rooms to put them in for this transition. Add more rooms or expand room capacity
    | NotAllTeamsAllocated
    | TransitionJQLSearchFailed
    | DatabaseRoomCreationFailure
    | DatabaseTeamCreationFailure
    | TeamMemberLookupFailure
    | IssuePropertyUpdateFailure

instance ToJSON AllocationFailure where
   toJSON (NoSpaceLeftInRooms rt) = object [ "errorType" .= ("no-space-left-in-rooms" :: String), "roundTransitionId" .= RT.rtId rt, "roundTransitionOrderNumber" .= RT.rtOrderNumber rt ]
   toJSON NotAllTeamsAllocated = standardAFError "not-all-teams-allocated"
   toJSON TransitionJQLSearchFailed = standardAFError "transition-jql-search-failed"
   toJSON DatabaseRoomCreationFailure = standardAFError "database-room-creation-failure"
   toJSON DatabaseTeamCreationFailure = standardAFError "database-team-creation-failure"
   toJSON TeamMemberLookupFailure = standardAFError "team-member-lookup-failure"
   toJSON IssuePropertyUpdateFailure = standardAFError "issue-property-update-failure"

standardAFError :: String -> Value
standardAFError name = object [ "errorType" .= name ]

-- Details about teams:
-- For teams that were allocated:
--  What is the issue key and name of this team?
--  What room did you come from?
--  How many votes did you get in the previous round?
--  Which transition rule put you in this new room?

data BalancedAllocation = BalancedAllocation
    { balancedRooms    :: BalancedRooms      -- ^ The balanced rooms that we were able to calculate
    , pendingTeams     :: [HT.HackathonTeam] -- ^ The teams that cannot be allocated yet
    , outvotedTeams    :: [HT.HackathonTeam] -- ^ The teams that have not made it into the next round.
    , unallocatedTeams :: [HT.HackathonTeam] -- ^ The teams that were not allocated to a room in the current round and were not moved forwards
    }

type BalancedRooms = [BalancedRoom]

data BalancedRoom = BalancedRoom
    { balancedTeams :: [BalancedTeam]
    , hackathonRoom :: HRM.HackathonRoom
    , roomCapacity  :: Maybe Integer
    }

data BalancedTeam = BalancedTeam
    { hackathonTeam    :: HT.HackathonTeam
    , allocationReason :: AllocationReason
    }

data AllocationReason
    = AlreadyInRoomReason -- You were already in this room
    | TransitionedByReason RT.RoundTransition

data BalancedAllocationResponse = BalancedAllocationResponse
    { barRooms            :: [BalancedRoomResponse]
    , barPendingTeams     :: [TeamResponse]
    , barOutvotedTeams    :: [TeamResponse]
    , barUnallocatedTeams :: [TeamResponse]
    } deriving (Show, Generic)

instance ToJSON BalancedAllocationResponse where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "bar" }

toBalancedAllocationResponse :: BalancedAllocation -> BalancedAllocationResponse
toBalancedAllocationResponse ba = BalancedAllocationResponse
    { barRooms = fmap toBalancedRoomResponse . balancedRooms $ ba
    , barPendingTeams = fmap toTeamResponse . pendingTeams $ ba
    , barOutvotedTeams = fmap toTeamResponse . outvotedTeams $ ba
    , barUnallocatedTeams = fmap toTeamResponse . unallocatedTeams $ ba
    }

data BalancedRoomResponse = BalancedRoomResponse
    { brrTeams        :: [BalancedTeamResponse]
    , brrRoom         :: RoomResponse
    , brrRoomCapacity :: Maybe Integer
    } deriving (Show, Generic)

instance ToJSON BalancedRoomResponse where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "brr" }

toBalancedRoomResponse :: BalancedRoom -> BalancedRoomResponse
toBalancedRoomResponse br = BalancedRoomResponse
    { brrTeams = fmap toBalancedTeamResponse . balancedTeams $ br
    , brrRoom = toRoomResponse . hackathonRoom $ br
    , brrRoomCapacity = roomCapacity br
    }

data RoomResponse = RoomResponse
    { rrspName        :: T.Text
    , rrspComponentId :: HI.ComponentId
    } deriving (Show, Generic)

instance ToJSON RoomResponse where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rrsp" }

toRoomResponse :: HRM.HackathonRoom -> RoomResponse
toRoomResponse hRoom = RoomResponse
    { rrspName = HRM.hrName hRoom
    , rrspComponentId = HRM.hrComponentId hRoom
    }

data TeamResponse = TeamResponse
    { trIssueId      :: AC.IssueId
    , trIssueKey     :: AC.IssueKey
    , trIssueSummary :: AC.IssueSummary
    } deriving (Show, Generic)

instance ToJSON TeamResponse where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "tr" }

toTeamResponse :: HT.HackathonTeam -> TeamResponse
toTeamResponse ht = TeamResponse
    { trIssueId = HT.htIssueId ht
    , trIssueKey = HT.htIssueKey ht
    , trIssueSummary = HT.htIssueSummary ht
    }

data BalancedTeamResponse = BalancedTeamResponse
    { btrDetails          :: TeamResponse
    , btrAllocationReason :: AllocationReasonResponse
    } deriving (Show, Generic)

instance ToJSON BalancedTeamResponse where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "btr" }

toBalancedTeamResponse :: BalancedTeam -> BalancedTeamResponse
toBalancedTeamResponse bt = BalancedTeamResponse
    { btrDetails = toTeamResponse . hackathonTeam $ bt
    , btrAllocationReason = toAllocationReasonResponse . allocationReason $ bt
    }

data AllocationReasonResponse = AllocationReasonResponse
    { arrReason           :: T.Text
    , arrTransitionNumber :: Maybe Integer
    } deriving (Show, Generic)

instance ToJSON AllocationReasonResponse where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "arr" }

toAllocationReasonResponse :: AllocationReason -> AllocationReasonResponse
toAllocationReasonResponse ar = AllocationReasonResponse
    { arrReason = textReason ar
    , arrTransitionNumber = transitionNumber ar
    }
    where
        textReason AlreadyInRoomReason = "AlreadyInRoom"
        textReason (TransitionedByReason _) = "Transitioned"

        transitionNumber (TransitionedByReason x) = Just . RT.rtOrderNumber $ x
        transitionNumber _ = Nothing
