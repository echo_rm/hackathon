#!/bin/bash -e

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

ROOT_DIR="$DIR/.."

FLYWAY="$ROOT_DIR/migrations/flyway"

$FLYWAY clean && $FLYWAY migrate $@
