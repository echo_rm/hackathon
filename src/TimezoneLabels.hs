{-# LANGUAGE OverloadedStrings #-}
module TimezoneLabels
   ( updateTimezoneLabels
   ) where

import           Application
import qualified Control.Arrow              as A
import           Control.Monad.Trans.Either
import           Data.Maybe                 (catMaybes)
import qualified Data.Text                  as T
import qualified HostRequests.IssueSearch   as IS
import qualified HostRequests.IssueUpdate   as IU
import qualified Persistence.HackathonTeam  as HT
import qualified Persistence.TenantUser     as TU
import qualified Snap.AtlassianConnect      as AC
import           Snap.Helpers
import           StandardJQL


updateTimezoneLabels :: AC.Tenant -> HT.HackathonTeam -> [TU.TenantUser] -> EitherT (Int, String) AppHandler ()
updateTimezoneLabels tenant hackathonTeam tenantUsers = do
   -- get the timezones for all of the users and convert them into label text
   issues <- EitherT (A.left (const issueSearchFailed) <$> IS.searchForAllIssues tenant (jqlForTeam hackathonTeam))
   case issues of
     [issue] -> do
        let labels = filterOutTimezoneLabels issue ++ catMaybes (fmap toTimezoneLabel tenantUsers)
        EitherT (A.left (const issueUpdateFailed) <$> IU.updateIssueDetails tenant (HT.htIssueId hackathonTeam) (issueUpdateLabels labels))
     xs -> left $ notSingletonResponse (length xs)
   where
      issueSearchFailed = (internalServer, "We could not get the details for this issue from JIRA.")
      notSingletonResponse x = (internalServer, "We expected one and only one issue to be returned from our search. Instead we recieved: " ++ show x)
      issueUpdateFailed = (internalServer, "Failed to update the issue with the new labels for the new team members.")

issueUpdateLabels :: [T.Text] -> IU.IssueUpdateRequest
issueUpdateLabels labels = IU.IssueUpdateRequest $ IU.emptyIssueUpdate
   { IU.iudLabels = Just labels
   }

filterOutTimezoneLabels :: IS.Issue -> [T.Text]
filterOutTimezoneLabels issue = filter (not . isTimezoneLabel) (IS.issueLabels issue)

isTimezoneLabel :: T.Text -> Bool
isTimezoneLabel = T.isPrefixOf timezoneLabelPrefix

timezoneLabelPrefix :: T.Text
timezoneLabelPrefix = "timezone-"

toTimezoneLabel :: TU.TenantUser -> Maybe T.Text
toTimezoneLabel tu = fmap (\tz -> timezoneLabelPrefix `T.append` T.replace " " "-" tz) (TU.tuTimeZone tu)
