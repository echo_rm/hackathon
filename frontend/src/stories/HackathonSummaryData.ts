import { HackathonSummary, RoundRoomStatus } from '../hackathon-client';

// tslint:disable
export const HackathonSummaryExample: HackathonSummary = {
  "startDate" : new Date("2018-11-14T21:00:00Z"),
  "projectName" : "Global ShipIt 44",
  "roundRoomVotes" : [
     {
        "round" : {
           "sequenceNumber" : 1,
           "name" : "Round 1",
           "id" : 415
        },
        "numberOfTeams" : 14,
        "isCurrentRound" : true,
        "room" : {
           "name" : "BLR - L3 - All Hands Area",
           "id" : 453,
           "shortUrl" : "https://goo.gl/CV753L"
        },
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 157
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 42,
        "numberOfTeams" : 6,
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "room" : {
           "id" : 458,
           "shortUrl" : "https://goo.gl/a1zqgR",
           "name" : "SYD - 341 - 6.01 Avengers"
        },
        "isCurrentRound" : true
     },
     {
        "room" : {
           "id" : 464,
           "shortUrl" : "https://goo.gl/oRtGrU",
           "name" : "SYD - 363 - 22.03 Bohemian Rhapsody"
        },
        "isCurrentRound" : true,
        "numberOfTeams" : 0,
        "round" : {
           "id" : 415,
           "name" : "Round 1",
           "sequenceNumber" : 1
        },
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 0
     },
     {
        "numberOfTeams" : 7,
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "room" : {
           "name" : "SYD - 341 - 3.02 Kwik.E.Mart",
           "id" : 469,
           "shortUrl" : "https://goo.gl/sosGW8"
        },
        "isCurrentRound" : true,
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 76
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 70,
        "room" : {
           "name" : "SYD - 341 - 8.09 Thundera",
           "shortUrl" : "https://goo.gl/ekdpAb",
           "id" : 471
        },
        "isCurrentRound" : true,
        "numberOfTeams" : 6,
        "round" : {
           "name" : "Round 1",
           "sequenceNumber" : 1,
           "id" : 415
        }
     },
     {
        "status" : RoundRoomStatus.Active,
        "numberOfVotes" : 0,
        "numberOfTeams" : 0,
        "round" : {
           "id" : 415,
           "name" : "Round 1",
           "sequenceNumber" : 1
        },
        "isCurrentRound" : false,
        "room" : {
           "id" : 489,
           "shortUrl" : "https://goo.gl/yE5oBb",
           "name" : "SF - Level 13 - Town Hall"
        }
     },
     {
        "numberOfVotes" : 0,
        "status" : RoundRoomStatus.Active,
        "room" : {
           "name" : "SYD - 341 - 6.02 Stark",
           "shortUrl" : "https://goo.gl/YVjqHQ",
           "id" : 452
        },
        "isCurrentRound" : false,
        "round" : {
           "name" : "Round 1",
           "sequenceNumber" : 1,
           "id" : 415
        },
        "numberOfTeams" : 0
     },
     {
        "room" : {
           "id" : 451,
           "shortUrl" : "https://goo.gl/aNZLjh",
           "name" : "SYD - 343 - 9.03 Zone"
        },
        "isCurrentRound" : true,
        "numberOfTeams" : 2,
        "round" : {
           "sequenceNumber" : 1,
           "name" : "Round 1",
           "id" : 415
        },
        "numberOfVotes" : 33,
        "status" : RoundRoomStatus.Locked
     },
     {
        "numberOfVotes" : 55,
        "status" : RoundRoomStatus.Locked,
        "numberOfTeams" : 5,
        "round" : {
           "id" : 415,
           "name" : "Round 1",
           "sequenceNumber" : 1
        },
        "room" : {
           "name" : "GDN - 4th floor - Falcon team (former Bamboo) room",
           "shortUrl" : "https://goo.gl/cUQHpv",
           "id" : 472
        },
        "isCurrentRound" : true
     },
     {
        "numberOfTeams" : 8,
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "isCurrentRound" : true,
        "room" : {
           "name" : "GDN - 4th floor - Sparta",
           "shortUrl" : "https://goo.gl/wrXvxL",
           "id" : 474
        },
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 143
     },
     {
        "status" : RoundRoomStatus.Active,
        "numberOfVotes" : 0,
        "room" : {
           "id" : 476,
           "shortUrl" : "https://goo.gl/UhYHJY",
           "name" : "GDN - Finals - Sparta"
        },
        "isCurrentRound" : false,
        "numberOfTeams" : 0,
        "round" : {
           "id" : 415,
           "name" : "Round 1",
           "sequenceNumber" : 1
        }
     },
     {
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "numberOfTeams" : 0,
        "room" : {
           "id" : 480,
           "shortUrl" : "https://goo.gl/2RTudM",
           "name" : "ATX - Broken Spoke"
        },
        "isCurrentRound" : true,
        "numberOfVotes" : 43,
        "status" : RoundRoomStatus.Locked
     },
     {
        "numberOfTeams" : 0,
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "room" : {
           "shortUrl" : "https://goo.gl/css5tD",
           "id" : 482,
           "name" : "SF - Level 11 - RadioWalker"
        },
        "isCurrentRound" : true,
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 62
     },
     {
        "numberOfTeams" : 0,
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "room" : {
           "shortUrl" : "https://goo.gl/sVwnfp",
           "id" : 484,
           "name" : "SF - Level 12 - The Exchange"
        },
        "isCurrentRound" : true,
        "numberOfVotes" : 101,
        "status" : RoundRoomStatus.Locked
     },
     {
        "isCurrentRound" : true,
        "room" : {
           "id" : 485,
           "shortUrl" : "https://goo.gl/GgRQ9L",
           "name" : "SF - Level 13 - The Paddock"
        },
        "round" : {
           "sequenceNumber" : 1,
           "name" : "Round 1",
           "id" : 415
        },
        "numberOfTeams" : 0,
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 40
     },
     {
        "room" : {
           "name" : "NYC",
           "id" : 487,
           "shortUrl" : "https://goo.gl/wBPSnb"
        },
        "isCurrentRound" : true,
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "numberOfTeams" : 4,
        "status" : RoundRoomStatus.Active,
        "numberOfVotes" : 23
     },
     {
        "numberOfVotes" : 64,
        "status" : RoundRoomStatus.Locked,
        "isCurrentRound" : true,
        "room" : {
           "name" : "MTV - Changi",
           "shortUrl" : "https://goo.gl/jUwuBi",
           "id" : 454
        },
        "numberOfTeams" : 5,
        "round" : {
           "name" : "Round 1",
           "sequenceNumber" : 1,
           "id" : 415
        }
     },
     {
        "numberOfTeams" : 0,
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "room" : {
           "shortUrl" : "https://goo.gl/1iqgn7",
           "id" : 455,
           "name" : "MTV - De Gaulle"
        },
        "isCurrentRound" : true,
        "numberOfVotes" : 0,
        "status" : RoundRoomStatus.Inactive
     },
     {
        "isCurrentRound" : false,
        "room" : {
           "shortUrl" : "https://goo.gl/zpZzcj",
           "id" : 478,
           "name" : "BOS-FC- Kitchen Room"
        },
        "numberOfTeams" : 0,
        "round" : {
           "id" : 415,
           "name" : "Round 1",
           "sequenceNumber" : 1
        },
        "status" : RoundRoomStatus.Active,
        "numberOfVotes" : 89
     },
     {
        "room" : {
           "name" : "Manila",
           "shortUrl" : "https://goo.gl/9xqpsg",
           "id" : 456
        },
        "isCurrentRound" : true,
        "numberOfTeams" : 5,
        "round" : {
           "id" : 415,
           "name" : "Round 1",
           "sequenceNumber" : 1
        },
        "numberOfVotes" : 139,
        "status" : RoundRoomStatus.Locked
     },
     {
        "room" : {
           "shortUrl" : "https://goo.gl/BwZZRh",
           "id" : 459,
           "name" : "SYD - 341 - 6.03 Justice"
        },
        "isCurrentRound" : true,
        "numberOfTeams" : 0,
        "round" : {
           "id" : 415,
           "name" : "Round 1",
           "sequenceNumber" : 1
        },
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 0
     },
     {
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "numberOfTeams" : 8,
        "isCurrentRound" : true,
        "room" : {
           "id" : 460,
           "shortUrl" : "https://goo.gl/R3X4Y9",
           "name" : "SYD - 341 - 7.09 Death Star"
        },
        "numberOfVotes" : 139,
        "status" : RoundRoomStatus.Locked
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 23,
        "room" : {
           "name" : "SYD - 341 - 9.02 Zen",
           "id" : 461,
           "shortUrl" : "https://goo.gl/pQmXdZ"
        },
        "isCurrentRound" : true,
        "numberOfTeams" : 5,
        "round" : {
           "sequenceNumber" : 1,
           "name" : "Round 1",
           "id" : 415
        }
     },
     {
        "room" : {
           "name" : "SYD - 363 - 22.01 Another Brick in the Wall",
           "shortUrl" : "https://goo.gl/FDNfVA",
           "id" : 463
        },
        "isCurrentRound" : true,
        "numberOfTeams" : 0,
        "round" : {
           "sequenceNumber" : 1,
           "name" : "Round 1",
           "id" : 415
        },
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 0
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 68,
        "numberOfTeams" : 8,
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "room" : {
           "name" : "SYD - 341 - 3.01 New New York",
           "shortUrl" : "https://goo.gl/u472tC",
           "id" : 466
        },
        "isCurrentRound" : true
     },
     {
        "room" : {
           "shortUrl" : "https://goo.gl/ENCw5G",
           "id" : 467,
           "name" : "SYD - 341 - 8.07 LV-426"
        },
        "isCurrentRound" : true,
        "numberOfTeams" : 9,
        "round" : {
           "sequenceNumber" : 1,
           "name" : "Round 1",
           "id" : 415
        },
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 115
     },
     {
        "numberOfTeams" : 0,
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "room" : {
           "name" : "SYD - 363 - Hungry like the wolf",
           "shortUrl" : "https://goo.gl/MGfsU2",
           "id" : 468
        },
        "isCurrentRound" : true,
        "numberOfVotes" : 0,
        "status" : RoundRoomStatus.Locked
     },
     {
        "numberOfTeams" : 8,
        "round" : {
           "name" : "Round 1",
           "sequenceNumber" : 1,
           "id" : 415
        },
        "isCurrentRound" : true,
        "room" : {
           "id" : 470,
           "shortUrl" : "https://goo.gl/7aZAdX",
           "name" : "SYD - 341 - 7.07 Discworld"
        },
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 43
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 56,
        "room" : {
           "name" : "SYD - 343 - 9.A Mugatu",
           "id" : 462,
           "shortUrl" : "https://goo.gl/3QSE7B"
        },
        "isCurrentRound" : false,
        "round" : {
           "id" : 415,
           "name" : "Round 1",
           "sequenceNumber" : 1
        },
        "numberOfTeams" : 3
     },
     {
        "status" : RoundRoomStatus.Active,
        "numberOfVotes" : 0,
        "round" : {
           "name" : "Round 1",
           "sequenceNumber" : 1,
           "id" : 415
        },
        "numberOfTeams" : 0,
        "room" : {
           "shortUrl" : "https://goo.gl/XKLw9V",
           "id" : 465,
           "name" : "SYD - 341 - 2.07 Aperture"
        },
        "isCurrentRound" : false
     },
     {
        "room" : {
           "shortUrl" : "https://goo.gl/tp59w8",
           "id" : 457,
           "name" : "SYD - 363 - Around the world"
        },
        "isCurrentRound" : false,
        "round" : {
           "name" : "Round 1",
           "sequenceNumber" : 1,
           "id" : 415
        },
        "numberOfTeams" : 10,
        "numberOfVotes" : 69,
        "status" : RoundRoomStatus.Locked
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 50,
        "numberOfTeams" : 3,
        "round" : {
           "sequenceNumber" : 1,
           "name" : "Round 1",
           "id" : 415
        },
        "room" : {
           "id" : 473,
           "shortUrl" : "https://goo.gl/fgzzAu",
           "name" : "GDN - 4th floor - Jira big room"
        },
        "isCurrentRound" : true
     },
     {
        "status" : RoundRoomStatus.Active,
        "numberOfVotes" : 128,
        "numberOfTeams" : 13,
        "round" : {
           "sequenceNumber" : 1,
           "name" : "Round 1",
           "id" : 415
        },
        "isCurrentRound" : true,
        "room" : {
           "name" : "Remote",
           "id" : 486,
           "shortUrl" : "https://goo.gl/CLzeRM"
        }
     },
     {
        "round" : {
           "id" : 415,
           "name" : "Round 1",
           "sequenceNumber" : 1
        },
        "numberOfTeams" : 14,
        "room" : {
           "name" : "ANK - Global Hall",
           "shortUrl" : "https://goo.gl/DXifPu",
           "id" : 475
        },
        "isCurrentRound" : true,
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 209
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 180,
        "numberOfTeams" : 0,
        "round" : {
           "id" : 415,
           "sequenceNumber" : 1,
           "name" : "Round 1"
        },
        "room" : {
           "shortUrl" : "https://goo.gl/owTwDj",
           "id" : 477,
           "name" : "AMS - Mokum"
        },
        "isCurrentRound" : true
     },
     {
        "isCurrentRound" : true,
        "room" : {
           "id" : 479,
           "shortUrl" : "https://goo.gl/izA3w2",
           "name" : "ATX - Paramount"
        },
        "round" : {
           "name" : "Round 1",
           "sequenceNumber" : 1,
           "id" : 415
        },
        "numberOfTeams" : 0,
        "numberOfVotes" : 104,
        "status" : RoundRoomStatus.Locked
     },
     {
        "numberOfTeams" : 0,
        "round" : {
           "sequenceNumber" : 1,
           "name" : "Round 1",
           "id" : 415
        },
        "isCurrentRound" : true,
        "room" : {
           "name" : "SF - Level 10 - Dolores Park",
           "id" : 481,
           "shortUrl" : "https://goo.gl/7jLdQz"
        },
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 107
     },
     {
        "numberOfTeams" : 0,
        "round" : {
           "name" : "Round 1",
           "sequenceNumber" : 1,
           "id" : 415
        },
        "room" : {
           "id" : 490,
           "shortUrl" : "https://goo.gl/PXmFXY",
           "name" : "MTV - Final"
        },
        "isCurrentRound" : false,
        "numberOfVotes" : 0,
        "status" : RoundRoomStatus.Locked
     },
     {
        "room" : {
           "shortUrl" : "https://goo.gl/hF68R6",
           "id" : 488,
           "name" : "MTV - DeGaulle"
        },
        "isCurrentRound" : true,
        "round" : {
           "id" : 415,
           "name" : "Round 1",
           "sequenceNumber" : 1
        },
        "numberOfTeams" : 9,
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 173
     },
     {
        "round" : {
           "sequenceNumber" : 1,
           "name" : "Round 1",
           "id" : 415
        },
        "numberOfTeams" : 0,
        "room" : {
           "name" : "ATX - Living Room",
           "id" : 483,
           "shortUrl" : "https://goo.gl/WUywUa"
        },
        "isCurrentRound" : false,
        "numberOfVotes" : 0,
        "status" : RoundRoomStatus.Locked
     },
     {
        "numberOfTeams" : 12,
        "round" : {
           "id" : 416,
           "sequenceNumber" : 2,
           "name" : "Round 2"
        },
        "room" : {
           "id" : 489,
           "shortUrl" : "https://goo.gl/yE5oBb",
           "name" : "SF - Level 13 - Town Hall"
        },
        "isCurrentRound" : true,
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 310
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 149,
        "round" : {
           "id" : 416,
           "sequenceNumber" : 2,
           "name" : "Round 2"
        },
        "numberOfTeams" : 0,
        "room" : {
           "shortUrl" : "https://goo.gl/YVjqHQ",
           "id" : 452,
           "name" : "SYD - 341 - 6.02 Stark"
        },
        "isCurrentRound" : true
     },
     {
        "numberOfTeams" : 0,
        "round" : {
           "id" : 416,
           "name" : "Round 2",
           "sequenceNumber" : 2
        },
        "isCurrentRound" : false,
        "room" : {
           "id" : 451,
           "shortUrl" : "https://goo.gl/aNZLjh",
           "name" : "SYD - 343 - 9.03 Zone"
        },
        "numberOfVotes" : 0,
        "status" : RoundRoomStatus.Inactive
     },
     {
        "room" : {
           "shortUrl" : "https://goo.gl/UhYHJY",
           "id" : 476,
           "name" : "GDN - Finals - Sparta"
        },
        "isCurrentRound" : true,
        "round" : {
           "id" : 416,
           "name" : "Round 2",
           "sequenceNumber" : 2
        },
        "numberOfTeams" : 13,
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 296
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 60,
        "numberOfTeams" : 16,
        "round" : {
           "id" : 416,
           "name" : "Round 2",
           "sequenceNumber" : 2
        },
        "room" : {
           "name" : "BOS-FC- Kitchen Room",
           "shortUrl" : "https://goo.gl/zpZzcj",
           "id" : 478
        },
        "isCurrentRound" : true
     },
     {
        "status" : RoundRoomStatus.Inactive,
        "numberOfVotes" : 0,
        "room" : {
           "name" : "SYD - 363 - 22.01 Another Brick in the Wall",
           "shortUrl" : "https://goo.gl/FDNfVA",
           "id" : 463
        },
        "isCurrentRound" : false,
        "numberOfTeams" : 0,
        "round" : {
           "sequenceNumber" : 2,
           "name" : "Round 2",
           "id" : 416
        }
     },
     {
        "status" : RoundRoomStatus.Inactive,
        "numberOfVotes" : 0,
        "numberOfTeams" : 0,
        "round" : {
           "sequenceNumber" : 2,
           "name" : "Round 2",
           "id" : 416
        },
        "room" : {
           "id" : 467,
           "shortUrl" : "https://goo.gl/ENCw5G",
           "name" : "SYD - 341 - 8.07 LV-426"
        },
        "isCurrentRound" : false
     },
     {
        "numberOfTeams" : 0,
        "round" : {
           "sequenceNumber" : 2,
           "name" : "Round 2",
           "id" : 416
        },
        "room" : {
           "id" : 468,
           "shortUrl" : "https://goo.gl/MGfsU2",
           "name" : "SYD - 363 - Hungry like the wolf"
        },
        "isCurrentRound" : false,
        "status" : RoundRoomStatus.Inactive,
        "numberOfVotes" : 0
     },
     {
        "room" : {
           "shortUrl" : "https://goo.gl/3QSE7B",
           "id" : 462,
           "name" : "SYD - 343 - 9.A Mugatu"
        },
        "isCurrentRound" : true,
        "round" : {
           "id" : 416,
           "name" : "Round 2",
           "sequenceNumber" : 2
        },
        "numberOfTeams" : 0,
        "numberOfVotes" : 164,
        "status" : RoundRoomStatus.Locked
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 144,
        "numberOfTeams" : 0,
        "round" : {
           "sequenceNumber" : 2,
           "name" : "Round 2",
           "id" : 416
        },
        "isCurrentRound" : true,
        "room" : {
           "shortUrl" : "https://goo.gl/XKLw9V",
           "id" : 465,
           "name" : "SYD - 341 - 2.07 Aperture"
        }
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 299,
        "numberOfTeams" : 0,
        "round" : {
           "id" : 416,
           "name" : "Round 2",
           "sequenceNumber" : 2
        },
        "isCurrentRound" : false,
        "room" : {
           "name" : "SYD - 363 - Around the world",
           "id" : 457,
           "shortUrl" : "https://goo.gl/tp59w8"
        }
     },
     {
        "isCurrentRound" : true,
        "room" : {
           "name" : "ATX - Living Room",
           "id" : 483,
           "shortUrl" : "https://goo.gl/WUywUa"
        },
        "numberOfTeams" : 6,
        "round" : {
           "sequenceNumber" : 2,
           "name" : "Round 2",
           "id" : 416
        },
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 257
     },
     {
        "room" : {
           "id" : 457,
           "shortUrl" : "https://goo.gl/tp59w8",
           "name" : "SYD - 363 - Around the world"
        },
        "isCurrentRound" : true,
        "round" : {
           "name" : "Round 3",
           "sequenceNumber" : 3,
           "id" : 417
        },
        "numberOfTeams" : 12,
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 1176
     },
     {
        "room" : {
           "name" : "SYD - 363 - Hungry like the wolf",
           "id" : 468,
           "shortUrl" : "https://goo.gl/MGfsU2"
        },
        "isCurrentRound" : false,
        "numberOfTeams" : 0,
        "round" : {
           "sequenceNumber" : 3,
           "name" : "Round 3",
           "id" : 417
        },
        "numberOfVotes" : 0,
        "status" : RoundRoomStatus.Inactive
     },
     {
        "status" : RoundRoomStatus.Locked,
        "numberOfVotes" : 267,
        "numberOfTeams" : 11,
        "round" : {
           "id" : 418,
           "sequenceNumber" : 4,
           "name" : "Round 4"
        },
        "isCurrentRound" : true,
        "room" : {
           "name" : "MTV - Final",
           "shortUrl" : "https://goo.gl/PXmFXY",
           "id" : 490
        }
     }
  ],
  "shortUrl" : "https://goo.gl/SxZuEm",
  "endDate" : new Date("2018-11-16T07:00:00Z"),
  "teamIssueTypeId" : 9
};
