{-# LANGUAGE DeriveGeneric #-}
module WebhookHandlers
   ( handleIssueCreateWebhook
   , handleIssueUpdateWebhook
   , handleIssueDeleteWebhook
   ) where

import           AesonHelpers
import           Application
import           Control.Monad
import           Control.Monad.Trans.Class         (lift)
import           Control.Monad.Trans.Either
import           Control.Monad.Trans.Maybe
import           Data.Aeson
import           Data.Aeson.Types                  (Options, defaultOptions,
                                                    fieldLabelModifier)
import           Data.HackathonConstants
import qualified Data.HTProperties                 as HTP
import           Data.List                         as DL
import           Data.Maybe                        (catMaybes, fromMaybe)
import qualified Data.Set                          as S
import qualified Data.Text                         as T
import           GHC.Generics
import qualified HostRequests.EntityProperties     as EP
import qualified HostRequests.IssueUpdate          as IU
import qualified Persistence.Hackathon             as PH
import qualified Persistence.HackathonIds          as HI
import qualified Persistence.HackathonRoom         as HRM
import qualified Persistence.HackathonRound        as RND
import qualified Persistence.HackathonRoundRoom    as HRR
import qualified Persistence.HackathonTeam         as PHT
import qualified Persistence.HackathonTeamMember   as PHTM
import qualified Persistence.TenantUser            as PTU
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Core                         as SC
import qualified Snap.Helpers                      as SH
import           Snap.Snaplet.PostgresqlSimple
import qualified TenantJWT                         as WT
import           Text.Read                         (readMaybe)
import qualified TimezoneLabels                    as TL
import qualified UserUtil                          as UU

-- Parse the Webhook Data
handleWebhook :: (AC.Tenant -> IssueUpdate -> AppHandler ()) -> AC.TenantWithUser -> AppHandler ()
handleWebhook webhookHandler (tenant, _) = do
   parsedRequest <- webhookDataFromRequest
   case parsedRequest of
      Left _ -> SH.respondNoContent
      Right webhookData -> do
         webhookHandler tenant (webhookDataToIssueUpdate webhookData)
         SH.respondNoContent

-- We want to not accept webhook responses larger than 10 MB
webhookDataFromRequest :: AppHandler (Either String WebhookData)
webhookDataFromRequest = eitherDecode <$> SC.readRequestBody SH.size10MB

-- The different webhook handlers
handleIssueCreateWebhook :: AppHandler ()
handleIssueCreateWebhook = acceptWebhookPostMT handleCreate

handleIssueUpdateWebhook :: AppHandler ()
handleIssueUpdateWebhook = acceptWebhookPost handleUpdate

handleIssueDeleteWebhook :: AppHandler ()
handleIssueDeleteWebhook = acceptWebhookPost handleDelete

acceptWebhookPostMT :: (AC.Tenant -> IssueUpdate -> MaybeT AppHandler ()) -> AppHandler ()
acceptWebhookPostMT f = acceptWebhookPost (\t -> void . runMaybeT . f t)

acceptWebhookPost :: (AC.Tenant -> IssueUpdate -> AppHandler ()) -> AppHandler ()
acceptWebhookPost handler = SH.handleMethods [(SC.POST, WT.withTenant (handleWebhook handler))]

-- Handle the IssueUpdate once you know that you have all of the details
handleCreate :: AC.Tenant -> IssueUpdate -> MaybeT AppHandler ()
handleCreate tenant iu = do
    hackathon <- MaybeT $ PH.getHackathonByProjectId tenant (iuProjectId iu)
    reporterTU <- MaybeT $ eitherToMaybe <$> UU.getOrCreateTenantUserByKey tenant (iuReporterKey iu)
    hackathonTeam <- tryCreateTeamInHackathon tenant hackathon reporterTU iu
    void . lift . runEitherT $ TL.updateTimezoneLabels tenant hackathonTeam [reporterTU]
    lift $ handleComponentCreate tenant iu hackathon
    MaybeT $ eitherToMaybe <$> EP.updateIssueProperties tenant (iuId iu) HTP.issuePropertyKey (HTP.fromUserKeys [iuReporterKey iu])

eitherToMaybe :: Either a b -> Maybe b
eitherToMaybe (Left _) = Nothing
eitherToMaybe (Right x) = Just x

handleComponentCreate :: AC.Tenant -> IssueUpdate -> PH.Hackathon -> AppHandler ()
handleComponentCreate tenant iu hackathon =
    case filter isRoomComponent . iuComponents $ iu of
        [] -> return ()
        (selectedRoom : xs) -> withPG $ do
            potentialRoom <- runMaybeT $ HRM.getOrCreateHackathonRoom hackathon (icId selectedRoom) (icName selectedRoom)
            PHT.updateTeamRoom tenant (iuId iu) potentialRoom
            potentiallySetupRoundRoom potentialRoom
            unless (null xs) $ removeRedundantRoomsFromTeam tenant iu (icId selectedRoom)

potentiallySetupRoundRoom :: Maybe HRM.HackathonRoom -> AppHandler ()
potentiallySetupRoundRoom (Just hRoom) = void $ HRR.getOrCreateCurrentRoundRoomForRoom hRoom HRR.Active
potentiallySetupRoundRoom _ = return ()
-- Every time that we get a new workflow status id then we should try and map it to existing workflow statuses for the
-- hackathon. If we can't find that round already in the database then we need to set it to null

handleUpdate :: AC.Tenant -> IssueUpdate -> AppHandler ()
handleUpdate tenant iu = do
    handleIssueTypeUpdate tenant iu
    handleTeamMoveUpdate tenant iu
    handleStatusUpdate tenant iu
    handleComponentUpdate tenant iu
    handleSummaryUpdate tenant iu

handleIssueTypeUpdate :: AC.Tenant -> IssueUpdate -> AppHandler ()
handleIssueTypeUpdate tenant iu =
   case iuNewIssueTypeId iu of
      Nothing -> return () -- Ignore this whole branch if there is nothing to worry about
      Just newIssueTypeId -> void . withPG . runMaybeT $ do
         hackathon <- MaybeT $ PH.getHackathonByProjectId tenant (iuProjectId iu)
         if PH.hackathonTeamIssueTypeId hackathon == newIssueTypeId
            then handleCreate tenant iu
            else void . lift $ PHT.deleteHackathonTeamByIssueId tenant (iuId iu)

fromEither :: Either a b -> Maybe b
fromEither (Right x) = Just x
fromEither _ = Nothing

handleTeamMoveUpdate :: AC.Tenant -> IssueUpdate -> AppHandler ()
handleTeamMoveUpdate tenant iu = void . runMaybeT $ do
   newProjectId <- MaybeT . return . iuNewProjectId $ iu -- Do this first so that you bail early if there is no new project
   reporterTU <- MaybeT (fromEither <$> UU.getOrCreateTenantUserByKey tenant (iuReporterKey iu))
   lift $ PHT.deleteHackathonTeamByIssueId tenant (iuId iu)
   newHackathon <- MaybeT $ PH.getHackathonByProjectId tenant newProjectId -- If this is not moving to a hackathon then processing will stop here
   hackathonTeam <- tryCreateTeamInHackathon tenant newHackathon reporterTU iu
   void . lift . runEitherT $ TL.updateTimezoneLabels tenant hackathonTeam [reporterTU]

handleStatusUpdate :: AC.Tenant -> IssueUpdate -> AppHandler ()
handleStatusUpdate tenant iu =
    case iuNewWorkflowStatusId iu of
        Nothing -> return ()
        Just workflowStatusId -> withPG $ do
            potentialRound <- RND.getRoundInProject tenant (iuProjectId iu) workflowStatusId
            void $ PHT.updateTeamRound tenant (iuId iu) potentialRound

handleComponentUpdate :: AC.Tenant -> IssueUpdate -> AppHandler ()
handleComponentUpdate tenant iu =
    case iuRoomChanges iu of
        ([]    , []) -> return () -- No changes, do nothing
        ([]    , removeChanges) -> void $ PHT.clearTeamRoomIfOneOf tenant (iuId iu) (fmap rcId removeChanges)
        (x : _, _) -> do -- Set the room to x, fire a request to remove the other components if there are any
            updateResult <- runMaybeT $ updateRoomForTeam tenant iu x
            case updateResult of
                Nothing -> return () -- Not a hackathon team, so don't try and modify it's components
                Just _ -> removeRedundantRoomsFromTeam tenant iu (rcId x)

handleSummaryUpdate :: AC.Tenant -> IssueUpdate -> AppHandler ()
handleSummaryUpdate tenant iu =
    case iuNewSummary iu of
        Nothing -> return ()
        Just newSummary -> void $ PHT.updateTeamIssueSummary tenant (iuId iu) newSummary

updateRoomForTeam :: AC.Tenant -> IssueUpdate -> RoomChange -> MaybeT AppHandler ()
updateRoomForTeam tenant iu roomChange = do
    hackathon <- MaybeT $ PH.getHackathonByProjectId tenant (iuProjectId iu)
    room <- HRM.getOrCreateHackathonRoom hackathon (rcId roomChange) (rcName roomChange)
    lift $ PHT.updateTeamRoom tenant (iuId iu) (Just room)
    lift $ potentiallySetupRoundRoom (Just room)

removeRedundantRoomsFromTeam :: AC.Tenant -> IssueUpdate -> HI.ComponentId -> AppHandler ()
removeRedundantRoomsFromTeam tenant iu roomComponentId =
    unless (allComponentIds == componentIdsToKeep) $ do
        updateResult <- IU.updateIssueDetails tenant (iuId iu) issueUpdateRequest
        case updateResult of
            Left err -> SH.logErrorS . T.unpack . AC.perMessage $ err
            Right _ -> return ()
    where
        allComponentIds :: S.Set HI.ComponentId
        allComponentIds = S.fromList . fmap icId . iuComponents $ iu

        componentIdsToKeep :: S.Set HI.ComponentId
        componentIdsToKeep = S.fromList $ roomComponentId : (fmap icId . filter isStandardComponent . iuComponents $ iu)

        issueUpdateRequest = IU.IssueUpdateRequest $ IU.emptyIssueUpdate
           { IU.iudComponents = Just . S.toList $ componentIdsToKeep
           }

-- Since deletes are will probably be infrequent don't even check to see if the team exists before trying to delete it
-- It's a pretty fast operation
handleDelete :: AC.Tenant -> IssueUpdate -> AppHandler ()
handleDelete tenant iu = void $ PHT.deleteHackathonTeamByIssueId tenant (iuId iu)

tryCreateTeamInHackathon :: AC.Tenant -> PH.Hackathon -> PTU.TenantUser -> IssueUpdate -> MaybeT AppHandler PHT.HackathonTeam
tryCreateTeamInHackathon tenant hackathon tenantUser iu = do
   if (PH.hackathonTeamIssueTypeId hackathon /= iuIssueTypeId iu)
      then (MaybeT $ eitherToMaybe <$> EP.updateIssueProperties tenant (iuId iu) HTP.issuePropertyKey HTP.notTeamProperties) >> MaybeT (return Nothing)
      else withPG $ do
         potentialRound <- lift $ RND.getRoundInProject tenant (iuProjectId iu) (iuWorkflowStatusId iu)
         hackathonTeam <- MaybeT $ PHT.createHackathonTeam hackathon (iuId iu) (iuKey iu) (iuSummary iu) potentialRound Nothing
         void . lift $ PHTM.addTeamMember tenantUser (PHT.htId hackathonTeam)
         -- Add entity properties to the new team
         MaybeT $ eitherToMaybe <$> EP.updateIssueProperties tenant (iuId iu) HTP.issuePropertyKey (HTP.fromUserKeys [iuReporterKey iu])
         return hackathonTeam

-- If the key changes...that means that the project has changed, so it should be handled with the same logic
-- If the summary changes, that does not matter for us, yet

webhookDataToIssueUpdate :: WebhookData -> IssueUpdate
webhookDataToIssueUpdate webhookData = IssueUpdate
   { iuId = read . wiId . wdIssue $ webhookData
   , iuKey = wiKey . wdIssue $ webhookData
   , iuSummary = wifSummary issueFields
   , iuProjectId = textRead . wifpId . wifProject $ issueFields
   , iuIssueTypeId = textRead . wifitId . wifIssuetype $ issueFields
   , iuReporterKey = wifrKey . wifReporter $ issueFields
   , iuWorkflowStatusId = textRead . wifsId . wifStatus $ issueFields
   , iuNewWorkflowStatusId = textReadMaybe =<< findCliTo (T.pack "status")
   , iuNewProjectId = textReadMaybe =<< findCliTo (T.pack "project")
   , iuNewIssueTypeId = textReadMaybe =<< findCliTo (T.pack "issuetype")
   , iuNewKey = findCliToString (T.pack "Key") -- Key is uppercased to start
   , iuNewSummary = findCliToString (T.pack "summary") -- Summary is lower cased to start
   , iuComponents = fmap toIssueComponent . wifComponents $ issueFields
   , iuRoomChanges = getRoomChanges cli
   }
   where
      issueFields = wiFields . wdIssue $ webhookData
      cli :: [ChangeLogItem]
      cli = maybe [] wcItems . wdChangelog $ webhookData

      findCliTo :: T.Text -> Maybe T.Text
      findCliTo = cliTo <=< findCli

      findCliToString :: T.Text -> Maybe T.Text
      findCliToString = cliToString <=< findCli

      findCli :: T.Text -> Maybe ChangeLogItem
      findCli fieldName = DL.find ((==) fieldName . cliField) cli

textRead :: Read a => T.Text -> a
textRead = read . T.unpack

textReadMaybe :: Read a => T.Text -> Maybe a
textReadMaybe = readMaybe . T.unpack

byFieldName :: T.Text -> ChangeLogItem -> Bool
byFieldName fn = (==) fn . cliField

getRoomChanges :: [ChangeLogItem] -> ([RoomChange], [RoomChange])
getRoomChanges = partition isAddRoomChange . catMaybes . fmap asRoomChange . filter (byFieldName . T.pack $ "Component")

asRoomChange :: ChangeLogItem -> Maybe RoomChange
asRoomChange cli = do
    potentialChange <- case (cliFrom cli, cliTo cli) of
        (_          , Just toId)    -> Just $ AddRoomChange    (textRead toId)     (fromMaybe T.empty $ cliToString cli)
        (Just fromId, Nothing)      -> Just $ RemoveRoomChange (textRead fromId)   (fromMaybe T.empty $ cliFromString cli)
        _ -> Nothing
    strippedPrefix <- roomComponentPrefix `T.stripPrefix` rcName potentialChange
    return potentialChange { rcName = strippedPrefix }

toIssueComponent :: WebhookIssueFieldComponent -> IssueComponent
toIssueComponent c = componentConstructor (textRead . wifcId $ c) (wifcName c)
    where
        componentConstructor = if roomComponentPrefix `T.isPrefixOf` wifcName c then RoomComponent else StandardComponent

data IssueUpdate = IssueUpdate
   { iuId                  :: AC.IssueId
   , iuKey                 :: AC.IssueKey
   , iuSummary             :: AC.IssueSummary
   , iuProjectId           :: AC.ProjectId
   , iuIssueTypeId         :: Integer
   , iuReporterKey         :: AC.UserKey
   , iuWorkflowStatusId    :: Integer
   , iuNewWorkflowStatusId :: Maybe Integer
   , iuNewProjectId        :: Maybe AC.ProjectId
   , iuNewIssueTypeId      :: Maybe Integer
   , iuNewKey              :: Maybe T.Text
   , iuNewSummary          :: Maybe T.Text
   , iuComponents          :: [IssueComponent]
   , iuRoomChanges         :: ([RoomChange], [RoomChange])
   } deriving (Show)

data RoomChange
    = AddRoomChange
        { rcId   :: Integer
        , rcName :: T.Text
        }
    | RemoveRoomChange
        { rcId   :: Integer
        , rcName :: T.Text
        }
    deriving (Show)

isAddRoomChange :: RoomChange -> Bool
isAddRoomChange (AddRoomChange {}) = True
isAddRoomChange _ = False

data IssueComponent
    = RoomComponent Integer T.Text
    | StandardComponent Integer T.Text
    deriving(Show)

icName :: IssueComponent -> T.Text
icName (RoomComponent _ t) = t
icName (StandardComponent _ t) = t

icId :: IssueComponent -> Integer
icId (RoomComponent x _) = x
icId (StandardComponent x _) = x

isRoomComponent :: IssueComponent -> Bool
isRoomComponent = not . isStandardComponent

isStandardComponent :: IssueComponent -> Bool
isStandardComponent (StandardComponent {}) = True
isStandardComponent _ = False

parseOptions :: String -> Options
parseOptions prefix = defaultOptions { fieldLabelModifier = stripFieldNamePrefix prefix }

data WebhookData = WebhookData
   { wdIssue     :: WebhookIssue
   , wdChangelog :: Maybe WebhookChangelog
   } deriving (Show, Generic)

instance FromJSON WebhookData where
   parseJSON = genericParseJSON (parseOptions "wd")

data WebhookIssue = WebhookIssue
   { wiId     :: String -- This is because it gets passed as a string
   , wiKey    :: T.Text
   , wiFields :: WebhookIssueFields
   } deriving (Show, Generic)

instance FromJSON WebhookIssue where
   parseJSON = genericParseJSON (parseOptions "wi")

data WebhookIssueFields = WebhookIssueFields
    { wifProject    :: WebhookIssueFieldProject
    , wifIssuetype  :: WebhookIssueFieldIssueType
    , wifReporter   :: WebhookIssueFieldReporter
    , wifStatus     :: WebhookIssueFieldStatus
    , wifComponents :: [WebhookIssueFieldComponent]
    , wifSummary    :: T.Text
    } deriving (Show, Generic)

instance FromJSON WebhookIssueFields where
    parseJSON = genericParseJSON (parseOptions "wif")

data WebhookIssueFieldProject = WebhookIssueFieldProject
    { wifpId   :: T.Text
    , wifpKey  :: T.Text
    , wifpName :: T.Text
    } deriving (Show, Generic)

instance FromJSON WebhookIssueFieldProject where
    parseJSON = genericParseJSON (parseOptions "wifp")

data WebhookIssueFieldIssueType = WebhookIssueFieldIssueType
    { wifitId      :: T.Text
    , wifitSubtask :: Bool
    } deriving (Show, Generic)

instance FromJSON WebhookIssueFieldIssueType where
    parseJSON = genericParseJSON (parseOptions "wifit")

data WebhookIssueFieldReporter = WebhookIssueFieldReporter
    { wifrKey :: AC.UserKey
    } deriving (Show, Generic)

instance FromJSON WebhookIssueFieldReporter where
    parseJSON = genericParseJSON (parseOptions "wifr")

data WebhookIssueFieldStatus = WebhookIssueFieldStatus
    { wifsId   :: T.Text
    , wifsName :: T.Text
    } deriving (Show, Generic)

instance FromJSON WebhookIssueFieldStatus where
    parseJSON = genericParseJSON (parseOptions "wifs")

data WebhookIssueFieldComponent = WebhookIssueFieldComponent
    { wifcId   :: T.Text
    , wifcName :: T.Text
    } deriving (Show, Generic)

instance FromJSON WebhookIssueFieldComponent where
    parseJSON = genericParseJSON (parseOptions "wifc")

data WebhookChangelog = WebhookChangelog
   { wcItems :: [ChangeLogItem]
   } deriving (Show, Generic)

instance FromJSON WebhookChangelog where
   parseJSON = genericParseJSON (parseOptions "wc")

data ChangeLogItem = ChangeLogItem
   { cliField      :: T.Text
   , cliFrom       :: Maybe T.Text
   , cliFromString :: Maybe T.Text
   , cliTo         :: Maybe T.Text
   , cliToString   :: Maybe T.Text
   } deriving (Show, Generic)

instance FromJSON ChangeLogItem where
   parseJSON = genericParseJSON (parseOptions "cli")
