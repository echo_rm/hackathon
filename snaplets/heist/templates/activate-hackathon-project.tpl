<apply template="connect-panel">
    <bind tag="body-class">activate-hackathon-project</bind>
    <bind tag="header-extra">
        <js src="/static/${resourcesVersion}/js/app/activate-hackathon-project.js" />
        <script>require(['app/activate-hackathon-project']);</script>

        <script id="hackathon-option" type="x-tmpl-mustache">
            <option value="{{id}}" {{#selected}}selected="selected"{{/selected}}>{{name}}</option>
        </script>

        <script id="hackathon-duration" type="x-tmpl-mustache">
            This hackathon will run for {{hours}} hours.
        </script>

        <script id="round-status-mapper" type="x-tmpl-mustache">
            <div class="field-group dynamic-round last-round">
                <label for="status-dynamic-round-{{roundNumber}}">Round {{roundNumber}}</label>
                <select
                    data-sequence-number="{{roundNumber}}"
                    class="select round voteable-round"
                    id="status-dynamic-round-{{roundNumber}}"
                    name="status-dynamic-round-{{roundNumber}}"></select>
                <span class="aui-icon aui-icon-small aui-iconfont-list-remove remove-round"></span>
            </div>
        </script>
    </bind>

    <h1>Activate hackathon</h1>
    <p>You can turn this JIRA Project into a hackathon project by mapping existing JIRA concepts to hackathon concepts.</p>

    <!-- Error message if your user is missing browse users permission -->
    <div id="missing-browse-users" class="aui-message aui-message-error hidden">
      <p class="title">
         <strong>The <a href="${productBaseUrl}/secure/admin/user/ViewUser.jspa?name=addon_${pluginKey}" target="_top">Hackathon add-on user</a> does not have the global 'Browse Users' permission.</strong> </p>
      <p>Hackathon needs the addon user to have this permission in order to function correctly. Please make sure that
      this user has the global <a href="${productBaseUrl}/secure/admin/GlobalPermissions!default.jspa" target="_top">Browse Users</a> permission before using this add-on
      otherwise this add-on will fail to work as expected.</p>
    </div>

    <!-- Error message if your user is missing project admin permission -->
    <div id="missing-project-admin" class="aui-message aui-message-error hidden">
      <p class="title">
         <strong>The <a href="${productBaseUrl}/secure/admin/user/ViewUser.jspa?name=addon_${pluginKey}" target="_top">Hackathon add-on user</a> does not have the project admin permission for this project.</strong> </p>
      <p>Hackathon needs the addon user to have this permission in order to function correctly. Please make sure that
      this user has the <a id="missing-project-admin-roles-link" href="#" target="_top">Project Administration</a> permission before using this add-on
      otherwise this add-on will fail to work as expected.</p>
    </div>

    <form action="#" method="post" id="d" class="aui">
        <h2>Hackathon details</h2>
        <fieldset>
            <div class="field-group">
                <label for="hackathon-start">Hackathon begins at</label>
                <input class="text" id="hackathon-start" type="text" />
            </div>
            <div class="field-group">
                <label for="hackathon-end">Hackathon ends at</label>
                <input class="text" id="hackathon-end" type="text" />
                <div id="hackathon-ends-description" class="description">Set dates for your hackathon projects so that you can track them.</div>
            </div>
            <div class="field-group">
                <label for="hackathon-issue-type">Hackathon team issue type</label>
                <select id="hackathon-issue-type" class="select"></select>
                <div class="description">Issues of this type will be treated as hackathon teams.</div>
            </div>
            <div class="field-group">
                <input id="single-hackathon-voting-room-checkbox" type="checkbox" />
                <span>One room for voting?</span>
                <div class="description">Check this if everyone's going to be voting in a single room.</div>
            </div>
            <div class="field-group">
                <input class="text hidden" id="hackathon-voting-room-name" type="text" placeholder="Voting Venue Name"/>
            </div>
        </fieldset>

        <h3>Hackathon team statuses</h3>
        <p>
            Hackathon teams can be in multiple different states. These states will be tracked using this JIRA Project's workflow.
            You need to map JIRA Workflow statuses to hackathon rounds. We highly recommend that you download the standard
            hackathon workflow and apply it to this project before doing the mapping.
        </p>
        <fieldset class="round-container">
            <legend><span>Hackathon team statuses</span></legend>
            <div class="field-group">
                <label for="status-static-team-formation">Team Formation</label>
                <select class="select round" id="status-static-team-formation" name="status-static-team-formation">
                    <option>Team Formation</option>
                </select>
            </div>
            <div class="field-group">
                <label for="status-static-outvoted">Outvoted</label>
                <select class="select round" id="status-static-outvoted" name="status-static-outvoted">
                    <option>Outvoted</option>
                </select>
            </div>
            <div class="field-group">
                <label for="status-static-winner">Winner</label>
                <select class="select round" id="status-static-winner" name="status-static-winner">
                    <option>Winner</option>
                </select>
            </div>
            <div class="field-group">
                <label for="status-static-round-1">Round 1</label>
                <select data-sequence-number="1" class="select round voteable-round" id="status-static-round-1" name="status-static-round-1">
                    <option>Round 1</option>
                </select>
            </div>
            <div id="hackathon-status-buttons" class="buttons-container">
                <div class="buttons">
                    <input class="button submit" type="submit" value="Add round" id="add-round-button">
                </div>
            </div>
            <div id="final-buttons" class="buttons-container">
                <div class="buttons">
                    <button id="activate-hackathon" class="aui-button aui-button-primary">Activate</button>
                </div>
            </div>
        </fieldset>

    </form>
</apply>
