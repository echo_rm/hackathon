<!DOCTYPE html>
<html lang="en">
    <head profile="http://www.w3.org/2005/10/profile">
        <apply template="headers/requirejs" />

        <link rel="icon" type="image/ico" href="/static/${resourcesVersion}/images/hackathon-favicon.ico" />
        <css href="/static/${resourcesVersion}/css/app.css" />

        <apply template="headers/aui" />
        <apply template="headers/aui-experimental" />

        <js src="/static/${resourcesVersion}/js/app/hackathon-leaderboard.js" />
        <script>require(['app/hackathon-leaderboard']);</script>

        <script id="team-row" type="x-tmpl-mustache">
            <li data-team-id="{{id}}" class="hackathon-team {{teamRank}} hidden">
                <div class="aui-group">
                    <div class="aui-item icon-container">
                        <div class="trophy-icon"></div>
                    </div>
                    <div class="aui-item details-container">
                        <h2><a href="{{issueLink}}" target="_blank">{{issueSummary}}</a></h2>
                        <div class="team-details">
                            {{#teamMembers}}
                            <div class="team-member">
                                <span class="aui-avatar aui-avatar-small">
                                    <span class="aui-avatar-inner">
                                         <img src="{{smallAvatarUrl}}" />
                                    </span>
                                </span> {{displayName}}
                            </div>
                            {{/teamMembers}}
                        </div>
                    </div>
                    <div class="aui-item votes-container">
                        <span class="votes">{{votes}}</span>
                    </div>
                </div>
            </li>
        </script>

        <script id="vote-count-message" type="x-tmpl-mustache">
            <p>Total votes tallied in this room: {{votes}}</p>
        </script>

        <script id="locked-message" type="x-tmpl-mustache">
            <span>Voting is now locked. No more votes may be placed...<br />
            {{#isTieOnThird}}<span class="tie">There is a tie on third place!</span><br />{{/isTieOnThird}}
            {{#isTieOnSecond}}<span class="tie">There is a tie on second place!</span><br />{{/isTieOnSecond}}
            {{#isTieOnFirst}}<span class="tie">There is a tie on first place!</span><br />{{/isTieOnFirst}}
            (Press spacebar to reveal each result)</span>
        </script>

        <script id="congratulations-third" type="x-tmpl-mustache">
            <span>Congratulations to third place!</span>
            {{#isTieOnSecond}}<span class="tie">There is a tie on second place!</span><br />{{/isTieOnSecond}}
            {{#isTieOnFirst}}<span class="tie">There is a tie on first place!</span><br />{{/isTieOnFirst}}
        </script>

        <script id="congratulations-second" type="x-tmpl-mustache">
            <span>Congratulations to second place!</span>
            {{#isTieOnFirst}}<span class="tie">There is a tie on first place!</span><br />{{/isTieOnFirst}}
        </script>

        <script id="congratulations-all" type="x-tmpl-mustache">
            <span>Congratulations to the winners!</span>
        </script>

        <title>Hackathon leaderboard</title>
    </head>
    <body class="body hackathon-leaderboard">
        <div id="leaderboard-container" class="leaderboard">
            <button id="lock-toggle-button" class="aui-button aui-button-primary lock-button"><span class="aui-icon aui-icon-small aui-iconfont-locked lock-icon"></span> <span class="description-text">Lock voting</span></button>
            <button id="reset-reveal-button" class="aui-button reset-button"><span class="aui-icon aui-icon-small aui-iconfont-undo"></span> <span class="description-text">Reset reveal</span></button>
            <div class="header-icon">
                <!-- TODO let the administrator provide this image to theme their hackathon. https://ecosystem.atlassian.net/browse/HACK-79 -->
                <img src="/static/images/shipit-badge.png" alt="Image for Hackathon" width="160"/>
            </div>
            <div class="leaderboard-container">
                <h1 class="title">
                    <div id="locked-icon" class="locked hidden"></div><span>Round Title</span>
                </h1>
                <div class="subtitle">
                    <h2>Hackathon Title</h2>
                </div>
                <div id="status-box" class="status"></div>
                <div class="qr-container">
                   <h2>To vote: <span class="vote-link"></span></h2>
                   <div id="voting-qr-code"></div>
                   <span>(Click to enlarge)</span>
                </div>
                <div id="notProjectAdminError" class="aui-message warning shadowed hidden">
                    <p class="title">
                        <span class="aui-icon icon-error"></span>
                        <strong>Voting is still underway!</strong>
                    </p>

                    <p>Check back here after your Hackathon administrator has locked voting for this room to see the results.<br /><br />
                        If you expected to be able to lock the leaderboard then you were not able to because you do not have
                        Project Administrator permissions to this JIRA project. You should request the permission to lock the
                        leaderboard from your Hackathon organiser.
                    </p>
                </div>
                <ul id="teams"></ul>
            </div>

            <div class="lightbox hidden">
               <div class="header">
                  <div class="close"><span class="aui-icon aui-icon-small aui-iconfont-close-dialog icon-white">Close</span></div>
                  <div class="title">To vote: <span class="vote-link"></span> <span class="aui-lozenge aui-lozenge-current vote-count">0</span></div>
               </div>
               <div class="image">
               </div>
            </div>

            <div class="instructions hidden">
                <p>Here are the commands that you can use:</p>
                <ul>
                    <li>Press 'Enter' or 'Space' to reveal another result. The first press locks the voting (unless your refresh the screen).</li>
                    <li>Press 's' to show all results and lock the current round.</li>
                    <li>Press 'r' to refresh the results and the revealed results back to nothing.</li>
                </ul>
            </div>
        </div>
        <footer id="footer">
            <section class="footer-body" style="background: none;">
                <ul>
                    <li>Copyright &copy; 2015 <a href="http://www.atlassian.com" targte="_blank">Atlassian</a>. All rights reserved</li>
                    <li><a href="/">Hackathon</a></li>
                </ul>
                <div id="footer-logo" style="display: none;"><a href="http://www.atlassian.com/" target="_blank">Atlassian</a></div>
            </section>
        </footer>
    </body>
</html>
