define([
   'd3',
   '../helpers/PageContext', 
   'connect/pagetoken'
], 
function(d3, PC) {
   var requestTeamSizes = function(projectId) {
      return AJS.$.ajax({
         url: '/rest/statistics/team-sizes',
         type: 'GET',
         cache: false,
         data: {
            projectId: projectId
         }
      });
   };

   var requestRoundVotes = function(projectId) {
      return AJS.$.ajax({
         url: '/rest/statistics/round-votes',
         type: 'GET',
         cache: false,
         data: {
            projectId: projectId
         }
      });
   };

   var translate = function(x, y) {
      return "translate(" + x + "," + y + ")";
   };

   var compareOn = function(get, comp) {
      return function(a, b) {
         return comp(get(a), get(b));
      };
   };

   var renderTeamSizes = function(selector, dataArray, userOptions) {
      var defaultOptions = {
         ylabel: "Frequency",
         width: 960,
         height: 500,
         getX: function(d) { return d.x; },
         getY: function(d) { return d.y; }
      };
      var options = AJS.$.extend({}, defaultOptions, userOptions);

      dataArray = dataArray.sort(compareOn(options.getX, d3.ascending));

      var margin = {top: 20, right: 20, bottom: 30, left: 40},
          marginWidth = margin.left + margin.right,
          marginHeight = margin.top + margin.bottom,
          width = options.width - marginWidth,
          height = options.height - marginHeight;

      var x = d3.scale.ordinal()
          .rangeRoundBands([10, width], .1);

      var y = d3.scale.linear()
          .range([height, 0]);

      var xAxis = d3.svg.axis()
          .scale(x)
          .orient("bottom");

      var yAxis = d3.svg.axis()
          .scale(y)
          .orient("left");

      var svg = selector.append("svg")
          .attr("width", width + marginWidth)
          .attr("height", height + marginHeight)
        .append("g")
          .attr("transform", translate(margin.left, margin.top));

      var yx = function(d) {
         return options.getY(d) * options.getX(d);
      }

      x.domain(dataArray.map(options.getX));
      y.domain([0, d3.max(dataArray, yx)]);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", translate(0, height))
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
          .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(options.ylabel);

        var entry = svg.selectAll(".bar").data(dataArray).enter();
        entry.append("rect")
               .attr("class", "bar teams")
               .attr("x", function(d) { return x(options.getX(d)); })
               .attr("width", x.rangeBand() / 2)
               .attr("y", function(d) { return y(options.getY(d)); })
               .attr("height", function(d) { return height - y(options.getY(d)); });
        entry.append("rect")
               .attr("class", "bar competitors")
               .attr("x", function(d) { return x(options.getX(d)) + x.rangeBand() / 2; })
               .attr("width", x.rangeBand() / 2)
               .attr("y", function(d) { return y(yx(d)); })
               .attr("height", function(d) { return height - y(yx(d)); });
   };

   var renderHistogram = function(selector, dataArray, userOptions) {
      var defaultOptions = {
         ylabel: "Frequency",
         width: 960,
         height: 500,
         getX: function(d) { return d.x; },
         getY: function(d) { return d.y; }
      };
      var options = AJS.$.extend({}, defaultOptions, userOptions);

      dataArray = dataArray.sort(compareOn(options.getX, d3.ascending));

      var margin = {top: 20, right: 20, bottom: 30, left: 40},
          marginWidth = margin.left + margin.right,
          marginHeight = margin.top + margin.bottom,
          width = options.width - marginWidth,
          height = options.height - marginHeight;

      var x = d3.scale.ordinal()
          .rangeRoundBands([10, width], .1);

      var y = d3.scale.linear()
          .range([height, 0]);

      var xAxis = d3.svg.axis()
          .scale(x)
          .orient("bottom");

      var yAxis = d3.svg.axis()
          .scale(y)
          .orient("left");

      var svg = selector.append("svg")
          .attr("width", width + marginWidth)
          .attr("height", height + marginHeight)
        .append("g")
          .attr("transform", translate(margin.left, margin.top));

        x.domain(dataArray.map(options.getX));
        y.domain([0, d3.max(dataArray, options.getY)]);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", translate(0, height))
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
          .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(options.ylabel);

        svg.selectAll(".bar")
            .data(dataArray)
          .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function(d) { return x(options.getX(d)); })
            .attr("width", x.rangeBand())
            .attr("y", function(d) { return y(options.getY(d)); })
            .attr("height", function(d) { return height - y(options.getY(d)); });
   };

   AJS.$(function() {
      var pageContext = PC.load();

      var loadStats = function() {
         loadTeamSizes();
         loadRoundVotes();
      };

      var loadTeamSizes = function() {
         var request = requestTeamSizes(pageContext.project.id);

         request.done(function(data) {
            AJS.$("#users-in-teams").empty();
            AJS.$("#no-users-in-teams").toggleClass("hidden", data.counts.length > 0);
            if(data.counts && data.counts.length > 0) {
               renderTeamSizes(d3.select("#users-in-teams"), data.counts, {
                  width: 800,
                  height: 400,
                  ylabel: "Teams"
               });
            }
         });

         request.fail(function() {
            AP.message('messages', function(message) {
               message.error("Could not load team size information.", "", {
                  delay: 5000
               });
            });
         });
      };

      var loadRoundVotes = function() {
         var request = requestRoundVotes(pageContext.project.id);

         request.done(function(data) {
            AJS.$("#votes-per-round").empty();
            var getCount = function(d) { return d.count; };
            var maxCount = d3.max(data.counts, getCount);
            AJS.$("#no-votes-per-round").toggleClass("hidden", maxCount > 0);
            if(data.counts && data.counts.length > 0) {
               renderHistogram(d3.select("#votes-per-round"), data.counts, {
                  width: 800,
                  height: 400,
                  ylabel: "Votes",
                  getX: function(d) { return d.label; },
                  getY: getCount
               });
            }
         });

         request.fail(function() {
            AP.message('messages', function(message) {
               message.error("Could not load voting information.", "", {
                  delay: 5000
               });
            });
         });
      };

      AJS.$(window).focus(function() {
         loadStats();
      });
      loadStats();
   });
});
