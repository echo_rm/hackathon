{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Persistence.Statistics
   ( BasicStats(..)
   , HistogramRow(..)
   , LabelAndCount(..)
   , getBasicStats
   , getTeamsPerHackathon
   , getVotesPerHackathon
   , getVotesPerRound
   , getVotesPerRoom
   , getTeamMemberCounts
   , getVotesPerRoundByHackathon
   , countTeamsInRoundTypeForHackathon
   , countUniqueTeamMembersForHackathon
   , countUniqueTeamMembersForIssueKeys
   , countVotesPerRoundForIssueKeys
   ) where

import           AesonHelpers
import           Application
import           Data.Aeson
import           Data.Aeson.Types                 (fieldLabelModifier)
import qualified Data.HackathonConstants          as HC
import qualified Data.Text                        as T
import           Database.PostgreSQL.Simple.SqlQQ
import           GHC.Generics
import qualified Persistence.Hackathon            as PH
import qualified Snap.AtlassianConnect              as AC
import           Snap.Snaplet.PostgresqlSimple

data BasicStats = BasicStats
   { bsActiveTenants   :: Integer
   , bsInactiveTenants :: Integer
   , bsHackathons      :: Integer
   , bsTeams           :: Integer
   , bsVotes           :: Integer
   } deriving (Show, Generic)

instance FromRow BasicStats where
   fromRow = BasicStats <$> field <*> field <*> field <*> field <*> field

instance ToJSON BasicStats where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "bs" }

data HistogramRow = HistogramRow
   { hrX :: Integer
   , hrY :: Integer
   } deriving (Show, Generic)

instance FromRow HistogramRow where
   fromRow = HistogramRow <$> field <*> field

instance ToJSON HistogramRow where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "hr" }

data LabelAndCount = LabelAndCount
   { lacLabel :: T.Text
   , lacCount :: Integer
   } deriving (Show, Generic)

instance FromRow LabelAndCount where
   fromRow = LabelAndCount <$> field <*> field

instance ToJSON LabelAndCount where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "lac" }

getBasicStats :: AppHandler BasicStats
getBasicStats = head <$> query_
   [sql|
   select
      (select count(*) from tenant where sleep_date is null) as active_tenants,
      (select count(*) from tenant where sleep_date is not null) as inactive_tenants,
      (select count(*) from hackathon) as hackathons,
      (select count(*) from hackathon_team) as teams,
      (select count(*) from vote) as votes
   |]

getTeamsPerHackathon :: AppHandler [HistogramRow]
getTeamsPerHackathon = query_
   [sql|
   select ((select count(*) from hackathon_team ht where hackathon_id = h.id) / 5 * 5) as new_x, count(*) as new_y
   FROM hackathon h
   GROUP BY new_x
   |]

getVotesPerHackathon :: AppHandler [HistogramRow]
getVotesPerHackathon = query_
   [sql|
   select ((select count(*) from vote v, hackathon_team ht where v.hackathon_team_id = ht.id and ht.hackathon_id = h.id) / 5 * 5) as new_x, count(*) as new_y
   FROM hackathon h
   GROUP BY new_x
   |]

getVotesPerRound :: AppHandler [HistogramRow]
getVotesPerRound = query_
   [sql|
   select ((select count(*) from vote v where v.round_id = r.id) / 5 * 5) as new_x, count(*) as new_y
   FROM round r
   GROUP BY new_x
   |]

getVotesPerRoom :: AppHandler [HistogramRow]
getVotesPerRoom = query_
   [sql|
   select ((select count(*) from vote v where v.room_id = r.id) / 5 * 5) as new_x, count(*) as new_y
   FROM room r
   GROUP BY new_x
   |]

getTeamMemberCounts :: PH.Hackathon -> AppHandler [HistogramRow]
getTeamMemberCounts hackathon = query
   [sql|
   SELECT (select count(*) from team_members tm where tm.hackathon_team_id = ht.id) as new_x, count(*) as new_y
   FROM hackathon_team ht
   WHERE ht.hackathon_id = ?
   GROUP BY new_x
   |] (Only . PH.hackathonId $ hackathon)

getVotesPerRoundByHackathon :: PH.Hackathon -> AppHandler [LabelAndCount]
getVotesPerRoundByHackathon hackathon = query
   [sql|
   SELECT r.name as label, (SELECT count(*) FROM vote where round_id = r.id) as count
   FROM round r
   WHERE r.hackathon_id = ?
   AND r.sequence_number IS NOT NULL
   ORDER BY r.sequence_number
   |] (Only . PH.hackathonId $ hackathon)

countTeamsInRoundTypeForHackathon :: PH.Hackathon -> HC.HackathonRoundType -> AppHandler Integer
countTeamsInRoundTypeForHackathon hackathon hrType = head . fmap fromOnly <$> query
   [sql|
   SELECT count(ht.*)
   FROM hackathon_team ht, round r
   WHERE ht.round_id = r.id
   AND r.type = ?
   AND ht.hackathon_id = ?
   |] (hrType, PH.hackathonId hackathon)

countUniqueTeamMembersForHackathon :: PH.Hackathon -> AppHandler Integer
countUniqueTeamMembersForHackathon hackathon = head . fmap fromOnly <$> query
   [sql|
   SELECT count(DISTINCT tm.tenant_user_id)
   FROM team_members tm, hackathon h, hackathon_team ht
   WHERE h.id = ?
   AND ht.hackathon_id = h.id
   AND tm.hackathon_team_id = ht.id
   |] (Only . PH.hackathonId $ hackathon)

countUniqueTeamMembersForIssueKeys :: AC.Tenant -> [T.Text] -> AppHandler Integer
countUniqueTeamMembersForIssueKeys tenant issueKeys = head . fmap fromOnly <$> query
    [sql|
    SELECT count(DISTINCT tm.tenant_user_id)
    FROM team_members tm, hackathon h, hackathon_team ht, tenant t
    WHERE t.id = ?
    AND h.tenant_id = t.id 
    AND ht.hackathon_id = h.id
    AND ht.issue_key in ?
    AND tm.hackathon_team_id = ht.id;
    |] (AC.tenantId tenant, In issueKeys)

countVotesPerRoundForIssueKeys :: AC.Tenant -> [T.Text] -> AppHandler [LabelAndCount]
countVotesPerRoundForIssueKeys tenant issueKeys = query
    [sql|
    SELECT r.name as label, (SELECT count(*) FROM vote v, hackathon_team ht where v.round_id = r.id AND v.hackathon_team_id = ht.id AND ht.issue_key in ?) as count
    FROM round r, hackathon h, tenant t
    WHERE t.id = ?
    AND h.tenant_id = t.id
    AND r.hackathon_id = h.id
    AND r.sequence_number IS NOT NULL
    ORDER BY r.sequence_number
    |] (In issueKeys, AC.tenantId tenant)

-- Admins are interested in the following statistics
-- How many teams do I have with X team members? (with the total number of team members)
-- How many votes were placed in each room in each round? (Bar chart)
