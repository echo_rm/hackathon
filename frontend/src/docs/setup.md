# Getting started

You are about to run your own hackathon: congratulations! Hackathon (the add-on) is the best way to run
a hackathon no matter wether there are 10 people competing or 10000. But what you want to know is:

> How do I get started with Hackathon in Cloud?

To let you run your own hackathons we are going to explain:

 - What makes a hackathon
 - How Hackathon tightly integrates with JIRA: unleashing the power of your hackathon
 - How to create a JIRA Project for your Hackathon
 - How to create a Workflow that represents how your hackathon will run
 - How to configure issue types for the Hackathon
 - How to use roles for Hackathon permissions
 - How to setup a Hackathon Notification Scheme
 - How to turn that JIRA Project into a Hackathon

Hackathon was designed with running large hackathon's in mind: this is because smaller hackathon's
can just ignore the features that were designed for running larger ones. In this guide we will let
you know when the features or configuration options that we are talking about are only relevant to
large teams.

## What makes a hackathon?

Before we walk through Hackathon we should first explore what key ideas exist in a hackathon:

 - The hackathon will have a name  
   For example, at Atlassian we have numbered hackathons "ShipIt 31", "ShipIt 32" etc.
 - You hackathon will have teams  
   You can have two or more teams in a Hackathon; a Hackathon with only one team is very boring.
 - Teams have Team Members  
   You can have a only one person on a 'team' which we will call a "solo team" or you can have many.

Each of the teams in your hackathon will present / demo their projects to a wider audience at the
end of the hackathon. Their audience will be made up of other *competitors* and *spectators*.

Atlassian usually gives teams 3-5 minutes to present their projects to the audience. Since we have
150 teams in Sydney that would be a minimum of 450 minutes worth of presentations: that's 7 hours
and 30 minutes of presentations minimum! This means that we cannot simply stick all of these teams
in the one room and have them present one after another. Instead are compelled to split them up into
smaller groups of teams and have a quater-finals, semi-finals and grand-final. In the quarter-finals
and semi finals we split up the teams over multiple rooms (or other locations). Each room usually
has 10-15 teams in it so that presentations will finish within one hour.

In the Hackathon addon you can model this structure:

 - Quater finals, semi finals and grand finals are known in Hackathon as *rounds*.
 - In quarter and semi finals teams gather in different presenting locations to demo their
   projects. This may be a stadium, auditorium, kitchen or meeting room. All of these different
   locations are collectively referred to as *rooms* in Hackathon. A room is any geographic location
   that a team will be presenting their project.

Therefore each round has multiple rooms. As the hackathon progresses teams will move through the
different rounds and rooms. This means that you can have one room in Emerica in round one while you
have another room in Europe in round two: using the hackathon addon for a global hackathon natively
supported.

After teams have finished presenting their projects the voting begins.  Competitors and spectators
alike pull out their mobile phones (or other devices with a web browser) and navitage to the voting app.
From there they will place their votes on the teams. This is repeated for each round until an ultimate
winner is selected for your hackathon.

This is the general way in which a hackathon runs and Hackathon fully supports this workflow; and
more.

## Tight integration with JIRA

One of the best features of Hackathon is that almost all of the hackathon concepts map directly to
JIRA concepts. Lets run through the mapping now:

 - A *hackathon* is represented as a JIRA project.
 - A *team* is represented as a JIRA Issue in that project.
 - A *team member* is a JIRA User that has been added to a *team*. By default, the reporter of an
   issue is the first team member.
 - A *round* is a Workflow Status (because workflows have transitions and are a natural way to think
   about teams moving through rounds).
 - A *room* is a Component on each team. (We use components instead of labels because components cannot
   be created by people that are not project administrators of the JIRA project)

If you prefer diagrams then this explains the previous points visually:

![hackathon mapped to jira][1]

As you can see most hackathon concepts have an equivalent concept in JIRA. This means that you can
use the standard JIRA interface (like the View Issue screen and the Bulk Edit tool) to manipulate
hackathon teams and everything will still work perfectly. This lets you harness the full power of
JIRA to manage your Hackathons.

However, the most important feature that JIRA does not give you is the ability to vote on teams.
This is because the 'Vote' button on JIRA has the following issues:

 - It has no concept of rounds and rooms.
 - You cannot vote on any issue more than once.
 - All votes that are made are public by default.
 - Votes can be added and removed at any time (even once voting has ended).
 - You have to log in to vote.

All of these points mean that you should not use the standard voting mechanism in JIRA to run a
hackathon. Instead you should use Hackathon. Hackathon provides an excellent voting mechanism!
Hackathon extends the power of JIRA to give you ultimate control and power over how your Hackathon
runs.

## Get JIRA

In order to use this addon you need to have JIRA Core; which you can either purchase individually or
comes bundled with JIRA Software and JIRA Servicedesk. If you already have a Cloud instance of JIRA
then you can use that and move on to the next step.

If you don't then you will need to [go and get one][2]. Follow that link now and get a Cloud
instance of JIRA. When you have successfully logged in then follow the steps below.

## Install Hackathon into your JIRA

Note: To install Hackathon in your JIRA instance you need to be a JIRA Administrator. If you are the
person that created the instance then you will be an administrator by default. If you are not then
you will need their help to finish this process.

You now need to install Hackathon via the Atlassian Marketplace. To do this follow these steps:

 1. Log into your JIRA Cloud instance as an administrator.
 1. Click the '.' button on your keyboard and a search dialog will appear.
 1. Type in 'Find new add-ons' and select that option. You should see something like this:  
    <img width="600" src="/static/images/docs/find-new-addons.png" />
 1. In the search box titled 'Search the Marketplace' search for 'Hackathon'.
 1. When Hackathon appears in the search results hit the Install buttons and follow the prompts.

This will result in Hackathon being installed on your JIRA Instance.

## Creating a hackathon (a new JIRA project)

To create a new Hackathon you need to create a new JIRA Project. We are going to be heavily
customising this JIRA Project so the template that you use does not really matter. With that in mind
click on "Projects" in the header and select "Create new project". Then create a "Simple Issue Tracking" project:

![create project dialog][4]

When deciding upon a project name and key please keep in mind that you will probably run more than
hackathon in your JIRA Cloud Instance. Thus it is usually a good idea to add numbers to your
hackathon name and key. For example, for ShipIt 33 at Atlassian we called the project: "Global
ShipIt 33" and the project key used roman numerals as follows "SHPXXXII". This is a good pattern
that you should use.

**IMPORTANT:** If you have followed all of these steps before then you can use the "Create with
shared configuration" button on the project create dialog to copy all of the settings from a
previous Hackathon JIRA Project. This removes almost all of the burden of configuring your new
Hackathon. If you do that then you can probably move straight from this step to Associating the JIRA
Project with a Hackathon.

## Creating a Hackathon Workflow

This section will be improved but for now you can just download the [Hackathon Workflow JIRA
Workflow Bundle][5] and import that into your JIRA project to be used as your workflow.

It contains the following workflow:

<img width="500" src="/static/images/docs/standard-hackathon-workflow.png" />

As you can see this workflow has:

 - Three rounds of voting (Quarter, Semi and Grand finals)
 - The ability to mark projects as Winners or Outvoted.
 - A post hackathon flow; so that you can track projects after the hackathon has finished.
 - The ability to trach projects that are still in team formation.

This workflow can be further customised to make it more robust. For example almost all of the
transitions that you perform on Hackathon issues should be restricted to the people that are
running the hackathon. You want to make it so that only the people running the hackathon can use the
following transitions:

 - Team Formation to Round 1
 - Round 1 to Round 2
 - Round 2 to Round 3
 - Round 3 to Winner
 - Any Round to Outvoted

You want to do this so that teams cannot cheat and manually move themselves into the next round;
this would be unfair. A good way to do this for the transitions is to edit the Conditions of each of
the Transitions and adding the "User is in Project Role" condition restricted to "Administrators".
This will mean that only Project Administrators can perform these transitions. Which is exactly what
you want. You should make the people that will be facillitating your Hackathon project
administrators.

<img width="600" src="/static/images/docs/transition-project-admin-condition.png" style="border: 1px solid black"/>

## Creating a Hackathon Project Issue Type

In JIRA you can have issues of many different types in every project; this are represented in JIRA
using [Project Issue Types][7]. For a Hackathon we only need two issue types:

 - Hackathon Team  
   This issue type represents a team that will compete in your hackathon. Every team that wants to
   participate in your hackathon must create an issue of this type (or you have to do it for them).
 - Hackathon Task  
   This issue type will be a subtask issue type. It is there so that teams can create JIRA issues to
   work on their team project while participating in the hackathon. These issues will be ignored for
   voting purposes.

You will need to create these two issue types. To do so click the '.' button on your keyboard and
navigate to "Issue types". On this page you can add two new issue types. First create the Hackathon
Team issue type:

<img width="500" src="/static/images/docs/hackathon-team-issue-type.png" />

And then the Hackathon Task issue type:

<img width="500" src="/static/images/docs/hackathon-task-issue-type.png" />

Note: please make sure you make this a sub-task issue type.

## Creating a Hackathon Issue Type Scheme

In your hackathon you only want two different types of issues: Hackathon Teams and Tasks. To
restrict the issue types that are avaliable to your Hackathon project you need to create an Issue
Type Scheme. Use the '.' dialog to navigate to "Issue type schemes" and add a new scheme that looks
like this:

<img width="500" src="/static/images/docs/hackathon-issue-type-scheme.png" style="border: 1px solid black;"/>

You will notice that this issue type scheme has the following properties:

 - Only the Hackathon Team and Task issue types are included.
 - The default issue type is the Hackathon Team

Creating a Issue Type Scheme like this sets us up to have the hackathon that we desire.

## Creating a Hackathon Workflow Scheme

In your hackathon you want the Hackathon Team JIRA issues to follow the Hackathon workflow that we
previously created. You will also want the Hackathon Task JIRA issues to follow a standard JIRA
workflow that is better suited to finishing tasks. You can make issues with different issue types
in the same JIRA project follow different Workflows by creating workflow schemes.

Use the '.' dialog to navigate to the workflow schemes page, on that page you should create a new
Workflow scheme that looks like this:

<img width="600" src="/static/images/docs/hackathon-workflow-scheme.png" style="border: 1px solid black" />

This workflow scheme has the following properties:

 - Hackathon Teams must folow the hackathon workflow.
 - All other issues (the Hackathon Tasks) will follow the default JIRA workflow.

Create this scheme and feel free to customise it to fit your needs.

## Applying your schemes to your Hackathon JIRA Project

Now that you have created the issue type and workflow schemes you will need to associate them with
your Hackathon JIRA project. Navigate to the administration section of your hackathon JIRA project
and in the "Issue types" and "Workflow" sections associate the two workflow schemes that you just
created with this JIRA Project.

Once you are done you should be able to navigate to the JIRA administration Summary page for your
hackathon project and see something like the following:

<img width="500" src="/static/images/docs/properly-configured-hackathon.png" style="border: 1px solid black" />

## Using roles for Hackathon Permissions

You have two different types of JIRA users in your hackathon:

 - Competitors  
   A competitor (or spectator) is a person that is either on a Hackathon Team or voting on teams.
   They should be able to participate in the hackathon, see the jira projects and vote on them.
   However, they should NOT be able to decide who moves from one round to the next or who the
   winners are.
 - Facillitators  
   A hackathon facillitator is one of the many people running the hackathon. They should have the
   ability to decide who moves into the next round, who wins, how the hackathon is configured and
   decide when voting starts and ends.

To distinguise between these two types of users in JIRA we use project roles. Navigate to the "Users
and roles" administration page in your Hackathon JIRA project. The people that are your competitors
should be given "users" and "developers" access while the people that are your facillitators should
be given "administrators" access. This is very important as parts of the hackathon will not be
accissible to somebody that does not have these permissions.

## Associating the JIRA Project with a Hackathon

Congratulations! You have now properly configured your JIRA project to act as a hackathon; only one
step remains. We are not going to associate this JIRA project as a Hackathon. To do so navigate to
your Hackathon JIRA project administration page, once there you will find a link called "Activate
Hackathon". Follow the instructions on that page:

<img width="600" src="/static/images/docs/activate-hackathon.png" />

After you have done that you are ready to go. You hackathon has been setup and you shoud have landed
on the Hackathon Dashboard.

 [1]: /static/images/docs/hackathon-jira-structure.png
 [2]: /redirect/jira-signup
 [3]: /static/images/docs/find-new-addons.png
 [4]: /static/images/docs/create-project-dialog.png
 [5]: https://marketplace.atlassian.com/plugins/com.atlassian.hackathon.workflow/server/overview
 [6]: /static/images/docs/standard-hackathon-workflow.png
 [7]: https://confluence.atlassian.com/jira/defining-issue-type-field-values-185729517.html
