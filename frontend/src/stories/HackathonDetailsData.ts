import { HackathonDetails, HackathonRestriction } from '../hackathon-client';

// tslint:disable
export const ExampleHackathons: Array<HackathonDetails> = [
   {
      "shortUrl" : "https://goo.gl/SPjIvj",
      "projectName" : "Global ShipIt 33",
      "endDate" : new Date("2015-12-11T06:00:00Z"),
      "startDate" : new Date("2015-12-10T00:00:00Z"),
      "restriction" : HackathonRestriction.Anonymous,
      "projectId" : 12800,
      "projectKey" : "SHPXXXIII"
   },
   {
      "projectId" : 13001,
      "endDate" : new Date("2015-12-15T22:00:00Z"),
      "startDate" : new Date("2015-12-14T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectKey" : "SHPXXXF",
      "projectName" : "Global ShipIt 33 (Finals)",
      "shortUrl" : "https://goo.gl/b2gs28"
   },
   {
      "shortUrl" : "https://goo.gl/iCWOF3",
      "projectName" : "Cloud Core Experience Innovation Week 1",
      "projectKey" : "CXIWI",
      "endDate" : new Date("2016-01-22T07:00:00Z"),
      "startDate" : new Date("2016-01-17T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectId" : 13002
   },
   {
      "startDate" : new Date("2016-01-20T19:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "endDate" : new Date("2016-01-22T01:00:00Z"),
      "projectId" : 13003,
      "projectKey" : "HHXVI",
      "shortUrl" : "https://goo.gl/dyrOqz",
      "projectName" : "Hackhouse 2016"
   },
   {
      "shortUrl" : "https://goo.gl/1IzFec",
      "projectName" : "Service Desk Engineering Health Week 1",
      "startDate" : new Date("2016-02-01T06:00:00Z"),
      "endDate" : new Date("2016-02-05T06:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectId" : 13005,
      "projectKey" : "SDEHWI"
   },
   {
      "projectName" : "CloudX Innovation Week III",
      "shortUrl" : "https://goo.gl/e517To",
      "projectId" : 13300,
      "endDate" : new Date("2016-07-22T02:00:00Z"),
      "startDate" : new Date("2016-07-17T19:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectKey" : "CXIWIII"
   },
   {
      "shortUrl" : "https://goo.gl/Bm7YSM",
      "projectName" : "Atlassian Connect Innovation Week (June 2015)",
      "startDate" : new Date("2015-07-27T05:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "endDate" : new Date("2015-07-31T05:00:00Z"),
      "projectId" : 12301,
      "projectKey" : "ACIWI"
   },
   {
      "shortUrl" : "https://goo.gl/0RYb1H",
      "projectName" : "Summit ShipIt Live Test",
      "restriction" : HackathonRestriction.LoginOnly,
      "startDate" : new Date("2015-07-30T00:00:00Z"),
      "endDate" : new Date("2015-07-31T00:00:00Z"),
      "projectId" : 12400,
      "projectKey" : "SSLT"
   },
   {
      "shortUrl" : "https://goo.gl/298XFj",
      "projectName" : "Ecosystem Innovation Week (Syd) (Feb 2017)",
      "startDate" : new Date("2017-02-05T20:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "endDate" : new Date("2017-02-10T08:00:00Z"),
      "projectId" : 14200,
      "projectKey" : "EIWSF"
   },
   {
      "projectName" : "Global ShipIt 32",
      "shortUrl" : "https://goo.gl/72jPFz",
      "projectId" : 12300,
      "startDate" : new Date("2015-08-20T01:00:00Z"),
      "endDate" : new Date("2015-08-21T07:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectKey" : "SHPXXXII"
   },
   {
      "startDate" : new Date("2015-09-06T22:00:00Z"),
      "endDate" : new Date("2015-09-18T07:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectId" : 12500,
      "projectKey" : "PIWV",
      "shortUrl" : "https://goo.gl/Cv5DS8",
      "projectName" : "Purchasing Innovation Week 5"
   },
   {
      "projectName" : "Confluence Platform Innovation Week 1",
      "shortUrl" : "https://goo.gl/1Fjp7W",
      "projectId" : 13500,
      "startDate" : new Date("2016-08-25T00:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "endDate" : new Date("2016-08-26T04:00:00Z"),
      "projectKey" : "CPIWI"
   },
   {
      "shortUrl" : "https://goo.gl/ABrqvo",
      "projectName" : "Global ShipIt 36",
      "startDate" : new Date("2016-09-07T22:00:00Z"),
      "endDate" : new Date("2016-09-10T00:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 13400,
      "projectKey" : "SHPXXXVI"
   },
   {
      "shortUrl" : "https://goo.gl/kJyb8Q",
      "projectName" : "Global ShipIt 34",
      "endDate" : new Date("2016-03-11T07:00:00Z"),
      "startDate" : new Date("2016-03-09T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 13004,
      "projectKey" : "SHPXXXIV"
   },
   {
      "shortUrl" : "https://goo.gl/yQLU2H",
      "projectName" : "CloudX Innovation Week 2",
      "projectKey" : "CXIWII",
      "endDate" : new Date("2016-04-22T07:00:00Z"),
      "startDate" : new Date("2016-04-17T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectId" : 13100
   },
   {
      "projectName" : "Summit ShipIt Live 2015",
      "shortUrl" : "https://goo.gl/L2Dxqx",
      "projectId" : 12700,
      "restriction" : HackathonRestriction.Anonymous,
      "startDate" : new Date("2015-11-05T04:00:00Z"),
      "endDate" : new Date("2015-11-05T06:00:00Z"),
      "projectKey" : "SUMXV"
   },
   {
      "projectKey" : "BPIWI",
      "projectId" : 13700,
      "startDate" : new Date("2016-10-06T21:00:00Z"),
      "endDate" : new Date("2016-10-13T07:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectName" : "Billing Platform Innovation Week 1",
      "shortUrl" : "https://goo.gl/BjBe6q"
   },
   {
      "projectKey" : "IIWI",
      "projectId" : 12900,
      "endDate" : new Date("2015-11-06T05:00:00Z"),
      "startDate" : new Date("2015-11-01T23:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectName" : "Identity Innovation Week",
      "shortUrl" : "https://goo.gl/9fxzwr"
   },
   {
      "projectName" : "Global ShipIt 38",
      "shortUrl" : "https://goo.gl/kaT5OU",
      "projectKey" : "SHPXXXVIII",
      "projectId" : 14300,
      "startDate" : new Date("2017-03-09T00:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "endDate" : new Date("2017-03-10T08:00:00Z")
   },
   {
      "shortUrl" : "https://goo.gl/z54ZMg",
      "projectName" : "Global ShipIt 37",
      "endDate" : new Date("2016-12-10T01:00:00Z"),
      "startDate" : new Date("2016-12-07T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 13800,
      "projectKey" : "SHPXXXVII"
   },
   {
      "projectName" : "Global ShipIt 35",
      "shortUrl" : "https://goo.gl/UuFdCo",
      "projectId" : 13101,
      "endDate" : new Date("2016-06-03T10:00:00Z"),
      "startDate" : new Date("2016-06-01T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectKey" : "SHPXXXV"
   },
   {
      "projectKey" : "PIW",
      "projectId" : 14400,
      "endDate" : new Date("2017-03-15T06:00:00Z"),
      "startDate" : new Date("2017-03-14T06:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectName" : "Purchasing Innovation Week 6",
      "shortUrl" : "https://goo.gl/ZbKcXi"
   },
   {
      "projectName" : "Growth Innovation Week 1",
      "shortUrl" : "https://goo.gl/nrGBN6",
      "projectKey" : "GIWI",
      "projectId" : 13900,
      "startDate" : new Date("2016-12-18T21:00:00Z"),
      "endDate" : new Date("2016-12-23T06:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly
   },
   {
      "projectName" : "Purchasing Innovation Week 6",
      "shortUrl" : "https://goo.gl/sQLpKT",
      "projectId" : 13000,
      "startDate" : new Date("2015-12-13T21:00:00Z"),
      "endDate" : new Date("2015-12-18T06:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectKey" : "PIWVI"
   },
   {
      "startDate" : new Date("2017-01-18T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "endDate" : new Date("2017-01-20T06:00:00Z"),
      "projectId" : 14000,
      "projectKey" : "HH2017",
      "shortUrl" : "https://goo.gl/2ml6uH",
      "projectName" : "Hackhouse 2017"
   },
   {
      "shortUrl" : "https://goo.gl/fUddhF",
      "projectName" : "Ecosystem Innovation Week 3",
      "restriction" : HackathonRestriction.LoginOnly,
      "startDate" : new Date("2017-10-22T21:00:00Z"),
      "endDate" : new Date("2017-10-27T07:00:00Z"),
      "projectId" : 15801,
      "projectKey" : "EIW3"
   },
   {
      "endDate" : new Date("2017-01-20T06:00:00Z"),
      "startDate" : new Date("2017-01-15T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 14001,
      "projectKey" : "CONFINV",
      "shortUrl" : "https://goo.gl/4JfXgb",
      "projectName" : "Confluence Cloud Innovation I"
   },
   {
      "projectKey" : "IVWI",
      "projectId" : 14100,
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "startDate" : new Date("2017-01-29T21:00:00Z"),
      "endDate" : new Date("2017-02-03T06:00:00Z"),
      "projectName" : "INFRAvation week I",
      "shortUrl" : "https://goo.gl/bL1SMZ"
   },
   {
      "projectKey" : "SIWI",
      "projectId" : 14500,
      "restriction" : HackathonRestriction.LoginOnly,
      "startDate" : new Date("2017-04-02T23:00:00Z"),
      "endDate" : new Date("2017-04-07T06:00:00Z"),
      "projectName" : "Simplify Innovation Week I",
      "shortUrl" : "https://goo.gl/1194tm"
   },
   {
      "shortUrl" : "https://goo.gl/6zIbLB",
      "projectName" : "Simply Powerful Innovation Week I",
      "startDate" : new Date("2017-03-20T03:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "endDate" : new Date("2017-03-21T03:00:00Z"),
      "projectId" : 14600,
      "projectKey" : "SPIWI"
   },
   {
      "projectId" : 14700,
      "endDate" : new Date("2017-04-07T08:00:00Z"),
      "startDate" : new Date("2017-04-02T23:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectKey" : "SPIWONE",
      "projectName" : "Simply Powerful Innovation Week I",
      "shortUrl" : "https://goo.gl/xSdZvj"
   },
   {
      "projectKey" : "IWII",
      "projectId" : 14800,
      "endDate" : new Date("2017-05-12T08:00:00Z"),
      "startDate" : new Date("2017-05-07T23:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectName" : "Infravation Week II",
      "shortUrl" : "https://goo.gl/3SsaEg"
   },
   {
      "endDate" : new Date("2017-08-18T05:00:00Z"),
      "startDate" : new Date("2017-08-06T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 15600,
      "projectKey" : "JSWINVI",
      "shortUrl" : "https://goo.gl/NV9HBo",
      "projectName" : "JSW Innovation Sprint #1"
   },
   {
      "shortUrl" : "https://goo.gl/CvA37S",
      "projectName" : "Impact Days 1",
      "projectKey" : "IDI",
      "endDate" : new Date("2017-04-07T08:00:00Z"),
      "startDate" : new Date("2017-04-05T22:00:00Z"),
      "restriction" : HackathonRestriction.Anonymous,
      "projectId" : 14900
   },
   {
      "projectName" : "Purchasing Innovation Week 7",
      "shortUrl" : "https://goo.gl/Y1Zrn9",
      "projectId" : 15701,
      "endDate" : new Date("2017-09-24T22:00:00Z"),
      "startDate" : new Date("2017-09-10T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectKey" : "PIWVII"
   },
   {
      "projectName" : "Growth Innovation Week 2",
      "shortUrl" : "https://goo.gl/5uYLen",
      "projectId" : 15700,
      "startDate" : new Date("2017-09-03T19:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "endDate" : new Date("2017-09-08T07:00:00Z"),
      "projectKey" : "GIW"
   },
   {
      "shortUrl" : "https://goo.gl/C17dWX",
      "projectName" : "Global ShipIt 40",
      "projectKey" : "SHPXL",
      "startDate" : new Date("2017-09-26T22:00:00Z"),
      "endDate" : new Date("2017-09-29T08:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 15400
   },
   {
      "projectId" : 15000,
      "endDate" : new Date("2017-06-09T10:00:00Z"),
      "startDate" : new Date("2017-06-08T00:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectKey" : "SHPXXXIX",
      "projectName" : "Global ShipIt 39",
      "shortUrl" : "https://goo.gl/6tSieE"
   },
   {
      "projectName" : "Infravation Week III",
      "shortUrl" : "https://goo.gl/LkQr2h",
      "projectKey" : "IWIII",
      "projectId" : 15200,
      "startDate" : new Date("2017-08-13T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "endDate" : new Date("2017-08-18T07:00:00Z")
   },
   {
      "projectName" : "Ecosystem Innovation Week (July 2017)",
      "shortUrl" : "https://goo.gl/q5Ga2c",
      "projectId" : 15300,
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "startDate" : new Date("2017-07-23T22:00:00Z"),
      "endDate" : new Date("2017-07-28T07:00:00Z"),
      "projectKey" : "EIWJXVII"
   },
   {
      "projectId" : 15501,
      "startDate" : new Date("2017-09-11T23:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "endDate" : new Date("2017-09-15T23:00:00Z"),
      "projectKey" : "SUMXVII",
      "projectName" : "ShipIt Summit Edition 2017",
      "shortUrl" : "https://goo.gl/az4bdi"
   },
   {
      "projectId" : 15803,
      "startDate" : new Date("2017-10-29T21:00:00Z"),
      "endDate" : new Date("2017-11-07T07:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectKey" : "FIW1",
      "projectName" : "Fabric Innovation Week 1",
      "shortUrl" : "https://goo.gl/GC7Vqq"
   },
   {
      "startDate" : new Date("2017-10-04T05:00:00Z"),
      "endDate" : new Date("2017-10-06T05:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 15800,
      "projectKey" : "ALS1",
      "shortUrl" : "https://goo.gl/etnNmR",
      "projectName" : "AUG Leader ShipIt 1"
   },
   {
      "projectId" : 15808,
      "startDate" : new Date("2017-12-17T23:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "endDate" : new Date("2017-12-22T07:00:00Z"),
      "projectKey" : "EIW4",
      "projectName" : "Ecosystem Innovation Week 4",
      "shortUrl" : "https://goo.gl/YSQ6nG"
   },
   {
      "projectName" : "Growth Innovation Week 3",
      "shortUrl" : "https://goo.gl/NyxQq7",
      "projectKey" : "GIW3",
      "projectId" : 15802,
      "startDate" : new Date("2017-10-22T21:00:00Z"),
      "endDate" : new Date("2017-10-27T07:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly
   },
   {
      "projectName" : "Jira Family Innovation Sprint I",
      "shortUrl" : "https://goo.gl/2PKxL6",
      "projectKey" : "JFISI",
      "projectId" : 15806,
      "endDate" : new Date("2017-12-04T04:00:00Z"),
      "startDate" : new Date("2017-11-19T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember
   },
   {
      "projectName" : "Infravation Week IV",
      "shortUrl" : "https://goo.gl/s8hpYf",
      "projectId" : 15804,
      "endDate" : new Date("2017-12-08T07:00:00Z"),
      "startDate" : new Date("2017-12-03T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectKey" : "IWIV"
   },
   {
      "projectId" : 15807,
      "restriction" : HackathonRestriction.LoginOnly,
      "startDate" : new Date("2017-12-08T13:00:00Z"),
      "endDate" : new Date("2017-12-15T12:00:00Z"),
      "projectKey" : "BD1",
      "projectName" : "Buzz Day 1",
      "shortUrl" : "https://goo.gl/8SbfrT"
   },
   {
      "projectName" : "Gradlassian ShipIt 2018",
      "shortUrl" : "https://goo.gl/9yi5TF",
      "projectId" : 15811,
      "startDate" : new Date("2018-01-17T22:00:00Z"),
      "endDate" : new Date("2018-01-19T07:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectKey" : "GS2018"
   },
   {
      "endDate" : new Date("2018-03-12T07:00:00Z"),
      "startDate" : new Date("2018-02-26T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 15812,
      "projectKey" : "JFISII",
      "shortUrl" : "https://goo.gl/eFLTyw",
      "projectName" : "Jira Family Innovation Sprint II"
   },
   {
      "projectKey" : "FIW2",
      "projectId" : 15813,
      "startDate" : new Date("2018-02-11T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "endDate" : new Date("2018-02-16T07:00:00Z"),
      "projectName" : "Fabric Innovation week #2",
      "shortUrl" : "https://goo.gl/ZmR7tT"
   },
   {
      "projectKey" : "CCI2",
      "projectId" : 15814,
      "endDate" : new Date("2018-02-27T07:00:00Z"),
      "startDate" : new Date("2018-02-20T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectName" : "Confluence Cloud Innovation 2",
      "shortUrl" : "https://goo.gl/mF1bLU"
   },
   {
      "endDate" : new Date("2018-02-09T07:00:00Z"),
      "startDate" : new Date("2018-02-07T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 15810,
      "projectKey" : "SHPXLI",
      "shortUrl" : "https://goo.gl/QeMstX",
      "projectName" : "Global ShipIt 41"
   },
   {
      "projectKey" : "GIW4",
      "projectId" : 15815,
      "endDate" : new Date("2018-03-02T07:00:00Z"),
      "startDate" : new Date("2018-02-25T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectName" : "Growth Innovation Week #4",
      "shortUrl" : "https://goo.gl/14CyCX"
   },
   {
      "projectKey" : "INFRAV",
      "projectId" : 15816,
      "startDate" : new Date("2018-03-11T21:00:00Z"),
      "endDate" : new Date("2018-03-16T07:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectName" : "INFRAvation week V",
      "shortUrl" : "https://goo.gl/gGP8cz"
   },
   {
      "projectName" : "Ecosystem Innovation Week 5",
      "shortUrl" : "https://goo.gl/Np2Tff",
      "projectKey" : "EIW5",
      "projectId" : 15818,
      "startDate" : new Date("2018-03-05T21:00:00Z"),
      "endDate" : new Date("2018-03-12T07:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember
   },
   {
      "shortUrl" : "https://goo.gl/pqQHkJ",
      "projectName" : "ShipIt Summit Edition 2018",
      "projectKey" : "SUMXVIII",
      "startDate" : new Date("2018-09-03T16:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "endDate" : new Date("2018-09-04T16:00:00Z"),
      "projectId" : 15821
   },
   {
      "projectId" : 15820,
      "restriction" : HackathonRestriction.LoginOnly,
      "startDate" : new Date("2018-04-02T22:00:00Z"),
      "endDate" : new Date("2018-04-09T08:00:00Z"),
      "projectKey" : "EIW6",
      "projectName" : "Ecosystem Innovation Week 6",
      "shortUrl" : "https://goo.gl/gGP2LF"
   },
   {
      "projectId" : 15822,
      "endDate" : new Date("2018-05-30T07:00:00Z"),
      "startDate" : new Date("2018-05-22T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectKey" : "CCI3",
      "projectName" : "Confluence Cloud Innovation 3",
      "shortUrl" : "https://goo.gl/LxSx7F"
   },
   {
      "projectId" : 15819,
      "restriction" : HackathonRestriction.LoginOnly,
      "startDate" : new Date("2018-05-09T23:00:00Z"),
      "endDate" : new Date("2018-05-11T08:00:00Z"),
      "projectKey" : "SHPXLII",
      "projectName" : "Global ShipIt 42",
      "shortUrl" : "https://goo.gl/nbomR2"
   },
   {
      "projectId" : 15823,
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "startDate" : new Date("2018-06-03T23:00:00Z"),
      "endDate" : new Date("2018-06-07T23:00:00Z"),
      "projectKey" : "GIW5",
      "projectName" : "Growth Innovation Week #5",
      "shortUrl" : "https://goo.gl/x92Gv8"
   },
   {
      "projectKey" : "ITIW18Q4",
      "startDate" : new Date("2018-05-24T23:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "endDate" : new Date("2018-06-01T07:00:00Z"),
      "projectId" : 15824,
      "shortUrl" : "https://goo.gl/cRSD44",
      "projectName" : "IT Innovation Week FY17Q4"
   },
   {
      "projectName" : "INFRAvation week VI",
      "shortUrl" : "https://goo.gl/MZGRnD",
      "projectKey" : "IN",
      "projectId" : 15825,
      "endDate" : new Date("2018-06-08T08:00:00Z"),
      "startDate" : new Date("2018-06-03T23:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly
   },
   {
      "projectKey" : "JSWIWI",
      "projectId" : 15100,
      "endDate" : new Date("2017-05-19T07:00:00Z"),
      "startDate" : new Date("2017-05-15T01:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectName" : "JSW Innovation Week 1",
      "shortUrl" : "https://goo.gl/9v1SUl"
   },
   {
      "shortUrl" : "https://goo.gl/PyZynu",
      "projectName" : "Ecosystem Innovation Week 7",
      "projectKey" : "EIW7",
      "endDate" : new Date("2018-06-13T06:00:00Z"),
      "startDate" : new Date("2018-06-04T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 15827
   },
   {
      "projectName" : "IT Innovation Week FY18Q2",
      "shortUrl" : "https://goo.gl/VMwdpZ",
      "projectKey" : "IT2",
      "projectId" : 15845,
      "endDate" : new Date("2018-11-09T06:00:00Z"),
      "startDate" : new Date("2018-11-04T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly
   },
   {
      "projectName" : "Purchasing Innovation Week 9",
      "shortUrl" : "https://goo.gl/qx8s5J",
      "projectId" : 15826,
      "endDate" : new Date("2018-06-15T07:00:00Z"),
      "startDate" : new Date("2018-06-10T22:00:00Z"),
      "restriction" : HackathonRestriction.Anonymous,
      "projectKey" : "PIW9"
   },
   {
      "projectName" : "Ecosystem Innovation Week 8",
      "shortUrl" : "https://goo.gl/gMQRfj",
      "projectId" : 15829,
      "startDate" : new Date("2018-07-09T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "endDate" : new Date("2018-07-16T08:00:00Z"),
      "projectKey" : "EIW8"
   },
   {
      "projectName" : "Gradlassian 2018 US Shipit",
      "shortUrl" : "https://goo.gl/27Jkwo",
      "projectKey" : "GS2018US",
      "projectId" : 15831,
      "startDate" : new Date("2018-07-25T22:00:00Z"),
      "endDate" : new Date("2018-07-27T08:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly
   },
   {
      "projectName" : "INFRAvation week VII",
      "shortUrl" : "https://goo.gl/yiCnuo",
      "projectKey" : "NFRVTN",
      "projectId" : 15832,
      "endDate" : new Date("2018-08-10T08:00:00Z"),
      "startDate" : new Date("2018-08-05T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember
   },
   {
      "projectName" : "Developer Platform Innovation Week 1",
      "shortUrl" : "https://goo.gl/xdLyqF",
      "projectId" : 15846,
      "startDate" : new Date("2018-12-02T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "endDate" : new Date("2018-12-07T07:00:00Z"),
      "projectKey" : "DP1"
   },
   {
      "projectId" : 15828,
      "restriction" : HackathonRestriction.LoginOnly,
      "startDate" : new Date("2018-08-22T22:00:00Z"),
      "endDate" : new Date("2018-08-24T08:00:00Z"),
      "projectKey" : "SHPXLIII",
      "projectName" : "Global ShipIt 43",
      "shortUrl" : "https://goo.gl/yy49tc"
   },
   {
      "projectKey" : "CCIW4",
      "startDate" : new Date("2018-09-11T23:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "endDate" : new Date("2018-09-18T07:00:00Z"),
      "projectId" : 15833,
      "shortUrl" : "https://goo.gl/CrmX64",
      "projectName" : "Confluence Cloud Innovation Week 4"
   },
   {
      "shortUrl" : "https://goo.gl/xoP6Uj",
      "projectName" : "Growth Innovation Week #7",
      "projectKey" : "GIW7",
      "startDate" : new Date("2018-12-02T22:00:00Z"),
      "endDate" : new Date("2018-12-06T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "projectId" : 15847
   },
   {
      "projectId" : 15834,
      "startDate" : new Date("2018-09-02T23:00:00Z"),
      "endDate" : new Date("2018-09-07T07:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectKey" : "IT",
      "projectName" : "IT Innovation Week FY18Q1",
      "shortUrl" : "https://goo.gl/CNVqXC"
   },
   {
      "projectKey" : "EIF9",
      "startDate" : new Date("2018-09-16T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginOnly,
      "endDate" : new Date("2018-09-28T08:00:00Z"),
      "projectId" : 15835,
      "shortUrl" : "https://goo.gl/Yz8uJh",
      "projectName" : "Ecosystem Innovation Fortnight 9"
   },
   {
      "shortUrl" : "https://goo.gl/MKxoWm",
      "projectName" : "Growth Innovation Week #6",
      "projectKey" : "GIW6",
      "startDate" : new Date("2018-09-16T23:00:00Z"),
      "endDate" : new Date("2018-09-20T23:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectId" : 15836
   },
   {
      "projectId" : 15840,
      "endDate" : new Date("2018-09-21T08:00:00Z"),
      "startDate" : new Date("2018-09-16T22:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectKey" : "COMINNOENV",
      "projectName" : "Commerce Innovation Week - Save the Environment",
      "shortUrl" : "https://goo.gl/eEadjc"
   },
   {
      "projectKey" : "SHPXLIV",
      "endDate" : new Date("2018-11-16T07:00:00Z"),
      "startDate" : new Date("2018-11-14T21:00:00Z"),
      "restriction" : HackathonRestriction.LoginAndTeamMember,
      "projectId" : 15844,
      "shortUrl" : "https://goo.gl/SxZuEm",
      "projectName" : "Global ShipIt 44"
   }
];
