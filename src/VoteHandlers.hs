{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module VoteHandlers
    ( handleHackathonVote
    , handleHackathonRoundMyVotes
    ) where

import           AesonHelpers
import           Application
import           Control.Monad.Trans.Class       (lift)
import           Control.Monad.Trans.Maybe
import           Data.Aeson
import           Data.Aeson.Types                (fieldLabelModifier)
import qualified Data.HackathonConstants         as HC
import qualified Data.HackathonRequest           as HR
import           Data.List                       (find)
import qualified Data.Text                       as T
import           Data.Tuples                     (fst3, snd3, thd3)
import           GHC.Generics
import qualified Persistence.Hackathon           as PH
import qualified Persistence.HackathonIds        as HI
import qualified Persistence.HackathonRoom       as HRM
import qualified Persistence.HackathonRound      as RND
import qualified Persistence.HackathonRoundRoom  as HRR
import qualified Persistence.HackathonTeam       as HT
import qualified Persistence.HackathonTeamMember as HTM
import qualified Persistence.HackathonVote       as HV
import qualified Snap.AtlassianConnect           as AC
import qualified Snap.Core                       as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import           TenantWithVoteableUser

handleHackathonVote :: AppHandler ()
handleHackathonVote = handleMethods
    [ (SC.POST, handleAddVote)
    , (SC.DELETE, handleRemoveVote)
    ]

data VoteRequest = VoteRequest
    { vrHackathon :: HR.HackathonRequest
    , vrRoomId    :: Integer
    , vrRoundId   :: Integer
    , vrTeamId    :: Integer
    } deriving (Show, Generic)

instance FromJSON VoteRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "vr"
        }

handleAddVote :: AppHandler ()
handleAddVote = do
    request <- SC.readRequestBody size10KB
    let potentialRequest = eitherDecode request :: Either String VoteRequest
    case potentialRequest of
        Left errMsg -> respondWithError badRequest $ "Did not send the right data in the request to make a vote: " ++ errMsg
        Right voteRequest -> tenantWithVoteableUserFromHackathonRequest (vrHackathon voteRequest) (addVoteWithAllDetails voteRequest)

addVoteWithAllDetails :: VoteRequest -> AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
addVoteWithAllDetails _ _ _ Nothing = respondWithError unauthorised "You are not authorised to place a vote on this hackathon. Please log in."
addVoteWithAllDetails voteRequest tenant hackathon (Just voteableUser) = withPG $ do
    potentialTeam <- HT.getHackathonTeamById hackathon (vrTeamId voteRequest)
    case potentialTeam of
        Nothing -> respondWithError notFound "There is no team with the given id in this hackathon."
        Just hTeam ->
            case (PH.hackathonVotingRestriction hackathon, voteableUser) of
                (HC.LoginAndTeamMember, HR.VoteableTenantUser tenantUser) -> do
                    -- Lookup if the tenant user is a member of the team
                    isTeamMember <- HTM.isTeamMember hTeam tenantUser
                    if isTeamMember
                        then writeJson $ voteResponse CannotVoteOnYourOwnTeam "You are a member of this team thus you are not allowed to vote on this team."
                        else addVoteWithAllDetailsAndTeam voteRequest tenant hackathon voteableUser hTeam
                _ -> addVoteWithAllDetailsAndTeam voteRequest tenant hackathon voteableUser hTeam

addVoteWithAllDetailsAndTeam :: VoteRequest -> AC.Tenant -> PH.Hackathon -> HR.VoteableUser -> HT.HackathonTeam -> AppHandler ()
addVoteWithAllDetailsAndTeam voteRequest _ hackathon voteableUser hTeam = withPG $ do
    phRound <- RND.getHackathonRoundById hackathon (vrRoundId voteRequest)
    phRoom <- HRM.getRoomById hackathon (vrRoomId voteRequest)
    case (phRound, phRoom) of
        (Nothing, _) -> writeJson $ voteResponse RoundMissing "There is no round with that id in the current hackathon."
        (_, Nothing) -> writeJson $ voteResponse RoomMissing "There is no room with that id in the current hackathon."
        (Just hRound, Just hRoom) -> do
            potentialRoundRoom <- HRR.getRoundRoom hRound hRoom
            case potentialRoundRoom of
                Nothing -> writeJson $ voteResponse RoundAndRoomDoesNotExist "Voting is not configured this room and round yet."
                Just roundRoom ->
                    case HRR.hrrStatus roundRoom of
                        HC.Active -> do
                            votesPlaced <- HV.countVotesOnTeamsInRound voteableUser hRound
                            case meetVotingRestrictions hackathon hRound hTeam hRoom votesPlaced of
                                VoteSuccessful -> do
                                    insertedVote <- HV.addVote voteableUser hTeam hRound hRoom
                                    case insertedVote of
                                        Nothing -> writeJson $ voteResponse DatabaseFailure "Could not place the vote in the database."
                                        Just _ -> writeJson $ voteResponse VoteSuccessful "Successfully placed your vote."
                                votingError -> writeJson $ voteResponse votingError "Failed to meet the voting conditions."
                        HC.Inactive -> writeJson $ voteResponse RoundAndRoomInactive "Voting has not started in this room and round yet."
                        HC.Locked -> writeJson $ voteResponse RoundAndRoomLocked "Voting has finished in this room and round."

meetVotingRestrictions :: PH.Hackathon -> RND.HackathonRound -> HT.HackathonTeam -> HRM.HackathonRoom -> [(Integer, HI.HackathonTeamId, HI.HackathonRoomId)] -> VotingStatus
meetVotingRestrictions hackathon hRound hTeam hRoom votesPlaced
    | votesOnTeam >= maxPerTeam = MaxVotesForTeamReached
    | totalVotes >= votesPerUser = MaxVotesForRoundReached
    | PH.hackathonSameRoomVoting hackathon && votesInDifferentRooms = CannotVoteInDifferentRooms
    | otherwise = VoteSuccessful
    where
        totalVotes = sum . fmap fst3 $ votesPlaced
        votesOnTeam = maybe 0 fst3 (find ((==) (HT.htId hTeam) . snd3) votesPlaced)
        votesInDifferentRooms = any (/= HRM.hrId hRoom) . fmap thd3 $ votesPlaced

        votesPerUser = RND.hroundVotesPerUser hRound
        maxPerTeam = RND.hroundVotesUserTeam hRound

voteResponse :: VotingStatus -> String -> VotingResponse
voteResponse a b = VotingResponse b a

data VotingResponse = VotingResponse
    { vrspMessage :: String
    , vrspStatus  :: VotingStatus
    } deriving (Show, Generic)

instance ToJSON VotingResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "vrsp"
        }

data VotingStatus
    = VoteSuccessful
    | CannotVoteOnYourOwnTeam
    | MaxVotesForTeamReached
    | MaxVotesForRoundReached
    | CannotVoteInDifferentRooms
    | RoundAndRoomDoesNotExist
    | RoundAndRoomInactive
    | RoundAndRoomLocked
    | RoomMissing
    | RoundMissing
    | DatabaseFailure
    deriving (Show, Generic)

instance ToJSON VotingStatus where
    toJSON = String . T.pack . show

handleRemoveVote :: AppHandler ()
handleRemoveVote = do
    request <- SC.readRequestBody size10KB
    let potentialRequest = eitherDecode request :: Either String VoteRequest
    case potentialRequest of
        Left errMsg -> respondWithError badRequest $ "Did not send the right data in the request to remove a vote: " ++ errMsg
        Right voteRequest -> tenantWithVoteableUserFromHackathonRequest (vrHackathon voteRequest) (removeVoteWithAllDetails voteRequest)

removeVoteWithAllDetails :: VoteRequest -> AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
removeVoteWithAllDetails _ _ _ Nothing = respondWithError unauthorised "You are not authorised to remove a vote from this hackathon. Please log in."
removeVoteWithAllDetails voteRequest _ hackathon (Just voteableUser) = withPG $ do
    loadedData <- do
        potentialTeam <- HT.getHackathonTeamById hackathon (vrTeamId voteRequest)
        potentialRound <- RND.getHackathonRoundById hackathon (vrRoundId voteRequest)
        potentialRoom <- HRM.getRoomById hackathon (vrRoomId voteRequest)
        case (potentialRound, potentialRoom) of
            (Just hRound, Just hRoom) -> do
                potentialRoundRoom <- HRR.getRoundRoom hRound hRoom
                return (potentialTeam, potentialRound, potentialRoom, potentialRoundRoom)
            _ -> return (potentialTeam, potentialRound, potentialRoom, Nothing)
    case loadedData of
        (Just hTeam, Just hRound, Just hRoom, Just roundRoom) ->
            case HRR.hrrStatus roundRoom of
                HC.Active -> do
                    voteRemoved <- HV.removeVote voteableUser hTeam hRound hRoom
                    if voteRemoved
                        then ur UndoSuccessful "Successfully removed one vote."
                        else ur NoVoteToUndo "There was no vote to remove from this team in this round and room."
                HC.Inactive -> ur UndoBlockedIsInactive "The room is currently inactive."
                HC.Locked -> ur UndoBlockedIsLocked "The room is locked."
        (Nothing, _, _, _) -> ur UndoNoSuchTeam "Could not find the given team."
        (_, Nothing, _, _) -> ur UndoNoSuchRound "Could not find the given round."
        (_, _, Nothing, _) -> ur UndoNoSuchRoom "Could not find the given room."
        (_, _, _, Nothing) -> ur UndoNoSuchRoundRoom "Could not find the given round-room."
    where
        ur a b = writeJson $ UndoingResponse a b

data UndoingResponse = UndoingResponse
    { urStatus  :: VoteUndoStatus
    , urMessage :: String
    } deriving (Show, Generic)

instance ToJSON UndoingResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "ur"
        }

data VoteUndoStatus
    = UndoSuccessful
    | NoVoteToUndo
    | UndoNoSuchTeam
    | UndoNoSuchRound
    | UndoNoSuchRoom
    | UndoNoSuchRoundRoom
    | UndoBlockedIsInactive
    | UndoBlockedIsLocked
    deriving (Show)

instance ToJSON VoteUndoStatus where
    toJSON = String . T.pack . show

handleHackathonRoundMyVotes :: AppHandler ()
handleHackathonRoundMyVotes = handleMethods
    [ (SC.POST, getMyVotes)
    ]

getMyVotes :: AppHandler ()
getMyVotes = do
    request <- SC.readRequestBody size10KB
    let potentialRequest = eitherDecode request :: Either String MyVotesRequest
    case potentialRequest of
        Left errMsg -> respondWithError badRequest $ "Did not send the right data in the request for my votes: " ++ errMsg
        Right myVotesRequest -> tenantWithVoteableUserFromHackathonRequest (mvrHackathon myVotesRequest) (handleGetMyVotes myVotesRequest)

handleGetMyVotes :: MyVotesRequest -> AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
handleGetMyVotes _ _ _ Nothing = respondWithError unauthorised "You are not logged in, no votes can be retrieved."
handleGetMyVotes myVotesRequest _ hackathon (Just voteableUser) = do
    potentialVotes <- withPG . runMaybeT $ do
        hRound <- MaybeT $ RND.getHackathonRoundById hackathon (mvrRoundId myVotesRequest)
        lift $ HV.countVotesOnTeamsInRound voteableUser hRound
    case potentialVotes of
        Nothing -> respondWithError notFound "Could not find any round with the given id"
        Just votes -> writeJson . MyVotesResponse . fmap toTeamVote $ votes

data MyVotesRequest = MyVotesRequest
    { mvrHackathon :: HR.HackathonRequest
    , mvrRoundId   :: Integer
    } deriving (Show, Generic)

instance FromJSON MyVotesRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "mvr"
        }

data MyVotesResponse = MyVotesResponse
    { mvrspTeamVotes :: [TeamVote]
    } deriving (Show, Generic)

instance ToJSON MyVotesResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "mvrsp"
        }

toTeamVote :: (Integer, HI.HackathonTeamId, HI.HackathonRoomId) -> TeamVote
toTeamVote (cnt, tId, _) = TeamVote
    { tvTeamId = tId
    , tvCount = cnt
    }

data TeamVote = TeamVote
    { tvTeamId :: HI.HackathonTeamId
    , tvCount  :: Integer
    } deriving (Show, Generic)

instance ToJSON TeamVote where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "tv"
        }
