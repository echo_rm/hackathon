{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module HackathonRoundHandlers
    ( handleHackathonRounds
    , handleRoundTransitionOptions
    ) where

import           AesonHelpers
import           Application
import qualified Control.Arrow                 as A
import           Control.Monad                 (void)
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Either
import           Data.Aeson
import           Data.Aeson.Types              (fieldLabelModifier)
import qualified Data.HackathonConstants       as HC
import           Data.MaybeUtil
import qualified Data.Text                     as T
import           GHC.Generics
import           HandlerHelpers
import qualified HostRequests.UserDetails      as UD
import qualified Persistence.Hackathon         as PH
import qualified Persistence.HackathonIds      as HI
import qualified Persistence.HackathonRound    as RND
import qualified Snap.AtlassianConnect         as AC
import qualified Snap.Core                     as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import qualified WithToken                     as WT

handleHackathonRounds :: AppHandler ()
handleHackathonRounds = handleMethods
    [ (SC.GET, WT.tenantFromToken getHackathonRounds)
    ]

getHackathonRounds :: AC.TenantWithUser -> AppHandler ()
getHackathonRounds (_, Nothing) = respondWithError unauthorised "You need to be logged in to get the hackathon rounds."
getHackathonRounds (tenant, Just _) =
    writeError . withPG . runEitherT $ do
        hackathon <- EitherT $ getHackathonFromProjectId tenant
        hRounds <- lift $ RND.getRoundsInHackathon hackathon
        void . lift $ writeJson . HackathonRoundsResponse . fmap toHackathonRoundEntity $ hRounds

data HackathonRoundsResponse = HackathonRoundsResponse
   { hrrRounds :: [HackathonRoundEntity]
   } deriving (Show, Generic)

instance ToJSON HackathonRoundsResponse where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "hrr" }

data HackathonRoundEntity = HackathonRoundEntity
   { hreId               :: HI.HackathonRoundId
   , hreStatusId         :: HI.AtlassianStatusId
   , hreName             :: T.Text
   , hreType             :: HC.HackathonRoundType
   , hreSequenceNumber   :: Maybe Integer
   , hreVotesPerUser     :: Integer
   , hreVotesUserTeam    :: Integer
   , hreTransitionTied   :: Bool
   , hreMatchUnallocated :: Bool
   } deriving (Show, Generic)

instance ToJSON HackathonRoundEntity where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "hre" }

toHackathonRoundEntity :: RND.HackathonRound -> HackathonRoundEntity
toHackathonRoundEntity hr = HackathonRoundEntity
   { hreId = RND.hroundId hr
   , hreStatusId = RND.hroundStatusId hr
   , hreName = RND.hroundName hr
   , hreType = RND.hroundType hr
   , hreSequenceNumber = RND.hroundSequenceNumber hr
   , hreVotesPerUser = RND.hroundVotesPerUser hr
   , hreVotesUserTeam = RND.hroundVotesUserTeam hr
   , hreTransitionTied = RND.hroundTransitionTied hr
   , hreMatchUnallocated = RND.hroundMatchUnallocated hr
   }

handleRoundTransitionOptions :: AppHandler ()
handleRoundTransitionOptions = handleMethods
   [ (SC.PUT, WT.tenantFromToken updateTransitionOptions)
   ]

updateTransitionOptions :: AC.TenantWithUser -> AppHandler ()
updateTransitionOptions (_, Nothing) = respondWithError unauthorised "You need to be logged in to update the round transition options."
updateTransitionOptions (tenant, Just userKey) = do
   request <- SC.readRequestBody size10KB
   writeError . withPG . runEitherT $ do
      updateRequest <- hoistEither (A.left invalidData $ eitherDecode request)
      hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant (utorProjectId updateRequest))
      EitherT $ requirePermissionsE [UD.ProjectAdmin] tenant userKey hackathon
      currentRound <- EitherT (m2e noSuchRound <$> RND.getHackathonRoundById hackathon (utorRoundId updateRequest))
      void . lift $ RND.updateTransitionOptions currentRound (tied currentRound updateRequest) (matchUnallocated currentRound updateRequest)
      void . lift $ respondNoContent
   where
      tied cr ur = if RND.hroundType cr == HC.TeamFormation then True else utorTransitionTied ur
      matchUnallocated cr ur = if RND.hroundType cr == HC.TeamFormation then True else utorMatchUnallocated ur

      noSuchHackathon = (notFound, "A hackathon for the provided project id could not be found.")
      noSuchRound = (notFound, "A round with the provided id could not be found.")

      invalidData :: String -> (Int, String)
      invalidData err = (badRequest, "Could not parse the request data: " ++ err)

data UpdateTransitionOptionsRequest = UpdateTransitionOptionsRequest
   { utorProjectId        :: AC.ProjectId
   , utorRoundId          :: HI.HackathonRoundId
   , utorTransitionTied   :: Bool
   , utorMatchUnallocated :: Bool
   } deriving (Show, Generic)

instance FromJSON UpdateTransitionOptionsRequest where
   parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "utor" }

