{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module HackathonRoomHandlers
    ( handleHackathonRoom
    , handleHackathonRoomCurrentRound
    , handleHackathonRoomTeams
    ) where

import           AesonHelpers
import           Application
import           Control.Monad                   (void)
import           Control.Monad.Trans.Class       (lift)
import           Control.Monad.Trans.Either
import           Control.Monad.Trans.Maybe
import           Data.Aeson
import           Data.Aeson.Types                (fieldLabelModifier)
import           Data.HackathonConstants
import qualified Data.HackathonRequest           as HR
import qualified Data.Map                        as M
import           Data.MaybeUtil
import qualified Data.Text                       as T
import qualified Entity.TeamDetails              as ETD
import           GHC.Generics
import           HandlerHelpers
import qualified HostRequests.Components         as HC
import qualified HostRequests.UserDetails        as HU
import qualified Persistence.Hackathon           as PH
import qualified Persistence.HackathonIds        as HI
import qualified Persistence.HackathonRoom       as HRM
import qualified Persistence.HackathonRound      as RND
import qualified Persistence.HackathonTeamMember as HTM
import qualified Snap.AtlassianConnect           as AC
import qualified Snap.Core                       as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import           TenantWithVoteableUser
import qualified WithToken                       as WT

standardAuthError :: AppHandler ()
standardAuthError = respondWithError unauthorised "You need to login before you can make this request."

handleHackathonRoom :: AppHandler ()
handleHackathonRoom = handleMethods
    [ (SC.PUT, WT.tenantFromToken createHackathonRoom)
    ]

createHackathonRoom :: AC.TenantWithUser -> AppHandler ()
createHackathonRoom (_, Nothing) = standardAuthError
createHackathonRoom (tenant, Just userKey) = do
   request <- SC.readRequestBody size10KB
   potentialRoom <- withPG . runEitherT $ do
       createRequest <- hoistEither . withErr badRequest parseFail . eitherDecode $ request
       hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant (chrrProjectId createRequest))
       EitherT $ requirePermissionsE [HU.ProjectAdmin] tenant userKey hackathon
       let nameWithPrefix = roomComponentPrefix `T.append` chrrRoomName createRequest
       component <- EitherT (withErr internalServer componentLookupFail <$> HC.getOrCreateComponentByName tenant (chrrProjectKey createRequest) nameWithPrefix)
       hRoom <- EitherT (m2e noSuchRoom <$> (runMaybeT $ HRM.getOrCreateHackathonRoom hackathon (HC.jcId component) (chrrRoomName createRequest)))
       return . toRoomDetails $ hRoom
   case potentialRoom of
       Left err -> uncurry respondWithError err
       Right hRoom -> writeJson hRoom
   where
      componentLookupFail = "Could not get / create the room component:"
      noSuchRoom = (internalServer, "Could not get or create the hackathon room from the database.")

parseFail :: String
parseFail = "Could not parse the request:"

noSuchHackathon :: (Int, String)
noSuchHackathon = (badRequest, "There is no hackathon with the provided project id.")

data CreateHackathonRoomRequest = CreateHackathonRoomRequest
    { chrrProjectId  :: AC.ProjectId
    , chrrProjectKey :: AC.ProjectKey -- The only reason that we are requesting this is because the Components request code uses projectkeys
    , chrrRoomName   :: T.Text
    } deriving (Show, Generic)

instance FromJSON CreateHackathonRoomRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "chrr"
        }

toRoomDetails :: HRM.HackathonRoom -> RoomDetails
toRoomDetails hRoom = RoomDetails
    { rmId = HRM.hrId hRoom
    , rmComponentId = HRM.hrComponentId hRoom
    , rmName = HRM.hrName hRoom
    }

data RoomDetails = RoomDetails
    { rmId          :: HI.HackathonRoomId
    , rmComponentId :: HI.ComponentId
    , rmName        :: T.Text
    } deriving (Show, Generic)

instance ToJSON RoomDetails where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rm"
        }

handleHackathonRoomCurrentRound :: AppHandler ()
handleHackathonRoomCurrentRound = handleMethods
    [ (SC.PUT, WT.tenantFromToken setCurrentRoundOnRoom)
    ]

setCurrentRoundOnRoom :: AC.TenantWithUser -> AppHandler ()
setCurrentRoundOnRoom (_, Nothing) = standardAuthError
setCurrentRoundOnRoom (tenant, Just userKey) = withPG $ do
    request <- SC.readRequestBody size10KB
    writeError . withPG . runEitherT $ do
       updateRequest <- hoistEither . withErr badRequest parseFail . eitherDecode $ request
       hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant (crurProjectId updateRequest))
       EitherT $ requirePermissionsE [HU.ProjectAdmin] tenant userKey hackathon
       hRound <- EitherT (m2e noSuchRound <$> RND.getHackathonRoundById hackathon (crurCurrentRoundId updateRequest))
       void . lift $ HRM.updateCurrentRounds hackathon hRound (crurRoomIds updateRequest)
       void . lift $ respondNoContent
    where
       noSuchRound = (badRequest, "There is no hackathon round with the provided id.")

data CurrentRoundUpdateRequest = CurrentRoundUpdateRequest
    { crurProjectId      :: AC.ProjectId
    , crurRoomIds        :: [HI.HackathonRoomId]
    , crurCurrentRoundId :: HI.HackathonRoundId
    } deriving (Show, Generic)

instance FromJSON CurrentRoundUpdateRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "crur"
        }

data RoomRequest = RoomRequest
    { rrRoomId    :: HI.HackathonRoomId
    , rrHackathon :: HR.HackathonRequest
    } deriving (Show, Generic)

instance FromJSON RoomRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rr"
        }

handleHackathonRoomTeams :: AppHandler ()
handleHackathonRoomTeams = handleMethods
    [ (SC.POST, getHackathonRoomTeams)
    ]

getHackathonRoomTeams :: AppHandler ()
getHackathonRoomTeams = do
    request <- SC.readRequestBody size10KB
    let potentialRequest = eitherDecode request :: Either String RoomRequest
    case potentialRequest of
        Left errMsg -> respondWithError badRequest $ "Could not parse json request: " ++ errMsg
        Right rr -> tenantWithVoteableUserFromHackathonRequest (rrHackathon rr) (getHackathonRoomTeamsHelper rr)

getHackathonRoomTeamsHelper :: RoomRequest -> AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
getHackathonRoomTeamsHelper _ _ _ Nothing = standardAuthError
getHackathonRoomTeamsHelper rr _ hackathon (Just _) = withPG $ do
   voteableTeams <- HTM.getTeamsAndMembersInRoomsCurrentRound hackathon (rrRoomId rr)
   writeJson . VoteableTeams . fmap (\(team, members) -> ETD.toTeamDetails team members Nothing) . M.toList $ voteableTeams

data VoteableTeams = VoteableTeams
    { vTeams :: [ETD.TeamDetails]
    } deriving (Show, Generic)

instance ToJSON VoteableTeams where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "v"
        }
