define(['../lib/URI'], function(URI) {
   "use strict";

   var links = {};

   var assertAddonContext = function(addonContext) {
      if(!addonContext) throw "No addon context provided";
      if(!addonContext.productBaseUrl) throw "No addon context product base url provided.";
      if(!addonContext.pluginKey) throw "No addon context plugin key provided";
   };

   var safePath = function(uri) {
      var origPath = uri.path();
      if(origPath == "/") return "";
      return origPath;
   };

   links.dashboard = function (addonContext, hackathon) {
      assertAddonContext(addonContext);
      if(!hackathon || !hackathon.projectId || !hackathon.projectKey) throw "Hackathon project id and key missing.";

      var uri = URI(addonContext.productBaseUrl);
      uri.path(safePath(uri) + "/plugins/servlet/ac/" + addonContext.pluginKey + "/hackathon-dashboard");
      uri.addQuery('project.id', hackathon.projectId);
      uri.addQuery('project.key', hackathon.projectKey);
      return uri.toString();
   };

   links.summary = function (addonContext, hackathon) {
      assertAddonContext(addonContext);
      if(!hackathon || !hackathon.projectKey) throw "Hackathon project key missing.";

      var uri = URI(addonContext.productBaseUrl);
      uri.path(safePath(uri) + "/projects/" + hackathon.projectKey);
      uri.addQuery('selectedItem', addonContext.pluginKey + '__hackathon-summary-web-item');
      return uri.toString();
   };

   links.viewTeams = function(productBaseUrl, hackathon, round, room) {
     if(!productBaseUrl) throw "No product base url provided for the view teams link generator!";
     if(!hackathon || !(hackathon.projectName || hackathon.projectKey)) throw "No hackathon provided for the view teams link generator!";
     if(!round) throw "No round provided for the view teams link generator!";
     if(!room) throw "Ro room provided for the view teams link generator!";

     // https://shipit.atlassian.net/issues/?jql=project%20%3D%20%22Global%20ShipIt%2035%22%20and%20component%20%3D%20%22Room%20-%20Justice%206.03%20-%20Syd%22%20AND%20status%20%3D%20%22Round%203%22
     var projectIdentifier = hackathon.projectName || hackathon.projectKey;
     var jql = 'project = "' + projectIdentifier + '" AND status = "' + round.name + '" AND component = "Room - ' + room.name + '"';

     var uri = URI(productBaseUrl);
     uri.path(safePath(uri) + "/issues/");
     uri.addQuery('jql', jql);

     return uri.toString();
   };

   links.leaderboard = function(tenantKey, projectId, projectKey, roundId, roomId, acpt) {
      if(!tenantKey) throw "No tenant key provided";
      if(!projectId) throw "No project id provided";
      if(!projectKey) throw "No project key provided";
      if(!roundId) throw "No round id provided";
      if(!roomId) throw "No room id provided";
      if(!acpt) throw "No acpt token provided";

      var uri = URI("/page/hackathon-leaderboard");
      uri.addQuery('tenant_key', tenantKey);
      uri.addQuery('project_id', projectId);
      uri.addQuery('project_key', projectKey);
      uri.addQuery('room_id', roomId);
      uri.addQuery('round_id', roundId);
      uri.addQuery('acpt', acpt);
      return uri.toString();
   };

   return links;
});
