{-# LANGUAGE OverloadedStrings #-}
module Data.JQL
    ( RawJQL
    , JQL(..)
    , toRawJQL
    , quote
    ) where

import qualified Data.Text as T

type RawJQL = T.Text

data JQL
    = And JQL [JQL]
    | Or JQL [JQL]
    | Eq T.Text T.Text
    | In T.Text [T.Text]
    | CustomJQL RawJQL

toRawJQL :: JQL -> RawJQL
toRawJQL (And x []) = toRawJQL x
toRawJQL (And x xs) = bracketWrap . T.intercalate " AND " . fmap toRawJQL $ (x : xs)
toRawJQL (Or x []) = toRawJQL x
toRawJQL (Or x xs) = bracketWrap . T.intercalate " OR " . fmap toRawJQL $ (x : xs)
toRawJQL (Eq x y) = x `T.append` " = " `T.append` y
toRawJQL (In key values) = key `T.append` " in " `T.append` (bracketWrap . T.intercalate "," $ values)
toRawJQL (CustomJQL rawJQL) = bracketWrap rawJQL

bracketWrap :: RawJQL -> RawJQL
bracketWrap x = "(" `T.append` x `T.append` ")"

quote :: RawJQL -> RawJQL
quote x = "\"" `T.append` x `T.append` "\""
