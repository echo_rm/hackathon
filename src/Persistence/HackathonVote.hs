{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Persistence.HackathonVote
    ( addVote
    , removeVote
    , countVotesOnRound
    , countAllVotesOnRoundByRoom
    , countAllVotesOnRoundByRoomByTeam
    , countAllVotesOnRoundRoomByTeam
    , countVotesOnTeamsInRound
    , countCurrentVotesOnTeamInRound
    , RankedResult(..)
    , getAllRankedVotesForTeam
    ) where

import           Application
import           Control.Monad                      (forM)
import qualified Data.HackathonRequest              as HR
import           Data.Maybe                         (listToMaybe)
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.SqlQQ
import qualified Persistence.AnonymousTenantUser    as ATU
import qualified Persistence.HackathonIds           as HI
import qualified Persistence.HackathonRoom          as HRM
import qualified Persistence.HackathonRound         as RND
import qualified Persistence.HackathonRoundRoom     as HRR
import qualified Persistence.HackathonTeam          as HT
import qualified Persistence.TenantUser             as TU
import           Snap.Snaplet.PostgresqlSimple
import qualified Data.Text as T
import qualified Data.HackathonConstants as HC

data Vote = Vote
    { voteId                    :: HI.VoteId
    , voteTenantUserId          :: Maybe HI.TenantUserId
    , voteAnonymousTenantUserId :: Maybe HI.AnonymousTenantUserId
    , voteHackathonTeamId       :: HI.HackathonTeamId
    , voteRoundId               :: HI.HackathonRoundId
    , voteRoomId                :: HI.HackathonRoomId
    }

instance FromRow Vote where
    fromRow = Vote <$> field <*> field <*> field <*> field <*> field <*> field

tenantUserId :: HR.VoteableUser -> Maybe HI.TenantUserId
tenantUserId (HR.VoteableTenantUser tenantUser) = Just . TU.tuId $ tenantUser
tenantUserId _ = Nothing

anonymousTenantUserId :: HR.VoteableUser -> Maybe HI.AnonymousTenantUserId
anonymousTenantUserId (HR.VoteableAnonUser anonUser) = Just . ATU.atuId $ anonUser
anonymousTenantUserId _ = Nothing

addVote :: HR.VoteableUser -> HT.HackathonTeam -> RND.HackathonRound -> HRM.HackathonRoom -> AppHandler (Maybe Vote)
addVote vu hTeam hRound hRoom = do
    potentialVoteId <- listToMaybe . fmap fromOnly <$> query
        [sql|
            INSERT INTO vote (tenant_user_id, anonymous_tenant_user_id, hackathon_team_id, round_id, room_id)
            VALUES (?, ?, ?, ?, ?) RETURNING ID
        |] (tuId, atuId, HT.htId hTeam, RND.hroundId hRound, HRM.hrId hRoom)
    case potentialVoteId of
        Nothing -> return Nothing
        Just vId -> return . Just $ Vote
            { voteId = vId
            , voteTenantUserId = tuId
            , voteAnonymousTenantUserId = atuId
            , voteHackathonTeamId = HT.htId hTeam
            , voteRoundId = RND.hroundId hRound
            , voteRoomId = HRM.hrId hRoom
            }
    where
        tuId = tenantUserId vu
        atuId = anonymousTenantUserId vu

removeVote :: HR.VoteableUser -> HT.HackathonTeam -> RND.HackathonRound -> HRM.HackathonRoom -> AppHandler Bool
removeVote (HR.VoteableTenantUser tenantUser) hTeam hRound hRoom = do
    votesRemoved <- execute
        [sql|
            DELETE FROM vote WHERE id IN (
              SELECT id
              FROM vote
              WHERE tenant_user_id = ?
              AND round_id = ?
              AND room_id = ?
              AND hackathon_team_id = ?
              LIMIT 1
            )
        |] (TU.tuId tenantUser, RND.hroundId hRound, HRM.hrId hRoom, HT.htId hTeam)
    return (votesRemoved > 0)
removeVote (HR.VoteableAnonUser anonUser) hTeam hRound hRoom = do
    votesRemoved <- execute
        [sql|
            DELETE FROM vote WHERE id IN (
              SELECT id
              FROM vote
              WHERE anonymous_tenant_user_id = ?
              AND round_id = ?
              AND room_id = ?
              AND hackathon_team_id = ?
              LIMIT 1
            )
        |] (ATU.atuId anonUser, RND.hroundId hRound, HRM.hrId hRoom, HT.htId hTeam)
    return (votesRemoved > 0)

countVotesOnRound :: HR.VoteableUser -> RND.HackathonRound -> AppHandler Integer
countVotesOnRound (HR.VoteableTenantUser tenantUser) hRound = head . fmap fromOnly <$> query
    [sql|
        SELECT count(hv.*)
        FROM vote hv
        WHERE hv.round_id = ?
        AND hv.tenant_user_id = ?
    |] (RND.hroundId hRound, TU.tuId tenantUser)
countVotesOnRound (HR.VoteableAnonUser anonUser) hRound = head . fmap fromOnly <$> query
    [sql|
        SELECT count(hv.*)
        FROM vote hv
        WHERE hv.round_id = ?
        AND hv.anonymous_tenant_user_id = ?
    |] (RND.hroundId hRound, ATU.atuId anonUser)

countAllVotesOnRoundByRoom :: HI.HackathonRoundId -> HI.HackathonRoomId -> AppHandler Integer
countAllVotesOnRoundByRoom hRoundId hRoomId = head . fmap fromOnly <$> query
    [sql|
        SELECT count(*)
        FROM vote
        WHERE round_id = ?
        AND room_id = ?
    |] (hRoundId, hRoomId)

countAllVotesOnRoundByRoomByTeam :: RND.HackathonRound -> HRM.HackathonRoom -> AppHandler [(Integer, HT.HackathonTeam)]
countAllVotesOnRoundByRoomByTeam hRound hRoom = do
    res <- query
        [sql|
        SELECT count(v.hackathon_team_id) as votes, t.*
        FROM vote v, hackathon_team t
        WHERE v.hackathon_team_id = t.id
        AND v.round_id = ?
        AND v.room_id = ?
        GROUP BY t.id;
        |] (RND.hroundId hRound, HRM.hrId hRoom)
    forM res $ \(Only votes :. hTeam) -> return (votes, hTeam)

countAllVotesOnRoundRoomByTeam :: HRR.HackathonRoundRoom -> AppHandler [(HT.HackathonTeam, Integer)]
countAllVotesOnRoundRoomByTeam hRoundRoom = do
    res <- query
        [sql|
        SELECT count(v.*), t.id, t.hackathon_id, t.room_id, t.round_id, t.issue_id, t.issue_key, t.issue_summary
        FROM hackathon_team t
        LEFT OUTER JOIN vote v ON (t.id = v.hackathon_team_id AND t.room_id = v.room_id AND t.round_id = v.round_id)
        WHERE t.round_id = ?
        AND t.room_id = ?
        GROUP BY t.id, v.hackathon_team_id
        |] (HRR.hrrRoundId hRoundRoom, HRR.hrrRoomId hRoundRoom)
    forM res $ \(Only votes :. hTeam) -> return (hTeam, votes)

countAllVotesOnRoundByTeam :: RND.HackathonRound -> AppHandler [(Integer, HT.HackathonTeam)]
countAllVotesOnRoundByTeam hRound = do
    res <- query
        [sql|
        SELECT count(v.hackathon_team_id) as votes, t.id, t.hackathon_id, t.room_id, t.round_id, t.issue_id, t.issue_key, t.issue_summary
        FROM vote v, hackathon_team t
        WHERE v.hackathon_team_id = t.id
        AND v.round_id = ?
        GROUP BY t.id;
        |] (Only . RND.hroundId $ hRound)
    forM res $ \(Only votes :. hTeam) -> return (votes, hTeam)

countVotesOnTeamsInRound :: HR.VoteableUser -> RND.HackathonRound -> AppHandler [(Integer, HI.HackathonTeamId, HI.HackathonRoomId)]
countVotesOnTeamsInRound (HR.VoteableTenantUser tenantUser) hRound = query
    [sql|
        SELECT count(*), hv.hackathon_team_id, hv.room_id
        FROM vote hv
        WHERE hv.round_id = ?
        AND hv.tenant_user_id = ?
        GROUP BY hv.hackathon_team_id, hv.room_id;
    |] (RND.hroundId hRound, TU.tuId tenantUser)
countVotesOnTeamsInRound (HR.VoteableAnonUser anonUser) hRound = query
    [sql|
        SELECT count(*), hv.hackathon_team_id, hv.room_id
        FROM vote hv
        WHERE hv.round_id = ?
        AND hv.anonymous_tenant_user_id = ?
        GROUP BY hv.hackathon_team_id, hv.room_id;
    |] (RND.hroundId hRound, ATU.atuId anonUser)

countCurrentVotesOnTeamInRound :: HR.VoteableUser -> HT.HackathonTeam -> AppHandler Integer
countCurrentVotesOnTeamInRound (HR.VoteableTenantUser tenantUser) hTeam = head . fmap fromOnly <$> query
    [sql|
        SELECT count(hv.*)
        FROM vote hv, hackathon_team ht
        WHERE hv.tenant_user_id = ?
        AND ht.id = ?
        AND hv.hackathon_team_id = ht.id
        AND hv.round_id = ht.round_id
    |] (TU.tuId tenantUser, HT.htId hTeam)
countCurrentVotesOnTeamInRound (HR.VoteableAnonUser anonUser) hTeam = head . fmap fromOnly <$> query
    [sql|
        SELECT count(hv.*)
        FROM vote hv, hackathon_team ht
        WHERE hv.anonymous_tenant_user_id = ?
        AND ht.id = ?
        AND hv.hackathon_team_id = ht.id
        AND hv.round_id = ht.round_id
    |] (ATU.atuId anonUser, HT.htId hTeam)

data RankedResult = RankedResult
    { rrTeamId :: HI.HackathonTeamId
    , rrRoundId :: HI.HackathonRoundId
    , rrRoomId :: HI.HackathonRoomId
    , rrRoundName :: T.Text
    , rrRoundSequenceNumber :: Integer
    , rrRoomName :: T.Text
    , rrVotes :: Integer
    , rrRank :: Integer
    }

instance FromRow RankedResult where
    fromRow = RankedResult <$> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field

getAllRankedVotesForTeam :: HT.HackathonTeam -> AppHandler [RankedResult]
getAllRankedVotesForTeam hTeam = query
    [sql|
    WITH counted_votes as
    (
        SELECT
        t.id as t_id,
        d.id as d_id,
        m.id as m_id,
        count(v.id) as votes
        FROM vote v, hackathon h, hackathon_team t, round_room rr, round d, room m
        WHERE v.round_id = d.id
            AND v.room_id = m.id
            AND v.hackathon_team_id = t.id
            AND rr.round_id = d.id
            AND rr.room_id = m.id
            AND rr.status = ?
            AND t.hackathon_id = h.id
            AND h.id = ?
            AND EXISTS(SELECT id FROM vote WHERE round_id = rr.round_id AND room_id = rr.room_id AND hackathon_team_id = ?)
        GROUP BY t.id, d.id, m.id
    ), ranked_counted_votes as
    (
        SELECT cv.*, rank() OVER (PARTITION BY cv.d_id, cv.m_id ORDER BY cv.votes DESC) FROM counted_votes cv
    )
    SELECT rcv.t_id, rcv.d_id, rcv.m_id, d.name as round_name, d.sequence_number as round_sequence_number, m.name as room_name, rcv.votes, rcv.rank
    FROM ranked_counted_votes rcv, round d, room m
    WHERE rcv.d_id = d.id 
    AND rcv.m_id = m.id
    AND rcv.t_id = ?
    |] (HC.Locked, HT.htHackathonId hTeam, HT.htId hTeam, HT.htId hTeam)

{-
TODO perform the results computation by running this
getAllRankedVotesForRoundRoom :: RND.HackathonRound -> AppHandler [RankedResult]
getAllRankedVotesForRoundRoom hRound = query 
    [sql|
    SELECT
        vbt.*,
        rank() OVER (PARTITION BY vbt.d_id, vbt.m_id ORDER BY vbt.votes DESC)
    FROM (
        SELECT
            t.id as t_id,
            d.id as d_id,
            m.id as m_id,
            d.name as round_name,
            m.name as room_name,
            (SELECT count(*) FROM vote v WHERE v.round_id = d.id AND v.hackathon_team_id = t.id) AS votes
        FROM hackathon h, hackathon_team t, round_room rr, round d, room m
        WHERE rr.round_id = d.id
                AND rr.room_id = m.id
                AND d.hackathon_id = h.id
                AND t.hackathon_id = h.id
                AND rr.status = ?
                AND h.id = ?
                AND rr.round_id = ?
        ) as vbt
    ;
    |] (HC.Locked, RND.hroundHackathonId hRound, RND.hroundId hRound)
-}