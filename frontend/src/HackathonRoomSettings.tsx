import * as React from 'react';

import { RoundRoomDetails } from './hackathon-client';
import DynamicTable, { DynamicTableHead } from '@atlaskit/dynamic-table';
import Select from '@atlaskit/select';

export type Props = {
  roundRooms: Array<RoundRoomDetails>;

};

export class HackathonRoomSettings extends React.PureComponent<Props> {
  private static RoundRoomTableHead: DynamicTableHead = {
    cells: [{
      key: 'select',
      content: <span>TODO checkbox</span>,
      isSortable: false
    }, {
       key: 'room',
       content: <span>Room</span>,
       isSortable: true
    }, {
       key: 'round',
       content: <span>Current round</span>,
       isSortable: true
    }, {
       key: 'status',
       content: <span>Status</span>,
       isSortable: true
    }, {
       key: 'teams',
       content: <span>Teams</span>,
       isSortable: true
    }]
 };

  render() {
    const { RoundRoomTableHead } = HackathonRoomSettings;

    return (
      <>
        <h2>Room Settings</h2>
        <p>
          Use this section to configure the current round and status for your rooms. Voters only get a certain
          number of votes per round. So the current round for a room should match which round of your hackathon you
          are in. And voters can only vote on rooms that are Active; a room becomes Locked automatically when you
          reveal the results on the leaderboard.
        </p>
        <div>

        </div>
        <Select
          options={[]}
          placeholder="Set current round"
          onChange={(option, action) => console.log(`${option}-${action}`)}
        />
        <Select
          options={[]}
          placeholder="Set status"
          onChange={(option, action) => console.log(`${option}-${action}`)}
        />
        <DynamicTable
          head={RoundRoomTableHead}
          rows={[]}
          defaultSortKey="round"
          defaultSortOrder="DESC"
        />
      </>
      )
    }
  }