module WithToken
    ( tenantFromToken
    , rawTenantFromToken
    ) where

import           Application
import           Control.Monad.IO.Class     (liftIO)
import           Control.Monad.Trans.Class  (lift)
import           Control.Monad.Trans.Either
import qualified Data.ByteString.Char8      as BSC
import qualified Data.CaseInsensitive       as DC
import           Data.MaybeUtil
import qualified Data.Time.Clock            as DTC
import           HandlerHelpers
import qualified Persistence.Tenant         as TN
import qualified Snap.AtlassianConnect      as AC
import qualified Snap.Core                  as SC
import qualified Snap.Helpers               as SH

acHeaderName :: DC.CI BSC.ByteString
acHeaderName = DC.mk . BSC.pack $ "X-acpt" -- See atlassian-connect-play-java PageTokenValidatorAction#TOKEN_KEY

tenantFromToken :: (AC.TenantWithUser -> AppHandler ()) -> AppHandler ()
tenantFromToken tenantApply = writeError . runEitherT $ do
    acTokenHeader <- EitherT (m2e noPageToken . SC.getHeader acHeaderName <$> SC.getRequest)
    tenant <- EitherT $ rawTenantFromToken acTokenHeader
    lift . tenantApply $ tenant
    where
       noPageToken = (SH.badRequest, "You need to provide a page token in the headers to use this resource. None was provided.")
       tooManyPageTokens = (SH.badRequest, "Too many page tokens were provided in the headers. Did not know which one to choose. Invalid request.")

rawTenantFromToken :: BSC.ByteString -> AppHandler (Either (Int, String) AC.TenantWithUser)
rawTenantFromToken acToken = runEitherT $ do
    connectData <- lift AC.getConnect
    pageToken <- hoistEither (handleFailedDecrypt $ AC.decryptPageToken (AC.connectAES connectData) acToken)
    let tokenExpiryTime = DTC.addUTCTime (getTokenTimeout connectData) (AC.pageTokenTimestamp pageToken)
    currentTime <- lift . liftIO $ DTC.getCurrentTime
    if DTC.diffUTCTime currentTime tokenExpiryTime < 0
       then EitherT (m2e noSuchTenant <$> lookupTenantWithPageToken pageToken)
       else left tokenExpired
    where
      handleFailedDecrypt = withErr SH.badRequest "Error decoding the token you provided:"
      noSuchTenant = (SH.notFound, "Your page token was valid but the tenant could not be found. Maybe it no longer exists.")
      tokenExpired = (SH.unauthorised, "Your token has expired. Please refresh the page.")

      getTokenTimeout = fromIntegral . AC.connectPageTokenTimeout

lookupTenantWithPageToken :: AC.PageToken -> AppHandler (Maybe AC.TenantWithUser)
lookupTenantWithPageToken pageToken = fmap (flip (,) (AC.pageTokenUser pageToken)) <$> TN.lookupTenant (AC.pageTokenHost pageToken)
