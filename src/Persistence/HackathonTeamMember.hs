{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards   #-}

module Persistence.HackathonTeamMember
  ( HackathonTeamMember(..)
  , addTeamMember
  , removeTeamMemberByIssueId
  , isTeamMember
  , getTeamsAndMembersInRoomsCurrentRound
  ) where

import           Application
import           Control.Arrow                      (second)
import           Data.Int                           (Int64)
import qualified Data.Map                           as M
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.SqlQQ
import qualified Persistence.Hackathon              as PH
import qualified Persistence.HackathonIds           as HI
import qualified Persistence.HackathonTeam          as HT
import qualified Persistence.TenantUser             as TU
import qualified Snap.AtlassianConnect              as AC
import           Snap.Snaplet.PostgresqlSimple

data HackathonTeamMember = HackathonTeamMember
    { htmTenantUserId    :: HI.TenantUserId
    , htmHackathonTeamId :: HI.HackathonTeamId
    } deriving (Show)

instance FromRow HackathonTeamMember where
    fromRow = HackathonTeamMember <$> field <*> field

-- TODO The tenant user and the hackathon team should belong to the same tenant but the query does not enforce this.
addTeamMember :: TU.TenantUser -> HI.HackathonTeamId -> AppHandler Int64
addTeamMember user teamId = execute
    [sql|
      INSERT INTO team_members (tenant_user_id, hackathon_team_id)
      SELECT ?, ?
      WHERE NOT EXISTS
        (SELECT *
         FROM team_members
         WHERE tenant_user_id = ?
         AND hackathon_team_id = ?)
    |] (TU.tuId user, teamId, TU.tuId user, teamId)

removeTeamMemberByIssueId :: AC.Tenant -> TU.TenantUser -> AC.IssueId -> AppHandler Int64
removeTeamMemberByIssueId tenant user issueId = execute
    [sql|
        DELETE FROM team_members tm
        USING hackathon_team ht, hackathon h
        WHERE tm.hackathon_team_id = ht.id
        AND ht.hackathon_id = h.id
        AND h.tenant_id = ?
        AND ht.issue_id = ?
        AND tm.tenant_user_id = ?
    |] (AC.tenantId tenant, issueId, TU.tuId user)

isTeamMember :: HT.HackathonTeam -> TU.TenantUser -> AppHandler Bool
isTeamMember hTeam tenantUser = do
    count <- head . fmap fromOnly <$> query
        [sql|
            SELECT count(*)
            FROM team_members
            WHERE tenant_user_id = ?
            AND hackathon_team_id = ?
        |] (TU.tuId tenantUser, HT.htId hTeam)
    return (count > (0 :: Integer))

getTeamsAndMembersInRoomsCurrentRound :: PH.Hackathon -> HI.HackathonRoomId -> AppHandler (M.Map HT.HackathonTeam [TU.TenantUser])
getTeamsAndMembersInRoomsCurrentRound hackathon roomId = do
   teamsAndUsers <- fmap (\(team :. user) -> (team, user)) <$> query
      [sql|
      SELECT ht.id, ht.hackathon_id, ht.room_id, ht.round_id, ht.issue_id, ht.issue_key, ht.issue_summary, tu.id, tu.tenant_id, tu.key, tu.email, tu.displayname, tu.timezone, tu.small_avatar_url, tu.last_updated
      FROM hackathon_team ht, room hr, team_members tm, tenant_user tu
      WHERE hr.id = ?
      AND ht.hackathon_id = ?
      AND hr.hackathon_id = ht.hackathon_id
      AND hr.id = ht.room_id
      AND hr.current_round_id = ht.round_id
      AND tm.hackathon_team_id = ht.id
      AND tm.tenant_user_id = tu.id
      ORDER BY ht.issue_id
      |] (roomId, PH.hackathonId hackathon)
   return . M.fromListWith (++) . fmap (second return) $ (teamsAndUsers :: [(HT.HackathonTeam, TU.TenantUser)])


