{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module HackathonHandlers
    ( handleHackathons
    , handleHackathon
    , handleHackathonSummary
    , handleHackathonVoteable
    , handleHackathonRoundRoomStatus
    , handleHackathonRoundConfiguration
    , handleHackathonRestrictions
    , handleHackathonVotingRules
    ) where

import           AesonHelpers
import           Application
import           Control.Monad                     (forM, void, when)
import           Control.Monad.Trans.Class         (lift)
import           Control.Monad.Trans.Either
import           Control.Monad.Trans.Maybe
import           Data.Aeson
import           Data.Aeson.Types                  (fieldLabelModifier)
import           Data.Either                       (partitionEithers)
import           Data.HackathonConstants
import qualified Data.HackathonRequest             as HR
import           Data.MaybeUtil
import qualified Data.Text                         as T
import qualified Data.Text.Encoding                as T
import qualified Data.Time.Clock                   as C
import           GHC.Generics
import           HandlerHelpers
import qualified HostRequests.UserDetails          as HU
import qualified Persistence.Hackathon             as PH
import qualified Persistence.HackathonIds          as HI
import qualified Persistence.HackathonRoom         as HRM
import qualified Persistence.HackathonRound        as RND
import qualified Persistence.HackathonRoundRoom    as HRR
import qualified Persistence.HackathonTeam         as HT
import qualified Persistence.HackathonVote         as HV
import qualified Persistence.Tenant                as PT
import           ShortenHelper
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Core                         as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import           TenantWithVoteableUser
import qualified WithToken                         as WT

data HackathonResponse = HackathonResponse
    { hHackathons :: [ HackathonDetails ]
    } deriving (Show, Generic)

data HackathonDetails = HackathonDetails
    { hProjectId   :: AC.ProjectId
    , hProjectKey  :: AC.ProjectKey
    , hProjectName :: T.Text
    , hRestriction :: VotingRestriction
    , hStartDate   :: C.UTCTime
    , hEndDate     :: C.UTCTime
    , hShortUrl    :: Maybe String
    } deriving (Show, Generic)

instance ToJSON HackathonResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "h" }

instance ToJSON HackathonDetails where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "h" }

standardAuthError :: AppHandler ()
standardAuthError = respondWithError unauthorised "You need to login before you can make this request."

toHackathonDetails :: PH.Hackathon -> HackathonDetails
toHackathonDetails phh = HackathonDetails
   { hProjectId  = PH.hackathonProjectId phh
   , hProjectKey = PH.hackathonProjectKey phh
   , hProjectName = PH.hackathonProjectName phh
   , hRestriction = PH.hackathonVotingRestriction phh
   , hStartDate  = PH.hackathonStartDate phh
   , hEndDate    = PH.hackathonEndDate phh
   , hShortUrl = PH.hackathonShortUrl phh
   }

handleHackathons :: AppHandler ()
handleHackathons = handleMethods
    [ (SC.GET, WT.tenantFromToken getHackathons) ]

getHackathons :: AC.TenantWithUser -> AppHandler ()
getHackathons (_, Nothing) = standardAuthError
getHackathons (tenant, Just _) = do
   hackathons <- PH.getAllHackathons tenant
   hackathonsWithUrls <- mapM (ensureHackathonWithShortUrl tenant) hackathons
   case partitionEithers hackathonsWithUrls of
      ([], hackathonsSuccessful)  -> writeJson (HackathonResponse $ fmap toHackathonDetails hackathonsSuccessful)
      (xs, _) -> respondWithErrors internalServer (fmap showError xs)

showError :: AC.ProductErrorResponse -> String
showError e = "Shorten error: " ++ (show . AC.perCode $ e) ++ " - " ++ (T.unpack . AC.perMessage $ e)

handleHackathon :: AppHandler ()
handleHackathon = handleMethods
    [ (SC.POST, getHackathonDetails)
    ]

getHackathonDetails :: AppHandler ()
getHackathonDetails = do
    request <- SC.readRequestBody size10KB
    let potentialHackathonRequest = eitherDecode request :: Either String HR.HackathonRequest
    case potentialHackathonRequest of
        Left errMsg -> respondWithError badRequest $ "Could not get the hackathon details: " ++ errMsg
        Right vud -> tenantWithVoteableUserFromHackathonRequest vud $ \_ hackathon _ -> writeJson (toHackathonDetails hackathon)

handleHackathonSummary :: AppHandler()
handleHackathonSummary = handleMethods
    [ (SC.POST, getHackathonSummary)
    ]

getHackathonSummary :: AppHandler ()
getHackathonSummary = do
    request <- SC.readRequestBody size10KB
    let potentialHackathonRequest = eitherDecode request :: Either String HR.HackathonRequest
    case potentialHackathonRequest of
        Left errMsg -> respondWithError badRequest $ "Could not get the hackathon summary: " ++ errMsg
        Right r -> tenantWithVoteableUserFromHackathonRequest r hackathonSummaryHelper

hackathonSummaryHelper :: AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
hackathonSummaryHelper _ _ Nothing = respondWithError forbidden "You cannot get round room details if you do not provide a valid token."
hackathonSummaryHelper tenant hackathon _ = do
    pRoundRoomDetails <- mapM (detailsForRound tenant hackathon) =<< RND.getVoteableRoundsInHackathon hackathon
    pHackathonWithUrl <- ensureHackathonWithShortUrl tenant hackathon
    case (pHackathonWithUrl, sequence pRoundRoomDetails) of
      (Left x, Left xs) -> respondWithErrors internalServer $ fmap showError (x : xs)
      (Left x, _) -> respondWithError internalServer (showError x)
      (_, Left xs) -> respondWithErrors internalServer $ fmap showError xs
      (Right hackathonWithUrl, Right roundRoomDetails) -> writeJson . toHackathonSummary hackathonWithUrl . concat $ roundRoomDetails

toHackathonSummary :: PH.Hackathon -> [RoundRoomVoteDetails] -> HackathonSummary
toHackathonSummary hackathon ds = HackathonSummary
    { hsProjectName = PH.hackathonProjectName hackathon
    , hsStartDate = PH.hackathonStartDate hackathon
    , hsEndDate = PH.hackathonEndDate hackathon
    , hsShortUrl = PH.hackathonShortUrl hackathon
    , hsTeamIssueTypeId = PH.hackathonTeamIssueTypeId hackathon
    , hsRoundRoomVotes = ds
    }

detailsForRound :: AC.Tenant -> PH.Hackathon -> RND.HackathonRound -> AppHandler (Either [AC.ProductErrorResponse] [RoundRoomVoteDetails])
detailsForRound tenant hackathon hRound = do
   details <- HRR.getRoomDetailsForRound hRound
   potentialDetails <- forM details $ \(roundRoom, hRoom) -> runEitherT $ do
      roomWithUrl <- EitherT $ ensureRoomWithShortUrl tenant hackathon hRoom
      lift $ detailsForComplex roundRoom hRound roomWithUrl
   case partitionEithers potentialDetails of
     ([], successfulDetails) -> return . Right $ successfulDetails
     (xs, _) -> return . Left $ xs

detailsForComplex :: HRR.HackathonRoundRoom -> RND.HackathonRound -> HRM.HackathonRoom -> AppHandler RoundRoomVoteDetails
detailsForComplex roundRoom hRound hRoom = do
   numberOfTeams <- HT.countTeamsInRoundRoom hRound hRoom
   numberOfVotes <- HV.countAllVotesOnRoundByRoom (RND.hroundId hRound) (HRM.hrId hRoom)
   return RoundRoomVoteDetails
        { rrvdRound = RoundVoteDetails
            { rvdId = RND.hroundId hRound
            , rvdName = RND.hroundName hRound
            , rvdSequenceNumber = RND.hroundSequenceNumber hRound
            }
        , rrvdRoom = RoomVoteDetails
            { rmvId = HRM.hrId hRoom
            , rmvName = HRM.hrName hRoom
            , rmvShortUrl = HRM.hrShortUrl hRoom
            }
        , rrvdStatus = T.pack . show . HRR.hrrStatus $ roundRoom
        , rrvdIsCurrentRound = HRR.hrrRoundId roundRoom == HRM.hrCurrentRoundId hRoom
        , rrvdNumberOfTeams = numberOfTeams
        , rrvdNumberOfVotes = numberOfVotes
        }

data HackathonSummary = HackathonSummary
    { hsProjectName     :: T.Text
    , hsStartDate       :: C.UTCTime
    , hsEndDate         :: C.UTCTime
    , hsShortUrl        :: Maybe String
    , hsTeamIssueTypeId :: Integer
    , hsRoundRoomVotes  :: [RoundRoomVoteDetails]
    } deriving (Show, Generic)

data RoundRoomVoteDetails = RoundRoomVoteDetails
    { rrvdRound          :: RoundVoteDetails
    , rrvdRoom           :: RoomVoteDetails
    , rrvdStatus         :: T.Text
    , rrvdIsCurrentRound :: Bool
    , rrvdNumberOfTeams  :: Integer
    , rrvdNumberOfVotes  :: Integer
    } deriving (Show, Generic)

data RoundVoteDetails = RoundVoteDetails
   { rvdId             :: HI.HackathonRoundId
   , rvdName           :: T.Text
   , rvdSequenceNumber :: Maybe Integer
   } deriving (Show, Generic)

data RoomVoteDetails = RoomVoteDetails
   { rmvId       :: HI.HackathonRoomId
   , rmvName     :: T.Text
   , rmvShortUrl :: Maybe String
   } deriving (Show, Generic)

instance ToJSON RoundRoomVoteDetails where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rrvd" }

instance ToJSON RoundVoteDetails where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rvd" }

instance ToJSON RoomVoteDetails where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rmv" }

instance ToJSON HackathonSummary where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "hs" }

handleHackathonVoteable :: AppHandler ()
handleHackathonVoteable = handleMethods
    [ (SC.GET, WT.tenantFromToken getVoteableRounds)
    ]

getVoteableRounds :: AC.TenantWithUser -> AppHandler ()
getVoteableRounds (_, Nothing) = standardAuthError
getVoteableRounds (tenant, Just _) =
    writeError . withPG . runEitherT $ do
        hackathon <- EitherT $ getHackathonFromProjectId tenant
        voteableRounds <- lift $ RND.getVoteableRoundsInHackathon hackathon
        lift $ writeJson . RoundsList . fmap toRoundDetails $ voteableRounds

toRoundDetails :: RND.HackathonRound -> RoundDetails
toRoundDetails h = RoundDetails
    { rdId              = RND.hroundId h
    , rdName            = RND.hroundName h
    , rdType            = T.pack . show . RND.hroundType $ h
    , rdSequenceNumber  = RND.hroundSequenceNumber h
    , rdVotesPerUser    = RND.hroundVotesPerUser h
    , rdVotesUserTeam   = RND.hroundVotesUserTeam h
    }

data RoundsList = RoundsList
    { rlRounds :: [RoundDetails]
    } deriving (Show, Generic)

instance ToJSON RoundsList where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rl"
        }

data RoundDetails = RoundDetails
    { rdId             :: HI.HackathonRoundId
    , rdName           :: T.Text
    , rdType           :: T.Text
    , rdSequenceNumber :: Maybe Integer
    , rdVotesPerUser   :: Integer
    , rdVotesUserTeam  :: Integer
    } deriving (Show, Generic)

instance ToJSON RoundDetails where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rd"
        }

handleHackathonRoundRoomStatus :: AppHandler ()
handleHackathonRoundRoomStatus = handleMethods
    [ ( SC.PUT, WT.tenantFromToken handleBulkRoundRoomStatusUpdate)
    ]

handleBulkRoundRoomStatusUpdate :: AC.TenantWithUser -> AppHandler ()
handleBulkRoundRoomStatusUpdate (_, Nothing) = standardAuthError
handleBulkRoundRoomStatusUpdate (tenant, Just userKey) = do
    request <- SC.readRequestBody size10KB
    writeError . withPG . runEitherT $ do
        updateRequest <- hoistEither . withErrCode badRequest . eitherDecode $ request
        hackathon <- EitherT (m2e missingHackathon <$> PH.getHackathonByProjectId tenant (surProjectId updateRequest))
        EitherT $ requirePermissionsE [HU.ProjectAdmin] tenant userKey hackathon
        void . lift $ mapM_ (runMaybeT . updateRoundRoomStatus hackathon (surNewStatus updateRequest)) (surRoundRoomIds updateRequest)
        void . lift $ respondNoContent
    where
        missingHackathon = (badRequest, "There is no hackathon with the provided project id.")

updateRoundRoomStatus :: PH.Hackathon -> RoundRoomStatus -> RoundRoomIdPair -> MaybeT AppHandler ()
updateRoundRoomStatus hackathon status roundRoomIds = do
    hRoom <- MaybeT $ HRM.getRoomById hackathon (rripRoomId roundRoomIds)
    hRound <- MaybeT $ RND.getHackathonRoundById hackathon (rripRoundId roundRoomIds)
    roundRoom <- MaybeT $ HRR.getOrCreateRoundRoom hRound hRoom status
    void . lift $ HRR.updateStatus roundRoom status

data StatusUpdateRequest = StatusUpdateRequest
    { surProjectId    :: AC.ProjectId
    , surRoundRoomIds :: [RoundRoomIdPair]
    , surNewStatus    :: RoundRoomStatus
    } deriving (Show, Generic)

instance FromJSON StatusUpdateRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "sur"
        }

data RoundRoomIdPair = RoundRoomIdPair
    { rripRoundId :: HI.HackathonRoundId
    , rripRoomId  :: HI.HackathonRoomId
    } deriving (Show, Generic)

instance FromJSON RoundRoomIdPair where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rrip"
        }

handleHackathonRoundConfiguration :: AppHandler ()
handleHackathonRoundConfiguration = handleMethods
    [ (SC.POST, getRoundConfiguration)
    , (SC.PUT, WT.tenantFromToken setRoundConfiguration)
    ]

getRoundConfiguration :: AppHandler ()
getRoundConfiguration = do
    request <- SC.readRequestBody size10KB
    let potentialRequest = eitherDecode request :: Either String RoundConfigurationRequest
    case potentialRequest of
        Left err -> respondWithError badRequest $ "Could not parse the request: " ++ err
        Right configurationRequest ->
            tenantWithVoteableUserFromHackathonRequest
                (rcrHackathon configurationRequest)
                (getRoundConfigurationWithAllDetails configurationRequest)

getRoundConfigurationWithAllDetails :: RoundConfigurationRequest -> AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
getRoundConfigurationWithAllDetails _ _ _ Nothing = respondWithError unauthorised "You need to be logged in to get the round configuration."
getRoundConfigurationWithAllDetails configRequest tenant hackathon (Just voteableUser) = do
    potentialRound <- RND.getHackathonRoundById hackathon (rcrRoundId configRequest)
    case potentialRound of
        Nothing -> respondWithError notFound "Could not find any round with the provided id."
        Just hRound -> writeJson . toRoundConfigurationResponse $ hRound

data RoundConfigurationRequest = RoundConfigurationRequest
    { rcrHackathon :: HR.HackathonRequest
    , rcrRoundId   :: Integer
    } deriving (Show, Generic)

instance FromJSON RoundConfigurationRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rcr"
        }

toRoundConfigurationResponse :: RND.HackathonRound -> RoundConfigurationResponse
toRoundConfigurationResponse hRound = RoundConfigurationResponse
    { rcrspVotesPerUser = RND.hroundVotesPerUser hRound
    , rcrspVotesUserTeam = RND.hroundVotesUserTeam hRound
    }

data RoundConfigurationResponse = RoundConfigurationResponse
    { rcrspVotesPerUser  :: Integer
    , rcrspVotesUserTeam :: Integer
    } deriving (Show, Generic)

instance ToJSON RoundConfigurationResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rcrsp"
        }

setRoundConfiguration :: AC.TenantWithUser -> AppHandler ()
setRoundConfiguration (_, Nothing) = standardAuthError
setRoundConfiguration tu@(tenant, Just userKey) = do
    request <- SC.readRequestBody size10KB
    writeError . withPG . runEitherT $ do
       updateRequest <- hoistEither . withErrCode badRequest . eitherDecode $ request
       hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant (rcurProjectId updateRequest))
       EitherT $ requirePermissionsE [HU.ProjectAdmin] tenant userKey hackathon
       maybe (return ()) (\vpu -> when (vpu > 0) . void . lift $ RND.updateVotesPerUser hackathon (rcurRoundIds updateRequest) vpu) (rcurNewVotesPerUser updateRequest)
       maybe (return ()) (\vpt -> when (vpt > 0) . void . lift $ RND.updateVotesUserTeam hackathon (rcurRoundIds updateRequest) vpt) (rcurNewVotesUserTeam updateRequest)
       void . lift $ respondNoContent

data RoundConfigurationUpdateRequest = RoundConfigurationUpdateRequest
    { rcurProjectId        :: AC.ProjectId
    , rcurRoundIds         :: [HI.HackathonRoundId]
    , rcurNewVotesPerUser  :: Maybe Integer
    , rcurNewVotesUserTeam :: Maybe Integer
    } deriving (Show, Generic)

instance FromJSON RoundConfigurationUpdateRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rcur"
        }

handleHackathonRestrictions :: AppHandler ()
handleHackathonRestrictions = handleMethods
    [ (SC.GET, getHackathonRestrictions)
    , (SC.PUT, WT.tenantFromToken setHackathonRestrictions)
    ]

-- This resource is read only and does not need to be protected
getHackathonRestrictions :: AppHandler ()
getHackathonRestrictions = do
    potentialTenant <- getTenantFromTenantKey
    case potentialTenant of
        Just tenant -> getHackathonRestrictionsWithTenant tenant
        Nothing -> WT.tenantFromToken $ \(tenant, _) -> getHackathonRestrictionsWithTenant tenant

getHackathonRestrictionsWithTenant :: AC.Tenant -> AppHandler ()
getHackathonRestrictionsWithTenant tenant =
    writeError . withPG . runEitherT $ do
        hackathon <- EitherT $ getHackathonFromProjectId tenant
        lift $ writeJson (HackathonRestrictionsResponse . PH.hackathonVotingRestriction $ hackathon)

getTenantFromTenantKey :: AppHandler (Maybe AC.Tenant)
getTenantFromTenantKey = do
    potentialTenantKey <- SC.getQueryParam "tenantKey"
    case potentialTenantKey of
        Nothing -> return Nothing
        Just tenantKey -> PT.lookupTenant . T.decodeUtf8 $ tenantKey

data HackathonRestrictionsResponse = HackathonRestrictionsResponse
    { hrrRestriction :: VotingRestriction
    } deriving (Show, Generic)

instance ToJSON HackathonRestrictionsResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "hrr"
        }

setHackathonRestrictions :: AC.TenantWithUser -> AppHandler ()
setHackathonRestrictions (_, Nothing) = standardAuthError
setHackathonRestrictions tu@(tenant, Just userKey) = do
    request <- SC.readRequestBody size10KB
    writeError . withPG . runEitherT $ do
       updateRequest <- hoistEither . withErrCode badRequest . eitherDecode $ request
       hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant (hruProjectId updateRequest))
       EitherT $ requirePermissionsE [HU.ProjectAdmin] tenant userKey hackathon
       void . lift $ PH.updateHackathonVotingRestriction tenant (hruProjectId updateRequest) (hruRestriction updateRequest)
       void . lift $ respondNoContent

data HackathonRestrictionUpdate = HackathonRestrictionUpdate
    { hruProjectId   :: AC.ProjectId
    , hruRestriction :: VotingRestriction
    } deriving (Show, Generic)

instance FromJSON HackathonRestrictionUpdate where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "hru"
        }

handleHackathonVotingRules :: AppHandler ()
handleHackathonVotingRules = handleMethods
    [ (SC.GET, WT.tenantFromToken getHackathonVotingRules)
    , (SC.PUT, WT.tenantFromToken setHackathonVotingRules)
    ]

getHackathonVotingRules :: AC.TenantWithUser -> AppHandler ()
getHackathonVotingRules (_, Nothing) = standardAuthError
getHackathonVotingRules (tenant, _) =
    writeError . withPG . runEitherT $ do
        hackathon <- EitherT $ getHackathonFromProjectId tenant
        lift $ writeJson (HackathonVotingRulesResponse . PH.hackathonMultipleRoomVoting $ hackathon)

data HackathonVotingRulesResponse = HackathonVotingRulesResponse
    { hvrrMultipleRoomVoting :: Bool
    } deriving (Show, Generic)

instance ToJSON HackathonVotingRulesResponse where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "hvrr"
        }

setHackathonVotingRules :: AC.TenantWithUser -> AppHandler ()
setHackathonVotingRules (_, Nothing) = standardAuthError
setHackathonVotingRules tu@(tenant, Just userKey) = do
   request <- SC.readRequestBody size10KB
   writeError . withPG . runEitherT $ do
      updateRequest <- hoistEither . withErrCode badRequest . eitherDecode $ request
      hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant (hvruProjectId updateRequest))
      EitherT $ requirePermissionsE [HU.ProjectAdmin] tenant userKey hackathon
      void . lift $ PH.updateHackathonVotingRules tenant (hvruProjectId updateRequest) (PH.HackathonVotingRules (hvruMultipleRoomVoting updateRequest))
      void . lift $ respondNoContent

data HackathonVotingRulesUpdate = HackathonVotingRulesUpdate
    { hvruProjectId          :: AC.ProjectId
    , hvruMultipleRoomVoting :: Bool
    } deriving (Show, Generic)

instance FromJSON HackathonVotingRulesUpdate where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "hvru"
        }

noSuchHackathon :: (Int, String)
noSuchHackathon = (badRequest, "There is no hackathon with the provided project id.")

