import * as React from 'react';
import { partialStoriesOf } from './stories-common';
import { DocsPage } from '../DocsPage';

const simpleMdContent = `
# Hello there

This is your captain speaking. Some points:

 - Nitwit
 - Odment
 - Blubber

And some code:

    1 + 2 = 3

There we go.
`;

export function stories(storyCreator: partialStoriesOf) {
    storyCreator('Docs page')
      .add('Default view', () => (
          <DocsPage mdContent={simpleMdContent} location="/docs/home" />
      ));
}