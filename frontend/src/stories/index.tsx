import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { HackathonDashboard } from '../HackathonDashboard';
import { partialStoriesOf } from './stories-common';
import { stories as storiesHackathonsOverview } from './stories-hackathons-overview';
import { stories as storiesHackathonSummary } from './stories-hackathon-summary';
import { stories as storiesDocs } from './stories-docs';
import { stories as storiesTeamPanel } from './stories-team-panel';
import { HackathonRestriction } from '../hackathon-client';

export function compileStories() {
  const localStoryCreator: partialStoriesOf = (name: string) => storiesOf(name, module);
  stories(localStoryCreator);
}

export default function stories(storyCreator: partialStoriesOf) {
  storiesDocs(storyCreator);
  storiesHackathonsOverview(storyCreator);
  storiesHackathonSummary(storyCreator);
  storiesTeamPanel(storyCreator);

  storyCreator('Dashboard')
    .add('Default Dashboard View', () => (
        <HackathonDashboard restriction={HackathonRestriction.LoginAndTeamMember} roundRooms={[]} />
    ));

  storyCreator('Leaderboard')
    .add('Default Team Panel View', () => (
        <div>TODO</div>
    ));

  storyCreator('Voter Room-Select')
    .add('Default', () => (<div>TODO</div>));

  storyCreator('Voter Team-Select')
    .add('Default', () => (<div>TODO</div>));

  storyCreator('Voter Team View')
    .add('Default', () => (<div>TODO</div>));
}