//The build will inline common dependencies into this file.

//For any third party dependencies, like jQuery, place them in the lib folder.

//Configure loading modules from the lib directory,
//except for 'app' ones, which are in a sibling
//directory.
require.config({
   'baseUrl': '/static/js/lib',
   'paths': {
      'app': '../app',
      'connect': '../connect',
      'host': '../host',
      'punycode': '//cdnjs.cloudflare.com/ajax/libs/punycode/1.2.3/punycode.min',
      'moment': '//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.min',
      'mustache': '//cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min',
      'underscore': '//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min',
      'jquery': '//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'
   },
   'shim': {
      'aui': {
         'deps': ['jquery'],
         'exports': 'AJS'
      },
      'jquery': {
         'deps': [],
         'exports': '$'
      }
   },
   'wrapShim': true
});

require(['connect/pagetoken']);
