{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module RoundTransitionHandlers
   ( handleRoundTransitions
   ) where

import           AesonHelpers
import           Application
import qualified Control.Arrow                  as A
import           Control.Monad                  (void)
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Either
import           Data.Aeson
import           Data.Aeson.Types               (fieldLabelModifier)
import           Data.Maybe                     (isJust, isNothing)
import           Data.MaybeUtil
import qualified Data.Set                       as S
import qualified Data.Text                      as T
import           GHC.Generics
import           HandlerHelpers
import qualified HostRequests.UserDetails       as UD
import qualified Persistence.Hackathon          as PH
import qualified Persistence.HackathonIds       as HI
import qualified Persistence.HackathonRoom      as HRM
import qualified Persistence.HackathonRound     as RND
import qualified Persistence.HackathonRoundRoom as HRR
import qualified Persistence.RoundTransition    as RT
import qualified Snap.AtlassianConnect          as AC
import qualified Snap.Core                      as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import qualified WithToken                      as WT

handleRoundTransitions :: AppHandler ()
handleRoundTransitions = handleMethods
   [ (SC.GET, WT.tenantFromToken getRoundTransitions)
   , (SC.POST, WT.tenantFromToken updateRoundTransitions)
   ]

getRoundTransitions :: AC.TenantWithUser -> AppHandler ()
getRoundTransitions (_, Nothing) = respondWithError unauthorised "You need to be logged in to get the round transitions."
getRoundTransitions (tenant, Just userKey) = do
   potentialRoundDetails <- runEitherT $ do
      roundId <- EitherT (m2e missingRoundId <$> getIntegerQueryParam "roundId")
      hackathon <- EitherT $ getHackathonFromProjectId tenant
      EitherT $ requirePermissionsE [UD.ProjectAdmin] tenant userKey hackathon
      hRound <- EitherT (m2e noSuchRound <$> RND.getHackathonRoundById hackathon roundId)
      lift $ RT.getRoundDetails hRound
   case potentialRoundDetails of
     Left err -> uncurry respondWithError err
     Right roundDetails -> writeJson . RoundTransitionsResponse . fmap toRoundTransitionResponse $ roundDetails
   where
      missingRoundId = (badRequest, "The round id was missing from the request.")
      noSuchRound = (badRequest, "The round that you requested did not exist.")

data RoundTransitionsResponse = RoundTransitionsResponse
   { rtrTransitions :: [RoundTransitionResponse]
   } deriving (Show, Generic)

instance ToJSON RoundTransitionsResponse where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rtr" }

toRoundTransitionResponse :: RT.RoundTransitionDetails -> RoundTransitionResponse
toRoundTransitionResponse (rt, rooms) = RoundTransitionResponse
   { rtId = RT.rtId rt
   , rtOrderNumber = RT.rtOrderNumber rt
   , rtJql = RT.rtJQL rt
   , rtRooms = fmap toRoundTransitionRoomRsp rooms
   }

data RoundTransitionResponse = RoundTransitionResponse
 { rtId          :: HI.RoundTransitionId
 , rtOrderNumber :: Integer
 , rtJql         :: Maybe T.Text
 , rtRooms       :: [RoundTransitionRoomRsp]
 } deriving (Show, Generic)

instance ToJSON RoundTransitionResponse where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rt" }

toRoundTransitionRoomRsp :: HRM.HackathonRoom -> RoundTransitionRoomRsp
toRoundTransitionRoomRsp rm = RoundTransitionRoomRsp
   { rtrrId = HRM.hrId rm
   , rtrrComponentId = HRM.hrComponentId rm
   , rtrrName = HRM.hrName rm
   }

data RoundTransitionRoomRsp = RoundTransitionRoomRsp
   { rtrrId          :: HI.HackathonRoomId
   , rtrrComponentId :: HI.ComponentId
   , rtrrName        :: T.Text
   } deriving (Show, Generic)

instance ToJSON RoundTransitionRoomRsp where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rtrr" }

updateRoundTransitions :: AC.TenantWithUser -> AppHandler ()
updateRoundTransitions (_, Nothing) = respondWithError unauthorised "You are not authorised to update the round transitions. You need to be logged in."
updateRoundTransitions (tenant, Just userKey) = do
   request <- SC.readRequestBody size10KB
   writeError . withPG . runEitherT $ do
      updateRequest <- hoistEither (A.left invalidData $ eitherDecode request)
      hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant (utrProjectId updateRequest))
      EitherT $ requirePermissionsE [UD.ProjectAdmin] tenant userKey hackathon
      currentRound <- EitherT (m2e noSuchRound <$> RND.getHackathonRoundById hackathon (utrRoundId updateRequest))
      nextRound <- EitherT (m2e noNextRound <$> RND.getNextRound currentRound)
      nextRooms <- lift (fmap snd <$> HRR.getRoomDetailsForRound nextRound)
      guardInvalidRounds updateRequest nextRooms
      lift $ RT.updateRoundTransitions currentRound (fmap toTransitionUpdate . utrTransitions $ updateRequest)
      void . lift $ respondNoContent
   where
      noSuchHackathon = (notFound, "A hackathon with the given project id could not be found.")
      noSuchRound = (notFound, "A round with the given roundId could not be found.")
      noNextRound = (badRequest, "The round that you provided has no next round.")

      invalidData :: String -> (Int, String)
      invalidData parseError = (badRequest, "The data that you passed into this request is not valid and could not be parsed: " ++ parseError)

guardInvalidRounds :: Monad x => UpdateTransitionsRequest -> [HRM.HackathonRoom] -> EitherT (Int, String) x ()
guardInvalidRounds ur rooms
   | noJql = left (badRequest, "You provided no transitions.")
   | missingJql = left (badRequest, "The last transition must have no jql and only the last transition is allowed to have no jql.")
   | requestedIds `S.isSubsetOf` actualIds = right ()
   | otherwise = left (badRequest, "Some of the room id's that you requested do not exist.")
   where
      noJql = null jqls
      missingJql = (any isNothing . init $ jqls) || (isJust . last $ jqls)
      jqls = fmap trJql . utrTransitions $ ur
      requestedIds = getRoomIds ur
      actualIds = S.fromList . fmap HRM.hrId $ rooms

toTransitionUpdate :: TransitionRequest -> RT.TransitionUpdate
toTransitionUpdate tr = RT.TransitionUpdate
   { RT.tuJql = trJql tr
   , RT.tuRooms = trRooms tr
   }

data UpdateTransitionsRequest = UpdateTransitionsRequest
   { utrProjectId   :: AC.ProjectId
   , utrRoundId     :: HI.HackathonRoundId
   , utrTransitions :: [TransitionRequest]
   } deriving (Show, Generic)

instance FromJSON UpdateTransitionsRequest where
   parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "utr" }

data TransitionRequest = TransitionRequest
   { trJql   :: Maybe T.Text
   , trRooms :: [HI.HackathonRoomId]
   } deriving (Show, Generic)

instance FromJSON TransitionRequest where
   parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "tr" }

getRoomIds :: UpdateTransitionsRequest -> S.Set HI.HackathonRoomId
getRoomIds = S.unions . fmap toRoomIds . utrTransitions
   where
      toRoomIds = S.fromList . trRooms
