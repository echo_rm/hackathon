{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Persistence.HackathonTeam
    ( HackathonTeam(..)
    , createHackathonTeam
    , getHackathonTeamById
    , getHackathonTeamByIssueId
    , deleteHackathonTeamByIssueId
    , updateTeamRound
    , updateTeamRoom
    , updateTeamIssueSummary
    , getRoundForTeam
    , getRoomForTeam
    , countTeamsInRoundRoom
    , clearTeamRoomIfOneOf
    , getTeamsInRoomsCurrentRound
    , getUnallocatedTeamsInRound
    , getTeamsInRoundRoom
    , getTeamsByIds
    , getHackathonFromTeam
    ) where

import           Application
import           Data.Int                           (Int64)
import           Data.Maybe                         (listToMaybe)
import           Data.Ord
import           Data.TextUtil
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.SqlQQ
import qualified Persistence.Hackathon              as PH
import qualified Persistence.HackathonIds           as HI
import qualified Persistence.HackathonRoom          as HRM
import qualified Persistence.HackathonRound         as RND
import qualified Persistence.HackathonRoundRoom     as HRR
import qualified Snap.AtlassianConnect              as AC
import           Snap.Snaplet.PostgresqlSimple

data HackathonTeam = HackathonTeam
    { htId           :: HI.HackathonTeamId
    , htHackathonId  :: HI.HackathonId
    , htRoomId       :: Maybe HI.HackathonRoomId
    , htRoundId      :: Maybe HI.HackathonRoundId
    , htIssueId      :: AC.IssueId
    , htIssueKey     :: AC.IssueKey
    , htIssueSummary :: AC.IssueSummary
    } deriving (Eq)

instance Ord HackathonTeam where
    compare = comparing htId

instance FromRow HackathonTeam where
    fromRow = HackathonTeam <$> field <*> field <*> field <*> field <*> field <*> field <*> field

createHackathonTeam
    :: PH.Hackathon
    -> AC.IssueId
    -> AC.IssueKey
    -> AC.IssueSummary
    -> Maybe RND.HackathonRound
    -> Maybe HRM.HackathonRoom
    -> AppHandler (Maybe HackathonTeam)
createHackathonTeam hackathon issueId issueKey issueSummary hRound hRoom = listToMaybe <$> query
    [sql|
      INSERT INTO hackathon_team (hackathon_id, issue_id, issue_key, issue_summary, round_id, room_id)
      VALUES (?, ?, ?, ?, ?, ?) RETURNING id, hackathon_id, room_id, round_id, issue_id, issue_key, issue_summary
    |] (PH.hackathonId hackathon, issueId, issueKey, ellipsisTruncate 512 issueSummary, fmap RND.hroundId hRound, fmap HRM.hrId hRoom)

getHackathonTeamById :: PH.Hackathon -> HI.HackathonTeamId -> AppHandler (Maybe HackathonTeam)
getHackathonTeamById hackathon teamId = listToMaybe <$> query
    [sql|
        SELECT ht.id, ht.hackathon_id, ht.room_id, ht.round_id, ht.issue_id, ht.issue_key, ht.issue_summary
        FROM hackathon_team ht
        WHERE ht.id = ?
        AND ht.hackathon_id = ?
    |] (teamId, PH.hackathonId hackathon)

getHackathonTeamByIssueId :: AC.Tenant -> AC.IssueId -> AppHandler (Maybe HackathonTeam)
getHackathonTeamByIssueId tenant issueId = listToMaybe <$> query
   [sql|
      SELECT ht.id, ht.hackathon_id, ht.room_id, ht.round_id, ht.issue_id, ht.issue_key, ht.issue_summary
      FROM hackathon_team ht, hackathon h
      WHERE ht.hackathon_id = h.id
      AND h.tenant_id = ?
      AND ht.issue_id = ?
   |] (AC.tenantId tenant, issueId)

deleteHackathonTeamByIssueId :: AC.Tenant -> AC.IssueId -> AppHandler Int64
deleteHackathonTeamByIssueId tenant issueId = execute
    [sql|
        DELETE FROM hackathon_team ht
        USING hackathon h
        WHERE ht.hackathon_id = h.id
        AND h.tenant_id = ?
        AND ht.issue_id = ?
    |] (AC.tenantId tenant, issueId)

updateTeamRound :: AC.Tenant -> AC.IssueId -> Maybe RND.HackathonRound -> AppHandler Int64
updateTeamRound tenant issueId hr = execute
    [sql|
        UPDATE hackathon_team
        SET round_id = ?
        FROM hackathon h
        WHERE hackathon_id = h.id
        AND h.tenant_id = ?
        AND issue_id = ?
    |] (fmap RND.hroundId hr, AC.tenantId tenant, issueId)

updateTeamRoom :: AC.Tenant -> AC.IssueId -> Maybe HRM.HackathonRoom -> AppHandler Int64
updateTeamRoom tenant issueId hr = execute
    [sql|
        UPDATE hackathon_team
        SET room_id = ?
        FROM hackathon h
        WHERE hackathon_id = h.id
        AND h.tenant_id = ?
        AND issue_id = ?
    |] (fmap HRM.hrId hr, AC.tenantId tenant, issueId)

updateTeamIssueSummary :: AC.Tenant -> AC.IssueId -> AC.IssueSummary -> AppHandler Int64
updateTeamIssueSummary tenant issueId summary = execute
    [sql|
        UPDATE hackathon_team
        SET issue_summary = ?
        FROM hackathon h
        WHERE hackathon_id = h.id
        AND h.tenant_id = ?
        AND issue_id = ?;
    |] (summary, AC.tenantId tenant, issueId)

getRoundForTeam :: HackathonTeam -> AppHandler (Maybe RND.HackathonRound)
getRoundForTeam team = listToMaybe <$> query
   [sql|
      SELECT r.id, r.hackathon_id, r.workflow_status_id, r.name, r.type, r.sequence_number, r.votes_per_user, r.votes_user_team, r.transition_tied_teams, r.match_unallocated_teams
      FROM round r
      WHERE r.id = ?
   |] (Only . htRoundId $ team)

getRoomForTeam :: HackathonTeam -> AppHandler (Maybe HRM.HackathonRoom)
getRoomForTeam team = listToMaybe <$> query
   [sql|
      SELECT id, hackathon_id, current_round_id, component_id, name, short_url
      FROM room
      WHERE id = ?
   |] (Only . htRoomId $ team)

countTeamsInRoundRoom :: RND.HackathonRound -> HRM.HackathonRoom -> AppHandler Integer
countTeamsInRoundRoom hRound hRoom = head . fmap fromOnly <$> query
    [sql|
        SELECT count(*)
        FROM hackathon_team ht
        WHERE ht.round_id = ? AND ht.room_id = ?
    |] (RND.hroundId hRound, HRM.hrId hRoom)

clearTeamRoomIfOneOf :: AC.Tenant -> AC.IssueId -> [HI.ComponentId] -> AppHandler Int64
clearTeamRoomIfOneOf tenant issueId componentIds = execute
    [sql|
        UPDATE hackathon_team
        SET room_id = NULL
        FROM hackathon h, room r
        WHERE hackathon_team.hackathon_id = h.id
        AND r.hackathon_id = h.id
        AND r.id = room_id
        AND h.tenant_id = ?
        AND hackathon_team.issue_id = ?
        AND r.component_id in ?
    |] (AC.tenantId tenant, issueId, In componentIds)

getTeamsInRoomsCurrentRound :: PH.Hackathon -> HI.HackathonRoomId -> AppHandler [HackathonTeam]
getTeamsInRoomsCurrentRound hackathon roomId = query
    [sql|
    SELECT ht.id, ht.hackathon_id, ht.room_id, ht.round_id, ht.issue_id, ht.issue_key, ht.issue_summary
    FROM hackathon_team ht, room hr
    WHERE hr.id = ?
    AND ht.hackathon_id = ?
    AND hr.hackathon_id = ht.hackathon_id
    AND hr.id = ht.room_id
    AND hr.current_round_id = ht.round_id;
    |] (roomId, PH.hackathonId hackathon)

getUnallocatedTeamsInRound :: RND.HackathonRound -> AppHandler [HackathonTeam]
getUnallocatedTeamsInRound hRound = query
    [sql|
    SELECT t.id, t.hackathon_id, t.room_id, t.round_id, t.issue_id, t.issue_key, t.issue_summary
    FROM hackathon_team t
    WHERE t.round_id = ?
    AND t.room_id IS NULL
    |] (Only . RND.hroundId $ hRound)

getTeamsInRoundRoom :: HRR.HackathonRoundRoom -> AppHandler [HackathonTeam]
getTeamsInRoundRoom hRoundRoom = query
    [sql|
    SELECT t.id, t.hackathon_id, t.room_id, t.round_id, t.issue_id, t.issue_key, t.issue_summary
    FROM hackathon_team t
    WHERE t.round_id = ?
    AND t.room_id = ?
    |] (HRR.hrrRoundId hRoundRoom, HRR.hrrRoomId hRoundRoom)

getTeamsByIds :: PH.Hackathon -> [AC.IssueId] -> AppHandler [HackathonTeam]
getTeamsByIds hackathon teamIssueIds = query
    [sql|
    SELECT t.id, t.hackathon_id, t.room_id, t.round_id, t.issue_id, t.issue_key, t.issue_summary
    FROM hackathon_team t
    WHERE t.hackathon_id = ?
    and t.issue_id in ?
    |] (PH.hackathonId hackathon, In teamIssueIds)

getHackathonFromTeam :: HackathonTeam -> AppHandler PH.Hackathon
getHackathonFromTeam hTeam = head <$> query
   [sql|
        SELECT id, tenant_id, project_id, project_key, project_name, hackathon_team_issue_type_id, activator, start_date, end_date, voting_restriction, multiple_room_voting, short_url
        FROM hackathon
        WHERE id = ?
   |] (Only . htHackathonId $ hTeam)
