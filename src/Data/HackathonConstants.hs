{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module Data.HackathonConstants
    ( roomComponentPrefix
    , pgEnvPre
    , projectSettingsKey
    , ProjectSettings
    , defaultProjectSettings
    , RoundRoomStatus(..)
    , VotingRestriction(..)
    , HackathonRoundType(..)
    ) where

import           AesonHelpers
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.Text                            as T
import           Database.PostgreSQL.Simple.FromField
import           Database.PostgreSQL.Simple.ToField
import           GHC.Generics

-- This is a constant that gets used regularily with JIRA
roomComponentPrefix :: T.Text
roomComponentPrefix = "Room - "

pgEnvPre :: String -> String
pgEnvPre = (++) "PG_HACKATHON_"

projectSettingsKey :: String
projectSettingsKey = "hackathon-settings"

data ProjectSettings = ProjectSettings
   { psIsHackathon :: Bool
   } deriving (Show, Generic)

instance ToJSON ProjectSettings where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "ps" }

defaultProjectSettings :: ProjectSettings
defaultProjectSettings = ProjectSettings True

-- This is a constant that gets used all over the code to represent round room status
data RoundRoomStatus
    = Active
    | Locked
    | Inactive
    deriving (Show, Enum, Eq)

-- Sending a RoundRoom to the database
instance ToField RoundRoomStatus where
    toField = toField . fromEnum

instance FromField RoundRoomStatus where
    fromField f = fmap toEnum . fromField f

-- Sending a round room over javascript
instance FromJSON RoundRoomStatus where
    parseJSON (String "active") = return Active
    parseJSON (String "inactive") = return Inactive
    parseJSON (String "locked") = return Locked
    parseJSON v = fail $ "Could not parse as a round-room status: " ++ show v

-- This is a constant that gets used for voting restrictions
data VotingRestriction
    = LoginAndTeamMember
    | LoginOnly
    | Anonymous
    deriving (Show, Enum, Eq)

instance ToField VotingRestriction where
    toField = toField . fromEnum

instance FromField VotingRestriction where
    fromField f = fmap toEnum . fromField f

instance ToJSON VotingRestriction where
    toJSON = toJSON . show

instance FromJSON VotingRestriction where
    parseJSON (String "LoginAndTeamMember") = return LoginAndTeamMember
    parseJSON (String "LoginOnly") = return LoginOnly
    parseJSON (String "Anonymous") = return Anonymous
    parseJSON v = fail $ "Could not parse as a voting restriction: " ++ show v

data HackathonRoundType
    = TeamFormation
    | Outvoted
    | Winner
    | Round
    deriving(Show, Read, Eq)

instance ToField HackathonRoundType where
    toField = toField . show

instance FromField HackathonRoundType where
    fromField f = fmap read . fromField f

instance ToJSON HackathonRoundType where
   toJSON = toJSON . show
