{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module StatisticsHandlers
   ( getStats
   , handleTeamMemberStats
   , handleRoundVotesStats
   , handleHackathonSummaryStatistics
   ) where

import           AesonHelpers
import qualified AppConfig                     as CONF
import qualified AppHelpers                    as AH
import           Application
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Either
import           Data.Aeson
import           Data.Aeson.Types              (fieldLabelModifier)
import qualified Data.HackathonConstants       as HC
import           Data.MaybeUtil
import           GHC.Generics
import           HandlerHelpers
import qualified Persistence.Hackathon         as PH
import qualified Persistence.Statistics        as PS
import qualified Snap.AtlassianConnect         as AC
import qualified Snap.Core                     as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import qualified WithToken                     as WT

getStats :: AppHandler ()
getStats = handleMethods
   [ (SC.GET, AH.getKeyAndConfirm CONF.acStatisticsKey handleStatisticsGather)
   ]

data HackathonStats = HackathonStats
   { hsBasicStats        :: PS.BasicStats
   , hsTeamsPerHackathon :: [PS.HistogramRow]
   , hsVotesPerHackathon :: [PS.HistogramRow]
   , hsVotesPerRound     :: [PS.HistogramRow]
   , hsVotesPerRoom      :: [PS.HistogramRow]
   } deriving (Show, Generic)

instance ToJSON HackathonStats where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "hs" }

handleStatisticsGather :: AppHandler ()
handleStatisticsGather = do
   basicStats <- PS.getBasicStats
   teamsPerHackathon <- PS.getTeamsPerHackathon
   votesPerHackathon <- PS.getVotesPerHackathon
   votesPerRound <- PS.getVotesPerRound
   votesPerRoom <- PS.getVotesPerRoom
   writeJson $ HackathonStats basicStats teamsPerHackathon votesPerHackathon votesPerRound votesPerRoom

handleTeamMemberStats :: AppHandler ()
handleTeamMemberStats = handleMethods
   [ (SC.GET, WT.tenantFromToken getTeamMemberStats)
   ]

getTeamMemberStats :: AC.TenantWithUser -> AppHandler ()
getTeamMemberStats (_, Nothing) = respondWithError unauthorised "You cannot get statistics unless you are logged in."
getTeamMemberStats (tenant, Just _) = writeError . runEitherT $ do
   projectId <- EitherT (m2e noProjectId <$> getIntegerQueryParam "projectId")
   hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant projectId)
   teamMemberCounts <- lift $ PS.getTeamMemberCounts hackathon
   lift . writeJson . TeamMemberStats $ teamMemberCounts

data TeamMemberStats = TeamMemberStats
   { tmsCounts :: [PS.HistogramRow]
   } deriving (Show, Generic)

instance ToJSON TeamMemberStats where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "tms" }

handleRoundVotesStats :: AppHandler ()
handleRoundVotesStats = handleMethods
   [ (SC.GET, WT.tenantFromToken getRoundVotesStats)
   ]

getRoundVotesStats :: AC.TenantWithUser -> AppHandler ()
getRoundVotesStats (_, Nothing) = respondWithError unauthorised "You cannot get statistics unless you are logged in."
getRoundVotesStats (tenant, Just _) = writeError . runEitherT $ do
   projectId <- EitherT (m2e noProjectId <$> getIntegerQueryParam "projectId")
   hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant projectId)
   roundVoteCounts <- lift $ PS.getVotesPerRoundByHackathon hackathon
   lift . writeJson . RoundVoteStats $ roundVoteCounts

noProjectId, noSuchHackathon :: (Int, String)
noProjectId = (badRequest, "You need to provide a project id in order to make this request.")
noSuchHackathon = (badRequest, "The given project is not a hackathon.")

data RoundVoteStats = RoundVoteStats
   { rvsCounts :: [PS.LabelAndCount]
   } deriving (Show, Generic)

instance ToJSON RoundVoteStats where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rvs" }

handleHackathonSummaryStatistics :: AppHandler ()
handleHackathonSummaryStatistics = handleMethods
   [ (SC.GET, WT.tenantFromToken getHackathonStatistics)
   ]

getHackathonStatistics :: AC.TenantWithUser -> AppHandler ()
getHackathonStatistics (_, Nothing) = respondWithError unauthorised "You cannot get forming team statistics unless you are logged in."
getHackathonStatistics (tenant, Just _) = writeError . withPG . runEitherT $ do
   projectId <- EitherT (m2e noProjectId <$> getIntegerQueryParam "projectId")
   hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant projectId)
   formingTeamCount <- lift $ PS.countTeamsInRoundTypeForHackathon hackathon HC.TeamFormation
   competitors <- lift $ PS.countUniqueTeamMembersForHackathon hackathon
   lift . writeJson $ HSS formingTeamCount competitors

data HackathonSummaryStatistics = HSS
   { hssFormingTeams :: Integer
   , hssCompetitors :: Integer
   } deriving (Show, Generic)

instance ToJSON HackathonSummaryStatistics where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "hss" }
