<!DOCTYPE html>
<html lang="en">
   <head profile="http://www.w3.org/2005/10/profile">
      <meta charset="utf-8" />
      <meta version="${resourcesVersion}" />
      <meta name="userKey" content="${userKey}" />
      <meta name="pluginKey" content="${pluginKey}" />

      <hasSplice name="connectPageToken">
      <meta name="acpt" content="${connectPageToken}">
      </hasSplice>

      <meta content="IE=EDGE" http-equiv="X-UA-Compatible" /> 

      <apply template="headers/requirejs" />

      <apply template="headers/tenant" />

      <apply template="headers/aui" />
      <apply template="headers/aui-experimental" />

      <link rel="icon" type="image/ico" href="/static/${resourcesVersion}/images/hackathon-favicon.ico" />
      <css href="/static/${resourcesVersion}/css/app.css" />

      <header-extra />
   </head>
   <body class="body ${body-class}" id="panel-body">
      <div class="ac-content"><apply-content /></div>
   </body>
</html>
