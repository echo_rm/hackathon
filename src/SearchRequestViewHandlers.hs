{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module SearchRequestViewHandlers (calculateHackathonStatistics) where

import           AesonHelpers
import           Application
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Either
import           Data.Aeson
import           Data.Aeson.Types               (fieldLabelModifier)
import           Data.MaybeUtil
import           GHC.Generics
import qualified TenantJWT as TJ
import qualified Data.Text                  as T
import qualified Data.Text.Encoding                as TE
import qualified Snap.AtlassianConnect      as AC
import qualified Snap.Core                  as SC
import qualified Snap.Helpers               as SH
import           HandlerHelpers
import qualified Persistence.Statistics as PS

calculateHackathonStatistics :: AppHandler ()
calculateHackathonStatistics = SH.handleMethods
   [ (SC.GET, TJ.withTenant calculateHackathonStatisticsWithTenant)
   ]

calculateHackathonStatisticsWithTenant :: AC.TenantWithUser -> AppHandler ()
calculateHackathonStatisticsWithTenant (tenant, _) = writeError . runEitherT $ do
    concatenatedIssues <- EitherT ((m2e missingIssues . fmap TE.decodeUtf8) <$> SC.getQueryParam "issues")
    let issues = T.splitOn "," concatenatedIssues
    uniqueTeamMembers <- lift $ PS.countUniqueTeamMembersForIssueKeys tenant issues
    votesPerRound <- lift $ PS.countVotesPerRoundForIssueKeys tenant issues
    lift . SH.writeJson $ HackathonStatistics 
        { hsIssues = issues
        , hsUniqueTeamMembers = uniqueTeamMembers
        , hsVotesPerRound = votesPerRound
        }
    where 
        missingIssues = (SH.badRequest, "No issues were provided to this request. Nothing can be looked up.")

data HackathonStatistics = HackathonStatistics
    { hsIssues :: [T.Text]     
    , hsUniqueTeamMembers :: Integer
    , hsVotesPerRound :: [PS.LabelAndCount]
    } deriving (Show, Generic)

instance ToJSON HackathonStatistics where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "hs" }