define(function() {
   "use strict";

   var self = {};

   self.restriction = {
      loginAndTeamMember: "LoginAndTeamMember",
      loginOnly: "LoginOnly",
      anonymous: "Anonymous"
   };

   self.loginRequired = function(restriction) {
      return restriction === self.restriction.loginAndTeamMember || restriction === self.restriction.loginOnly;
   };

   return self;
});
