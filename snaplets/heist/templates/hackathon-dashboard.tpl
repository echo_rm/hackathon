<apply template="connect-panel">
   <bind tag="body-class">hackathon-dashboard</bind>
   <bind tag="header-extra">
     <js src="/static/${resourcesVersion}/js/app/hackathon-dashboard.js" />
     <script>require(['app/hackathon-dashboard']);</script>

     <script id="round-room-row" type="x-tmpl-mustache">
         <tr {{#hideRow}}class="hidden"{{/hideRow}} data-room-id="{{room.id}}" data-round-id="{{round.id}}">
             <td headers="round-room-select"><input class="checkbox round-room-selector" type="checkbox" /></td>
             <td headers="room-name"><a target="_blank" href="{{{viewTeamsUrl}}}">{{room.name}}</a></td>
             <td headers="round-name"><span class="aui-lozenge aui-lozenge-subtle">{{round.name}}</span></td>
             <td headers="round-room-status"><span class="aui-lozenge {{statusLozengeClass}}">{{status}}</span></td>
             <td headers="round-room-team-count"><span class="aui-badge">{{numberOfTeams}}</span></td>
         </tr>
     </script>

     <script id="round-option" type="x-tmpl-mustache">
         <li><a class="round-option" data-round-id="{{id}}" data-sequence-number="{{sequenceNumber}}" href="#">{{name}}</a></li>
     </script>

     <script id="round-settings-row" type="x-tmpl-mustache">
     <tr data-round-id={{id}} data-sequence-number={{sequenceNumber}}>
         <td headers="round-settings-select"><input type="checkbox" class="checkbox round-selector" /></td>
         <td headers="round-settings-round-name"><span class="aui-lozenge aui-lozenge-subtle">{{name}}</span></td>
         <td headers="round-settings-votes-per-round"><span class="aui-badge">{{votesPerUser}}</span></td>
         <td headers="round-settings-votes-per-team"><span class="aui-badge">{{votesUserTeam}}</span></td>
     </tr>
     </script>
   </bind>

   <h2>Hackathon dashboard</h2>

   <!-- Error message if your user is missing browse users permission -->
   <div id="missing-browse-users" class="aui-message aui-message-error hidden">
      <p class="title">
         <strong>The <a href="${productBaseUrl}/secure/admin/user/ViewUser.jspa?name=addon_${pluginKey}" target="_top">Hackathon add-on user</a> does not have the global 'Browse Users' permission.</strong> </p>
      <p>Hackathon needs the addon user to have this permission in order to function correctly. Please make sure that
      this user has the global <a href="${productBaseUrl}/secure/admin/GlobalPermissions!default.jspa" target="_top">Browse Users</a> permission before using this add-on
      otherwise this add-on will fail to work as expected.</p>
   </div>

   <!-- Error message if your user is missing project admin permission -->
   <div id="missing-project-admin" class="aui-message aui-message-error hidden">
      <p class="title">
         <strong>The <a href="${productBaseUrl}/secure/admin/user/ViewUser.jspa?name=addon_${pluginKey}" target="_top">Hackathon add-on user</a> does not have the project admin permission for this project.</strong> </p>
      <p>Hackathon needs the addon user to have this permission in order to function correctly. Please make sure that
      this user has the <a id="missing-project-admin-roles-link" href="#" target="_top">Project Administration</a> permission before using this add-on
      otherwise this add-on will fail to work as expected.</p>
   </div>

   <!-- The hackathon dashboard must give an overview of the current state of the Hackathon. Past and future states should not be
   considered. -->
   <a id="view-summary" href="#" target="_top">(View hackathon summary)</a>
   <div id="dashboard-help-text" class="aui-expander-content">
      <p>On this page you can control the configuration of your hackathon. Everything that needs to be
      configured about your hackathon can be done from here. Please read through the options carefully.</p>
      <p>This is the administrators view of your hackathon; spectators and competitors can only view
      the Hackathon summary.</p>
      <p>If you have just finished a round and you need to move teams into the next round then you
      should use the Round transitions page in the left sidebar.</p>
   </div>
   <a id="replace-text-trigger" data-replace-text="Read less" class="aui-expander-trigger"
   aria-controls="dashboard-help-text">Read more</a>
   <h3>Hackathon settings</h3>
   <form class="aui">
      <fieldset class="group voting-restrictions">
          <legend><span>Voting restrictions</span></legend>
          <div class="radio">
              <input class="radio" type="radio" checked="checked" name="rads" id="vr-login-and-membership">
              <label for="vr-login-and-membership">Login required. Users can not vote on their own team.
                  <span class="aui-icon aui-icon-small aui-iconfont-help help-tooltip"
                          title="Users are required to login to vote. If a user is a member of a team then they cannot vote for that team.">Help</span>
              </label>
          </div>
          <div class="radio">
              <input class="radio" type="radio" name="rads" id="vr-login-without-membership">
              <label for="vr-login-without-membership">Login required. Users can vote on any team.
                  <span class="aui-icon aui-icon-small aui-iconfont-help help-tooltip"
                        title="Users are required to login to vote. Users can vote on any team.">Help</span>
              </label>
          </div>
          <div class="radio">
              <input class="radio" type="radio" name="rads" id="vr-anonymous">
              <label for="vr-anonymous">Login optional. Users can vote on any team
                  <span class="aui-icon aui-icon-small aui-iconfont-help help-tooltip"
                        title="Users do not need to log in to vote (anonymous users can vote). Users can vote on any team.">Help</span>
              </label>
          </div>
      </fieldset>
   </form>
   <form class="aui">
      <fieldset class="group voting-rules">
          <legend><span>Multiple Room Voting</span></legend>
          <div class="checkbox">
              <input class="checkbox" type="checkbox" id="vr-multiple-room-voting">
              <label for="vr-multiple-room-voting">Users can place votes in multiple rooms in the same round.
              </label>
          </div>
      </fieldset>
   </form>

   <h3>Room settings</h3>
   <p>Use this section to configure the current round and status for your rooms. Voters only get a
   certain number of votes per round. So the current round for a room should match which round of
   your hackathon you are in. And voters can only vote on rooms that are Active; a room becomes
   Locked automatically when you reveal the results on the leaderboard.</p>

   <div class="room-configuration-section section-container">
      <div class="right-room-round-actions right-actions">
          <button id="set-current-round" class="aui-button aui-dropdown2-trigger" aria-owns="set-current-round-options" aria-haspopup="true">Set current round</button>
          <div id="set-current-round-options" class="aui-dropdown2 aui-style-default">
              <ul class="aui-list-truncate">
                  <li><a href="#">Round 1</a></li>
                  <li><a href="#">Round 2</a></li>
                  <li><a href="#">Round 3</a></li>
              </ul>
          </div>
          <button id="set-status" class="aui-button aui-dropdown2-trigger" aria-owns="set-status-options" aria-haspopup="true">Set status</button>
          <div id="set-status-options" class="aui-dropdown2 aui-style-default">
              <ul class="aui-list-truncate">
                  <li><a data-status="active"     href="#">Active</a></li>
                  <li><a data-status="locked"     href="#">Locked</a></li>
                  <li><a data-status="inactive"   href="#">Inactive</a></li>
              </ul>
          </div>
      </div>

      <form class="aui left-room-round-actions left-actions">
          <input class="text" type="text"
                 id="new-room-name" name="new-room-name"
                 title="New room name" placeholder="New room name...">
          <button id="create-new-room" class="aui-button">Add</button>
          <input id="hide-inactive-rooms" type="checkbox" class="checkbox" />
          <label for="hide-inactive-rooms">Hide inactive rooms</label>
      </form>

      <table id="current-room-state" class="aui">
          <thead>
              <tr>
                  <th id="round-room-select"><input id="round-room-select-all" class="checkbox" type="checkbox" /></th>
                  <th id="room-name">Room</th>
                  <th id="round-name">Current round</th>
                  <th id="round-room-status">Status</th>
                  <th id="round-room-team-count">Teams</th>
              </tr>
          </thead>
          <tbody></tbody>
      </table>

      <div id="no-rooms-warning" class="aui-message aui-message-warning hidden">
          <p class="title">
              <strong>Whoops, you have not yet added any rooms to your hackathon.</strong>
          </p>
          <p>When the time comes for the competitors to present their projects they will gather in one or more rooms to demo and then be voted on by the spectators. You need to create one room for each location that people will be voting in. If you don't create any rooms then nobody will be able to vote.</p>
      </div>
   </div>

   <h3>Round settings</h3>
   <p>Use these options to configure how many votes each spectator / competitor can place.</p>
   <br />
   <div class="section-container">
      <form class="aui">
          <label for="votes-per-round">Votes per round</label>
          <input class="text short-field" type="text" id="votes-per-round" title="votes per round">
          <label for="votes-per-team">Votes per user per team</label>
          <input class="text short-field" type="text" id="votes-per-team" title="votes per team">
          <button id="round-configuration-update" class="aui-button">Update</button>
      </form>

      <table id="round-settings" class="aui">
          <thead>
              <tr>
                  <th id="round-settings-select"><input id="round-settings-select-all" class="checkbox" type="checkbox" /></th>
                  <th id="round-settings-round-name">Round</th>
                  <th id="round-settings-votes-per-round">Maximum votes per user
                      <span class="aui-icon aui-icon-small aui-iconfont-help help-tooltip"
                            title="The number of votes that each user can place in this round.">Help</span>
                  </th>
                  <th id="round-settings-votes-per-team">Maximum votes per user per team
                      <span class="aui-icon aui-icon-small aui-iconfont-help help-tooltip"
                            title="The number of votes that a user can place on the same team. If greater than one, this allows the possibilty that a user can vote for the same team more than once.">Help</span>
                  </th>
              </tr>
          </thead>
          <tbody></tbody>
      </table>
   </div>

   <div class="hidden">
     <p>The old version of this page did the following:</p>
     <ul>
         <li>Let the user set the current round <span class="aui-icon aui-icon-small aui-iconfont-success"></span></li>
         <li>Let the user configure settings on a per round basis <span class="aui-icon aui-icon-small aui-iconfont-success"></span></li>
         <li>See how many votes has been placed on the current round (This could be moved out)</li>
         <li>Lets the users see which rooms have how many teams in them and wether or not those rooms are locked for voting. <span class="aui-icon aui-icon-small aui-iconfont-success"></span></li>
         <li>We let you view the leaderboard of any room in the current round.</li>
     </ul>
     <p>Here are some questions that we need to answer:</p>
     <ul>
         <li>How does the user set the current round for each room? <span class="aui-icon aui-icon-small aui-iconfont-success"></span></li>
         <li>What is the timeline of running a hackathon project? What are the actions that admins / users want to perform at all points in time?</li>
         <li>Should we show a list of rooms with configuration options per room? And maybe global / round level settings?</li>
         <li>I need to be able to assign rooms to teams if I want to have a chance of having the voting pages working.</li>
         <li>We should automatically create rooms but never delete them from the database ever. However, maybe we should allow a "deactivate" experience for rooms.</li>
         <li>After the administrator has activated a hackathon what do they want to do next?</li>
     </ul>
   </div>
</apply>
