{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Persistence.HackathonRoom
    ( HackathonRoom(..)
    , getRoomById
    , getOrCreateHackathonRoom
    , getRoomsForHackathon
    , getRoomByComponentId
    , getRoomDirectlyByComponentId
    , updateCurrentRounds
    , updateShortUrl
    ) where

import           Application
import           Control.Monad.Trans.Class          (lift)
import           Control.Monad.Trans.Maybe
import           Data.Int                           (Int64)
import           Data.Maybe                         (listToMaybe)
import qualified Data.Text                          as T
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.SqlQQ
import           Network.URI                        (URI)
import qualified Persistence.Hackathon              as PH
import qualified Persistence.HackathonIds           as HI
import qualified Persistence.HackathonRound         as RND
import qualified Snap.AtlassianConnect              as AC
import           Snap.Snaplet.PostgresqlSimple

data HackathonRoom = HackathonRoom
    { hrId             :: HI.HackathonRoomId
    , hrHackathonId    :: HI.HackathonId
    , hrCurrentRoundId :: HI.HackathonRoundId
    , hrComponentId    :: HI.ComponentId
    , hrName           :: T.Text
    , hrShortUrl       :: Maybe String
    }

instance FromRow HackathonRoom where
    fromRow = HackathonRoom <$> field <*> field <*> field <*> field <*> field <*> field

getRoomById :: PH.Hackathon -> HI.HackathonRoomId -> AppHandler (Maybe HackathonRoom)
getRoomById hackathon roomId = listToMaybe <$> query
    [sql|
        SELECT r.id, r.hackathon_id, r.current_round_id, r.component_id, r.name, r.short_url
        FROM room r
        WHERE r.hackathon_id = ? AND r.id = ?
    |] (PH.hackathonId hackathon, roomId)

getRoomsForHackathon :: PH.Hackathon -> AppHandler [HackathonRoom]
getRoomsForHackathon hackathon = query
    [sql|
        SELECT r.id, r.hackathon_id, r.current_round_id, r.component_id, r.name, r.short_url
        FROM room r
        WHERE r.hackathon_id = ?
    |] (Only . PH.hackathonId $ hackathon)

getOrCreateHackathonRoom :: PH.Hackathon -> HI.ComponentId -> T.Text -> MaybeT AppHandler HackathonRoom
getOrCreateHackathonRoom hackathon componentId name = do
    potentialRoom <- lift $ getRoomByComponentId hackathon componentId
    case potentialRoom of
        (Just x) -> return x
        Nothing -> do
            -- Look up the first hackathon round
            hRound <- MaybeT $ RND.getFirstRoundForHackathon hackathon
            roomId <- MaybeT $ createHackathonRoom hackathon hRound componentId name
            return HackathonRoom
                { hrId = roomId
                , hrHackathonId = PH.hackathonId hackathon
                , hrCurrentRoundId = RND.hroundId hRound
                , hrComponentId = componentId
                , hrName = name
                , hrShortUrl = Nothing
                }

createHackathonRoom :: PH.Hackathon -> RND.HackathonRound -> HI.ComponentId -> T.Text -> AppHandler (Maybe HI.HackathonRoomId)
createHackathonRoom hackathon hRound componentId roomName = listToMaybe . fmap fromOnly <$> query
    [sql|
        INSERT INTO room (hackathon_id, current_round_id, component_id, name)
        VALUES (?, ?, ?, ?) RETURNING id
    |] (PH.hackathonId hackathon, RND.hroundId hRound, componentId, roomName)

getRoomByComponentId :: PH.Hackathon -> HI.ComponentId -> AppHandler (Maybe HackathonRoom)
getRoomByComponentId hackathon componentId = listToMaybe <$> query
    [sql|
        SELECT r.id, r.hackathon_id, r.current_round_id, r.component_id, r.name, r.short_url
        FROM room r
        WHERE r.hackathon_id = ?
        AND r.component_id = ?
    |] (PH.hackathonId hackathon, componentId)

getRoomDirectlyByComponentId :: AC.Tenant -> AC.ProjectId -> HI.ComponentId -> AppHandler (Maybe HackathonRoom)
getRoomDirectlyByComponentId tenant projectId componentId = listToMaybe <$> query
    [sql|
        SELECT r.id, r.hackathon_id, r.current_round_id, r.component_id, r.name, r.short_url
        FROM room r, hackathon h
        WHERE r.hackathon_id = h.id
        AND h.tenant_id = ?
        AND h.project_id = ?
        AND r.component_id = ?
    |] (AC.tenantId tenant, projectId, componentId)

updateCurrentRounds :: PH.Hackathon -> RND.HackathonRound -> [HI.HackathonRoomId] -> AppHandler Int64
updateCurrentRounds hackathon hRound roomIds = execute
    [sql|
        UPDATE room
        SET current_round_id = ?
        WHERE hackathon_id = ?
        AND id in ?
    |] (RND.hroundId hRound, PH.hackathonId hackathon, In roomIds)

updateShortUrl :: HackathonRoom -> URI -> AppHandler Int64
updateShortUrl room shortUri = execute
   [sql|
      UPDATE room
      SET short_url = ?
      WHERE id = ?
   |] (show shortUri, hrId room)
