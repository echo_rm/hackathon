# The development docker file for the Hackathon Connect Haskell project.
# This docker file is designed to help us build the production executables
# but should not be used to actually generate the production docker images.
# Instead we should make a new production image that takes the executables
# from this image and only includes them. That way we do not need to carry
# the entire Haskell platform with us into production. Just the small set
# of required dependencies.

FROM kkarczmarczyk/node-yarn as frontend
LABEL maintainer="Robert Massaioli <rmassaioli@atlassian.com>"

# Build the Frontend
ADD /package.json /home/package.json
ADD /build.js /home/build.js
ADD /static   /home/static
WORKDIR /home
RUN yarn install && yarn build-javascript && yarn build-css

FROM haskell:8.0.2 AS backend
# FROM ubuntu:14.04
MAINTAINER Robert Massaioli <rmassaioli@atlassian.com>

# Expose the default port, port 8000
EXPOSE 8000

# Install the missing packages
USER root
RUN apt-get update && apt-get install -y libpq-dev pkgconf

# Copy our context into the build directory and start working from there
USER root
ADD /   /home/haskell/build

# Setup the Haskell Envoronment
WORKDIR /home/haskell/build
ENV LANG en_US.UTF-8 # See: https://github.com/haskell/cabal/issues/1883#issuecomment-44150139
ENV PATH /home/haskell/.cabal/bin:$PATH

# Get the Haskell Dependencies
# TODO Do we require the Haskell Platform [http://packages.ubuntu.com/trusty/haskell-platform] or GHC [http://packages.ubuntu.com/trusty/ghc6]?
# RUN apt-get update && apt-get install -y haskell-platform && cabal update && cabal install cabal-install
RUN cabal update && cabal install cabal-install-1.24.0.2

# Initiate the build environment and build the executable (assumes that the
# atlassian-connect-haskell source can be found in the vendor/atlassian-connect directory AND that
# it has not been released to hackage yet (which is really where it should live).
#
# IMPORTANT: This must produce a statically-compiled binary (with respect to
# Cabal dependencies) that does not depend on a local cabal installation. The
# production Docker image will not run a cabal install.
RUN cabal sandbox init && cabal update && cabal install --force-reinstalls -O2

# Setup the default command to run for the container.
CMD ["/home/haskell/build/.cabal-sandbox/bin/hackathon", "--access-log=-", "--error-log=stderr"]

FROM ubuntu:16.04
MAINTAINER Robert Massaioli <rmassaioli@atlassian.com>

# Expose the default port, port 8080
EXPOSE 8080

# Install the missing packages
USER root
RUN apt-get update && apt-get install -y libpq5 libgmp10 openjdk-8-jre-headless libnss3 libnss-lwres libnss-mdns netbase

# Copy our context into the build directory and start working from there
# Add in backend content
COPY --from=backend /home/haskell/build/migrations /service/migrations
COPY --from=backend /home/haskell/build/snaplets /service/snaplets
COPY --from=backend /home/haskell/build/static /service/static
COPY --from=backend /home/haskell/build/.cabal-sandbox/bin/hackathon /service/hackathon

COPY --from=frontend /home/static-css /service/static-css
COPY --from=frontend /home/static-js /service/static-js

# Setup the Haskell Envoronment
WORKDIR /service
# TODO is the LANG used by Snap or Haskell?
ENV LANG en_US.UTF-8 #

# Setup the default command to run for the container.
CMD ["/service/hackathon", "--access-log=-", "--error-log=stderr", "--port=8080"]
