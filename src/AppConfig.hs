{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}

module AppConfig
   ( AppConf(..)
   , HasAppConf(..)
   , initAppConfOrFail
   ) where

import           ConfigurationHelpers
import           Control.Monad              (join, when)
import qualified Control.Monad.IO.Class     as MI
import qualified Data.ByteString.Char8      as BSC
import qualified Data.Configurator.Types    as DCT
import           Data.ConfiguratorTimeUnits ()
import qualified Data.Connect.Descriptor    as D
import qualified Data.EnvironmentHelpers    as DE
import           Data.List                  (find)
import           Data.Maybe                 (fromMaybe, isJust)
import           Data.Text.Encoding         (encodeUtf8)
import qualified Data.Time.Units            as DTU
import qualified MicrosZone                 as MZ
import           Network.HTTP.Client        (Proxy (..))
import qualified Network.URI                as NU
import qualified Snap.Snaplet               as SS
import qualified System.Environment         as SE
import           Text.PrettyPrint.Boxes

data AppConf = AppConf
   { acPurgeKey        :: D.Key BSC.ByteString AppConf
   , acPurgeRetention  :: DTU.Day
   , acHttpProxy       :: Maybe Proxy
   , acHttpSecureProxy :: Maybe Proxy
   , acMigrationKey    :: D.Key BSC.ByteString AppConf
   , acStatisticsKey   :: D.Key BSC.ByteString AppConf
   , acAdminKey        :: D.Key BSC.ByteString AppConf
   , acUrlShortenerKey :: BSC.ByteString
   }

class HasAppConf m where
   getAppConf :: m AppConf

initAppConfOrFail :: SS.SnapletInit b AppConf
initAppConfOrFail = SS.makeSnaplet "app-config" "Application configuration and state." Nothing $ do
   configurationDirectory <- SS.getSnapletFilePath
   MI.liftIO $ SS.loadAppConfig "hackathon.cfg" configurationDirectory >>= loadAppConfOrFail

data EnvConf = EnvConf
   { ecPurgeKey        :: Maybe String
   , ecMigrationKey    :: Maybe String
   , ecStatisticsKey   :: Maybe String
   , ecAdminKey        :: Maybe String
   , ecUrlShortenerKey :: Maybe String
   , ecHttpProxy       :: Maybe String
   , ecHttpSecureProxy :: Maybe String
   , ecZone            :: MZ.Zone
   } deriving (Eq, Show)

loadConfFromEnvironment :: IO EnvConf
loadConfFromEnvironment = do
   environment <- SE.getEnvironment
   let get = search environment
   return EnvConf
      { ecPurgeKey         = get "PURGE_KEY"
      , ecMigrationKey     = get "MIGRATION_KEY"
      , ecStatisticsKey    = get "STATISTICS_KEY" 
      , ecAdminKey         = get "ADMIN_KEY" 
      , ecUrlShortenerKey  = get "URL_SHORTENER_KEY"
      , ecHttpProxy        = get "http_proxy"
      , ecHttpSecureProxy  = get "https_proxy"
      , ecZone             = fromMaybe MZ.Dev $ MZ.zoneFromString =<< get "ZONE"
      }

search :: [(String, String)] -> String -> Maybe String
search pairs key = snd <$> find ((==) key . fst) pairs

guardConfig :: EnvConf -> IO ()
guardConfig ec = when (isDogOrProd && not allKeysPresent) $
   fail $ "[Fatal Error] All of the environmental configuration is required in: " ++ (show . ecZone $ ec)
   where
      isDogOrProd = ecZone ec `elem` [MZ.Dog, MZ.Prod]
      allKeysPresent = all isJust $ [ecPurgeKey, ecMigrationKey] <*> pure ec

instance DCT.Configured (D.Key BSC.ByteString a) where
  convert (DCT.String s) = Just (D.Key (encodeUtf8 s))
  convert _ = Nothing

loadAppConfOrFail :: DCT.Config -> IO AppConf
loadAppConfOrFail config = do
   environmentConf <- loadConfFromEnvironment
   printEnvironmentConf environmentConf
   guardConfig environmentConf

   migrationKey <- require config "migration-key" "Missing 'migration-key': for trigerring migrations."
   statisticsKey <- require config "statistics-key" "Missing 'statistics-key': for getting statistics information from this addon."
   adminKey <- require config "admin-key" "Missing 'admin-key': for getting statistics information from this addon."
   urlShortenerKey <- require config "url-shortener-key" "Missing 'url-shortener-key': for shortening urls for public consumption."
   purgeKey <- require config "purge-key" "Missing 'purge-key': for triggering customer data cleanups."
   purgeRetentionDays <- require config "purge-retention-days" "Missing 'purge-retention-days': the length of time that uninstalled customer data should remain before we delete it."

   let httpProxy = parseProxy standardHttpPort (ecHttpProxy environmentConf)
   let httpSecureProxy = parseProxy standardHttpSecurePort (ecHttpSecureProxy environmentConf)
   let fromConf = envOrDefault environmentConf
   return AppConf
      { acPurgeKey = fromConf (fmap packInKey . ecPurgeKey) purgeKey
      , acPurgeRetention = fromInteger purgeRetentionDays
      , acHttpProxy = httpProxy
      , acHttpSecureProxy = httpSecureProxy
      , acMigrationKey = fromConf (fmap packInKey . ecMigrationKey) migrationKey
      , acStatisticsKey = fromConf (fmap packInKey . ecStatisticsKey) statisticsKey
      , acAdminKey = fromConf (fmap packInKey . ecAdminKey) adminKey
      , acUrlShortenerKey = fromConf (fmap BSC.pack . ecUrlShortenerKey) urlShortenerKey
      }

packInKey :: String -> D.Key BSC.ByteString a
packInKey = D.Key . BSC.pack

envOrDefault :: EnvConf -> (EnvConf -> Maybe a) -> a -> a
envOrDefault env f def = fromMaybe def $ f env

boxEnvironmentConf :: EnvConf -> Box
boxEnvironmentConf c =
   text "## Environmental Configuration" //
   (vcat left
      [ text " - Purge Key:"
      , text " - HTTP Proxy:"
      , text " - HTTP Secure Proxy:"
      ]
   <+> vcat left
      [ text . DE.showMaybe . ecPurgeKey $ c
      , text . DE.showMaybe . ecHttpProxy $ c
      , text . DE.showMaybe . ecHttpSecureProxy $ c
      ]
   )

printEnvironmentConf :: EnvConf -> IO ()
printEnvironmentConf = printBox . boxEnvironmentConf

type HttpPort = Int

standardHttpPort :: HttpPort
standardHttpPort = 80

standardHttpSecurePort :: HttpPort
standardHttpSecurePort = 443

parseProxy :: HttpPort -> Maybe String -> Maybe Proxy
parseProxy defaultPort potentialRawProxy = do
   rawProxy <- potentialRawProxy
   authority <- join $ NU.uriAuthority <$> NU.parseURI rawProxy
   let (host, portAndColon) = (NU.uriRegName authority, NU.uriPort authority)
   let port = if null portAndColon then [] else tail portAndColon
   return $ Proxy (BSC.pack host) (if null port then defaultPort else read port)
