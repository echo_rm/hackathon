<apply template="connect-panel">
   <bind tag="body-class">hackathon-round-transition</bind>
   <bind tag="header-extra">
      <js src="/static/${resourcesVersion}/js/app/hackathon-round-transition.js" />
      <script>require(['app/hackathon-round-transition']);</script>

      <script id="transition-option" type="x-tmpl-mustache">
         <option value="{{id}}" data-status-id="{{statusId}}">{{name}}</option>
      </script>

      <script id="current-room-row" type="x-tmpl-mustache">
         <tr class="current-room" data-room-id="{{id}}">
            <td headers="hackathon-room-col">{{name}}</td>
            <td headers="hackathon-status-col">
               {{#isLocked}}
               <span class="aui-lozenge aui-lozenge-success">Locked and will transition</span>
               {{/isLocked}}
               {{^isLocked}}
               <span class="aui-lozenge aui-lozenge-current">Active and will not transition</span>
               {{/isLocked}}
            </td>
            <td headers="hackathon-winner-col">
               <input class="text short-field winning-teams" title="number of winning teams" type="text" value="{{winners}}" />
               <span class="aui-icon aui-icon-small status hidden">Update status</span>
            </td>
         </tr>
      </script>

      <script id="current-rooms-empty-message" type="x-tmpl-mustache">
         <div class="aui-message">
             <p class="title">
                 <strong>There are currently no rooms in this round.</strong>
             </p>
             {{#unallocatedTeams}}
             <p>However, because you have allowed unallocated teams to be transitioned to the next
             round, performing a transition may move some teams.</p>
             {{/unallocatedTeams}}
             {{^unallocatedTeams}}
             <p>You can still configure this page but attempting to perform a transition will have no
             affect because there are no teams to move into the next round.</p>
             {{/unallocatedTeams}}
         </div>
      </script>

      <script id="assignable-room-row" type="x-tmpl-mustache">
         <tr class="assignable-room" data-room-id="{{id}}">
            <td headers="capacity-room-name">{{name}}</td>
            <td headers="capacity-unlimited"><input type="checkbox" class="checkbox unlimited" {{#unlimited}}checked="checked"{{/unlimited}} /></td>
            <td headers="capacity-limit"><input class="text short-field maximum-teams" title="the maximum number of teams in this room" type="text" value="{{maximumTeams}}" {{#unlimited}}disabled{{/unlimited}} /></td>
            <td headers="capacity-status"><span class="aui-icon aui-icon-small status hidden">Status</span></td>
         </tr>
      </script>

      <script id="select-unallocated-room" type="x-tmpl-mustache">
         <select class="select2-load medium-long-field assignable-room-select">
            {{#rooms}}
            <option value="{{id}}">{{name}}</option>
            {{/rooms}}
         </select>
      </script>

      <script id="round-transition-row" type="x-tmpl-mustache">
            <tr class="sortable transition-row" data-round-transition-id="{{id}}">
               <td headers="transition-reorder-row"><span class="aui-icon aui-icon-small aui-iconfont-appswitcher selector-handle">Reorder</span></td>
               <td headers="transition-number-row" class="order-cell">#{{orderNumber}}</td>
               <td headers="transition-jql-row">
                  <input 
                     class="text full-width-field success jql-query"
                     title="number of winning teams" 
                     type="text" 
                     placeholder="Put your custom JQL in here" value="{{jql}}"/>
               </td>
               <td headers="transition-status">
                  <a class="jql-editor-link" target="_blank">
                     <span class="aui-icon aui-icon-small aui-iconfont-sidebar-link in-advanced-editor">Open</span>
                  </a>
               </td>
               <td headers="transition-empty-row">into</td>
               <td headers="transition-balance-row">
                  <select class="select2-load medium-long-field rooms" multiple="">
                     {{#avaliableRooms}}
                     <option value="{{id}}">{{name}}</option>
                     {{/avaliableRooms}}
                  </select>
               </td>
               <td><span class="aui-icon aui-icon-small aui-iconfont-list-remove transition-remove">Remove</span></td>
            </tr>
      </script>

      <script id="select-option" type="x-tmpl-mustache">
         <option value="{{id}}">{{name}}</option>
      </script>

      <script id="rounds-preview" type="x-tmpl-mustache">
         {{#rooms}}
         <div class="room" data-component-id={{room.componentId}}>
             <h1 class="room-name">{{room.name}} <span class="aui-lozenge team-count">{{teams.length}}</span></h1>
             <button class="aui-button apply-allocation"><span class="aui-icon aui-icon-small aui-iconfont-approve">tick</span> Apply</button>
             {{#teams}}
             <div class="team {{allocationClass}} {{even}}" data-issue-id="{{details.issueId}}" data-issue-key="{{details.issueKey}}">
                 <a class="description" target="_blank" href="{{issueUrl}}">
                     <span class="team-key">{{details.issueKey}}</span>: 
                     <span class="team-name">{{details.issueSummary}}</span>
                 </a>
                 {{#isAllocated}}
                 <span 
                   class="aui-lozenge aui-lozenge-success status tooltip-required" 
                   title="Allocated by transition #{{allocationReason.transitionNumber}}"
                   data-transition-number="{{allocationReason.transitionNumber}}">#{{allocationReason.transitionNumber}}</span>
                 {{/isAllocated}}
                 {{^isAllocated}}
                 <span class="aui-icon aui-icon-small aui-iconfont-locked status tooltip-required" title="This team is already in this room in the next round. It will not be moved by this transition.">Already there</span>
                 {{/isAllocated}}
             </div>
             {{/teams}}
             {{^teams}}
             <div class="no-teams">There were no teams allocated to this room</div>
             {{/teams}}
         </div>
         {{/rooms}}
         <br />
         {{#hasOutvoted}}
         <div class="room outvoted">
            <h1 class="room-name">Outvoted teams <span class="aui-icon aui-icon-small aui-iconfont-help help tooltip-required" title="These teams did not win in their respective rounds and will be placed in the outvoted state with their rooms removed."></span></h1>
            <button class="aui-button apply-allocation"><span class="aui-icon aui-icon-small aui-iconfont-approve">tick</span> Apply</button>
            {{#outvotedTeams}}
            <div class="team outvoted {{even}}" data-issue-id="{{issueId}}" data-issue-key="{{issueKey}}">
               <a class="description" target="_blank" href="{{issueUrl}}">
                  <span class="team-key">{{issueKey}}</span>:
                  <span class="team-name">{{issueSummary}}</span>
               </a>
               <span class="aui-icon aui-icon-small aui-iconfont-locked status hidden tooltip-required" title="This team is already in this room in the next round. It will not be moved by this transition.">Already there</span>
            </div>
            {{/outvotedTeams}}
         </div>
         {{/hasOutvoted}}
         {{#hasUnallocated}}
         <div class="room unallocated">
            <h1 class="room-name">Unallocated teams <span class="aui-icon aui-icon-small aui-iconfont-help help tooltip-required" title="Enable 'Match unallocated' in order to allocated these teams to rooms. Otherwise they will remain in this round without a room."></span></h1>
            {{#unallocatedTeams}}
            <div class="team unallocated">
               <a class="description" target="_blank" href="{{issueUrl}}">
                  <span class="team-key">{{issueKey}}</span>:
                  <span class="team-name">{{issueSummary}}</span>
               </a>
            </div>
            {{/unallocatedTeams}}
         </div>
         {{/hasUnallocated}}
      </script>
   </bind>

   <form class="aui">
      <div class="field-group transition-round-selector">
         <label for="dBase">Transition from</label>
         <select class="select round-from" id="select-round-from" title="From round"></select>
         <div class="round-to">
            <span>to </span><span class="aui-lozenge round-name">Round 1</span>
         </div>
      </div>
      <fieldset class="round-options">
         <div class="field-group">
            <label for="include-tied">Transition tied teams</label>
            <input id="include-tied" class="checkbox" checked="checked" type="checkbox" /> Transition teams that are tied for the last open slot

            <div class="description">If there can only be three winning teams for this round and one three people tie for second place then should the second place teams go to the next round?</div>
         </div>
      </fieldset>
      <fieldset class="round-options">
         <div class="field-group">
            <label for="unallocated-teams">Match unallocated</label>
            <input id="unallocated-teams" class="checkbox" type="checkbox" /> Also allocate teams that are not in rooms into rooms

            <div class="description">By selecting this option we will balance teams that are not currently in a room into avaliable rooms.</div>
         </div>
      </fieldset>
      <h3 class="current-rooms">Current rooms</h3>
      <div class="current-rooms">
         <table class="aui text-center current-rooms">
            <thead>
               <tr>
                  <th id="hackathon-room-col">Room</th>
                  <th id="hackathon-status-col">Status</th>
                  <th id="hackathon-winners-col">Number of winning teams</th>
               </tr>
            </thead>
            <tbody> </tbody>
         </table>
         <div id="current-rooms-empty-container"></div>
      </div>
   </form>

   <h3>Assignable rooms</h3>
   <p>This is the list of rooms that you can allocate teams into in the next round.</p>
   <form class="aui assignable-rooms">
      <table class="aui text-center">
         <thead>
            <tr>
               <th id="capacity-room-name">Room</th>
               <th id="capacity-unlimited">Unlimited capacity?</th>
               <th id="capacity-limit">Maximum teams in room</th>
               <th id="capacity-status">&nbsp;</th>
            </tr>
         </thead>
         <tbody>
            <tr class="room-adder-row">
               <td colspan="3">
                  <select class="select2-load medium-long-field assignable-room-select">
                     <option value="CONF">Hall of Justice</option>
                     <option value="JIRA">Avengers Mansion</option>
                     <option value="BAM">Stark Tower</option>
                     <option value="JAG">Moes Tavern</option>
                  </select>
                  <button class="button add-room">Add assignable room</button>
               </td>
            </tr>
         </tbody>
      </table>
   </form>

   <h3>Team transitions</h3>
   <form class="aui team-transitions">
      <input id="add-transition-condition" class="button submit" type="submit" value="Add condition" />
      <table id="transitions" class="aui text-center">
         <thead>
            <tr>
               <th id="transition-reorder-row"></th>
               <th id="transition-number-row">#</th>
               <th id="transition-jql-row">JQL Condition</th>
               <th id="transition-ops">&nbsp;</th>
               <th id="transition-empty-row"></th>
               <th id="transition-balance-row">Assignable rooms</th>
               <th id="transition-operations"></th>
            </tr>
         </thead>
         <tbody>
            <tr class="otherwise-transition">
               <td headers="transition-reorder-row"></td>
               <td headers="transition-number-row" class="order-cell"></td>
               <td headers="transition-jql-row" colspan="2"><div class="otherwise">otherwise move all remaining unmatched teams</div></td>
               <td headers="transition-empty-row">into</td>
               <td headers="transition-balance-row">
                  <select class="select2-load medium-long-field rooms" multiple="">
                     <option value="CONF">Hall of Justice</option>
                     <option value="JIRA">Avengers Mansion</option>
                     <option value="BAM">Stark Tower</option>
                     <option value="JAG">Moes Tavern</option>
                  </select>
               </td>
               <td></td>
            </tr>
         </tbody>
      </table>
   </form>
   <h3>Preview</h3>
   <form class="aui">
      <div class="transition-actions">
         <input class="button submit calculate" type="submit" value="Preview allocation" />
         <div class="preview-spinner"></div>
      </div>
   </form>

   <div class="rooms-preview"></div>
</apply>
