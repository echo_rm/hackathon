{-# LANGUAGE DeriveGeneric #-}
module Data.HTProperties
  ( HTProperties
  , notTeamProperties
  , fromUserKeys
  , issuePropertyKey
  ) where

import           AesonHelpers
import           Data.Aeson
import           Data.Aeson.Types      (fieldLabelModifier)
import           GHC.Generics
import qualified Snap.AtlassianConnect as AC

issuePropertyKey :: String
issuePropertyKey = "hackathon"

-- This data represents the properties that we want to save against issues in Hackathon projects
data HTProperties = HTP
   { htpTeamMembers     :: Maybe [AC.UserKey]
   , htpTeamMemberCount :: Maybe Int
   , htpIsTeam          :: Bool
   } deriving (Show, Generic)

fromUserKeys :: [AC.UserKey] -> HTProperties
fromUserKeys keys = HTP (Just keys) (Just . length $ keys) True

notTeamProperties :: HTProperties
notTeamProperties = HTP Nothing Nothing False

instance ToJSON HTProperties where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "htp" }

instance FromJSON HTProperties where
   parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "htp" }
