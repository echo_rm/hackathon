define([
   '../helpers/PageContext', 
   "../host/request", 
   '../helpers/MustacheLoader', 
   'underscore', 
   '../helpers/Handler',
   'connect/pagetoken'
],
function(PC, HostRequest, MustacheLoader, _, H) {
   var templates;

   AJS.$.fn.flashClass = function(c, userOptions) {
      var defaults = {
         starton: true,
         timeout: 1500,
         finishedCallback: null
      };
      var options = AJS.$.extend({}, defaults, userOptions);

      var self = this;
      self.toggleClass(c, options.starton);
      return setTimeout(function() { 
         self.toggleClass(c, !options.starton); 
         options.finishedCallback && options.finishedCallback();
      }, options.timeout);
   };

   AJS.$(function() {
      var pageContext = PC.load();
      templates = MustacheLoader.load();

      if (isNaN(pageContext.issue.id)) {
         throw "No issue id on the page. Cannot do anything.";
      }

      var teamMembersRequest;
      var refreshTeamMembers = function() {
         if (teamMembersRequest) {
            teamMembersRequest.abort();
            teamMembersRequest = null;
         }

         teamMembersRequest = AJS.$.ajax({
            url: "/rest/team/team-members",
            type: "GET",
            data: {
               issueId: pageContext.issue.id
            },
            cache: false
         });

         teamMembersRequest.done(function(data) {
            node.teamMembersContainer.empty();
            var hideRemoveButton = data.teamMembers.length <= 1;
            AJS.$("#team-member-count").text(data.teamMembers.length);
            AJS.$.each(data.teamMembers, function(i, teamMember) {
               teamMember.hideRemoveButton = hideRemoveButton;
               node.teamMembersContainer.append(templates.render("team-member-view", teamMember));
            });
         });
      };

      var teamDetailsRequest;
      var refreshTeamDetails = function() {
         if(teamDetailsRequest) {
            teamDetailsRequest.abort();
            teamDetailsRequest = null;
         }

         teamDetailsRequest = AJS.$.ajax({
            url: "/rest/team",
            type: "GET",
            data: {
               issueId: pageContext.issue.id
            },
            cache: false
         });

         teamDetailsRequest.done(function(data) {
            var roomName = "Not available";
            if(data.room) roomName = data.room.name;
            node.roomLozenge.text(roomName);

            var roundName = "Not in a round";
            if(data.round) roundName = data.round.name;
            node.roundLozenge.text(roundName);

            // Sort the round sequence numbers in descending order
            data.pastResults.sort(function(a, b) { return b.roundSequenceNumber - a.roundSequenceNumber; });
            
            var getClassForRank = function(rank) {
                if(rank <= 3) return 'aui-lozenge-success';
                if(rank <= 6) return 'aui-lozenge-complete';
                return '';
            }

            var pastResults = AJS.$.map(data.pastResults, function(pastResult) {
                pastResult.rankLozengeClass = getClassForRank(pastResult.rank);
                return pastResult;
            });

            node.pastResults.empty();
            node.pastResults.append(templates.render('past-results-view', { pastResults: pastResults }));
            node.pastResults.toggleClass('hidden', pastResults.length == 0);
         });
      };

      var genericErrorTimer;

      var handleGenericError = function(jqXHR) {
         var jsonError = JSON.parse(jqXHR.responseText);
         if (jsonError && jsonError.errorMessages) {
            // Clear the previous token
            if (genericErrorTimer) {
               clearTimeout(genericErrorTimer);
               genericErrorTimer = null;
            }

            // Just show the first error message because most of the time we just return one.
            AJS.$("#error-message").removeClass("hidden").find(".title").text(jsonError.errorMessages[0]);
            AP.resize();
            genericErrorTimer = setTimeout(function() {
               AJS.$("#error-message").addClass("hidden");
               AP.resize();
            }, 10000);
         }
      };

      var node = {
         addTeamMemberElements: AJS.$(".add-team-member"),
         teamMemberSelector: AJS.$("#team-member-selector"),
         addTeamMember: AJS.$("#add-team-member"),
         teamMembersContainer: AJS.$(".team-members-container"),
         roomLozenge: AJS.$("#room-lozenge"),
         roundLozenge: AJS.$("#round-lozenge"),
         refresh: AJS.$(".refresh-button"),
         pastResults: AJS.$(".past-results")
      };

      var userSelectorSetup = false;

      var refreshIssuePageTimeout;

      var immediatelyRefreshIssuePage = function() {
         AP.require('jira', function(jira){
            jira.refreshIssuePage();
         });
      };

      var resetIssuePageTimeout = function() {
         if(refreshIssuePageTimeout) {
            clearTimeout(refreshIssuePageTimeout);
         }
         refreshIssuePageTimeout = setTimeout(immediatelyRefreshIssuePage, 1000 * 30);
      };

      var setupUserSelector = function() {
         if(!userSelectorSetup) {
            userSelectorSetup = true;
            var lastSearchedUsers;

            var teamMemberSelect = node.teamMemberSelector.auiSelect2({
               placeholder: "Add team member...",
               minimumInputLength: 1,
               multiple: false,
               maximumSelectionSize: 1,
               escapeMarkup: _.identity,
               query: function (query) {
                  AP.require('request', function(request){
                     request({
                        url: '/rest/api/2/user/picker',
                        type: "GET",
                        data: {
                           query: query.term,
                           showAvatar: true
                        },
                        cache: false,
                        success: function (rawData) {
                           lastSearchedUsers = JSON.parse(rawData).users;
                           var results = lastSearchedUsers.map(function (user) { return {id: user.key, text: user.html } });
                           query.callback({ results: results });
                           AP.resize();
                        }
                     });
                  });
               }
            });

            teamMemberSelect.change(function() {
               var newTeamMemberKey = teamMemberSelect.val();
               if(newTeamMemberKey !== "") {
                  // Make the request to add the new team member
                  var addTeamMemberRequest = AJS.$.ajax({
                     url: "/rest/team/team-member",
                     type: "PUT",
                     data: JSON.stringify({
                        issueId: pageContext.issue.id,
                        userKey: newTeamMemberKey
                     })
                  });

                  // Put the user in the list instantly (don't wait for the request to finish)
                  if(lastSearchedUsers) {
                     var selectedUser = _.find(lastSearchedUsers, function(user) { return user.key === newTeamMemberKey; });
                     if(selectedUser) {
                        selectedUser.smallAvatarUrl = selectedUser.avatarUrl;
                        node.teamMembersContainer.append(templates.render("team-member-view", selectedUser));
                        AP.resize();
                     }
                  }
                  lastSearchedUsers = null;

                  // Clear the way for the next user to be added
                  teamMemberSelect.select2("val", "");

                  addTeamMemberRequest.done(function () {
                     resetIssuePageTimeout();
                  });

                  // When completed refresh the team members and clear state
                  addTeamMemberRequest.complete(function () {
                     refreshTeamMembers();
                  });
               }
            });
         }
      };

      var init = function() {
         AJS.$(document).ajaxError(function(e, jqXHR) {
            handleGenericError(jqXHR);
         });

         setupUserSelector();

         node.refresh.click(H.h(function() {
            var request = AJS.$.ajax({
               url: "/rest/team/refresh?issueId=" + pageContext.issue.id,
               type: "PUT",
               cache: false
            });

            request.done(function() {
               initialLoad();

               immediatelyRefreshIssuePage();
            });
         }));

         node.teamMembersContainer.on("click", ".remove-team-member", function() {
            var self = AJS.$(this);
            var container = self.closest('tr');
            container.addClass("deleting");

            var tenantUserKey = self.data("user-key");

            var deleteTeamMemberRequest = AJS.$.ajax({
               url: "/rest/team/team-member",
               type: "DELETE",
               data: JSON.stringify({
                  issueId: pageContext.issue.id,
                  userKey: tenantUserKey
               })
            });

            deleteTeamMemberRequest.done(function() {
               resetIssuePageTimeout();
            });

            deleteTeamMemberRequest.fail(function() {
               container.removeClass("deleting");
            });

            deleteTeamMemberRequest.complete(function() {
               refreshTeamMembers();
            });
         });

         var initialLoad = function() {
            refreshTeamMembers();
            refreshTeamDetails();
         };
         setTimeout(initialLoad, 1); // So that the Acpt token has time to be injected

         AP.resize();
      };

      init();
   });
});
