define([
   'underscore',
   '../host/request'
], function(_, HostRequest) {
   // Get the teams current components
   // Filter out any rooms that are on that team
   // If there is a next room then add it to the list of components
   // Set the teams components
   // Transition the team to the next round if the win and the outvoted round if they lost
   
   function stringStartsWith(string, prefix) {
      return string.slice(0, prefix.length) === prefix;
   }

   var runRequests = function(requestPromises) {
      var r = AJS.$.Deferred().resolve();

      while(requestPromises.length > 0) {
         r = r.then(requestPromises.shift());
      }

      return r;
   };

   var arraysEqual = function(one, two) {
      if(!one || !two) return false;
      if(one.length !== two.length) return false;

      for(var i = 0; i < one.length; i++) {
         if(one[i] !== two[i]) return false;
      }
      return true;
   };

   // Update the components on an issue
   var updateComponents = function(issue, componentIds) {
      var toSetterId = function(componentId) {
         return { id: '' + componentId };
      };

      var updateData = {
         update: {
            components: [{
               set: _.map(componentIds, toSetterId)
            }]
         }
      };

      return AJS.$.Deferred(function() {
         var self = this;

         AP.request({
            url: "/rest/api/2/issue/" + issue.key,
            type: "PUT",
            cache: false,
            data: JSON.stringify(updateData),
            contentType: 'application/json',
            success: self.resolve,
            error: self.reject
         });
      });
   };

   var getTransitions = function(issue) {
      return AJS.$.Deferred(function() {
         var self = this;

         AP.request({
            url: "/rest/api/2/issue/" + issue.key + "/transitions",
            type: "GET",
            cache: false,
            contentType: 'application/json',
            success: self.resolve,
            error: self.reject
         });
      });
   };

   // Transition an issue into another round
   var transitionTeam = function(issue, transitionId) {
      return AJS.$.Deferred(function() {
         var self = this;

         AP.request({
            url: "/rest/api/2/issue/" + issue.key + "/transitions",
            type: "POST",
            cache: false,
            data: JSON.stringify({ transition: { id: transitionId }}),
            contentType: 'application/json',
            success: self.resolve,
            error: self.reject
         });
      });
   };

   var getTeamIssueId = function(team) { return team.issueId; };

   var jqlForIssueIds = function(issueIds) {
      return "id in (" + issueIds.join() + ")";
   };

   var transitionTeams = function(request) {
      return AJS.$.Deferred(function() {
         var self = this;

         // Get the issue ids for the teams to be migrated
         var teamIssueIds = _.map(request.teams, getTeamIssueId);

         var requiredFields = [
            "components",
            "status"
         ];

         // Search for all of the issues that represent these teams
         var searchRequest = HostRequest.issueSearchAll(jqlForIssueIds(teamIssueIds), requiredFields);

         // If the search fails then reject this deferred
         searchRequest.fail(self.reject);

         // Continue if the search succeeds
         searchRequest.done(function(issues) {
            // Filter out the issues that are not in the current round (status) because we don't need
            // to move them
            var isInCurrentRound = function(issue) {
               return issue && issue.fields && issue.fields.status && parseInt(issue.fields.status.id) === request.roundFrom.statusId;
            };

            var isNextRound = function(status) {
               if(!status) return false;
               var statusId = status.id;
               if(typeof statusId === "string") {
                  statusId = parseInt(statusId);
               }
               return statusId === request.roundTo.statusId;
            };

            var isRoomComponent = function(component) {
               return component && component.name && stringStartsWith(component.name, "Room -");
            };

            var getComponentId = function(component) { return parseInt(component.id); };

            var computeOriginalComponentIds = function(rawIssue) {
               var defaultStructure = {
                  fields: {
                     components: []
                  }
               };

               var issue = AJS.$.extend({}, defaultStructure, rawIssue);

               return _.map(issue.fields.components, getComponentId);
            };

            var computeNewComponents = function(rawIssue) {
               var defaultStructure = {
                  fields: {
                     components: []
                  }
               };

               var issue = AJS.$.extend({}, defaultStructure, rawIssue);

               var nonRoomComponents = _.filter(issue.fields.components, _.negate(isRoomComponent));
               var componentIds = _.map(nonRoomComponents, getComponentId);

               if(request.room) {
                  componentIds.push(request.room.componentId);
               }

               return componentIds;
            };

            var createDone = function() {
               return AJS.$.Deferred(function() { this.resolve(); });
            };

            var requests = [];
            _.each(issues, function(issue) {
               requests.push(function() {
                  // Calculate which components should be updated
                  var componentUpdateRequest;
                  var originalComponents = computeOriginalComponentIds(issue);
                  var newComponents = computeNewComponents(issue);
                  if(!arraysEqual(originalComponents, newComponents)) {
                     componentUpdateRequest = updateComponents(issue, newComponents);
                  } else {
                     componentUpdateRequest = createDone();
                  }

                  // Calculate which transition needs to be performed
                  var issueTransitionRequest;
                  if(isInCurrentRound(issue)) {
                     // Fire request for the transition to occur
                     issueTransitionRequest = getTransitions(issue).then(function(rawTransitions) {
                        var data = JSON.parse(rawTransitions);
                        // Todo find the matching transition
                        var nextTransition = _.find(data.transitions, function(transition) {
                           return isNextRound(transition.to);
                        });

                        if(!nextTransition) {
                           return AJS.$.Deferred.reject({
                              error: "Could not transition issue " + issue.key + " to round " + request.roundTo.name
                           });
                        }

                        return transitionTeam(issue, nextTransition.id);
                     });
                  } else {
                     issueTransitionRequest = createDone();
                  }

                  // When both requests have finished the update the UI
                  return AJS.$.when(componentUpdateRequest, issueTransitionRequest).then(function() {
                     var intIssueId = parseInt(issue.id);
                     var team = _.find(request.teams, function(team) { return team.issueId === intIssueId; });
                     request.updateTeam && request.updateTeam(team);
                  });
               });
            });

            // Run all of the promises sequentially and resolve when done
            var finalResult = runRequests(requests);
            finalResult.done(self.resolve);
            finalResult.fail(self.reject);
         });
      });
   };

   return {
      transitionTeams: transitionTeams
   };
});
