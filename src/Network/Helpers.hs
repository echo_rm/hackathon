{-# LANGUAGE OverloadedStrings #-}
module Network.Helpers
   ( webRequest
   , webRequestGeneric
   , errorOnContent
   ) where

import           Control.Monad.IO.Class
import           Data.Aeson
import qualified Data.ByteString.Lazy.Char8        as BL
import           Data.Monoid                       (Endo (..))
import qualified Data.Text                         as T
import qualified Data.Text.Encoding                as T
import           Network.Api.Support
import           Network.HTTP.Client
import           Network.HTTP.Client.TLS           (tlsManagerSettings)
import           Network.HTTP.Types
import           Network.URI
import qualified Snap.AtlassianConnect.HostRequest as AC

webRequest :: (FromJSON a, MonadIO m, Functor m) => StdMethod -> URI -> Endo Request -> m (Either AC.ProductErrorResponse a)
webRequest method' uri modifications = expectBody <$> webRequestGeneric method' uri modifications

webRequestGeneric :: (FromJSON a, MonadIO m) => StdMethod -> URI -> Endo Request -> m (Either AC.ProductErrorResponse (Maybe a))
webRequestGeneric standardHttpMethod requestUri requestModifications = do
   liftIO $ runRequest connectionManagerSettings standardHttpMethod url
      (  addHeader ("Accept", "application/json")
      <> requestModifications
      )
      (basicResponder responder)
   where
      connectionManagerSettings = if "https://" `T.isPrefixOf` url then tlsManagerSettings else defaultManagerSettings
      url = T.pack . show $ requestUri

responder :: FromJSON a => Int -> BL.ByteString -> Either AC.ProductErrorResponse (Maybe a)
responder responseCode body
   | responseCode == 204 = Right Nothing
   | 200 <= responseCode && responseCode < 300 =
      case eitherDecode body of
         Right jsonResponse -> Right . Just $ jsonResponse
         Left err -> Left $ AC.ProductErrorResponse responseCode (T.pack $ "Could not parse the json response: " ++ show err)
   | otherwise = Left $ AC.ProductErrorResponse responseCode (T.decodeUtf8 . BL.toStrict $ body)

expectBody :: Either AC.ProductErrorResponse (Maybe x) -> Either AC.ProductErrorResponse x
expectBody (Left x) = Left x
expectBody (Right (Just x)) = Right x
expectBody (Right Nothing) = Left noResponse
   where
      noResponse = AC.ProductErrorResponse 204 "We expected to get content back from this resource but instead we recieved no content."

errorOnContent :: Either AC.ProductErrorResponse (Maybe Value) -> Either AC.ProductErrorResponse ()
errorOnContent (Left x) = Left x
errorOnContent (Right (Just _)) = Left $ AC.ProductErrorResponse 200 "We recieved content from this request but we expected none."
errorOnContent (Right Nothing) = Right ()
