{-# LANGUAGE OverloadedStrings #-}
module HostRequests.WatchIssue where

import           Application
import qualified Control.Arrow                     as A
import           Data.Aeson
import qualified Data.ByteString                   as BS
import qualified Data.ByteString.Char8             as BSC
import qualified Persistence.HackathonTeam         as HT
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Snaplet                      as SS

watchTeam :: AC.Tenant -> HT.HackathonTeam -> AC.UserKey -> AppHandler (Either AC.ProductErrorResponse ())
watchTeam tenant team userKey = SS.with connect $ A.right toNothing <$> AC.hostPostRequestExtended tenant watchIssueUrl [] (AC.setJson (String userKey))
    where
       watchIssueUrl :: BS.ByteString
       watchIssueUrl = "/rest/api/2/issue/" `BSC.append` (BSC.pack . show . HT.htIssueId $ team) `BSC.append` "/watchers"

       toNothing :: Maybe () -> ()
       toNothing = const ()
