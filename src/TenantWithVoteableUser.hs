module TenantWithVoteableUser
    ( tenantWithVoteableUserFromHackathonRequest
    , tenantWithVoteableUser
    ) where

import           Application
import           Control.Monad.Trans.Either
import qualified Data.HackathonConstants         as HC
import qualified Data.HackathonRequest           as HR
import           Data.MaybeUtil
import qualified Data.Text.Encoding              as T
import           HandlerHelpers
import qualified Persistence.AnonymousTenantUser as ATU
import qualified Persistence.Hackathon           as PH
import qualified Persistence.Tenant              as PT
import qualified Snap.AtlassianConnect           as AC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import           UserUtil
import qualified WithToken                       as WT

-- The purpose of this module is to simplify any code that needs to handle tenant users and anonymous users in the same
-- snap handler

tenantWithVoteableUserFromHackathonRequest
    :: HR.HackathonRequest
    -> (AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ())
    -> AppHandler ()
tenantWithVoteableUserFromHackathonRequest hr
    = tenantWithVoteableUser (HR.toVoteableUserToken . HR.hreqToken $ hr) (HR.hreqProjectId hr) (HR.toAnonToken . HR.hreqToken $ hr)

tenantWithVoteableUser
    :: HR.VoteableUserToken
    -> AC.ProjectId
    -> Maybe ATU.AnonymousUserToken
    -> (AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ())
    -> AppHandler ()
tenantWithVoteableUser userToken projectId potentialAnonToken handler = do
    response <- rawTenantWithVoteableUser userToken projectId potentialAnonToken
    case response of
        Left err -> uncurry respondWithError err
        Right (tenant, hackathon, potentialUser) -> handler tenant hackathon potentialUser

rawTenantWithVoteableUser
    :: HR.VoteableUserToken
    -> AC.ProjectId
    -> Maybe ATU.AnonymousUserToken
    -> AppHandler (Either HR.HttpError (AC.Tenant, PH.Hackathon, Maybe HR.VoteableUser))
rawTenantWithVoteableUser (HR.RawPageToken rawToken) projectId _ = withPG . runEitherT $ do
   (tenant, potentialUserKey) <- EitherT $ WT.rawTenantFromToken (T.encodeUtf8 rawToken)
   hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant projectId)
   case (PH.hackathonVotingRestriction hackathon, potentialUserKey) of
     (HC.LoginAndTeamMember, Nothing) -> loginRequired
     (HC.LoginOnly         , Nothing) -> loginRequired
     (_                    , Nothing) -> return (tenant, hackathon, Nothing)
     (_                    , Just userKey) -> do
        tenantUser <- EitherT (withErrCode internalServer <$> getOrCreateTenantUserByKey tenant userKey)
        return (tenant, hackathon, Just . HR.VoteableTenantUser $ tenantUser)
rawTenantWithVoteableUser (HR.RawTenantKey tenantKey) projectId potentialAnonToken = withPG . runEitherT $ do
   tenant <- EitherT (m2e noSuchTenant <$> PT.lookupTenant tenantKey)
   hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant projectId)
   guardHackathonAcceptsAnonymous hackathon
   case potentialAnonToken of
     Nothing -> return (tenant, hackathon, Nothing)
     Just anonToken -> do
        anonUser <- EitherT (m2e noSuchAnonUser <$> ATU.lookupAnonymousUser tenant anonToken)
        return (tenant, hackathon, Just . HR.VoteableAnonUser $ anonUser)

guardHackathonAcceptsAnonymous :: Monad m => PH.Hackathon -> EitherT HR.HttpError m ()
guardHackathonAcceptsAnonymous hackathon
   | PH.hackathonVotingRestriction hackathon == HC.Anonymous = return ()
   | otherwise = loginRequired

loginRequired :: Monad m => EitherT HR.HttpError m b
loginRequired = left (unauthorised, "This hackathon requires that you be logged in to make this request.")

noSuchTenant :: HR.HttpError
noSuchTenant = (notFound, "There is no tenant with that key.")

noSuchHackathon :: HR.HttpError
noSuchHackathon = (badRequest, "There is no hackathon with the provided project id.")

noSuchAnonUser :: HR.HttpError
noSuchAnonUser = (unauthorised, "There is no such anosymous user with the provided token.")
