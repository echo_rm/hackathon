define(function() {
   "use strict";

   var requestUserDetails = function(userkey) {
      return AJS.$.Deferred(function() {
         var self = this;
         AP.request({
            url: "/rest/api/2/user",
            type: "GET",
            cache: true,   // This does not work thanks to https://ecosystem.atlassian.net/browse/AC-1253
            dataType: "text",
            data: {
               key: userkey
            },
            success: self.resolve,
            error: self.reject
         });
      });
   };

   var requestIssueDetails = function(issueKey) {
      return AJS.$.Deferred(function() {
         var self = this;
         AP.request({
            url: "/rest/api/2/issue/" + issueKey,
            type: "GET",
            cache: true,  // This does not work thanks to https://ecosystem.atlassian.net/browse/AC-1253
            dataType: "text",
            success: self.resolve,
            error: self.reject
         });
      });
   };

   var requestProjectDetails = function(projectId) {
      return AJS.$.Deferred(function() {
         var self = this;
         AP.request({
            url: "/rest/api/2/project/" + projectId,
            type: "GET",
            cache: true,
            dataType: "text",
            success: self.resolve,
            error: self.reject
         });
      });
   };

   var requestStatusesFromProject = function(projectId) {
      return AJS.$.Deferred(function() {
         var self = this;
         AP.request({
            url: "/rest/api/2/project/" + projectId + "/statuses",
            type: "GET",
            cache: true,
            dataType: "text",
            success: self.resolve,
            error: self.reject
         });
      });
   };

   var requestAllProjects = function () {
      return AJS.$.Deferred(function () {
         var self = this;
         AP.request({
            url: "/rest/api/2/project",
            type: "GET",
            cache: false,
            dataType: "text",
            success: self.resolve,
            error: self.reject
         });
      });
   };

   var searchForIssues = function(data, options) {
      var defaultOptions = {
         cache: false
      };

      var opts = AJS.$.extend({}, defaultOptions, options);

      return AJS.$.Deferred(function() {
         var self = this;
         AP.request({
            url: "/rest/api/2/search",
            type: "POST",
            cache: opts.cache,
            data: JSON.stringify(data),
            contentType: 'application/json',
            success: self.resolve,
            error: self.reject
         });
      });
   };

   /*
    { rspStartAt    :: Integer
    , rspMaxResults :: Integer
    , rspTotal      :: Integer
    , rspIssues     :: [RawIssue]
   */
   var maxPageSize = 1000;

   var searchedTo = function(searchResponse) {
      return searchResponse.startAt + searchResponse.maxResults;
   };

   var isLastPage = function(searchResponse) {
      return searchedTo(searchResponse) >= searchResponse.total;
   };

   var searchForAll = function(jql, fields) {
      var responseData = [];

      var getRequestData = function(start) {
         return {
            jql: jql,
            fields: fields,
            startAt: start,
            maxResults: maxPageSize
         };
      };

      return AJS.$.Deferred(function() {
         var self = this;
         var resultantIssues = [];

         var go = function(pageStart) {
            var response = searchForIssues(getRequestData(pageStart));

            response.done(function(sr) {
               var searchResponse = JSON.parse(sr);
               resultantIssues = resultantIssues.concat(searchResponse.issues);

               if(isLastPage(searchResponse)) {
                  self.resolve(resultantIssues);
               } else {
                  go(searchedTo(searchResponse));
               }
            });

            response.fail(self.reject);
         };

         go(0);
      });
   };

   return {
      userDetails: requestUserDetails,
      issueDetails: requestIssueDetails,
      projectDetails: requestProjectDetails,
      projectStatuses: requestStatusesFromProject,
      allProjects: requestAllProjects,
      issueSearch: searchForIssues,
      issueSearchAll: searchForAll
   };
});
