{-# LANGUAGE DeriveGeneric, FlexibleContexts #-}

module Healthcheck
   ( healthcheckRequest
   ) where

import           AesonHelpers           (baseOptions, stripFieldNamePrefix)
import           Application
import qualified Control.Exception      as E
import           Control.Monad.IO.Class (liftIO)
import           Data.Aeson
import           Data.Aeson.Types
import           Data.Maybe             (isNothing)
import qualified Data.Text              as T
import           Data.Time.Clock
import           GHC.Generics
import           Persistence.Tenant     (getTenantCount)
import qualified Snap.Core              as SC
import           Snap.Helpers
import qualified Control.Exception.Lifted as LE
import           Control.Monad.Trans.Control (MonadBaseControl)

healthcheckRequest :: AppHandler ()
healthcheckRequest = handleMethods
   [ ( SC.GET, getHealthcheckRequest )
   ]

getHealthcheckRequest :: AppHandler ()
getHealthcheckRequest = do
   runResult <- runHealthchecks curatedHealthchecks
   if anyHealthcheckFailed runResult
      then writeJson runResult >> respondWith serviceUnavaliable
      else respondNoContent

-- Important: This is our curated list of healthchecks. Anything in this list will run as a
-- healthcheck for this service.
curatedHealthchecks :: [Healthcheck]
curatedHealthchecks =
   [ databaseHealthCheck -- Currently this is the only service that we depend on so it is our only healthcheck
   --, failCheck
   ]

runHealthchecks :: [Healthcheck] -> AppHandler HealthcheckRunResult
runHealthchecks healthchecks = HealthcheckRunResult <$> sequence healthchecks

anyHealthcheckFailed :: HealthcheckRunResult -> Bool
anyHealthcheckFailed (HealthcheckRunResult statuses) = any (not . hsIsHealthy) statuses

simpleCatch :: (MonadBaseControl IO m, Functor m) => m a -> m (Either E.SomeException a)
simpleCatch = LE.tryJust exceptionFilter

databaseHealthCheck :: Healthcheck
databaseHealthCheck = do
   currentTime <- liftIO getCurrentTime
   result <- simpleCatch getTenantCount
   return $ status (either Just (const Nothing) result) currentTime
   where
      status :: E.Exception e => Maybe e -> UTCTime -> HealthStatus
      status potentialException currentTime = HealthStatus
         { hsName = T.pack "Database Connection Check"
         , hsDescription = T.pack "Ensures that this service can connect to the PostgreSQL Relational Database."
         , hsIsHealthy = isNothing potentialException
         , hsFailureReason = do
             exception <- potentialException
             return . T.pack $ "Could not connect to the remote database. Addon will not work correctly. Message: " ++ show exception
         , hsApplication = application
         , hsTime = currentTime
         , hsSeverity = CRITICAL
         , hsDocumentation = Just . T.pack $ "If you see this error you might want to check out the database and see "
            ++ "what is going on there. And then ensure that the application has been passed the correct database credentials."
         }

application :: HealthcheckApplication
application = ConnectApplication { haName = T.pack "hackathon" }

{-
-- This Healthcheck remains for testing purposes
failCheck :: Healthcheck
failCheck = do
   ct <- liftIO getCurrentTime
   return $ HealthStatus
      { hsName = T.pack "Failing healthcheck"
      , hsDescription = T.pack "I always fail...that's how this healthcheck rolls."
      , hsIsHealthy = False
      , hsFailureReason = Just . T.pack $ "I always fail. Read the description"
      , hsApplication = application
      , hsTime = ct
      , hsSeverity = UNDEFINED
      , hsDocumentation = Nothing
      }
-}


-- For now we just catch everything but in the future we might choose to be more
-- selective...maybe. 
exceptionFilter :: E.SomeException -> Maybe E.SomeException
exceptionFilter = Just

type Healthcheck = AppHandler HealthStatus

data HealthcheckRunResult = HealthcheckRunResult
   { hrrStatus :: [HealthStatus]
   }

instance ToJSON HealthcheckRunResult where
   toJSON hrr@(HealthcheckRunResult {}) = object [ T.pack "status" .= hrrStatus hrr ]

data HealthStatus = HealthStatus
   { hsName          :: T.Text
   , hsDescription   :: T.Text
   , hsIsHealthy     :: Bool
   , hsFailureReason :: Maybe T.Text
   , hsApplication   :: HealthcheckApplication
   , hsTime          :: UTCTime
   , hsSeverity      :: HealthStatusSeverity
   , hsDocumentation :: Maybe T.Text
   } deriving (Generic)

instance ToJSON HealthStatus where
   toJSON = genericToJSON (baseOptions { fieldLabelModifier = stripFieldNamePrefix "hs" })

data HealthcheckApplication = ConnectApplication
   { haName :: T.Text
   }

instance ToJSON HealthcheckApplication where
   toJSON = toJSON . haName

data HealthStatusSeverity
   = UNDEFINED
   | MINOR
   | MAJOR
   | WARNING
   | CRITICAL
   deriving(Eq, Ord, Show, Generic)

instance ToJSON HealthStatusSeverity
