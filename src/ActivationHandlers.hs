{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module ActivationHandlers
    ( handleHackathonActivation
    ) where

import qualified AesonHelpers                      as AH
import           Application
import qualified Control.Arrow                     as A
import           Control.Monad                     (void)
import           Control.Monad.Trans.Class         (lift)
import           Control.Monad.Trans.Either
import           Control.Monad.Trans.Maybe         (runMaybeT)
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.HackathonConstants           as HC
import           Data.List                         (group, sort)
import qualified Data.Map                          as M
import           Data.Maybe                        (catMaybes)
import           Data.MaybeUtil
import qualified Data.Text                         as T
import qualified Data.Text.Encoding                as T
import qualified Data.Time.Clock                   as C
import           GHC.Generics
import           HandlerHelpers
import qualified HostRequests.Components           as HC
import qualified HostRequests.EntityProperties     as EP
import qualified HostRequests.UserDetails          as HU
import qualified Persistence.Hackathon             as PH
import qualified Persistence.HackathonIds          as HI
import qualified Persistence.HackathonRoom         as HRM
import qualified Persistence.HackathonRound        as PHR
import qualified Persistence.TenantUser            as PTU
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as ACH
import qualified Snap.Core                         as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import           StandardJQL
import qualified TeamLoader                        as TL
import           UTCTimeHelpers
import qualified WithToken                         as WT

handleHackathonActivation :: AppHandler ()
handleHackathonActivation = handleMethods
    [ (SC.POST, handleActivation)
    ]

handleActivation :: AppHandler ()
handleActivation = WT.tenantFromToken handleActivationWithTenant

data ActivationRequest = ActivationRequest
    { arStartDate      :: C.UTCTime
    , arEndDate        :: C.UTCTime
    , arProjectId      :: AC.ProjectId
    , arProjectKey     :: AC.ProjectKey
    , arProjectName    :: T.Text
    , arIssueTypeId    :: Integer
    , arSingleRoomName :: Maybe T.Text
    , arRounds         :: [ActivationRound]
    } deriving(Show, Generic)

data ActivationRound = ActivationRound
    { roundWorkflowStatusId :: Integer
    , roundName             :: T.Text
    , roundType             :: RoundType
    , roundNumber           :: Maybe Integer
    } deriving(Show, Generic)

newtype RoundType = RT HC.HackathonRoundType
    deriving (Show)

instance FromJSON ActivationRequest where
    parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = AH.stripFieldNamePrefix "ar" }

instance FromJSON ActivationRound where
    parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = AH.stripFieldNamePrefix "round" }

instance FromJSON RoundType where
    parseJSON (String "TEAM_FORMATION") = return . RT $ HC.TeamFormation
    parseJSON (String "OUTVOTED") = return . RT $ HC.Outvoted
    parseJSON (String "WINNER") = return . RT $ HC.Winner
    parseJSON (String "ROUND") = return . RT $ HC.Round
    parseJSON _ = fail "Could not parse the round type"

type ErrorMessage = String

oneOrNothing :: [a] -> Maybe [a]
oneOrNothing [] = Nothing
oneOrNothing xs = Just xs

maybeToError :: Maybe [ErrorMessage] -> Either [ErrorMessage] ()
maybeToError (Just errors) = Left errors
maybeToError Nothing = Right ()

validate :: ActivationRequest -> Maybe [ErrorMessage]
validate activationRequest = oneOrNothing . concat . catMaybes $ (validators <*> pure activationRequest)
    where
        validators = [validateDates, validateRounds]

validateDates :: ActivationRequest -> Maybe [ErrorMessage]
validateDates request = if arEndDate request < arStartDate request
    then Just ["The end date was before the start. This is invalid."]
    else Nothing

validateRounds :: ActivationRequest -> Maybe [ErrorMessage]
validateRounds request = if any (> 1) (fmap length groupedIds)
    then return ["You have mapped some statuses twice. Each workflow status can only be mapped once."]
    else Nothing
    where
        groupedIds = group . sort $ statusIds
        statusIds = fmap roundWorkflowStatusId rounds
        rounds = arRounds request

-- TODO validate that all of the rounds that we are given are in ascending order and increase by one

handleActivationWithTenant :: AC.TenantWithUser -> AppHandler ()
handleActivationWithTenant (_, Nothing) = respondWithError forbidden "You cannot activate a hackathon if you are not logged in."
handleActivationWithTenant (tenant, Just userKey) = handleHostRequest (HU.getUserDetails tenant userKey) (handleActivationWithTenantAndUserDetails tenant)

-- TODO refactor this code to use the UserUtil code for TenantUser because this currently will request user details every single time
handleActivationWithTenantAndUserDetails :: AC.Tenant -> HU.UserWithDetails -> AppHandler ()
handleActivationWithTenantAndUserDetails tenant userWithDetails = do
    request <- SC.readRequestBody size10KB
    writeErrors . withPG . withTransaction . runEitherT $ do
       activationRequest <- hoistEither . pe . withErrCode badRequest . eitherDecode $ request
       hoistEither . withErrs badRequest . maybeToError . validate $ activationRequest
       let tenantUser = toTenantUser tenant userWithDetails
       tenantUserId <- EitherT (m2e userInsertFailed <$> PTU.insertTenantUserIfNotExists tenant tenantUser)
       let hackathonTemplate = extractHackathonFromRequest tenant tenantUserId activationRequest
       -- Set the project properties on the hackathon to make it a real hackathon
       EitherT (A.left propertiesCreationError <$> EP.updateProjectProperties tenant (arProjectId activationRequest) HC.projectSettingsKey HC.defaultProjectSettings)
       -- Create the hackathon and rounds in the db
       hackathonId <- EitherT (m2e allocationFailed <$> PH.createHackathon hackathonTemplate)
       let hackathonRounds = fmap (toHackathonRound hackathonId) (arRounds activationRequest)
       void .lift $ PHR.createRounds hackathonRounds
       let hackathon = hackathonTemplate { PH.hackathonId = hackathonId }
       -- Create a room for the hackathon if the user has opted in to this
       lift $ createSingleHackathonRoom activationRequest tenant hackathon (arSingleRoomName activationRequest)
       -- Load the existing issues for the hackathon
       EitherT (A.left (fromILF . TL.tlfError) <$> TL.loadTeamsFromJQL tenant hackathon (jqlForHackathonIssues hackathon))
       return ()
    where
       userInsertFailed = (internalServer, return "Could not persist your user details. Failed to activate Hackathon.")
       allocationFailed = (internalServer, return "Failed to allocate the hackathon details.")
       propertiesCreationError e = (internalServer, return $ "Failed to update the project properties to mark this as a Hackathon. " ++ show e)

       fromILF :: TL.IssueLoadFailure -> (Int, [String])
       fromILF TL.JQLSearchFailure = (internalServer, return "Failed to search for the existing issues in this JIRA project.")
       fromILF TL.RoomCreationFailure = (internalServer, return "Failed to create a room while importing issues from this JIRA project.")
       fromILF TL.TeamCreationFailure = (internalServer, return "Failed to import a team while importing issues from this JIRA project.")
       fromILF TL.TeamMemberLookupFailure = (internalServer, return "Failed to get the initial team members of an issue while trying to import it.")
       fromILF TL.NoSuchHackathon = (internalServer, return "Failed to find a valid hackathon for the issue that was searched for.")
       fromILF TL.EntityPropertyUpdateFailure = (internalServer, return "Failed to update the issue properties on an issue.")

createSingleHackathonRoom :: ActivationRequest -> AC.Tenant -> PH.Hackathon -> Maybe T.Text -> AppHandler ()
createSingleHackathonRoom _ _ _ Nothing = return ()
createSingleHackathonRoom activationRequest tenant hackathon (Just singleRoomName) = do
   let nameWithPrefix = HC.roomComponentPrefix `T.append` singleRoomName
   potentialComponent <- HC.getOrCreateComponentByName tenant (arProjectKey activationRequest) nameWithPrefix
   case potentialComponent of
       Left componentError -> respondWithError internalServer "Could not get or create the room component."
       Right component -> void . runMaybeT $ HRM.getOrCreateHackathonRoom hackathon (HC.jcId component) singleRoomName

toTenantUser :: AC.Tenant -> HU.UserWithDetails -> PTU.TenantUser
toTenantUser tenant ud = PTU.TenantUser
    { PTU.tuId          = 0
    , PTU.tuTenantId    = AC.tenantId tenant
    , PTU.tuKey         = HU.key ud
    , PTU.tuEmail       = T.encodeUtf8 . HU.emailAddress $ ud
    , PTU.tuDisplayName = HU.displayName ud
    , PTU.tuTimeZone = Just . HU.timeZone $ ud
    , PTU.tuSmallAvatarUrl = T.pack <$> M.lookup "24x24" (HU.avatarUrls ud)
    , PTU.tuLastUpdated = dummyUtcTime
    }

toHackathonRound :: HI.HackathonId -> ActivationRound -> PHR.HackathonRound
toHackathonRound hackathonId ar = PHR.HackathonRound
    { PHR.hroundId = 0 -- Unused
    , PHR.hroundHackathonId = hackathonId
    , PHR.hroundStatusId = roundWorkflowStatusId ar
    , PHR.hroundName = roundName ar
    , PHR.hroundType = rt
    , PHR.hroundSequenceNumber = roundNumber ar
    , PHR.hroundVotesPerUser = 3  -- Default to 3 votes per user
    , PHR.hroundVotesUserTeam = 1 -- Default to only voting once per team
    , PHR.hroundTransitionTied = if rt == HC.TeamFormation then True else PHR.defaultTransitionTied
    , PHR.hroundMatchUnallocated = if rt == HC.TeamFormation then True else PHR.defaultMatchUnallocated
    }
   where
      rt = toRoundType . roundType $ ar

toRoundType :: RoundType -> HC.HackathonRoundType
toRoundType (RT rt) = rt

handleHostRequest :: AppHandler (Either ACH.ProductErrorResponse a) -> (a -> AppHandler b) -> AppHandler ()
handleHostRequest hr handler = do
    potentialData <- hr
    case potentialData of
        Left e -> respondWithError (ACH.perCode e) (T.unpack . ACH.perMessage $ e)
        Right goodData -> void $ handler goodData

extractHackathonFromRequest :: AC.Tenant -> HI.TenantUserId -> ActivationRequest -> PH.Hackathon
extractHackathonFromRequest tenant userId r = PH.Hackathon
    { PH.hackathonId = 0
    , PH.hackathonTenantId = AC.tenantId tenant
    , PH.hackathonProjectId = arProjectId r
    , PH.hackathonProjectKey = arProjectKey r
    , PH.hackathonProjectName = arProjectName r
    , PH.hackathonTeamIssueTypeId = arIssueTypeId r
    , PH.hackathonActivatorId = userId
    , PH.hackathonStartDate = arStartDate r
    , PH.hackathonEndDate = arEndDate r
    , PH.hackathonVotingRestriction = PH.LoginAndTeamMember -- Default to the highest restriction upon activation
    , PH.hackathonMultipleRoomVoting = False -- Default to disallowing multiple room voting
    , PH.hackathonShortUrl = Nothing
    }
