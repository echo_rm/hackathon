create table tenant_user
   ( id SERIAL PRIMARY KEY
   , tenant_id INTEGER NOT NULL REFERENCES tenant(id) ON DELETE CASCADE
   , key VARCHAR(255) NOT NULL
   , email VARCHAR(512) NOT NULL
   , displayName VARCHAR(255) NOT NULL
   , small_avatar_url VARCHAR(255) -- This field can be null, at which point we show a dummy icon
   , last_updated TIMESTAMP WITH TIME ZONE NOT NULL
   -- There should only be unique user keys for each tenant
   , CONSTRAINT tenant_user_unique_pair UNIQUE (tenant_id, key)
   );

create table anonymous_tenant_user
   ( id SERIAL PRIMARY KEY
   , tenant_id INTEGER NOT NULL REFERENCES tenant(id) ON DELETE CASCADE
   -- We expect uuid's in this field, this means that they should not be more than 37 chars
   , token VARCHAR(40) NOT NULL
   , CONSTRAINT unique_token_for_tenant UNIQUE (tenant_id, token)
   );

create table hackathon
   ( id SERIAL PRIMARY KEY 
   , tenant_id INTEGER NOT NULL REFERENCES tenant(id) ON DELETE CASCADE
   , project_id INTEGER NOT NULL -- The id of the JIRA Project
   , project_key VARCHAR(50) NOT NULL -- The key of the JIRA Project (restricted despite https://jira.atlassian.com/browse/JRA-28577)
   , project_name VARCHAR(255) NOT NULL -- For chosen length see: https://jira.atlassian.com/browse/JRA-29056
   , hackathon_team_issue_type_id INTEGER NOT NULL -- The Issue Type that maps to a hackathon team
   , activator INTEGER NOT NULL REFERENCES tenant_user(id) ON DELETE CASCADE
   , start_date TIMESTAMP WITH TIME ZONE NOT NULL
   , end_date TIMESTAMP WITH TIME ZONE NOT NULL
   , voting_restriction INTEGER NOT NULL -- This integer maps to a style of voting restriction
   , multiple_room_voting BOOLEAN DEFAULT FALSE
   , created TIMESTAMP WITH TIME ZONE NOT NULL
   );

-- TODO we are going to have to create indexs on team_members because we want to know:
--  - All of the team members for a given hackathon team
--  - All of the hackathon teams for a given person...this sucks in a way. It means that you will not be able to search for a project by team members.
-- CREATE INDEX team_member_on_mapping_idx ON team_member_to_hackathon_team(team_member_id);

-- TODO what do we do when the workflow is modified? All of the mappings will no longer be correct. And how would we even know?
create table round
   ( id SERIAL PRIMARY KEY
   , hackathon_id INTEGER NOT NULL REFERENCES hackathon(id) ON DELETE CASCADE
   , workflow_status_id INTEGER NOT NULL
   , name VARCHAR(100) NOT NULL
   , type VARCHAR(30) NOT NULL
   , sequence_number INTEGER -- The round number in sequence, this is so that we can tell which round follows the previous round, if null then not a voteable round
   , votes_per_user INTEGER NOT NULL -- The allocated votes per user in a round
   , votes_user_team INTEGER NOT NULL -- The maximum number of votes that a user can place on a single team in this round.
   , transition_tied_teams BOOLEAN NOT NULL -- Whether or not to transition tied teams out of this round
   , match_unallocated_teams BOOLEAN NOT NULL -- Whether or not to transition unallocated teams out of this round (teams that are not in a room)
   , CONSTRAINT round_sequence_numbers_unique UNIQUE (sequence_number, hackathon_id)
   );

-- TODO will I be notified when a component is deleted (or will I just see a bunch of issue update events?) In not then I have to handle the fact that my
-- component could be deleted at any time. Importantly, we should never delete a room from our systems even if the
-- component is. That way we can never loose information about the room.
create table room
   ( id SERIAL PRIMARY KEY
   , hackathon_id INTEGER NOT NULL REFERENCES hackathon(id) ON DELETE CASCADE
   , current_round_id INTEGER NOT NULL REFERENCES round(id) on DELETE CASCADE -- Now each room has a current round. That way we can do global voting.
   , component_id INTEGER NOT NULL -- The id of the jira component that this maps to
   , name VARCHAR(255) NOT NULL -- The name of this room
   );
-- You should set the current round for the room on creation to the round with the lowest sequence number

create table hackathon_team
   ( id SERIAL PRIMARY KEY
   , hackathon_id INTEGER NOT NULL REFERENCES hackathon(id) ON DELETE CASCADE
   , room_id INTEGER REFERENCES room(id) ON DELETE CASCADE
   , round_id INTEGER REFERENCES round(id) ON DELETE CASCADE
   , issue_id INTEGER NOT NULL -- The id of the JIRA issue
   -- Issue Keys can be as long as you please: https://jira.atlassian.com/browse/JRA-28577 but pragmatically we need to set a limit
   , issue_key VARCHAR(50) NOT NULL -- The key of the JIRA issue. Use webhooks to update this detail.
   , issue_summary VARCHAR(512) NOT NULL
   );

-- TODO Create a unique index on the hackathon table to prevent slow sequential queries on the getTeamMembersForIssueId method
-- CREATE UNIQUE INDEX issue_id_on_hackathon_team_idx ON hackathon_team(hackathon_id, issue_id);
-- CREATE UNIQUE INDEX issue_id_on_hackathon_team_idx ON hackathon_team(issue_id);
-- DROP INDEX issue_id_on_hackathon_team_idx;

create table team_members
   ( tenant_user_id INTEGER NOT NULL REFERENCES tenant_user(id) ON DELETE CASCADE
   , hackathon_team_id INTEGER NOT NULL REFERENCES hackathon_team(id) ON DELETE CASCADE
   , CONSTRAINT one_tenant_user_per_team UNIQUE (tenant_user_id, hackathon_team_id)
   );
   
-- TODO this table needs the appropriate indexes so that we can make querying it fast. Performance test it.
create table vote
   ( id SERIAL PRIMARY KEY
   , tenant_user_id INTEGER REFERENCES tenant_user(id) ON DELETE CASCADE
   , anonymous_tenant_user_id INTEGER REFERENCES anonymous_tenant_user(id) ON DELETE CASCADE
   , hackathon_team_id INTEGER NOT NULL REFERENCES hackathon_team(id) ON DELETE CASCADE
   , round_id INTEGER NOT NULL REFERENCES round(id) ON DELETE CASCADE
   , room_id INTEGER NOT NULL REFERENCES room(id) ON DELETE CASCADE
   , CONSTRAINT anonymous_xor_tenant_user CHECK (
      (tenant_user_id IS NULL     AND anonymous_tenant_user_id IS NOT NULL) OR
      (tenant_user_id IS NOT NULL AND anonymous_tenant_user_id IS NULL)
     )
   );

CREATE INDEX vote_by_round_idx ON vote (round_id);

create table round_room
   ( room_id INTEGER NOT NULL REFERENCES room(id) ON DELETE CASCADE
   , round_id INTEGER NOT NULL REFERENCES round(id) ON DELETE CASCADE
   , status INTEGER NOT NULL -- This should be INACTIVE, ACTIVE or LOCKED
   , maximum_teams INTEGER CONSTRAINT positive_maximum_teams CHECK (maximum_teams > 0) -- The maximum number of teams that the room can hold if unlimited is not set
   , winners INTEGER NOT NULL CONSTRAINT positive_winners CHECK (winners > 0) -- The number of teams that should make it through.
   , last_updated TIMESTAMP NOT NULL
   , PRIMARY KEY (round_id, room_id)
   );

create table round_transition
   ( id SERIAL PRIMARY KEY
   , round_id INTEGER NOT NULL REFERENCES round(id) ON DELETE CASCADE -- TODO Maybe create an index on the round id because that will be looked up frequently
   , order_number INTEGER NOT NULL
   , jql TEXT -- This may be null if it is the last option
   );

create table round_transition_room
   ( round_transition_id INTEGER NOT NULL REFERENCES round_transition(id) ON DELETE CASCADE
   , room_id INTEGER NOT NULL REFERENCES room(id) ON DELETE CASCADE
   , CONSTRAINT room_only_once_per_transition UNIQUE (round_transition_id, room_id)
   );

-- The record of the purged tenants
CREATE TABLE purged_tenant
   ( id        SERIAL PRIMARY KEY
   , baseUrl   VARCHAR(512) not null
   , purgeDate TIMESTAMP WITH TIME ZONE
   );

CREATE INDEX purge_date_idx ON purged_tenant (purgeDate);
