import { TeamDetailsWithPastResults, HackathonRoundType } from '../hackathon-client';

// tslint:disable
export const TeamDetailsExample: TeamDetailsWithPastResults = {
  "pastResults" : [
     {
        "rank" : 1,
        "roomName" : "SYD - 343 - 9.03 Zone",
        "votes" : 10,
        "roundSequenceNumber" : 1,
        "roundName" : "Round 1"
     },
     {
        "roundName" : "Round 2",
        "roundSequenceNumber" : 2,
        "votes" : 39,
        "roomName" : "SYD - 341 - 6.02 Stark",
        "rank" : 1
     },
     {
        "roomName" : "SYD - 363 - Around the world",
        "votes" : 201,
        "roundSequenceNumber" : 3,
        "roundName" : "Round 3",
        "rank" : 1
     }
  ],
  "room" : {
     "currentRoundId" : 417,
     "id" : 457,
     "name" : "SYD - 363 - Around the world",
     "componentId" : 17790
  },
  "round" : {
     "type" : HackathonRoundType.ROUND,
     "name" : "Round 3",
     "sequenceNumber" : 3,
     "id" : 417,
     "statusId" : 10003
  },
  "id" : 6372
};
