<apply template="connect-panel">
    <bind tag="header-extra">
        <js src="/static/${resourcesVersion}/js/app/hackathon-vote-redirect.js" />
        <script>require(['app/hackathon-vote-redirect']);</script>
    </bind>

    <p>Getting login token...</p>
    <div id="redirect-failed" class="aui-message aui-message-error hidden">
        <p class="title">
            <strong>I don't know where to redirect you</strong>
        </p>
        <p>Please go back to the voting link.</p>
    </div>
</apply>