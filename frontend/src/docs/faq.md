# FAQ

## How do I install Hackathon for Cloud?

Hackathon for Cloud is an Atlassian Connect addon for JIRA which means that you will need to either
have an already existing JIRA Cloud instance or [sign up for one][1].

Then you can install Hackathon for Cloud into your JIRA Cloud instance by [navigating to the
Atlassian Marketplace][2] and clicking the install button and following the instructions thereafter.
For more instructions [read our getting started guide][10].

## How do I use the hackathon plugin as a administrator / facillitator?

A [section of our getting started guide][11] is dedicated to explaining how to setup:

 - Competitors / Spectators
 - Facillitators / Administrators

Please read that for more information.

## How do I create a Hackathon?

First you start with a regular JIRA project with the following features:

 - An [issue type][3] that can represent Hackathon Teams.
 - A [workflow][4] that has will support a hackathon with atleast one round.

From there you can associate that JIRA project as a Hackathon and start using all of the features of
Hackathon for Cloud.

For more instructions [read our getting started guide][10].

## How do I invite the competitors to use JIRA?

Competitors need to be JIRA users, so you should invite them using the [standard
user invite tools provided by Atlassian Cloud][6].

You can either [invite them manually][9], enable [public signup][8] or [enable Google signup][9].

## How can my competitors create their teams?

Your users should navigate from Hackathons => Summary => Create Team.

<img src="/static/images/docs/creating-team.gif" />

To create their Hackathon Teams your competitors will need to have access to JIRA and the ability to
create issues.

## How do I provide custom theming?

Custom theming has not been implemented yet but is on the list of features that we would like to
implement. Please [follow the issue][5] for more details.

## What are all of the features that it gives me?

Letting people vote on issues and showing a leaderboard is tricky but it is relatively easy compared
to the effort that it requires to run a Hackathon. The features for Hackathon for Cloud can be
roughly separated into two categories:

 - Features build for Competitors / Spectators
 - Features built for Hackathon administrator

That said these are the primary features that the addon provides.

 - An easy to use, Mobile ready, voting Page
 - A leaderboard for viewing the voting results complete with a QR Code and Short Url that points to
   the correct voting page. 
 - The ability to vote anonymously and not require a JIRA account.
 - The option to prevent teams from voting on themselves.
 - The power to run a global Hackathon at the same time in the one Hackathon.

## Why can't I remove the last team member from a team? 

Every team needs at-least one team member. The person that creates the hackathon team, by default,
becomes the first team member. If you wish to swap them for somebody else then first add that other
person and then remove the original person.

## Can somebody be a team member of multiple teams?

There is no technical reason that you cannot be a member of multiple teams in a single hackathon.
There are many perfectly valid reasons that you might be on multiple teams so the Hackathon addon
does not place this restriction on you.

 [1]: /redirect/jira-signup
 [2]: /redirect/install
 [3]: https://confluence.atlassian.com/display/JIRACLOUD/What+is+an+Issue#WhatisanIssue-IssueType
 [4]: https://confluence.atlassian.com/display/JIRA/Configuring+Workflow
 [5]: https://ecosystem.atlassian.net/browse/HACK-79
 [6]: https://confluence.atlassian.com/display/Cloud/Manage+users+and+groups
 [7]: https://confluence.atlassian.com/display/Cloud/Add%2C+edit+and+remove+users
 [8]: https://confluence.atlassian.com/display/Cloud/Enable+self+sign+up
 [9]: https://confluence.atlassian.com/display/Cloud/Integrate+with+Google+Apps
 [10]: /docs/setup
 [11]: /docs/setup#using-roles-for-hackathon-permissions
