{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module HostRequests.IssueSearch
    ( searchForAllIssues
    , searchForIssues
    , Page(..)
    , IssueSearchResponse(..)
    , Issue(..)
    , IssueComponent(..)
    , IssueStatus(..)
    ) where

import           AesonHelpers
import           Application
import           Data.Aeson
import           Data.Aeson.Types                  (fieldLabelModifier)
import qualified Data.ByteString.Char8             as B
import qualified Data.JQL                          as J
import           Data.Monoid
import qualified Data.Text                         as T
import           GHC.Generics
import qualified Persistence.HackathonIds          as HI
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Snaplet                      as SS
import           Text.Read                         (readEither)

data Page = Page
    { pageStart  :: Integer
    , pageLength :: Integer
    } deriving (Show)

searchForAllIssues :: AC.Tenant -> J.JQL -> AppHandler (Either AC.ProductErrorResponse [Issue])
searchForAllIssues tenant jql = fmap extractIssues <$> go [] initialPage
    where
        go :: [IssueSearchResponse] -> Page -> AppHandler (Either AC.ProductErrorResponse [IssueSearchResponse])
        go isrs page = do
            potentialResponse <- searchForIssues tenant jql page
            case potentialResponse of
                Left e -> return . Left $ e
                Right isr -> if isLastPage isr
                    then return . Right $ (isr : isrs)
                    else go (isr : isrs) (nextPage isr page)

        isLastPage :: IssueSearchResponse -> Bool
        isLastPage isr = searchedTo isr >= isrTotal isr

        nextPage :: IssueSearchResponse -> Page -> Page
        nextPage isr (Page _ len) = Page (searchedTo isr) len

        searchedTo :: IssueSearchResponse -> Integer
        searchedTo isr = isrStartAt isr + isrMaxResults isr

        initialPage = Page 0 maxPageSize
        maxPageSize = 10000 -- This is just a crazy upper bound that JIRA won't listen to, inspect the response to see the real result

extractIssues :: [IssueSearchResponse] -> [Issue]
extractIssues = concat . fmap isrIssues

searchForIssues :: AC.Tenant -> J.JQL -> Page -> AppHandler (Either AC.ProductErrorResponse IssueSearchResponse)
searchForIssues tenant jql page = do
    potentialDetails <- SS.with connect $ AC.hostPostRequest tenant issueSearchUrl [] (AC.setJson searchRequest)
    case potentialDetails of
        Left e -> return . Left $ e
        Right rawISR -> case fromRawIssueSearchResponse rawISR of
            Left errorMsg -> return . Left $ AC.ProductErrorResponse 500 (T.pack $ "Hackathon parsing error: " ++ errorMsg)
            Right isr -> return . Right $ isr
    where
        searchRequest = IssueSearchRequest
            { reqJql = J.toRawJQL jql
            , reqStartAt = pageStart page
            , reqMaxResults = pageLength page
            , reqFields = ["summary", "project", "components", "labels", "status", "reporter"] -- We decide this because we need to match the json response
            }

issueSearchUrl :: B.ByteString
issueSearchUrl = "/rest/api/2/search"

data IssueSearchRequest = IssueSearchRequest
    { reqJql        :: J.RawJQL
    , reqStartAt    :: Integer
    , reqMaxResults :: Integer
    , reqFields     :: [String]
    } deriving (Show, Generic)

instance ToJSON IssueSearchRequest where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "req" }

fromRawIssueSearchResponse :: RawIssueSearchResponse -> Either String IssueSearchResponse
fromRawIssueSearchResponse risr = do
    issues <- mapM fromRawIssue . rspIssues $ risr
    return IssueSearchResponse
        { isrStartAt = rspStartAt risr
        , isrMaxResults = rspMaxResults risr
        , isrTotal = rspTotal risr
        , isrIssues = issues
        }

fromRawIssue :: RawIssue -> Either String Issue
fromRawIssue ri = do
    iid <- readEither . riId $ ri
    ipId <- readEither . ripId . rifProject . riFields $ ri
    components <- mapM fromRawIssueComponent . rifComponents . riFields $ ri
    status <- fromRawIssueStatus . rifStatus . riFields $ ri
    return Issue
        { issueId = iid
        , issueKey = riKey ri
        , issueProjectId = ipId
        , issueProjectKey = ripKey . rifProject . riFields $ ri
        , issueSummary = rifSummary . riFields $ ri
        , issueComponents = components
        , issueLabels = rifLabels . riFields $ ri
        , issueStatus = status
        , issueReporterKey = rirKey . rifReporter . riFields $ ri
        }

fromRawIssueComponent :: RawIssueComponent -> Either String IssueComponent
fromRawIssueComponent ric = do
    cid <- readEither . ricId $ ric
    return IssueComponent
        { icId = cid
        , icName = ricName ric
        , icDescription = ricDescription ric
        }

fromRawIssueStatus :: RawIssueStatus -> Either String IssueStatus
fromRawIssueStatus ris = do
    sid <- readEither . risId $ ris
    return IssueStatus
        { isId = sid
        , isName = risName ris
        , isDescription = risDescription ris
        }

data IssueSearchResponse = IssueSearchResponse
    { isrStartAt    :: Integer
    , isrMaxResults :: Integer
    , isrTotal      :: Integer
    , isrIssues     :: [Issue]
    } deriving (Show)

data Issue = Issue
    { issueId          :: AC.IssueId
    , issueKey         :: AC.IssueKey
    , issueProjectId   :: AC.ProjectId
    , issueProjectKey  :: AC.ProjectKey
    , issueSummary     :: T.Text
    , issueComponents  :: [IssueComponent]
    , issueLabels      :: [T.Text]
    , issueStatus      :: IssueStatus
    , issueReporterKey :: AC.UserKey
    } deriving (Show)

data IssueComponent = IssueComponent
    { icId          :: HI.ComponentId
    , icName        :: T.Text
    , icDescription :: T.Text
    } deriving (Show)

data IssueStatus = IssueStatus
    { isId          :: Integer
    , isName        :: T.Text
    , isDescription :: T.Text
    } deriving (Show)

data RawIssueSearchResponse = RawIssueSearchResponse
    { rspStartAt    :: Integer
    , rspMaxResults :: Integer
    , rspTotal      :: Integer
    , rspIssues     :: [RawIssue]
    } deriving (Show, Generic)

data RawIssue = RawIssue
    { riId     :: String
    , riKey    :: T.Text
    , riFields :: RawIssueFields
    } deriving (Show, Generic)

data RawIssueFields = RawIssueFields
    { rifSummary    :: T.Text
    , rifProject    :: RawIssueProject
    , rifComponents :: [RawIssueComponent]
    , rifStatus     :: RawIssueStatus
    , rifReporter   :: RawIssueReporter
    , rifLabels     :: [T.Text]
    } deriving (Show, Generic)

data RawIssueProject = RawIssueProject
    { ripId   :: String
    , ripKey  :: T.Text
    , ripName :: T.Text
    } deriving (Show, Generic)

data RawIssueComponent = RawIssueComponent
    { ricId          :: String
    , ricName        :: T.Text
    , ricDescription :: T.Text
    } deriving (Show, Generic)

data RawIssueStatus = RawIssueStatus
    { risId          :: String
    , risName        :: T.Text
    , risDescription :: T.Text
    } deriving (Show, Generic)

data RawIssueReporter = RawIssueReporter
   { rirKey :: T.Text
   } deriving (Show, Generic)

instance FromJSON RawIssueSearchResponse where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rsp" }

instance FromJSON RawIssue where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "ri" }

instance FromJSON RawIssueFields where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rif" }

instance FromJSON RawIssueProject where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rip" }

instance FromJSON RawIssueComponent where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "ric" }

instance FromJSON RawIssueStatus where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "ris" }

instance FromJSON RawIssueReporter where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rir" }
