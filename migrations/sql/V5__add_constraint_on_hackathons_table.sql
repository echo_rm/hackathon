-- We encountered an issue whereby somebody managed to create two hackathons on the one jira project

-- First delete all of the duplicate data from the hackathon table
WITH all_data AS (SELECT count(*) as c, tenant_id, project_id FROM hackathon GROUP BY tenant_id, project_id)
DELETE FROM hackathon WHERE id in (SELECT id FROM hackathon h, all_data a WHERE h.tenant_id = a.tenant_id AND h.project_id = a.project_id AND a.c > 1);

-- Then alter the table to add the constraint
ALTER TABLE hackathon ADD CONSTRAINT one_hackathon_per_project UNIQUE (tenant_id, project_id);
