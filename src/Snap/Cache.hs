{-# LANGUAGE OverloadedStrings #-}
module Snap.Cache
    ( cacheFor
    , oneYear
    , noCache
    ) where

import qualified Data.ByteString.Char8 as BSC
import qualified Data.CaseInsensitive  as CI
import           Data.Time.Units
import qualified Snap.Core             as SC
import qualified Snap.Snaplet          as SS

cacheFor :: TimeUnit t => t -> SS.Handler a b ()
cacheFor time = cache $ "max-age=" `BSC.append` secondsString
    where
        secondsString = BSC.pack . show $ toMicroseconds time `div` 1000000

oneYear :: Day
oneYear = 365

noCache :: SS.Handler a b ()
noCache = cache "no-cache"

cache :: BSC.ByteString -> SS.Handler a b ()
cache v = SC.modifyResponse (SC.setHeader (CI.mk "Cache-Control") v)
