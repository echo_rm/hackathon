<apply template="connect-panel">
    <bind tag="body-class">hackathon-summary</bind>
    <bind tag="header-extra">
        <js src="/static/${resourcesVersion}/js/app/hackathon-summary.js" />
        <script>require(['app/hackathon-summary']);</script>

        <script id="summary-statistics" type="x-tmpl-mustache">
            <h4>Information</h4>
            <table>
               <tbody>
                  <tr>
                     <td>Duration:</td>
                     <td><i>{{formattedStartDate}}</i> &mdash; <i>{{formattedEndDate}}</i></td>
                  </tr>
                  <tr>
                     <td>Competitors:</td>
                     <td><span class="aui-badge">{{competitors}}</span></li></td>
                  </tr>
                  <tr>
                     <td>Forming teams:</td>
                     <td><span class="aui-badge">{{formingTeams}}</span></li></td>
                  </tr>
                  <tr>
                     <td>Presenting teams:</td>
                     <td><span class="aui-badge">{{presentingTeams}}</span></li></td>
                  </tr>
                  <tr>
                     <td>Total votes:</td>
                     <td><span class="aui-badge">{{totalVotes}}</span></li></td>
                  </tr>
               </tbody>
            </table>
        </script>

        <script id="my-team" type="x-tmpl-mustache">
            <li><a href="{{{viewUrl}}}" target="_top">{{key}}: {{fields.summary}}</a> <span class="aui-lozenge {{lozengeClass}}">{{fields.status.name}}</span></li>
        </script>

        <script id="room-summary-round-heading" type="x-tmpl-mustache">
            <tr class="round-heading">
               <td colspan="5">{{name}}</td>
            </tr>
        </script>

        <script id="room-summary-row" type="x-tmpl-mustache">
            <tr> 
                <td headers="room-name"><a target="_blank" href="{{{viewTeamsUrl}}}">{{room.name}}</a></td>
                <!--td headers="round-name"><span class="aui-lozenge aui-lozenge-subtle">{{round.name}}</span></td-->
                <td headers="teams-in-room"><span class="aui-badge">{{numberOfTeams}}</span></td>
                <td headers="votes"><span class="aui-badge">{{numberOfVotes}}</span></td>
                <td headers="status"><span class="aui-lozenge {{statusLozengeClass}}">{{status}}</span></td>
                <td class="action" headers="action">
                    <ul class="menu">
                        {{#showVotingLink}}
                        <li><a target="_blank" href="{{{room.shortUrl}}}">Vote</a></li>
                        {{/showVotingLink}}
                        <li><a target="_blank" href="{{{leaderboardUrl}}}">View leaderboard</a></li>
                    </ul>
                </td>
            </tr>
        </script>
    </bind>


    <h2 id="project-title">Hackathon</h2>
    <form class="aui">
      <div class="field-group">
         <label for="go-create">Create a team</label>
         <button id="go-create" class="aui-button">Create team</button>
      </div>
      <div class="field-group">
         <label for="go-vote">To vote</label>
         <button id="go-vote" class="aui-button">Go vote!</button>
         (<a class="go-vote-link" target="_blank" href="#"><span class="aui-icon aui-icon-small aui-iconfont-link">Permalink</span> share with other voters</a>)
      </div>
      <!--
      <div class="field-group">
         <label for="go-manage">To manage</label>
         <a id="go-manage">View the dashboard</a>
      </div>
      -->
    </form>

    <div id="hackathon-statistics" class="section"></div>

    <div class="active-rooms hidden section">
       <h4>Active rooms</h4>
       <table id="active-room-summary-table" class="aui">
           <thead>
               <tr>
                   <th id="room-name">Round and Room</th>
                   <!--th id="round-name">Round</th-->
                   <th id="teams-in-room">Teams</th>
                   <th id="votes">Votes</th>
                   <th id="status">Status</th>
                   <th id="action">Action</th>
               </tr>
           </thead>
           <tbody>
           </tbody>
       </table>
    </div>

    <div class="your-teams hidden section">
       <h4>Your team</h4>
       <ul id="my-teams"></ul>
    </div>

    <div class="locked-rooms hidden section">
       <h4>Locked rooms</h4>
       <table id="locked-room-summary-table" class="aui">
           <thead>
               <tr>
                   <th id="room-name">Round and Room</th>
                   <!--th id="round-name">Round</th-->
                   <th id="teams-in-room">Teams</th>
                   <th id="votes">Votes</th>
                   <th id="status">Status</th>
                   <th id="action">Action</th>
               </tr>
           </thead>
           <tbody>
           </tbody>
       </table>
    </div>
</apply>
