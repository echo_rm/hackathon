{-# LANGUAGE OverloadedStrings #-}
module TeamLoader
   ( loadTeamsFromJQL
   , loadIssues
   , refreshTeam
   , TeamLoadFailure(..)
   , IssueLoadFailure(..)
   ) where

import           Application
import qualified Control.Arrow                     as A
import           Control.Monad                     (void, when)
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Either
import           Control.Monad.Trans.Maybe
import           Control.Monad.Trans.State
import           Data.Either                       (rights)
import qualified Data.HackathonConstants           as HC
import qualified Data.HTProperties                 as HTP
import qualified Data.IntMap.Strict                as IM
import qualified Data.JQL                          as J
import           Data.List                         (partition)
import           Data.Maybe                        (catMaybes, isJust,
                                                    listToMaybe)
import           Data.MaybeUtil
import qualified Data.Set                          as S
import qualified Data.Text                         as T
import qualified HostRequests.EntityProperties     as EP
import qualified HostRequests.IssueSearch          as IS
import qualified HostRequests.IssueUpdate          as IU
import qualified HostRequests.WatchIssue           as WI
import qualified Persistence.Hackathon             as PH
import qualified Persistence.HackathonRoom         as HRM
import qualified Persistence.HackathonRound        as RND
import qualified Persistence.HackathonTeam         as HT
import qualified Persistence.HackathonTeamMember   as HTM
import qualified Persistence.TenantUser            as TU
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import           Snap.Snaplet.PostgresqlSimple
import qualified TimezoneLabels                    as TL
import qualified UserUtil                          as UU

data TeamLoadFailure = TeamLoadFailure
   { tlfError   :: IssueLoadFailure
   , tlfMessage :: T.Text
   } deriving (Show)


data IssueLoadFailure
   = JQLSearchFailure
   | RoomCreationFailure
   | TeamCreationFailure
   | TeamMemberLookupFailure
   | EntityPropertyUpdateFailure
   | NoSuchHackathon
   deriving (Show, Eq)

refreshTeam :: AC.Tenant -> AC.IssueId -> AppHandler (Either TeamLoadFailure HT.HackathonTeam)
refreshTeam tenant issueId = withPG . runEitherT $ do
   issues <- EitherT (A.left searchFailure <$> IS.searchForAllIssues tenant searchJQL)
   case issues of
      [issue] -> do
         hackathon <- EitherT (m2e noSuchHackathon <$> PH.getHackathonByProjectId tenant (IS.issueProjectId issue))
         potentialTeam <- lift $ HT.getHackathonTeamByIssueId tenant issueId
         case potentialTeam of
           Just team -> do
              lift $ HT.updateTeamIssueSummary tenant issueId (IS.issueSummary issue)
              potentialRound <- lift $ RND.getRoundInProject tenant (PH.hackathonProjectId hackathon) (IS.isId . IS.issueStatus $ issue)
              lift $ HT.updateTeamRound tenant issueId potentialRound
              potentialRoom <- lift $ organiseComponents tenant hackathon issueId (IS.issueComponents issue)
              lift $ HT.updateTeamRoom tenant issueId potentialRoom
              teamMembers <- lift $ TU.getTeamMembersForIssueId tenant issueId
              updatedTeamMembers <- lift $ mapM (UU.updateTenantUserByKey tenant) teamMembers
              lift . runEitherT $ TL.updateTimezoneLabels tenant team (rights updatedTeamMembers)
              lift . mapM_ (WI.watchTeam tenant team) . fmap TU.tuKey $ teamMembers
              EitherT (A.left (const . entityPropertyUpdateFailure $ issueId) <$> EP.updateIssueProperties tenant issueId HTP.issuePropertyKey (HTP.fromUserKeys . fmap TU.tuKey $ teamMembers))
              return team
                 { HT.htIssueSummary = IS.issueSummary issue
                 , HT.htRoundId = fmap RND.hroundId potentialRound
                 , HT.htRoomId = fmap HRM.hrId potentialRoom
                 }
           Nothing -> EitherT $ evalStateT (loadIssueAsTeam tenant hackathon issue) initialCache
      _ -> left $ TeamLoadFailure JQLSearchFailure (T.pack $ "More than one issue was returned and that was not expected: " ++ (show . length $ issues))
   where
      searchJQL = J.Eq "id" (T.pack . show $ issueId)

      searchFailure searchError = errorFromHttpResponse JQLSearchFailure searchError

      noSuchHackathon = TeamLoadFailure NoSuchHackathon "Could not find a hackathon for that team."

      entityPropertyUpdateFailure issueId' = TeamLoadFailure EntityPropertyUpdateFailure (T.pack $ "Failed to update the issue properties on the issue: " ++ show issueId')

organiseComponents :: AC.Tenant -> PH.Hackathon -> AC.IssueId -> [IS.IssueComponent] -> AppHandler (Maybe HRM.HackathonRoom)
organiseComponents tenant hackathon issueId cs = case A.first (catMaybes . fmap convertRoomComponent) . partition isRoomComponent $ cs of
   ([], _) -> return Nothing
   (x : toRemove, toRemain) -> do
      room <- runMaybeT $ HRM.getOrCreateHackathonRoom hackathon (IS.icId x) (IS.icName x)
      when (not . null $ toRemove) $
         void $ IU.updateIssueDetails tenant issueId (issueUpdateFromComponents (x : toRemain))
      return room

issueUpdateFromComponents :: [IS.IssueComponent] -> IU.IssueUpdateRequest
issueUpdateFromComponents components = IU.IssueUpdateRequest $ IU.emptyIssueUpdate
   { IU.iudComponents = Just . fmap IS.icId $ components
   }

isRoomComponent :: IS.IssueComponent -> Bool
isRoomComponent = isJust . convertRoomComponent

convertRoomComponent :: IS.IssueComponent -> Maybe IS.IssueComponent
convertRoomComponent ic = do
   newName <- HC.roomComponentPrefix `T.stripPrefix` IS.icName ic
   return ic { IS.icName = newName }

errorFromHttpResponse :: IssueLoadFailure -> AC.ProductErrorResponse -> TeamLoadFailure
errorFromHttpResponse ilf e = TeamLoadFailure ilf message
   where
      message = "HTTP " `T.append` (T.pack . show . AC.perCode $ e) `T.append` ": " `T.append` (AC.perMessage e)

loadTeamsFromJQL :: AC.Tenant -> PH.Hackathon -> J.JQL -> AppHandler (Either TeamLoadFailure [HT.HackathonTeam])
loadTeamsFromJQL tenant hackathon searchJQL = do
    potentialIssues <- IS.searchForAllIssues tenant searchJQL
    case potentialIssues of
        Left e -> return . Left $ errorFromHttpResponse JQLSearchFailure e
        Right issues -> loadIssues tenant hackathon issues

loadIssues :: AC.Tenant -> PH.Hackathon -> [IS.Issue] -> AppHandler (Either TeamLoadFailure [HT.HackathonTeam])
loadIssues tenant hackathon issues = do
   existingTeams <- HT.getTeamsByIds hackathon (fmap IS.issueId issues)
   let existingIssueIds = S.fromList . fmap HT.htIssueId $ existingTeams
   let unmatchedIssues = filter ((`S.notMember` existingIssueIds) . IS.issueId) issues
   potentialCreatedTeams <- loadIssuesImmediately tenant hackathon unmatchedIssues
   case potentialCreatedTeams of
       Left failure -> return . Left $ failure
       Right createdTeams -> return . Right $ existingTeams ++ createdTeams

loadIssuesImmediately :: AC.Tenant -> PH.Hackathon -> [IS.Issue] -> AppHandler (Either TeamLoadFailure [HT.HackathonTeam])
loadIssuesImmediately tenant hackathon issues = evalStateT (loadIssuesAsTeams tenant hackathon issues) initialCache

loadIssuesAsTeams :: AC.Tenant -> PH.Hackathon -> [IS.Issue] -> StateT CachedLookups AppHandler (Either TeamLoadFailure [HT.HackathonTeam])
loadIssuesAsTeams tenant hackathon issues = sequence <$> mapM (loadIssueAsTeam tenant hackathon) issues

loadIssueAsTeam :: AC.Tenant -> PH.Hackathon -> IS.Issue -> StateT CachedLookups AppHandler (Either TeamLoadFailure HT.HackathonTeam)
loadIssueAsTeam tenant hackathon issue = do
    potentialRoom <- lookupRoom hackathon issue
    case potentialRoom of
        Left failure -> return . Left $ failure
        Right hRoom -> do
            potentialRound <- lookupRound hackathon issue
            potentialTeam <- lift $ HT.createHackathonTeam hackathon (IS.issueId issue) (IS.issueKey issue) (IS.issueSummary issue) potentialRound hRoom
            case potentialTeam of
                Nothing -> return . Left $ teamCreateFailure issue
                Just hTeam -> do
                   potentialTeamMember <- lift $ UU.getOrCreateTenantUserByKey tenant (IS.issueReporterKey issue)
                   case potentialTeamMember of
                      Left _ -> return . Left $ teamMemberLookupFailure issue
                      Right teamMember -> do
                         void . lift $ HTM.addTeamMember teamMember (HT.htId hTeam)
                         void . lift . fmap (A.left (const . teamMemberTimezoneFailure $ issue)) . runEitherT $ TL.updateTimezoneLabels tenant hTeam [teamMember]
                         return . Right $ hTeam
   where
      teamCreateFailure issue = TeamLoadFailure TeamCreationFailure ("Failed to create an internal representation for the team with key: " `T.append` (IS.issueKey issue))

      teamMemberLookupFailure issue = TeamLoadFailure TeamMemberLookupFailure ("Failed to lookup the reporter '" `T.append` (IS.issueReporterKey issue) `T.append` "' on the issue '" `T.append` (IS.issueKey issue) `T.append` "'.")

      teamMemberTimezoneFailure issue = TeamLoadFailure TeamMemberLookupFailure ("Failed to update the timezone labels for '" `T.append` (IS.issueReporterKey issue) `T.append` "' on the issue '" `T.append` (IS.issueKey issue) `T.append` "'.")

lookupRound :: PH.Hackathon -> IS.Issue -> StateT CachedLookups AppHandler (Maybe RND.HackathonRound)
lookupRound hackathon issue = do
   cache <- get
   case IM.lookup s (clRoundByStatus cache) of
      Just hRound -> return hRound
      Nothing -> do
         potentialRound <- lift $ RND.getRoundInHackathon hackathon (IS.isId status)
         put (cache { clRoundByStatus = IM.insert s potentialRound (clRoundByStatus cache) })
         return potentialRound
   where
      s = sid status

      sid :: IS.IssueStatus -> Int
      sid = fromInteger . IS.isId

      status = IS.issueStatus issue

lookupRoom :: PH.Hackathon -> IS.Issue -> StateT CachedLookups AppHandler (Either TeamLoadFailure (Maybe HRM.HackathonRoom))
lookupRoom hackathon issue =
    case roomComponent of
        Nothing -> return . Right $ Nothing
        Just (ic, realName) -> do
            cache <- get
            case IM.lookup (cid ic) (clRoomByComponent cache) of
                Just room -> return . Right . Just $ room
                Nothing -> do
                    potentialRoom <- lift . runMaybeT $ HRM.getOrCreateHackathonRoom hackathon (IS.icId ic) realName
                    case potentialRoom of
                        Nothing -> return . Left $ TeamLoadFailure RoomCreationFailure ("Failed to get or create room with name: " `T.append` realName)
                        Just hRoom -> do
                            put (cache { clRoomByComponent = IM.insert (cid ic) hRoom (clRoomByComponent cache) })
                            return . Right . Just $ hRoom

    where
        cid :: IS.IssueComponent -> Int
        cid = fromInteger . IS.icId

        roomComponent :: Maybe (IS.IssueComponent, T.Text)
        roomComponent = listToMaybe . catMaybes . fmap stripNamePrefix . IS.issueComponents $ issue

        stripNamePrefix :: IS.IssueComponent -> Maybe (IS.IssueComponent, T.Text)
        stripNamePrefix ic = do
            realName <- HC.roomComponentPrefix `T.stripPrefix` IS.icName ic
            return (ic, realName)

initialCache :: CachedLookups
initialCache = CL IM.empty IM.empty

data CachedLookups = CL
    { clRoomByComponent :: IM.IntMap HRM.HackathonRoom
    , clRoundByStatus   :: IM.IntMap (Maybe RND.HackathonRound)
    }
