{-# LANGUAGE OverloadedStrings #-}
module MicrosZone
    ( Zone(..)
    , zoneFromString
    , fromEnv
    , fromEnvDefaultDev
    , modifyDescriptorUsingZone
    ) where

import           Control.Monad           (join)
import qualified Data.Connect.Descriptor as D
import qualified Data.EnvironmentHelpers as DE
import           Data.Maybe              (fromMaybe)
import qualified Data.Text               as T

data Zone = Dev | Dog | Prod
   deriving(Eq, Show, Ord)

zoneFromString :: String -> Maybe Zone
zoneFromString "domain.dev.atlassian.io" = Just Dev
zoneFromString "application.dev.atlassian.io" = Just Dev
zoneFromString "platform.dev.atlassian.io" = Just Dev
zoneFromString "useast.staging.atlassian.io" = Just Dog
zoneFromString "uswest.staging.atlassian.io"  = Just Dog
zoneFromString "useast.atlassian.io" = Just Prod
zoneFromString "uswest.atlassian.io"  = Just Prod
zoneFromString "dev.public.atl-paas.net" = Just Dev
zoneFromString "staging.public.atl-paas.net" = Just Dog
zoneFromString "prod.public.atl-paas.net" = Just Prod
zoneFromString _        = Nothing

fromEnv :: IO (Maybe Zone)
fromEnv = do
   envStr <- DE.getEnv "ZONE"
   return . join $ fmap zoneFromString envStr

fromEnvDefaultDev :: IO Zone
fromEnvDefaultDev = fmap (fromMaybe Dev) fromEnv

modifyDescriptorUsingZone :: Maybe Zone -> D.Plugin -> D.Plugin
modifyDescriptorUsingZone potentialZone descriptor = descriptor
    { D.pluginName = case D.pluginName descriptor of
        Nothing -> Nothing
        (Just (D.Name n)) -> Just . D.Name $ n `T.append` nameKeyAppend potentialZone
    , D.pluginKey = D.PluginKey rawPluginKey
    , D.modules = newModules
    }
    where
       newModules = fmap (\o -> o
          { D.jiraModules = (D.jiraModules o)
             { D.jmWebItems = join newWebItems
             , D.jmWebPanels = join newWebPanels
             }
          }) originalModules
       newWebItems = (fmap (fmap updateWebItem)) <$> originalWebItems
       newWebPanels = (fmap (fmap updateWebPanel)) <$> originalWebPanels
       updateWebItem wi = wi { D.wiUrl = replacePluginKey . D.wiUrl $ wi }
       updateWebPanel wp = wp
          { D.wpUrl = replacePluginKey . D.wpUrl $ wp
          , D.wpLocation = replacePluginKey . D.wpLocation $ wp
          }

       originalWebItems = D.jmWebItems <$> originalJiraModules
       originalWebPanels = D.jmWebPanels <$> originalJiraModules
       originalJiraModules = D.jiraModules <$> originalModules
       originalModules = D.modules descriptor
       replacePluginKey = T.replace "{pluginKey}" rawPluginKey
       rawPluginKey = case D.pluginKey descriptor of (D.PluginKey k) -> k `T.append` zoneKeyAppend potentialZone

nameKeyAppend :: Maybe Zone -> T.Text
nameKeyAppend (Just Prod)   = T.empty
nameKeyAppend (Just zone)   = T.pack $ " (" ++ show zone ++ ")"
nameKeyAppend Nothing       = T.pack " (Local)"

zoneKeyAppend :: Maybe Zone -> T.Text
zoneKeyAppend (Just Prod)   = T.empty
zoneKeyAppend (Just Dog)    = T.pack ".dog"
zoneKeyAppend (Just Dev)    = T.pack ".dev"
zoneKeyAppend Nothing       = T.pack ".local"
