import { PageContext } from './page-context';
import * as hc from './hackathon-client';

export function createConfiguration(pc: PageContext): hc.Configuration {
    return {
        apiKey: pc.acpt,
        basePath: '/rest'
    };
}

export function createHackathonApi(pc: PageContext): hc.HackathonApi {
    return new hc.HackathonApi(createConfiguration(pc));
}

export function createTenantApi(pc: PageContext): hc.TenantApi {
    return new hc.TenantApi(createConfiguration(pc));
}

export function createRoundRoomApi(pc: PageContext): hc.RoundRoomApi {
    return new hc.RoundRoomApi(createConfiguration(pc));
}

export function createTeamApi(pc: PageContext): hc.TeamApi {
    return new hc.TeamApi(createConfiguration(pc));
}