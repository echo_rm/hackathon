#!/bin/bash -e

tempFile=`mktemp /tmp/hackathon.XXXX`
dotFile=`mktemp /tmp/hackathon.XXXX`
outputGraph="foreign-key-graph.png"

bash run-psql.bash > "$tempFile" <<GETFK
SELECT    
   tc.table_name,    
   ccu.table_name AS foreign_table_name
FROM     
   information_schema.table_constraints AS tc     
   JOIN information_schema.key_column_usage AS kcu      
     ON tc.constraint_name = kcu.constraint_name    
   JOIN information_schema.constraint_column_usage AS ccu      
     ON ccu.constraint_name = tc.constraint_name
WHERE constraint_type = 'FOREIGN KEY';
GETFK

echo "digraph hackathon {" > "$dotFile"
grep "|" "$tempFile" | grep -v "foreign_table_name" | sed 's/|/->/' | sed 's/$/;/' >> "$dotFile"
echo "}" >> "$dotFile"

dot -Tpng "$dotFile" -o "$outputGraph"
