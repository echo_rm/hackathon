import * as URI from 'urijs';

export interface UserDetails {
    timeZone: string;
    emailAddress?: string;
    displayName: string;
    name: string;
    key: string;
    accountId: string;
    active: boolean;
}

export function requestUserDetails(userKey: string): Promise<UserDetails> {
    const url = URI('/rest/api/2/user').addSearch('key', userKey);
    return AP.request({
        url: url.toString(),
        type: 'GET'
    }).then(rsp => {
        const parsedBody = JSON.parse(rsp.body);
        return parsedBody as UserDetails;
    });
}

export interface IssueDetails {
    id: string;
    key: string;
    fields: IssueFieldDetails;
}

export interface IssueFieldDetails {
    summary: string;
    status: IssueFieldStatus;
}

export interface IssueFieldStatus {
    name: string;
    statusCategory: IssueFieldStatusCategory;
}

export interface IssueFieldStatusCategory {
    colorName: string;
    name: string;
}

export function requestIssueDetails(issueKey: string): Promise<IssueDetails> {
    return AP.request({
        url: `/rest/api/2/issue/${issueKey}`,
        type: 'GET'
    }).then(rsp => {
        return JSON.parse(rsp.body) as IssueDetails;
    });
}