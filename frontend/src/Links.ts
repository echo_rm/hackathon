import * as URI from 'urijs';

export type AddonContext = {
  productBaseUrl: string;
  pluginKey: string;
};

export type HackathonContext = {
  projectId: number;
  projectKey: string;
};

export type RoomContext = {
  name: string;
};

export type RoundContext = {
  name: string;
};

function safePath(uri: uri.URI): string {
  const origPath = uri.path();
  return origPath === '/' ? '' : origPath;
}

export function linkToDashboard(addonContext: AddonContext, hackathon: HackathonContext): string {
  const newUri = new URI(addonContext.productBaseUrl);
  newUri.path(`${safePath(newUri)}/plugins/servlet/ac/${addonContext.pluginKey}/hackathon-dashboard`);
  newUri.addQuery('project.id', hackathon.projectId);
  newUri.addQuery('project.key', hackathon.projectKey);
  return newUri.toString();
}

export function linkToSummary(addonContext: AddonContext, hackathon: HackathonContext): string {
  var newUri = new URI(addonContext.productBaseUrl);
  newUri.path(`${safePath(newUri)}/projects/${hackathon.projectKey}`);
  newUri.addQuery('selectedItem', `${addonContext.pluginKey}__hackathon-summary-web-item`);
  return newUri.toString();
}

export function linkToViewTeams(
  productBaseUrl: string,
  hackathon: HackathonContext,
  round: RoundContext,
  room: RoomContext): string {
  // var projectIdentifier = hackathon.projectName || hackathon.projectKey;
  var projectIdentifier = hackathon.projectKey;
  var jql = `project = "${projectIdentifier}" AND status = "${round.name}" AND component = "Room - ${room.name}"`;

  var newUri = URI(productBaseUrl);
  newUri.path(`${safePath(newUri)}/issues/`);
  newUri.addQuery('jql', jql);

  return newUri.toString();
}

export function linkToLeaderboard(
  tenantKey: string,
  hackathon: HackathonContext,
  roundId: number,
  roomId: number,
  acpt: string) {
  var newUri = URI('/page/hackathon-leaderboard');
  newUri.addQuery('tenant_key', tenantKey);
  newUri.addQuery('project_id', hackathon.projectId);
  newUri.addQuery('project_key', hackathon.projectKey);
  newUri.addQuery('room_id', roomId);
  newUri.addQuery('round_id', roundId);
  newUri.addQuery('acpt', acpt);
  return newUri.toString();
}