define([
   '../lib/URI', 
   '../lib/js.cookie', 
   'underscore', 
   '../helpers/MustacheLoader',
   '../helpers/Handler',
   '../lib/qrcode',
   '../helpers/Links'
],
function(URI, Cookie, _, MustacheLoader, H, qrcode, Links) {
    "use strict";

    // parse the page context
    var queryParams = URI(window.location.href).query(true);
    var pageContext = {
        tenantKey: queryParams['tenant_key'],
        projectId: parseInt(queryParams['project_id']),
        projectKey: queryParams['project_key'],
        roundId: parseInt(queryParams['round_id']),
        roomId: parseInt(queryParams['room_id']),
        acptToken: queryParams['acpt']
    };

    // Remote acpt token from the query params in the url
    delete queryParams['acpt'];
    var newUri = URI(window.location.href).query(queryParams);
    window.history.pushState('',"Hackathon Leaderboard",newUri);

    if(!pageContext.tenantKey || !pageContext.projectId || !pageContext.roundId || !pageContext.roomId) {
        throw "We are missing some of the crucial details for this hackathon.";
    }

    var getDescriptor = function() {
        return AJS.$.ajax({
            url: "/connect/atlassian-connect.json",
            type: "GET",
            cache: false
        });
    };

    var getTenantBaseUrl = function (tenantKey) {
        return AJS.$.ajax({
            url: "/rest/tenant/base-url",
            type: "GET",
            cache: true, // This will never change
            data: {
                tenantKey: tenantKey
            }
        });
    };

    var requestHackathonDetails = function(voteableUser, projectId) {
        return AJS.$.ajax({
            url: "/rest/hackathon",
            type: "POST",
            cache: false,
            data: JSON.stringify({
                token: voteableUser,
                projectId: projectId
            })
        });
    };

    var getRoundRoomDetails = function(context) {
        return AJS.$.ajax({
            url: "/rest/round-room",
            type: "POST",
            cache: false,
            data: JSON.stringify({
                hackathon: {
                    token: {
                        acptToken: context.acptToken
                    },
                    projectId: context.projectId
                },
                roundId: context.roundId,
                roomId: context.roomId
            })
        });
    };

    var toggleVotingLock = function(context, lockVoting) {
        return AJS.$.ajax({
            url: "/rest/hackathon/round-room/status",
            type: "PUT",
            cache: false,
            headers: {
                "X-acpt": context.acptToken
            },
            data: JSON.stringify({
                projectId: context.projectId,
                roundRoomIds: [{
                    roundId: context.roundId,
                    roomId: context.roomId
                }],
                newStatus: lockVoting ? "locked" : "active"
            })
        });
    };

    var getLeaderboardResults = function(context) {
        return AJS.$.ajax({
            url: "/rest/hackathon/leaderboard",
            type: "POST",
            cache: false,
            data: JSON.stringify({
                hackathon: {
                    token: {
                        acptToken: context.acptToken
                    },
                    projectId: context.projectId
                },
                roundId: context.roundId,
                roomId: context.roomId
            })
        });
    };

    var getVotesPlaced = function(context) {
        return AJS.$.ajax({
            url: "/rest/hackathon/leaderboard/votes",
            type: "GET",
            cache: false,
            data: {
                roundId: context.roundId,
                roomId: context.roomId
            }
        });
    };

    var isInactive = function(status) {
        return status === "Inactive";
    };

    var isLocked = function(status) {
        return status === "Locked";
    };

    AJS.$(function() {
        var voteCountRefresher = function(container) {
            var refreshTimerId;
            var pageHasFocus = true;
            var polling = false;

            AJS.$(window).focus(function() {
               pageHasFocus = true;
               reload();
            });

            AJS.$(window).blur(function() {
               pageHasFocus = false;
            });

            var reload = function() {
               if(pageHasFocus && polling) {
                  getVotesPlaced(pageContext).done(function(data) {
                     container.html(templates.render('vote-count-message', data));
                     AJS.$(".lightbox .vote-count").text(data.votes + " votes");
                  });
               }
            };

            var start = function() {
                polling = true;
                reload();
                refreshTimerId = setInterval(reload, 4000);
            };

            var stop = function() {
                polling = false;
                if(refreshTimerId) {
                    clearInterval(refreshTimerId);
                    refreshTimerId = null;
                }
            };

            return {
                start: start,
                stop: stop
            }
        };

        var voteRefresher = voteCountRefresher(AJS.$("#status-box"));

        var templates = MustacheLoader.load();

        var lockButton = AJS.$("#lock-toggle-button");
        var resetRevealButton = AJS.$("#reset-reveal-button");

        var toggleLockButton = function(locked) {
            var lockIcon = lockButton.find('.lock-icon');
            lockIcon.toggleClass("aui-iconfont-locked", !locked);
            lockIcon.toggleClass("aui-iconfont-unlocked", locked);
            var lockText = lockButton.find('.description-text');
            lockText.text(locked ? "Unlock voting" : "Lock voting");
        };

        var updateResetRevealButton = function(stage) {
           resetRevealButton.toggleClass("hidden", stage <= revealStages.votingLocked);
        };

        var revealStages = {
            votingUnlocked: 0,
            votingLocked: 1,
            votingRevealThird: 2,
            votingRevealSecond: 3,
            votingRevealAll: 4
        };

        var revealStage;

        var setStageUnlocked = function() {
            revealStage = revealStages.votingUnlocked;
            showStage(revealStage);
        };

        var setStageLocked = function() {
            revealStage = revealStages.votingLocked;
            showStage(revealStage);
        };

        var nextStage = function() {
            if(revealStage >= revealStages.votingLocked) {
                revealStage++;
                if(revealStage > revealStages.votingRevealAll) {
                   revealStage = revealStages.votingRevealAll;
                }
                if(revealStage === revealStages.votingRevealThird && AJS.$("#teams li.third").length === 0) {
                   revealStage++;
                }
                if(revealStage === revealStages.votingRevealSecond && AJS.$("#teams li.second").length === 0) {
                   revealStage++;
                }
                showStage(revealStage);
            }
        };

        var showAll = function() {
            if(revealStage >= revealStages.votingLocked) {
                revealStage = revealStages.votingRevealAll;
                showStage(revealStage);
            }
        };

        var showStage = function(stage) {
            var thirdPlaces = AJS.$("#teams li.third");
            var secondPlaces = AJS.$("#teams li.second");
            var firstPlaces = AJS.$("#teams li.first");
            var allResults = AJS.$("#teams li");
            var statusBox = AJS.$("#status-box");
            var lockedIcon = AJS.$("#locked-icon");

            updateResetRevealButton(stage);
            lockedIcon.toggleClass("hidden", stage < revealStages.votingLocked);
            var qrcodeContainer = document.getElementById('voting-qr-code');
            if(stage < revealStages.votingLocked) {
                voteRefresher.start();

                rrdRequest.done(function(data) {
                   AJS.$(".vote-link").text(data.room.shortUrl);

                   AJS.$(".qr-container").removeClass("hidden");
                   AJS.$(qrcodeContainer).empty();
                   var qrcode = new QRCode(qrcodeContainer, {
                      text: data.room.shortUrl,
                      width: 256,
                      height: 256,
                      colorDark : "#000000",
                      colorLight : "#ffffff",
                      correctLevel : QRCode.CorrectLevel.H
                   });

                   new QRCode(AJS.$(".lightbox .image").get(0), {
                      text: data.room.shortUrl,
                      width: 1024,
                      height: 1024,
                      colorDark : "#000000",
                      colorLight : "#ffffff",
                      correctLevel : QRCode.CorrectLevel.H
                   });
                });
            } else {
                AJS.$(".qr-container").addClass("hidden");
                AJS.$(qrcodeContainer).empty();
                voteRefresher.stop();
            }

            var isTieOnThird = stage <= revealStages.votingLocked && thirdPlaces.length > 1;
            var isTieOnSecond = stage <= revealStages.votingRevealThird && secondPlaces.length > 1 && !isTieOnThird;
            var twoWayTieFirst = firstPlaces.length == 2;
            var threeWayTieFirst = firstPlaces.length >= 3;
            var isTieOnFirst = stage <= revealStages.votingRevealSecond && (threeWayTieFirst || (stage >= revealStages.votingRevealThird && twoWayTieFirst)) && !isTieOnThird && !isTieOnSecond;

            var ties = {
               isTieOnThird: isTieOnThird,
               isTieOnSecond: isTieOnSecond,
               isTieOnFirst: isTieOnFirst
            };

            switch(stage) {
                case revealStages.votingUnlocked:
                    allResults.addClass("hidden");
                    break;

                case revealStages.votingLocked:
                    allResults.addClass("hidden");
                    statusBox.html(templates.render("locked-message", ties));
                    break;

                case revealStages.votingRevealThird:
                    thirdPlaces.removeClass("hidden");
                    statusBox.html(templates.render("congratulations-third", ties));
                    break;

                case revealStages.votingRevealSecond:
                    secondPlaces.removeClass("hidden");
                    statusBox.html(templates.render("congratulations-second", ties));
                    break;

                case revealStages.votingRevealAll:
                    allResults.removeClass("hidden");
                    statusBox.html(templates.render("congratulations-all"));
                    break;

                default:
                    AJS.log("This is not a valid reveal state anymore.");
                    break;
            }
        };

         var loadLeaderboardResults = function(showAllOnLoad) {
             setStageLocked();
             var loadingResults = getLeaderboardResults(pageContext);
             
             AJS.$.when(loadingResults, baseUrlRequest).then(function(lr, bur) {
                 var loadedResults = lr[0];
                 var baseUrl = bur[0].baseUrl;

                 // Sort the teams in descending order
                 loadedResults.teams = loadedResults.teams.sort(function(a, b) {
                     return b.votes - a.votes;
                 });

                 // Group the teams by number of votes
                 var groupedTeams = groupBy(loadedResults.teams, function(team) {
                     return team.votes;
                 });
                 AJS.log(groupedTeams);

                 var teamsContainer = AJS.$("#teams");
                 teamsContainer.empty();
                 var teamsConsumed = 0;
                 // For each of the teams
                 _.each(groupedTeams, function(teamGroup, groupIndex) {
                     var groupClass = getClassForPosition(teamsConsumed);
                     teamsConsumed += teamGroup.length;
                     AJS.log("Group class: " + groupClass + " " + groupIndex);
                     _.each(teamGroup, function(team) {
                         // Render this team
                         team.teamRank = groupClass;
                         team.issueLink = baseUrl + "/browse/" + team.issueKey;
                         teamsContainer.append(templates.render("team-row", team));
                     });
                 });

                 if(showAllOnLoad) {
                     showAll();
                 }
             });
         };

        var rrdRequest = getRoundRoomDetails(pageContext);

        rrdRequest.done(function(data) {
             AJS.$(".title span").text(data.round.name + ": " + data.room.name);
             // Put the lock button in the correct state
             lockButton.data('status', data.status);
             var inactive = isInactive(data.status);
             var locked = isLocked(data.status);

             // If the room is inactive then we should just do nothing. People should not be
             // navigating to the leaderboard of an inactive room though
             if(inactive) {
                 lockButton.attr('aria-disabled', 'true');
             } else {
                 toggleLockButton(locked);
                 if(locked) {
                     loadLeaderboardResults(true);
                 }
             }
        });


        rrdRequest.fail(function() {
           lockButton.addClass("hidden");
        });

        var baseUrlRequest = getTenantBaseUrl(pageContext.tenantKey);

        // By default it is unlocked
        setStageUnlocked();

        baseUrlRequest.done(function(data) {
            var baseUrl = data.baseUrl;

            if(!pageContext.acptToken) {
               getDescriptor().done(function(descriptor) {
                  var addonContext = {
                     productBaseUrl: baseUrl,
                     pluginKey: descriptor.key
                  };

                  var hackathon = {
                     projectKey: pageContext.projectKey
                  };

                  window.location.href = Links.summary(addonContext, hackathon);
               });
            } else {
               var voteableUser = { acptToken: pageContext.acptToken };
               requestHackathonDetails(voteableUser, pageContext.projectId).done(function(data) {
                  AJS.$(".subtitle h2").text(data.projectName);
               });

               lockButton.click(H.h(function(e) {
                   var status = lockButton.data('status');

                   if(isInactive(status)) {
                       // Do nothing on the inactive state
                       lockButton.attr('aria-disabled', 'true');
                   } else {
                       var locked = isLocked(status);
                       var toggleRequest = toggleVotingLock(pageContext, !locked);

                       toggleRequest.done(function() {
                           lockButton.data('status', locked ? "Active" : "Locked");
                           toggleLockButton(!locked);

                           var isNowLocked = !locked;
                           if(isNowLocked) {
                               loadLeaderboardResults();
                           } else {
                               setStageUnlocked();
                           }
                       });

                       toggleRequest.fail(function() {
                           AJS.$("#notProjectAdminError").removeClass("hidden");
                       });
                   }
               }));
            }
        });

        baseUrlRequest.fail(function() {
            // TODO if we fail this then the tenant key is wrong. Just show an error message and give up
        });

        resetRevealButton.click(H.h(function() {
           setStageLocked();
        }));

        AJS.$(document).keydown(function(e) {
            switch (e.which) {
                case 13:
                case 32:
                    e.preventDefault();
                    AJS.log("Pressed button to reveal a result.");
                    nextStage();
                    break;

                case 115:
                case 83:
                    AJS.log("Pressed a button to reveal all results.");
                    showAll();
                    break;

                default:
                    AJS.log("The user pressed a key but it was not a supported show key: " + e.which);
                    break;
            }
        });

        AJS.$(".lightbox .close").click(function() {
           AJS.$(".lightbox").addClass("hidden");
        });

        AJS.$("#voting-qr-code").click(function() {
           AJS.$(".lightbox").removeClass("hidden");
        });
    });

    var getClassForPosition = function(position) {
        var classes = ["first", "second", "third"];
        return classes[position] || "did-not-place";
    };

    // This method takes in an ordered list of elements and groups them by equivalence
    var groupBy = function(elements, grabber) {
        var elementsClone = elements.slice();
        var results = [];
        var lastElement;
        if(elementsClone.length > 0) {
            var nextElement = elementsClone.shift();
            lastElement = grabber(nextElement);
            var accum = [nextElement];

            while(elementsClone.length > 0) {
                nextElement = elementsClone.shift();
                var toCompare = grabber(nextElement);
                if(lastElement === toCompare) {
                    accum.push(nextElement);
                } else {
                    lastElement = toCompare;
                    results.push(accum);
                    accum = [nextElement];
                }
            }

            if(accum.length > 0) {
                results.push(accum);
            }
        }
        return results;
    };
});
