<apply template="connect-panel">
   <bind tag="body-class">hackathon-team-panel</bind>
   <bind tag="header-extra">
      <js src="/static/${resourcesVersion}/js/app/hackathon-team-panel.js" />
      <script>require(['app/hackathon-team-panel']);</script>

      <script id="avatar" type="x-tmpl-mustache">
         <span class="aui-avatar aui-avatar-xsmall">
             <span class="aui-avatar-inner">
                 <img src="{{{avatarRef}}}"></img>
             </span>
         </span>
      </script>

      <script id="team-member-icon" type="x-tmpl-mustache">
          <span class="aui-avatar aui-avatar-small">
              <span class="aui-avatar-inner"><img src="{{{avatar}}}"></img></span>
          </span>
      </script>

      <script id="team-member-view" type="x-tmpl-mustache">
          <tr>
            <td class="team-member-name-block">
                <span class="aui-avatar aui-avatar-small">
                    <span class="aui-avatar-inner"><img src="{{{smallAvatarUrl}}}" /></span>
                </span>
                <span class="team-member-name" data-user-key="{{key}}">{{displayName}}</span>
            </td>
            <td>
                <span
                    class="aui-icon aui-icon-small aui-iconfont-list-remove remove-team-member {{#hideRemoveButton}}hidden{{/hideRemoveButton}}"
                    data-user-key="{{key}}"></span>
            </td>
          </tr>
      </script>

      <script id="past-results-view" type="x-tmpl-mustache">
        <label>Past results</label>
        <table width="100%">
            <thead>
                <th>Round</th>
                <th>Votes</th>
                <th>Rank</th>
            </thead>
            <tbody>
                {{#pastResults}}
                <tr>
                    <td>{{roundName}}</td>
                    <td><aui-badge>{{votes}}</aui-badge></td>
                    <td><span class="aui-lozenge {{rankLozengeClass}}">#{{rank}}</span></td>
                </tr>
                {{/pastResults}}
            </tbody>
        </table>
      </script>
   </bind>

    <button class="aui-button aui-button-subtle refresh-button"><span class="aui-icon aui-icon-small
    aui-iconfont-refresh-small">Refresh</span></button>
    <form class="aui top-label">
      <fieldset>
        <div class="field-group top-label">
            <label id="team-members-label" for="show-add-team-member">Team members</label>
            <span id="team-member-count" class="aui-lozenge">?</span>
            <table>
                <tbody class="team-members-container"></tbody>
            </table>
             <input
                 class="text large-field"
                 type="text"
                 id="team-member-selector"
                 name="team-member-selector"
                 title="team-member" />
        </div>
        <div class="field-group top-label">
            <label>Round</label>
            <span id="round-lozenge" class="aui-lozenge aui-lozenge-subtle hackathon-info-lozenge"></span>
        </div>
        <div class="field-group top-label">
            <label>Room</label>
            <span id="room-lozenge" class="aui-lozenge aui-lozenge-subtle hackathon-info-lozenge"></span>
        </div>
        <div class="field-group top-label past-results hidden"></div>
      </fieldset>
    </form>

   <div id="error-message" class="aui-message aui-message-warning hidden">
       <p class="title">
           <span class="aui-icon icon-error"></span>
           <strong class="title">Loading error message...</strong>
       </p>
   </div>
</apply>
