{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Persistence.Hackathon
    ( Hackathon(..)
    , VotingRestriction(..)
    , HackathonVotingRules(..)
    , hackathonVotingRules
    , hackathonSameRoomVoting
    , getHackathonByProjectId
    , getAllHackathons
    , createHackathon
    , updateHackathonVotingRestriction
    , updateHackathonVotingRules
    , updateHackathonShortUrl
    , purgeHackathonById
    ) where

import           Application
import           Control.Monad                      (join)
import           Data.HackathonConstants
import           Data.Int                           (Int64)
import           Data.Maybe                         (listToMaybe)
import qualified Data.Text                          as T
import qualified Data.Time.Clock                    as C
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.SqlQQ
import           Database.PostgreSQL.Simple.ToField
import           Database.PostgreSQL.Simple.ToRow
import           Network.URI                        (URI (..))
import qualified Persistence.HackathonIds           as HI
import qualified Snap.AtlassianConnect              as AC
import           Snap.Snaplet.PostgresqlSimple

data Hackathon = Hackathon
    { hackathonId                 :: HI.HackathonId
    , hackathonTenantId           :: HI.TenantId
    , hackathonProjectId          :: AC.ProjectId
    , hackathonProjectKey         :: AC.ProjectKey
    , hackathonProjectName        :: T.Text
    , hackathonTeamIssueTypeId    :: Integer
    , hackathonActivatorId        :: Integer
    , hackathonStartDate          :: C.UTCTime
    , hackathonEndDate            :: C.UTCTime
    , hackathonVotingRestriction  :: VotingRestriction
    , hackathonMultipleRoomVoting :: Bool
    , hackathonShortUrl           :: Maybe String
    } deriving (Show)

instance FromRow Hackathon where
    fromRow = Hackathon <$> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field

instance ToRow Hackathon where
    toRow h =
       [ toField . hackathonTenantId $ h
       , toField . hackathonProjectId $ h
       , toField . hackathonProjectKey $ h
       , toField . hackathonProjectName $ h
       , toField . hackathonTeamIssueTypeId $ h
       , toField . hackathonActivatorId $ h
       , toField . hackathonStartDate $ h
       , toField . hackathonEndDate $ h
       , toField . hackathonVotingRestriction $ h
       , toField . hackathonMultipleRoomVoting $ h
       , toField . hackathonShortUrl $ h
       ]

data HackathonVotingRules = HackathonVotingRules
    { hackathonVotingRuleMultipleRoom :: Bool
    } deriving (Show)

hackathonVotingRules :: Hackathon -> HackathonVotingRules
hackathonVotingRules = HackathonVotingRules . hackathonMultipleRoomVoting

hackathonSameRoomVoting :: Hackathon -> Bool
hackathonSameRoomVoting = not . hackathonMultipleRoomVoting

getHackathonByProjectId :: AC.Tenant -> AC.ProjectId -> AppHandler (Maybe Hackathon)
getHackathonByProjectId tenant projectId = listToMaybe <$> query
   [sql|
        SELECT id, tenant_id, project_id, project_key, project_name, hackathon_team_issue_type_id, activator, start_date, end_date, voting_restriction, multiple_room_voting, short_url
        FROM hackathon
        WHERE tenant_id = ? AND project_id = ?
   |] (AC.tenantId tenant, projectId)

getAllHackathons :: AC.Tenant -> AppHandler [Hackathon]
getAllHackathons tenant = query
   [sql|
      SELECT id, tenant_id, project_id, project_key, project_name, hackathon_team_issue_type_id, activator, start_date, end_date, voting_restriction, multiple_room_voting, short_url
      FROM hackathon
      WHERE tenant_id = ?
   |] (Only $ AC.tenantId tenant)

createHackathon :: Hackathon -> AppHandler (Maybe HI.HackathonId)
createHackathon h = do
    sid <- query
        [sql|
          INSERT INTO hackathon (tenant_id, project_id, project_key, project_name, hackathon_team_issue_type_id, activator, start_date, end_date, voting_restriction, multiple_room_voting, short_url, created)
          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now()) RETURNING id
        |] h
    return . listToMaybe $ join sid

updateHackathonVotingRestriction :: AC.Tenant -> AC.ProjectId -> VotingRestriction -> AppHandler Int64
updateHackathonVotingRestriction tenant projectId restriction = execute
    [sql|
        UPDATE hackathon
        SET voting_restriction = ?
        WHERE tenant_id = ? AND project_id = ?
    |] (restriction, AC.tenantId tenant, projectId)

updateHackathonVotingRules :: AC.Tenant -> AC.ProjectId -> HackathonVotingRules -> AppHandler Int64
updateHackathonVotingRules tenant projectId (HackathonVotingRules multiRoom) = execute
    [sql|
        UPDATE hackathon
        SET multiple_room_voting = ?
        WHERE tenant_id = ? AND project_id = ?
    |] (multiRoom, AC.tenantId tenant, projectId)

updateHackathonShortUrl :: Hackathon -> URI -> AppHandler Int64
updateHackathonShortUrl h shortUri = execute
   [sql|
      UPDATE hackathon
      SET short_url = ?
      WHERE id = ?
   |] (show shortUri, hackathonId h)

purgeHackathonById :: AC.Tenant -> AC.ProjectId -> AppHandler Int64
purgeHackathonById tenant projectId = execute
   [sql|
      DELETE FROM hackathon WHERE tenant_id = ? AND project_id = ?
   |] (AC.tenantId tenant, projectId)
