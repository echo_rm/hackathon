{-# LANGUAGE OverloadedStrings #-}
module HandlerHelpers
    ( getHackathonFromProjectId
    , requirePermissions
    , requirePermissionsE
    , withErr
    , withErrCode
    , withErrs
    , writeError
    , writeErrors
    , pe
    ) where

import           Application
import qualified Control.Arrow            as A
import           Data.String.ToString
import qualified HostRequests.UserDetails as HU
import qualified Persistence.Hackathon    as PH
import qualified Snap.AtlassianConnect    as AC
import           Snap.Helpers

getHackathonFromProjectId :: AC.Tenant -> AppHandler (Either (Int, String) PH.Hackathon)
getHackathonFromProjectId tenant = do
    potentialProjectId <- getIntegerQueryParam "projectId"
    case potentialProjectId of
        Nothing -> return . Left $ (badRequest, "You need to provide a projectId.")
        Just projectId -> do
            potentialHackathon <- PH.getHackathonByProjectId tenant projectId
            case potentialHackathon of
                Nothing -> return . Left $ (badRequest, "There is no hackathon with the provided projectId.")
                Just hackathon -> return . Right $ hackathon

requirePermissions
    :: [HU.JIRAPermission]
    -> AC.Tenant
    -> AC.UserKey
    -> PH.Hackathon
    -> AppHandler ()
    -> AppHandler ()
requirePermissions permissions tenant userKey hackathon handler = do
    potentialUsers <- requirePermissionsE permissions tenant userKey hackathon
    case potentialUsers of
        (Left err) -> uncurry respondWithError err
        (Right _) -> handler

requirePermissionsE
    :: [HU.JIRAPermission]
    -> AC.Tenant
    -> AC.UserKey
    -> PH.Hackathon
    -> AppHandler (Either (Int, String) ())
requirePermissionsE permissions tenant userKey hackathon = do
    potentialUsers <- HU.usersWithPermissions tenant (Just userKey) permissions (HU.PK . PH.hackathonProjectKey $ hackathon)
    case potentialUsers of
        (Left err) -> return . Left $ (forbidden, "Failed to make the request to the host server.")
        (Right []) -> return . Left $ (forbidden, "You do not have the required permissions to make this request against this hackathon.")
        (Right _) -> return . Right $ ()

-- pe stands for "pluralize errors"
pe :: Either (Int, String) b -> Either (Int, [String]) b
pe = A.left (A.second return)

-- TODO the names for these error methods are a mess, rename them to be sensibly named
withErr :: ToString a => Int -> String -> Either a b -> Either (Int, String) b
withErr code prefix = A.left (withErrHelper code prefix)

withErrHelper :: ToString a => Int -> String -> a -> (Int, String)
withErrHelper code prefix msg = (code, prefix ++ " " ++ toString msg)

withErrCode :: ToString a => Int -> Either a b -> Either (Int, String) b
withErrCode code (Left msg) = Left (code, toString msg)
withErrCode _    (Right x)  = Right x

withErrs :: Int -> Either a b -> Either (Int, a) b
withErrs code = A.left ((,) code)

writeError :: AppHandler (Either (Int, String) b) -> AppHandler ()
writeError m = do
   res <- m
   case res of
      Right _ -> return ()
      Left err -> uncurry respondWithError err

writeErrors :: AppHandler (Either (Int, [String]) b) -> AppHandler ()
writeErrors m = do
   res <- m
   case res of
     Right _ -> return ()
     Left err -> uncurry respondWithErrors err
