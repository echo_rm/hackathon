{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module RoundRoomHandlers
    ( handleRoundRoomDetails
    , handleRoundRoomCreate
    , handleRoundRoomUnallocated
    , handleRoundRoomsDetails
    , handleCurrentRoundRoomDetails
    , handleRoundRoomUpdateWinners
    , handleRoundRoomUpdateMaxTeams
    ) where

import           AesonHelpers
import           Application
import           Control.Monad                  (forM, void)
import           Control.Monad.Trans.Class      (lift)
import           Control.Monad.Trans.Either
import           Control.Monad.Trans.Maybe
import           Data.Aeson
import           Data.Aeson.Types               (fieldLabelModifier)
import qualified Data.HackathonRequest          as HR
import           Data.Maybe                     (catMaybes)
import           Data.MaybeUtil
import qualified Data.Text                      as T
import           GHC.Generics
import           HandlerHelpers
import qualified HostRequests.UserDetails       as UD
import qualified Persistence.Hackathon          as PH
import qualified Persistence.HackathonIds       as HI
import qualified Persistence.HackathonRoom      as HRM
import qualified Persistence.HackathonRound     as RND
import qualified Persistence.HackathonRoundRoom as HRR
import qualified Persistence.HackathonTeam      as HT
import           ShortenHelper
import qualified Snap.AtlassianConnect          as AC
import qualified Snap.Core                      as SC
import           Snap.Helpers
import           Snap.Snaplet.PostgresqlSimple
import           TenantWithVoteableUser
import qualified WithToken                      as WT

handleRoundRoomDetails :: AppHandler ()
handleRoundRoomDetails = handleMethods
    [ (SC.POST, getRoundRoomDetails)
    ]

getRoundRoomDetails :: AppHandler ()
getRoundRoomDetails = do
    request <- SC.readRequestBody size10KB
    let potentialRequest = eitherDecode request :: Either String GetRoundRoomDetailsRequest
    case potentialRequest of
        Left errMsg -> respondWithError badRequest $ "Could not parse the request: " ++ errMsg
        Right details -> tenantWithVoteableUserFromHackathonRequest (grrdHackathon details) (getRoundRoomDetailsHelper details)

getRoundRoomDetailsHelper :: GetRoundRoomDetailsRequest -> AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
getRoundRoomDetailsHelper _ _ _ Nothing = respondWithError unauthorised "You need to be logged in to check the status of this round-room."
getRoundRoomDetailsHelper details tenant hackathon (Just voteableUser) = withPG $ do
    potentialData <- runMaybeT $ do
        hRound <- MaybeT $ RND.getHackathonRoundById hackathon (grrdRoundId details)
        hRoom <- MaybeT $ HRM.getRoomById hackathon (grrdRoomId details)
        hRoundRoom <- MaybeT $ HRR.getRoundRoom hRound hRoom
        return (hRound, hRoom, hRoundRoom)
    case potentialData of
        Nothing -> respondWithError notFound "Could not find a configuration for that round and room."
        Just (hRound, hRoom, hRoundRoom) -> do
            pRoomWithUrl <- ensureRoomWithShortUrl tenant hackathon hRoom
            case pRoomWithUrl of
              Left x -> respondWithError internalServer "Could not generate the short url for this room"
              Right roomWithUrl -> writeJson RoundRoomDetails
                  { rrdRound = RoundDetails
                      { rndId = RND.hroundId hRound
                      , rndName = RND.hroundName hRound
                      , rndSequenceNumber = RND.hroundSequenceNumber hRound
                      }
                  , rrdRoom = toRoomDetails roomWithUrl
                  , rrdStatus = T.pack . show . HRR.hrrStatus $ hRoundRoom
                  , rrdNumberOfTeams = Nothing
                  , rrdMaximumTeams = HRR.hrrMaximumTeams hRoundRoom
                  , rrdWinners = HRR.hrrWinners hRoundRoom
                  }

data GetRoundRoomDetailsRequest = GetRoundRoomDetailsRequest
    { grrdHackathon :: HR.HackathonRequest
    , grrdRoundId   :: HI.HackathonRoundId
    , grrdRoomId    :: HI.HackathonRoomId
    } deriving (Show, Generic)

instance FromJSON GetRoundRoomDetailsRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "grrd"
        }

handleRoundRoomCreate :: AppHandler ()
handleRoundRoomCreate = handleMethods
   [ (SC.PUT, WT.tenantFromToken ensureRoundRoomExistsDefaultInactive)
   ]

ensureRoundRoomExistsDefaultInactive :: AC.TenantWithUser -> AppHandler ()
ensureRoundRoomExistsDefaultInactive (_, Nothing) = respondWithError unauthorised "You need to be logged in to create rooms."
ensureRoundRoomExistsDefaultInactive (tenant, Just _) = do
   request <- SC.readRequestBody size10KB
   writeError . runEitherT $ do
      createRequest <- hoistEither . withErrCode badRequest . eitherDecode $ request
      hackathon <- EitherT (m2e missingHackathon <$> PH.getHackathonByProjectId tenant (rrcrProjectId createRequest))
      hRound <- EitherT (m2e missingRound <$> RND.getHackathonRoundById hackathon (rrcrRoundId createRequest))
      hRoom <- EitherT (m2e missingRoom <$> HRM.getRoomById hackathon (rrcrRoomId createRequest))
      EitherT (m2e creationFailed <$> HRR.getOrCreateRoundRoom hRound hRoom HRR.Inactive)
      lift $ respondNoContent
   where
      missingHackathon = (badRequest, "The provided hackathon id does not exist.")
      missingRound = (badRequest, "The was no round with the provided id.")
      missingRoom = (badRequest, "The was no room with the provided id.")
      creationFailed = (internalServer, "Failed to create the round room for this hackathon.")

data RoundRoomCreateRequest = RoundRoomCreateRequest
   { rrcrProjectId :: AC.ProjectId
   , rrcrRoundId   :: HI.HackathonRoundId
   , rrcrRoomId    :: HI.HackathonRoomId
   } deriving (Show, Generic)

instance FromJSON RoundRoomCreateRequest where
    parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rrcr" }

-- Get all of the rooms that don't have round rooms in them
handleRoundRoomUnallocated :: AppHandler ()
handleRoundRoomUnallocated = handleMethods
   [ (SC.GET, WT.tenantFromToken getUnallocatedRoomsForRound)
   ]

getUnallocatedRoomsForRound :: AC.TenantWithUser -> AppHandler ()
getUnallocatedRoomsForRound (_, Nothing) = respondWithError unauthorised "You need to be logged in to get the unallocated rooms."
getUnallocatedRoomsForRound (tenant, Just _) = do
   potentialRooms <- withPG . runEitherT $ do
      roundID <- EitherT (m2e missingRoundId <$> getIntegerQueryParam "roundId")
      hackathon <- EitherT $ getHackathonFromProjectId tenant
      hRound <- EitherT (m2e missingRound <$> RND.getHackathonRoundById hackathon roundID)
      lift $ HRR.getUnallocatedRooms hRound
   case potentialRooms of
      Left err -> uncurry respondWithError err
      Right rooms -> writeJson . UnallocatedRoomsResponse . fmap toRoomDetails $ rooms
   where
      missingRoundId = (badRequest, "There was no roundId provided. You need to give a round to get unallocated rooms for.")
      missingRound = (badRequest, "There was no round with the provided id.")

data UnallocatedRoomsResponse = UnallocatedRoomsResponse
   { urrRooms :: [RoomDetails]
   } deriving (Show, Generic)

instance ToJSON UnallocatedRoomsResponse where
   toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "urr" }

handleRoundRoomsDetails :: AppHandler ()
handleRoundRoomsDetails = handleMethods
   [ (SC.POST, getRoundRoomsForRound)
   ]

getRoundRoomsForRound :: AppHandler ()
getRoundRoomsForRound = do
    request <- SC.readRequestBody size10KB
    let potentialRequest = eitherDecode request :: Either String GetRoundRoomsDetailsRequest
    case potentialRequest of
        Left errMsg -> respondWithError badRequest $ "Could not parse the request: " ++ errMsg
        Right details -> tenantWithVoteableUserFromHackathonRequest (grrsdHackathon details) (getRoundRoomsDetailsHelper details)

getRoundRoomsDetailsHelper :: GetRoundRoomsDetailsRequest -> AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
getRoundRoomsDetailsHelper _ _ _ Nothing = respondWithError unauthorised "You need to be logged in to get the status of the round-rooms's for this round."
getRoundRoomsDetailsHelper details _ hackathon (Just voteableUser) = withPG $ do
   potentialRound <- RND.getHackathonRoundById hackathon (grrsdRoundId details)
   case potentialRound of
      Nothing -> respondWithError badRequest "There is no round with the provided id in this Hackathon."
      Just hRound -> do
         roomDetails <- HRR.getRoomDetailsForRound hRound
         entities <- forM roomDetails $ \(hRoundRoom, hRoom) ->
            return . Just $ RoundRoomEntity
               { rreRoom = toRoomDetails hRoom
               , rreStatus = T.pack . show . HRR.hrrStatus $ hRoundRoom
               , rreMaximumTeams = HRR.hrrMaximumTeams hRoundRoom
               , rreWinners = HRR.hrrWinners hRoundRoom
               }
         writeJson . RoundRoomResponse . catMaybes $ entities

data GetRoundRoomsDetailsRequest = GetRoundRoomsDetailsRequest
    { grrsdHackathon :: HR.HackathonRequest
    , grrsdRoundId   :: HI.HackathonRoundId
    } deriving (Show, Generic)

instance FromJSON GetRoundRoomsDetailsRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "grrsd"
        }

data RoundRoomResponse = RoundRoomResponse
   { rrrRoundRooms :: [RoundRoomEntity]
   } deriving (Show, Generic)

instance ToJSON RoundRoomResponse where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rrr" }

data RoundRoomEntity = RoundRoomEntity
   { rreRoom         :: RoomDetails
   , rreStatus       :: T.Text
   , rreMaximumTeams :: Maybe Integer
   , rreWinners      :: Integer
   } deriving (Show, Generic)

instance ToJSON RoundRoomEntity where
    toJSON = genericToJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rre" }

handleCurrentRoundRoomDetails :: AppHandler ()
handleCurrentRoundRoomDetails = handleMethods
    [ (SC.POST, parseCurrentRoundRoomDetailsRequest)
    ]

parseCurrentRoundRoomDetailsRequest :: AppHandler ()
parseCurrentRoundRoomDetailsRequest = do
    request <- SC.readRequestBody size10KB
    let potentialRequest = eitherDecode request :: Either String HR.HackathonRequest
    case potentialRequest of
        Left errMsg -> respondWithError badRequest $ "Could not parse the request: " ++ errMsg
        Right r -> tenantWithVoteableUserFromHackathonRequest r getCurrentRoundRoomDetails

getCurrentRoundRoomDetails :: AC.Tenant -> PH.Hackathon -> Maybe HR.VoteableUser -> AppHandler ()
getCurrentRoundRoomDetails _ _ Nothing = respondWithError forbidden "You cannot get round room details if you do not provide a valid token."
getCurrentRoundRoomDetails tenant hackathon _ = withPG $ do
    roundRoomDetails <- do
        hackathonRooms <- HRM.getRoomsForHackathon hackathon
        mapM (runMaybeT . roomsToRoundRoomDetails hackathon) hackathonRooms
    writeJson (RoundRoomList . catMaybes $ roundRoomDetails)

roomsToRoundRoomDetails :: PH.Hackathon -> HRM.HackathonRoom -> MaybeT AppHandler RoundRoomDetails
roomsToRoundRoomDetails hackathon hRoom = do
    roundRoom <- MaybeT $ HRR.getOrCreateCurrentRoundRoomForRoom hRoom HRR.Active
    hRound <- MaybeT $ RND.getHackathonRoundById hackathon (HRR.hrrRoundId roundRoom)
    numberOfTeams <- lift $ HT.countTeamsInRoundRoom hRound hRoom
    return RoundRoomDetails
        { rrdRound = RoundDetails
            { rndId = RND.hroundId hRound
            , rndName = RND.hroundName hRound
            , rndSequenceNumber = RND.hroundSequenceNumber hRound
            }
        , rrdRoom = RoomDetails
            { rmId = HRM.hrId hRoom
            , rmName = HRM.hrName hRoom
            , rmShortUrl = HRM.hrShortUrl hRoom
            }
        , rrdStatus = T.pack . show . HRR.hrrStatus $ roundRoom
        , rrdNumberOfTeams = Just numberOfTeams
        , rrdMaximumTeams = HRR.hrrMaximumTeams roundRoom
        , rrdWinners = HRR.hrrWinners roundRoom
        }

data RoundRoomList = RoundRoomList
    { rrlRoundRooms :: [RoundRoomDetails]
    } deriving (Show, Generic)

instance ToJSON RoundRoomList where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rrl"
        }

data RoundRoomDetails = RoundRoomDetails
    { rrdRound         :: RoundDetails
    , rrdRoom          :: RoomDetails
    , rrdStatus        :: T.Text
    , rrdNumberOfTeams :: Maybe Integer
    , rrdMaximumTeams  :: Maybe Integer
    , rrdWinners       :: Integer
    } deriving (Show, Generic)

instance ToJSON RoundRoomDetails where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rrd"
        }

data RoundDetails = RoundDetails
    { rndId             :: HI.HackathonRoundId
    , rndName           :: T.Text
    , rndSequenceNumber :: Maybe Integer
    } deriving (Show, Generic)

instance ToJSON RoundDetails where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rnd"
        }

toRoomDetails :: HRM.HackathonRoom -> RoomDetails
toRoomDetails hRoom = RoomDetails
   { rmId = HRM.hrId hRoom
   , rmName = HRM.hrName hRoom
   , rmShortUrl = HRM.hrShortUrl hRoom
   }

data RoomDetails = RoomDetails
    { rmId       :: HI.HackathonRoomId
    , rmName     :: T.Text
    , rmShortUrl :: Maybe String
    } deriving (Show, Generic)

instance ToJSON RoomDetails where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "rm"
        }

handleRoundRoomUpdateWinners :: AppHandler ()
handleRoundRoomUpdateWinners = handleMethods
   [ (SC.PUT, WT.tenantFromToken updateWinners)
   ]

updateWinners :: AC.TenantWithUser -> AppHandler ()
updateWinners (_, Nothing) = respondWithError unauthorised "You have to be logged in to make this request."
updateWinners (tenant, Just userKey) = do
   request <- SC.readRequestBody size10KB
   writeError . withPG . runEitherT $ do
      ur <- hoistEither . withErrCode badRequest . eitherDecode $ request
      hackathon <- EitherT (m2e missingHackathon <$> PH.getHackathonByProjectId tenant (rruwrProjectId ur))
      EitherT $ requirePermissionsE [UD.ProjectAdmin] tenant userKey hackathon
      hRound <- EitherT (m2e missingRound <$> RND.getHackathonRoundById hackathon (rruwrRoundId ur))
      hRoom <- EitherT (m2e missingRoom <$> HRM.getRoomById hackathon (rruwrRoomId ur))
      roundRoom <- EitherT (m2e missingRoundRoom <$> HRR.getRoundRoom hRound hRoom)
      void . lift $ HRR.updateWinners roundRoom (rruwrWinners ur)
      void . lift $ respondNoContent
   where
      missingHackathon = (notFound, "There was no hackathon with the given project id.")
      missingRound = (notFound, "There was no round with the given id.")
      missingRoom = (notFound, "There was no room with the given id.")
      missingRoundRoom = (notFound, "There was no round-room with the given id.")


data RoundRoomUpdateWinnersRequest = RoundRoomUpdateWinnersRequest
   { rruwrProjectId :: AC.ProjectId
   , rruwrRoundId   :: HI.HackathonRoundId
   , rruwrRoomId    :: HI.HackathonRoomId
   , rruwrWinners   :: Integer
   } deriving (Show, Generic)

instance FromJSON RoundRoomUpdateWinnersRequest where
   parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "rruwr" }

handleRoundRoomUpdateMaxTeams :: AppHandler ()
handleRoundRoomUpdateMaxTeams = handleMethods
   [ (SC.PUT, WT.tenantFromToken updateMaxTeams)
   ]

updateMaxTeams :: AC.TenantWithUser -> AppHandler ()
updateMaxTeams (_, Nothing) = respondWithError unauthorised "You have to be logged in to make this request"
updateMaxTeams (tenant, Just userKey) = do
   request <- SC.readRequestBody size10KB
   writeError . withPG . runEitherT $ do
      ur <- hoistEither . withErrCode badRequest . eitherDecode $ request
      hackathon <- EitherT (m2e missingHackathon <$> PH.getHackathonByProjectId tenant (umtrProjectId ur))
      EitherT $ requirePermissionsE [UD.ProjectAdmin] tenant userKey hackathon
      hRound <- EitherT (m2e missingRound <$> RND.getHackathonRoundById hackathon (umtrRoundId ur))
      hRoom <- EitherT (m2e missingRoom <$> HRM.getRoomById hackathon (umtrRoomId ur))
      roundRoom <- EitherT (m2e missingRoundRoom <$> HRR.getRoundRoom hRound hRoom)
      void . lift $ HRR.updateMaximumTeams roundRoom (umtrMaxTeams ur)
   where
      missingHackathon = (notFound, "There was no hackathon with the given project id.")
      missingRound = (notFound, "There was no round with the given id.")
      missingRoom = (notFound, "There was no room with the given id.")
      missingRoundRoom = (notFound, "There was no round-room with the given id.")

data UpdateMaxTeamsRequest = UpdateMaxTeamsRequest
   { umtrProjectId :: AC.ProjectId
   , umtrRoundId   :: HI.HackathonRoundId
   , umtrRoomId    :: HI.HackathonRoomId
   , umtrMaxTeams  :: Maybe Integer
   } deriving (Show, Generic)

instance FromJSON UpdateMaxTeamsRequest where
   parseJSON = genericParseJSON baseOptions { fieldLabelModifier = stripFieldNamePrefix "umtr" }
