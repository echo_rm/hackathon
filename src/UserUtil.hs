{-# LANGUAGE OverloadedStrings #-}

module UserUtil
    ( getOrCreateTenantUserByKey
    , updateTenantUserByKey
    ) where

import           Application
import           Control.Monad.IO.Class   (liftIO)
import qualified Data.Map                 as M
import qualified Data.Text                as T
import           Data.Text.Encoding       (encodeUtf8)
import qualified Data.Time.Clock          as DTC
import           Data.Time.Units
import           Data.TimeUnitUTC
import qualified HostRequests.UserDetails as HU
import qualified Persistence.TenantUser   as TU
import qualified Snap.AtlassianConnect    as AC
import           UTCTimeHelpers

-- This method will just get the current user details that we have but will not call the host server if we have cached
-- details on hand. It will also call on the host server if it has not been updated in a certain amount of time. That way
-- our details for this user are always up to date.
getOrCreateTenantUserByKey :: AC.Tenant -> AC.UserKey -> AppHandler (Either T.Text TU.TenantUser)
getOrCreateTenantUserByKey tenant key = do
    potentialTenantUser <- TU.getTenantUserByKey tenant key
    case potentialTenantUser of
        Just tenantUser -> do
            currentTime <- liftIO DTC.getCurrentTime
            -- While (now() - (1d + lastUpdated) <= 0) don't update the user, otherwise refresh the user again
            if DTC.diffUTCTime currentTime (DTC.addUTCTime refreshDiffTime (TU.tuLastUpdated tenantUser)) <= 0
                then return . Right $ tenantUser 
                else updateTenantUserByKey tenant tenantUser
        Nothing -> createTenantUserByKey tenant key

refreshDiffTime :: DTC.NominalDiffTime
refreshDiffTime = timeUnitToDiffTime userDetailsRefreshPeriod

userDetailsRefreshPeriod :: Day
userDetailsRefreshPeriod = 1

createTenantUserByKey :: AC.Tenant -> AC.UserKey -> AppHandler (Either T.Text TU.TenantUser)
createTenantUserByKey tenant key = do
    potentialUserDetails <- HU.getUserDetails tenant key
    case potentialUserDetails of
        Left _ -> return . Left . missingUser $ key
        Right userWithDetails -> do
            let tenantUser = toInsertableTenantUser tenant userWithDetails
            potentialTenantUserId <- TU.insertTenantUser tenant tenantUser
            case potentialTenantUserId of
                Nothing -> return . Left . insertionFailure $ key
                Just tenantUserId -> return . Right $ (tenantUser { TU.tuId = tenantUserId })

updateTenantUserByKey :: AC.Tenant -> TU.TenantUser -> AppHandler (Either T.Text TU.TenantUser)
updateTenantUserByKey tenant tu = do
    potentialUserDetails <- HU.getUserDetails tenant (TU.tuKey tu)
    case potentialUserDetails of
        Left _ -> return . Left . missingUser . TU.tuKey $ tu
        Right userWithDetails -> do
            let newTenantUser = toUpdateableTenantUser userWithDetails tu
            TU.updateTenantUser newTenantUser
            return . Right $ newTenantUser


missingUser :: T.Text -> T.Text
missingUser x = "There is no record of a user with the key '" `T.append` x `T.append` "' in the system."

insertionFailure :: T.Text -> T.Text
insertionFailure x = "Got the users details from the host product but could not store it for user key: " `T.append` x

toUpdateableTenantUser :: HU.UserWithDetails -> TU.TenantUser -> TU.TenantUser
toUpdateableTenantUser newDetails original = original
    { TU.tuEmail        = encodeUtf8 . HU.emailAddress $ newDetails
    , TU.tuDisplayName  = HU.displayName newDetails
    , TU.tuTimeZone     = Just . HU.timeZone $ newDetails
    , TU.tuSmallAvatarUrl = T.pack <$> M.lookup "24x24" (HU.avatarUrls newDetails)
    }

toInsertableTenantUser :: AC.Tenant -> HU.UserWithDetails -> TU.TenantUser
toInsertableTenantUser tenant u = TU.TenantUser
    { TU.tuId          = 0 -- Not important for insertion
    , TU.tuTenantId    = AC.tenantId tenant
    , TU.tuKey         = HU.key u
    , TU.tuEmail       = encodeUtf8 . HU.emailAddress $ u
    , TU.tuDisplayName = HU.displayName u
    , TU.tuTimeZone = Just . HU.timeZone $ u
    , TU.tuSmallAvatarUrl = T.pack <$> M.lookup "24x24" (HU.avatarUrls u)
    , TU.tuLastUpdated = dummyUtcTime -- Not important for insertion
    }
