{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module Data.HackathonRequest
    ( HttpError    -- This probably does not belong here
    , VoteableUser(..)
    , VoteableUserToken(..)
    , VoteableUserData -- Explicitly not exporting the constructors
    , HackathonRequest(..)
    , toVoteableUserToken
    , toAnonToken
    ) where

import           AesonHelpers
import           Data.Aeson
import           Data.Aeson.Types                (fieldLabelModifier)
import qualified Data.Text                       as T
import           GHC.Generics
import qualified Persistence.AnonymousTenantUser as ATU
import qualified Persistence.TenantUser          as TU
import qualified Snap.AtlassianConnect           as AC

type HttpError = (Int, String)

data VoteableUser
    = VoteableAnonUser ATU.AnonymousTenantUser
    | VoteableTenantUser TU.TenantUser

data VoteableUserToken
    = RawPageToken T.Text       -- The JWT token
    | RawTenantKey AC.ClientKey -- The tenant key

data HackathonRequest = HackathonRequest
    { hreqProjectId :: AC.ProjectId
    , hreqToken     :: VoteableUserData
    } deriving (Show, Generic)

instance FromJSON HackathonRequest where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "hreq"
        }

data VoteableUserData
    = VoteablePageTokenData T.Text
    | VoteableAnonTokenData AC.ClientKey (Maybe ATU.AnonymousUserToken)
    deriving (Show)

instance FromJSON VoteableUserData where
    parseJSON (Object obj) = do
        potentialPageToken <- obj .:? "acptToken"
        case potentialPageToken of
            Just (String rawPageToken) -> return . VoteablePageTokenData $ rawPageToken
            Just v -> fail $ "Expected a string page token in the acptToken but instead recieved: " ++ show v
            Nothing -> do
                tenantKey <- obj .: "tenantKey"
                potentialAnonToken <- obj .:? "anonToken"
                return $ VoteableAnonTokenData tenantKey potentialAnonToken
    parseJSON v = fail $ "Expected object but instead recieved: " ++ show v

toVoteableUserToken :: VoteableUserData -> VoteableUserToken
toVoteableUserToken (VoteablePageTokenData pageToken) = RawPageToken pageToken
toVoteableUserToken (VoteableAnonTokenData tenantKey _) = RawTenantKey tenantKey

toAnonToken :: VoteableUserData -> Maybe T.Text
toAnonToken (VoteableAnonTokenData _ anonToken) = anonToken
toAnonToken _ = Nothing
