{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module HostRequests.Components
    ( getOrCreateComponentByName
    , createComponent
    , defaultComponentCreate
    , getComponentsForProject
    , getComponentById
    , JiraComponent(..)
    ) where

-- NOTE: I would prefer to use AC.ProjectId in here exclusively instead of AC.ProjectKey (to get around project rename)
-- but I cannot do this completely until this issue is solved: https://jira.atlassian.com/browse/JRA-42945

import           AesonHelpers
import           Application
import           Data.Aeson
import           Data.Aeson.Types                  (fieldLabelModifier)
import qualified Data.ByteString                   as BS
import qualified Data.ByteString.Char8             as BSC
import           Data.List                         (find)
import           Data.Monoid                       (mempty)
import qualified Data.Text                         as T
import qualified Data.Text.Encoding                as T
import           GHC.Generics
import           Network.Api.Support
import qualified Persistence.HackathonIds          as HI
import qualified Snap.AtlassianConnect             as AC
import qualified Snap.AtlassianConnect.HostRequest as AC
import qualified Snap.Snaplet                      as SS

data JiraComponentCreate = JiraComponentCreate
    { jccName                :: T.Text
    , jccDescription         :: T.Text
    , jccAssigneeType        :: T.Text
    , jccIsAssigneeTypeValid :: Bool
    , jccProject             :: AC.ProjectKey
    } deriving (Show, Generic)

instance ToJSON JiraComponentCreate where
    toJSON = genericToJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "jcc"
        }

getOrCreateComponentByName :: AC.Tenant -> AC.ProjectKey -> T.Text -> AppHandler (Either T.Text JiraComponent)
getOrCreateComponentByName tenant projectKey componentName = do
    potentialComponents <- getComponentsForProject tenant projectKey
    case potentialComponents of
        Left e -> return . Left . AC.perMessage $ e
        Right components ->
            case find ((==) componentName . jcName) components of
                Just component -> return . Right $ component
                Nothing -> do
                    potentialComponent <- createComponent tenant (defaultComponentCreate projectKey componentName)
                    case potentialComponent of
                        Left createError -> return . Left . T.pack . show $ createError
                        Right createdComponent -> return . Right $ createdComponent

defaultRoomDescription :: T.Text
defaultRoomDescription = T.pack "A room for demos and voting in the Hackathon."

defaultComponentCreate :: AC.ProjectKey -> T.Text -> JiraComponentCreate
defaultComponentCreate projectKey componentName =
    JiraComponentCreate
        { jccName = componentName
        , jccDescription = defaultRoomDescription
        , jccAssigneeType = "PROJECT_LEAD"
        , jccIsAssigneeTypeValid = False
        , jccProject = projectKey
        }

createComponent :: AC.Tenant -> JiraComponentCreate -> AppHandler (Either AC.ProductErrorResponse JiraComponent)
createComponent tenant jcc = SS.with connect $ fmap toJiraComponent <$> AC.hostPostRequest tenant componentCreateUrl [] (AC.setJson jcc)
    where
       componentCreateUrl :: BS.ByteString
       componentCreateUrl = "/rest/api/2/component"

getComponentsForProject :: AC.Tenant -> AC.ProjectKey -> AppHandler (Either AC.ProductErrorResponse [JiraComponent])
getComponentsForProject tenant projectKey = SS.with connect $ fmap (fmap toJiraComponent) <$> AC.hostGetRequest tenant getProjectComponentsUrl [] mempty
    where
        getProjectComponentsUrl :: BS.ByteString
        getProjectComponentsUrl = "/rest/api/2/project/" `BSC.append` T.encodeUtf8 projectKey `BSC.append` "/components"

getComponentById :: AC.Tenant -> HI.ComponentId -> AppHandler (Either AC.ProductErrorResponse JiraComponent)
getComponentById tenant componentId = SS.with connect $ fmap toJiraComponent <$> AC.hostGetRequest tenant getComponentUrl [] mempty
    where
        getComponentUrl :: BS.ByteString
        getComponentUrl = "/rest/api/2/component/" `BSC.append` (BSC.pack . show $ componentId)

data JiraComponentResponse = JCR
    { jcrId   :: T.Text
    , jcrName :: T.Text
    } deriving (Show, Generic)

instance FromJSON JiraComponentResponse where
    parseJSON = genericParseJSON baseOptions
        { fieldLabelModifier = stripFieldNamePrefix "jcr"
        }

toJiraComponent :: JiraComponentResponse -> JiraComponent
toJiraComponent jcr = JiraComponent
    { jcId = read . T.unpack . jcrId $ jcr
    , jcName = jcrName jcr
    }

data JiraComponent = JiraComponent
    { jcId   :: HI.ComponentId
    , jcName :: T.Text
    } deriving (Show, Generic)
