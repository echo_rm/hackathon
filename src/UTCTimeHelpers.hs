module UTCTimeHelpers
    ( dummyUtcTime
    ) where

import Data.Time.Clock
import Data.Time.Calendar

dummyUtcTime :: UTCTime
dummyUtcTime = UTCTime (fromGregorian 0 0 0) (secondsToDiffTime 0)
