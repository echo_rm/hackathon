<meta name="productBaseUrl" content="${productBaseUrl}" />
<hasSplice name="connectAllJsOptions">
   <script type="text/javascript" src="${productBaseUrl}/atlassian-connect/all.js" data-options="${connectAllJsOptions}"></script>
</hasSplice>
<missingSplice name="connectAllJsOptions">
   <script type="text/javascript" src="${productBaseUrl}/atlassian-connect/all.js"></script>
</missingSplice>
<link rel="stylesheet" type="text/css" href="${productBaseUrl}/atlassian-connect/all.css"/>
