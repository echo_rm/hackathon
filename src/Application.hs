{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TemplateHaskell   #-}

------------------------------------------------------------------------------
-- | This module defines our application's state type and an alias for its
-- handler monad.
module Application where

------------------------------------------------------------------------------
import qualified AppConfig                     as CONF
import           Control.Lens
import           Control.Monad.Reader          (local)
import           Control.Monad.State           (get)
import qualified Snap.AtlassianConnect         as AC
import           Snap.Snaplet
import           Snap.Snaplet.Heist
import           Snap.Snaplet.PostgresqlSimple
import qualified StaticSnaplet                 as STATIC

------------------------------------------------------------------------------
data App = App
    { _heist   :: Snaplet (Heist App)
    , _db      :: Snaplet Postgres
    , _connect :: Snaplet AC.Connect
    , _appconf :: Snaplet CONF.AppConf
    , _static  :: Snaplet STATIC.StaticConf
    }

makeLenses ''App

instance HasHeist App where
    heistLens = subSnaplet heist

instance HasPostgres (Handler b App) where
  getPostgresState = with db get
  setLocalPostgresState s = local . set (db . snapletValue) $ s

-- TODO Uncomment this and implement a MonadCatchIO for it so that we can use withPG in the EitherT monad
-- There seems to be contention around the equivalent mtl implementation of this however: https://hackage.haskell.org/package/MonadCatchIO-transformers-0.3.1.3/docs/Control-Monad-CatchIO.html
{-
instance HasPostgres (EitherT r (Handler b App)) where
  getPostgresState = lift $ with db get
  setLocalPostgresState s = local . set (db . snapletValue) $ s
-}

instance AC.HasConnect (Handler b App) where
   getConnect = with connect get

instance CONF.HasAppConf (Handler b App) where
   getAppConf = with appconf get

------------------------------------------------------------------------------
type AppHandler = Handler App App
