{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Persistence.TenantUser (
    TenantUser(..)
  , insertTenantUserIfNotExists
  , insertTenantUser
  , updateTenantUser
  , getTenantUserByKey
  , getTenantUsers
  , getTeamMembersForIssueId
  ) where

import           Application
import           Control.Monad
import           Data.Int
import           Data.Maybe
import qualified Data.Text                          as T
import           Data.Time.Clock                    (UTCTime)
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.SqlQQ
import qualified Persistence.HackathonIds           as HI
import qualified Snap.AtlassianConnect              as AC
import           Snap.Snaplet.PostgresqlSimple

data TenantUser = TenantUser
    { tuId             :: HI.TenantUserId
    , tuTenantId       :: HI.TenantId
    , tuKey            :: AC.UserKey
    , tuEmail          :: AC.UserEmail
    , tuDisplayName    :: T.Text
    , tuTimeZone       :: Maybe T.Text
    , tuSmallAvatarUrl :: Maybe T.Text
    , tuLastUpdated    :: UTCTime
    } deriving (Show)

instance FromRow TenantUser where
    fromRow = TenantUser <$> field <*> field <*> field  <*> field <*> field <*> field <*> field <*> field

insertTenantUserIfNotExists :: AC.Tenant -> TenantUser -> AppHandler (Maybe Integer)
insertTenantUserIfNotExists tenant tu = do
    potentialUser <- getTenantUserByKey tenant (tuKey tu) 
    case potentialUser of
        Just user -> return . Just . tuId $ user
        Nothing -> insertTenantUser tenant tu 

getTenantUserByKey :: AC.Tenant -> AC.UserKey -> AppHandler (Maybe TenantUser)
getTenantUserByKey tenant key = listToMaybe <$> query
   [sql|
      SELECT id, tenant_id, key, email, displayname, timezone, small_avatar_url, last_updated FROM tenant_user WHERE tenant_id = ? AND key = ?
   |] (AC.tenantId tenant, key)

insertTenantUser :: AC.Tenant -> TenantUser -> AppHandler (Maybe Integer)
insertTenantUser tenant tu = do
  tenantUserId <- query
    [sql|
      INSERT INTO tenant_user (tenant_id, key, email, displayname, timezone, small_avatar_url, last_updated)
      VALUES (?, ?, ?, ?, ?, ?, now()) RETURNING id
    |] (AC.tenantId tenant, tuKey tu, tuEmail tu, tuDisplayName tu, tuTimeZone tu, tuSmallAvatarUrl tu)
  return . listToMaybe $ join tenantUserId

updateTenantUser :: TenantUser -> AppHandler Int64
updateTenantUser tu = execute
   [sql|
      UPDATE tenant_user
      SET email = ?, displayname = ?, timezone = ?, small_avatar_url = ?, last_updated = now()
      WHERE id = ?
   |] (tuEmail tu, tuDisplayName tu, tuTimeZone tu, tuSmallAvatarUrl tu, tuId tu)

getTenantUsers :: AC.Tenant -> AppHandler [TenantUser]
getTenantUsers tenant = query
   [sql|
      SELECT id, tenant_id, key, email, displayname, timezone, last_updated FROM tenant_user WHERE tenant_id = ?
   |] (Only . AC.tenantId $ tenant)

getTeamMembersForIssueId :: AC.Tenant -> AC.IssueId -> AppHandler [TenantUser]
getTeamMembersForIssueId tenant issueId = query
   [sql|
      SELECT tu.id, tu.tenant_id, tu.key, tu.email, tu.displayname, tu.timezone, tu.small_avatar_url, tu.last_updated
      FROM hackathon h, hackathon_team ht, team_members tm, tenant_user tu
      WHERE h.tenant_id = ?
      AND ht.hackathon_id = h.id
      AND ht.issue_id = ?
      AND tm.hackathon_team_id = ht.id
      AND tm.tenant_user_id = tu.id
   |] (AC.tenantId tenant, issueId)
