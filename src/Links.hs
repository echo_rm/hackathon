{-# LANGUAGE OverloadedStrings #-}
module Links 
   ( votingLink
   , addQueryParams
   ) where

import qualified Data.ByteString.Char8     as BSC
import           Data.Maybe                (catMaybes)
import qualified Data.Text                 as T
import           Network.HTTP.Types.URI
import           Network.URI
import qualified Persistence.Hackathon     as PH
import qualified Persistence.HackathonRoom as HRM
import qualified Snap.AtlassianConnect     as AC

votingLink :: (Monad m, AC.HasConnect m) => AC.Tenant -> PH.Hackathon -> Maybe HRM.HackathonRoom -> m URI
votingLink tenant hackathon potentialRoom = do
   ac <- AC.getConnect
   let basePath = appendPath votingPath (AC.connectBaseUrl ac)
   return $ addQueryParams params basePath
   where
      votingPath = "/page/hackathon-vote"

      params :: QueryText
      params = catMaybes
         [ Just ("tenant_key", Just . AC.key $ tenant)
         , Just ("project_id", Just . st . PH.hackathonProjectId $ hackathon)
         , ((,) "initial_room_id" . Just . st . HRM.hrId) <$> potentialRoom
         ]

st :: Show s => s -> T.Text
st = T.pack . show

appendPath :: String -> URI -> URI
appendPath p uri = uri
   { uriPath = uriPath uri ++ p
   }

addQueryParams :: QueryText -> URI -> URI
addQueryParams qt uri = uri
   { uriQuery = uriQuery uri ++ sep uri ++ renderedQuery
   }
   where
      renderedQuery = BSC.unpack $ renderQuery False (queryTextToQuery qt)
      sep uri = if queryEmpty uri then "?" else "&"
      queryEmpty = null . uriQuery
