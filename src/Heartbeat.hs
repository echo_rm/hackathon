module Heartbeat (heartbeatRequest) where

import           Application
import qualified Snap.Core    as SC
import           Snap.Helpers (handleMethods, ok, respondWith)

heartbeatRequest :: AppHandler ()
heartbeatRequest = handleMethods
   [ ( SC.GET, respondWith ok)
   ]
