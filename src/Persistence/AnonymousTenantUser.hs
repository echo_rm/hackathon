{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
module Persistence.AnonymousTenantUser
    ( lookupAnonymousUser
    , createAnonymousUser
    , AnonymousTenantUser(..)
    , AnonymousUserToken
    ) where

import           Application
import           Control.Monad.IO.Class             (liftIO)
import           Data.Maybe                         (listToMaybe)
import qualified Data.Text                          as T
import qualified Data.UUID                          as UUID
import qualified Data.UUID.V4                       as UUID
import           Database.PostgreSQL.Simple.FromRow
import           Database.PostgreSQL.Simple.SqlQQ
import qualified Persistence.HackathonIds           as HI
import qualified Snap.AtlassianConnect              as AC
import           Snap.Snaplet.PostgresqlSimple

type AnonymousUserToken = T.Text

data AnonymousTenantUser = AnonymousTenantUser
    { atuId       :: HI.AnonymousTenantUserId
    , atuTenantId :: HI.TenantId
    , atuToken    :: AnonymousUserToken
    } deriving (Show)

instance FromRow AnonymousTenantUser where
    fromRow = AnonymousTenantUser <$> field <*> field <*> field

lookupAnonymousUser :: AC.Tenant -> AnonymousUserToken -> AppHandler (Maybe AnonymousTenantUser)
lookupAnonymousUser tenant token = listToMaybe <$> query
    [sql|
        SELECT id, tenant_id, token FROM anonymous_tenant_user WHERE tenant_id = ? and token = ?
    |] (AC.tenantId tenant, token)

createAnonymousUser :: AC.Tenant -> AppHandler AnonymousTenantUser
createAnonymousUser tenant = do
    randomUuid <- liftIO UUID.nextRandom
    newId <- head . fmap fromOnly <$> query
        [sql|
            INSERT INTO anonymous_tenant_user (tenant_id, token) VALUES (?, ?) RETURNING id
        |] (AC.tenantId tenant, UUID.toString randomUuid)
    return AnonymousTenantUser
        { atuId = newId
        , atuTenantId = AC.tenantId tenant
        , atuToken = T.pack . UUID.toString $ randomUuid
        }
